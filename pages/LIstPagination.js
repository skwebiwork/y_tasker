import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Alert,Image,AppRegistry,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView,BackHandler,NetInfo,ActivityIndicator,FlatList} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from '../sideBar';

//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./listStyle";

var {width, height} = Dimensions.get('window');
export class OfferComponent extends Component{
    
	render(){
		if(this.props.result){
			var res = this.props.result.map((offer,i)=>{
				return (
					<View style = {styles.mainBrowse}>
					<View style = {styles.browseMen}>
					{!rowData.user_detail.profile_image && 
					 <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
				   }
					{rowData.user_detail.profile_image && 
					 <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
				   }
						 
					 </View>
					 <View style = {styles.browseContent}>    
						   <Text style = {styles.browseContentHeading} >{rowData.title}</Text> 
						   <Text style = {styles.browseSubContent} >{rowData.description}</Text> 
						   <Text style={{paddingVertical:3,color:'#337ab7'}}>{rowData.user_detail.first_name} {rowData.user_detail.last_name}</Text>
						 <View style={{flexDirection:'row'}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
						 </View>
						 <Text style={{color:'#000'}}>
						 <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row'}}>{Icons.locationArrow}</FontAwesome> <Text>{rowData.address ==null ? rowData.locationType : rowData.address}</Text>
						 </Text>
						 <Text style={{color:'#000'}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> Due date <Text>{this.DateChangeFormate(rowData.dueDate)}</Text> </Text>
							
					 </View>
					<View style = {styles.browseOffer}>
						 <Text style = {styles.browseOfferText}> ${rowData.totalAmount}</Text>
						 <Text style = {styles.browseOfferView} onPress={()=>navigate('SingleTask',{id:rowData.id})}>View</Text>
		 
					 </View>
			</View>
					)
			})
		}
		 return (
				  <View>
						 {res}
				  </View>
		 )
	}
}
export default class list extends Component {
			
	static navigationOptions = {
		header: null,
	};
	
	constructor(){
		super();
		global.count = 0;
		this.state={
			touchMsg : false,
			taskPeopleRequired:"",
			taskBudget:"",
			 LodingStatus:false,
			 status : "",
			 error : "",
			 visible : false,
			 netInfoMsg : false,
			 data : [],
			dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			searchShow : false,
			page:1,
			isLoading : false,
		//link: 'http://hardeepcoder.com/laravel/easyshop/api/products/' + params.id,
		}
			
		
	}
	componentDidMount(){
		// const { params } = this.props.navigation.state;
		this.getTaskList();
		BackHandler.addEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
			NetInfo.isConnected.fetch().done(
			  (isConnected) => { this.setState({ status: isConnected }); }
			);
	   
	}
	componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
		BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
	  }
	  handleBack() {
		global.count = global.count+1;
		if(global.count>1){
		  Alert.alert(
			'Confirmation',
			'Are sure want to quit this App',
			[
			  {text: 'OK', onPress: () => BackHandler.exitApp()},
			]
		);
		}else{  
		  global.setTimeout(() => {
			global.count = 0;
		  }, 1000);
		}		  
		return true;		
		  }
	  handleConnectionChange = (isConnected) => {
		this.setState({ status: isConnected });
	   let networkInfo = this.state.status;
	   if(isConnected){
		this.setState({ visible: false });
		this.setState({ netInfoMsg: false });	   
	   }else{
		this.setState({ visible: true });
		this.setState({ netInfoMsg: true });		
	   }
	}

	getTaskList = async() =>{
		console.log('Call Function...');
		//this.setState({ visible: true });	
     const response = await fetch(
		'http://api.yellotasker.com/api/v1/getPostTask?page_size=10&page_num=${this.state.page}'
	 );
	 const json = await response.json();
	// alert(json.data);
	 this.setState(state =>({data :[ ...state.data,...json.data]}));
	 //this.setState({ visible: false });	
	}
	handleEnd = ()=>{
	//	alert('test')
		 this.setState( state=> ({page : state.page + 1}), ()=>this.getTaskList());
		 alert(this.state.page);
		 
	}
	closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
	  onDashboardRender(){
		const { navigate } = this.props.navigation;
		 navigate('Dashboard');
 }
onRegisterValidate(){
	if(this.state.taskBudget==""){
			this.setState({error: 'Please enter required fields'});
	}else if(this.state.taskPeopleRequired==""){
		   this.setState({error: 'Please enter required fields'});
	}else{
			this.onRegisterPressed();
	}

   
 }
  onBrowsePressed(){
	const { navigate } = this.props.navigation;
	navigate('Browse'); 
 }
  onTaskPressed(){
   let userId = global.userId;
  // alert('userId'+userId);
   const { navigate } = this.props.navigation;
   if(userId){
   
  navigate('Mainview'); 
  }else{
   Alert.alert(
	  'Message',
	  'Please Login to post a task',
	  [
		{text : 'OK', onPress:()=>{this.onLoginPressed()}}
	  ]
	 );
  }
 }
   onLoginPressed(){
   const { navigate } = this.props.navigation;
navigate('Login');
 }
 onMyTaskPressed(){
	let userId = global.userId;
   // alert('userId'+userId);
	const { navigate } = this.props.navigation;
	if(userId){
	
   navigate('MyTask'); 
   }else{
	Alert.alert(
	   'Message',
	   'Please Login to go to My task',
	   [
		 {text : 'OK', onPress:()=>{this.onLoginPressed()}}
	   ]
	  );
   }
  }
	
 onNextStep(){
   alert('chgghdhg');
 }
 DateChangeFormate(duedate){
	 if(duedate){
	var date = duedate;
//		var date = "2017-12-04";
	  //var arr1 = date.split(' ');
	  var arr2 = date.split('-');
	  var month;
	  if(arr2[1]=='01'){
		  month = 'Jan';
	  }else if(arr2[1]=='02'){
		month = 'Feb';
	}else if(arr2[1]=='03'){
		month = 'March';
	}else if(arr2[1]=='04'){
		month = 'April';
	}else if(arr2[1]=='05'){
		month = 'May';
	}else if(arr2[1]=='06'){
		month = 'June';
	}else if(arr2[1]=='07'){
		month = 'July';
	}else if(arr2[1]=='08'){
		month = 'Aug';
	}else if(arr2[1]=='09'){
		month = 'Sep';
	}else if(arr2[1]=='10'){
		month = 'Oct';
	}else if(arr2[1]=='11'){
		month = 'Nov';
	}else{
		month = 'Dec';
	} 
	  var dt = month +' '+arr2[2]+','+arr2[0];
	  return dt;
}
	 //alert(dt);
	  
  }		
  onsearchShow(){
	  this.setState({searchShow : true});

  }
  onsearchHide(){
	this.setState({searchShow : false});

	}
	
	render(){
		 const { params } = this.props.navigation.state;
		 const { navigate } = this.props.navigation;
		return(
			<Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >	
		<View style={styles.mainviewStyle}>
      
		
   <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#FFFFFF',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
      <ActivityIndicator  color="#333" size = "large" style={ {flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80 }
   }></ActivityIndicator>
   </View>
   </View>
   </Modal>
	 <View style={{flexDirection:'row',paddingVertical:10,backgroundColor:'#000000'}}>
	 <View style={{flex:0.2,alignItems:'flex-start',paddingVertical:12,paddingLeft:10}}>
	 <TouchableHighlight onPress={this.onDashboardRender.bind(this)} underlayColor={'transparent'}>
	 <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.arrowLeft}</FontAwesome>
	
	 </TouchableHighlight>
	 
	 </View>
<View style={{flex:0.6,alignItems:'center'}}>
	 <Image source={require('../img/logo-head.png')}/></View>
	 <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:12,paddingRight:10}}>
	 <TouchableHighlight onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
	 <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
	 </TouchableHighlight>
	 </View>

</View>
        <ScrollView style = {styles.scrollViewStyle}>
               <View style = {styles.formViewStyle} >
                       <View style = {styles.secondHeader}>
					   {this.state.searchShow && 
					   <TextInput
       placeholder="Search" style={[styles.InputText,this.state.taskValiMsg && styles.textInputAlt]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
      
      keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false}
       
       ></TextInput>
	}
	 {!this.state.searchShow && 
                             <Text style = {styles.headerText}>Latest Task Posted</Text>
	 }
                             <View style = {styles.settingDiv}>
                                 <FontAwesome style={{fontSize: 18,color:'#000000',}}>{Icons.gear}</FontAwesome>
                             </View>
                             <View style = {styles.searchDiv}>
							 {!this.state.searchShow && 
							 <TouchableHighlight onPress={this.onsearchShow.bind(this)} underlayColor={'transparent'}>
		<FontAwesome style={{fontSize: 18,color:'#000000',}}>{Icons.search}</FontAwesome>
		</TouchableHighlight>
							 }
							 	 {this.state.searchShow && 
							 <TouchableHighlight onPress={this.onsearchHide.bind(this)} underlayColor={'transparent'}>
		<FontAwesome style={{fontSize: 18,color:'#000000',}}>{Icons.search}</FontAwesome>
		</TouchableHighlight>
							 }
                             </View>
                       </View>  
		   
                    <View style={{marginBottom:70}}>
					<FlatList 
                      data={this.state.data}
					  keyExtractor={(x,i)=>i}
					  onEndReached={() =>this.handleEnd()}
						onEndReachedThreshold={0.5}
						
                      renderItem={({item})=>
                      <View>
                   <Text>testSSS{item.id}</Text>
                      </View>
                   }
/>
                     </View>
                        
                       
                </View>
        </ScrollView>

		<View style={styles.footer}>
		<View style={styles.browseButtons}>
		<TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
		  </TouchableHighlight>
			<Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
		</View>
		<View style={styles.postButtons}>
<TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
			 
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
			
			 </TouchableHighlight>
			<Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
		</View>
		<View style={styles.myTaskButtons}>
		<TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
			</TouchableHighlight>
				 <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
			 </View>
		 <View style={styles.messageButtons}>
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>
			 
			<Text style={styles.footerText}>Message</Text>
		</View>
		</View>
      </View>
		</Drawer>
			
		);
	}
	
		
	
}



//AppRegistry.registerComponent('list', () => list) //Entry Point    and Root Component of The App

