import Faker from 'Faker'
import { random, min, times } from 'lodash'

const totalCount = 100
const perPage = 10

export default {
  getPosts(page) {
   // console.log(page)
   var res;
    res = new Promise(resolve => {
      setTimeout(() => {
        const pageCount = min([totalCount - (page - 1) * perPage, perPage])
        const pagination = { page, perPage, pageCount, totalCount }
        const records = times(
          10,
          () => ({
            title: Faker.Lorem.sentence(),
            description: Faker.Lorem.paragraph()
          })
        )

        resolve({ pagination, records })
       // console.log(resolve({ pagination, records }))
      }, random(1000, 3000))
    })
   // console.log(res);
    return res;
  }
}
