import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableHighlight,Picker,ActivityIndicator, ScrollView,FlatList, Alert, Image, AppRegistry, TextInput, Button, AsyncStorage, Modal, Dimensions,ListView,BackHandler,NetInfo,Keyboard } from "react-native";
import styles from './singleTaskStyle';
//import Spinner from 'react-native-loading-spinner-overlay';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from '../sideBar';
var { width, height } = Dimensions.get('window');
import FontAwesome, { Icons } from 'react-native-fontawesome';
import TimeAgo from 'react-native-timeago';
import DatePicker from 'react-native-datepicker';
import RNPickerSelect from 'react-native-picker-select';
//import Selectbox from '../selectOption';
import { Dropdown } from 'react-native-material-dropdown';
//import Selectbox from 'react-native-selectbox'
export class SubChildComponent extends Component{
    render(){
        if(this.props.result){
            console.log(this.props.result);
            var res = this.props.result.map((item,i)=>{
               
                return (
                    <View style={styles.subreplymainsBrowse}>
                    <View style={styles.browseMen}>
                        <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.subreplymenImage} />
                    </View>
                    <View style = {{flex:0.8,flexDirection:'column',paddingHorizontal:10,}}>
                    {item.user_detail && 
                    <Text style={{color:'#337ab7'}}>
                    {item.user_detail.first_name} {item.user_detail.last_name}</Text>
                    }
                    <Text style = {styles.browseSubContent} >{item.commentDescription}</Text> 
                    <View style={{flexDirection:'row',paddingVertical:5,}}>
                       <View>
                         <Text style={{fontWeight:'bold',fontSize:14,color:'#000000'}}><TimeAgo time={item.created_at}/></Text>
                    </View>
                    <View>

                         <Text style={{fontWeight:'normal',fontSize:14,paddingLeft:25}} > 
                         <FontAwesome style={{fontSize: 14,color:'#337ab7'}}>{Icons.reply}</FontAwesome>  Reply</Text>
                    </View>
                    </View>
              </View>
                    </View>
                )
            })
        }
         return (
                  <View>
                         {res}
                  </View>
         )
    }
}
export class ChildComponent extends Component{
   
    render(){
        if(this.props.result){
            var res = this.props.result.reverse().map((item,i)=>{
                if(item){
                return (
                    <View key={i}>
                    <View style={styles.replymainsBrowse}>
                    <View style={styles.browseMen}>
                    {item.user_detail.profile_image &&  <Image source={{uri : item.user_detail.profile_image}} style={styles.subreplymenImage} />}
                       {!item.user_detail.profile_image &&  <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.subreplymenImage} />}
                          
                    
                    </View>
                    <View style = {{flex:0.8,flexDirection:'column',paddingHorizontal:10,}}>
                    {item.user_detail && 
                    <Text style={{color:'#337ab7'}}>
                    {item.user_detail.first_name} {item.user_detail.last_name}</Text>
                    }
                    <Text style = {styles.browseSubContent} >{item.commentDescription}</Text> 
                    <View style={{flexDirection:'row',paddingVertical:6,}}>
                       <View>
                         <Text style={{fontWeight:'bold',fontSize:14,color:'#000000'}}><TimeAgo time={item.created_at}/></Text>
                    </View>
                    <View>

                         <Text style={{fontWeight:'normal',fontSize:14,paddingLeft:25}}> 
                         <FontAwesome style={{fontSize: 14,color:'#337ab7'}}>{Icons.reply}</FontAwesome>  Reply</Text>
                    </View>
                    </View>
              </View>
                    </View>
                    {item.comments && 
                     <SubChildComponent  result={item.comments} />
                    }
                     </View>
                )
            }
            })
        }
         return (
                  <View>
                         {res}
                  </View>
         )
    }
}
export class OfferComponent extends Component{
    
     render(){
         if(this.props.result){
             console.log(this.props.result);
             var res = this.props.result.map((offer,i)=>{
                 return (
    <View key={i}>
                          <View style={styles.mainBrowses}>
                        <View style={{ flex: 0.2, marginLeft: '2%', width: '15%',alignSelf:'center',paddingVertical:10}}>
                        {offer.profile_image &&    <Image source={{uri : offer.profile_image}} style={styles.replymenImage}/>}
                           {!offer.profile_image &&  <Image source={require('../img/browse-man.png')} style={styles.replymenImage} />}
                            
                        </View>
                        <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4}}>
                        <View style={{flexDirection:'row',marginTop:6}}>
                      <View style={{flex:0.7,flexDirection:'row'}}><Text style={{marginTop:5,textAlign:'center',color:'#337ab7'}}> {offer.first_name} {offer.last_name}</Text>
                      <View style={{flexDirection:'row',marginTop:6,marginLeft:7}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                     </View>
                      </View>
                      <View style={{flex:0.3,}}>
                      <TouchableHighlight>
                          <Text style={styles.PercentBtn }>
                         80%
                          </Text>
                          </TouchableHighlight>
                      </View>
                      </View>
                      
                     <View style={{flexDirection:'row'}}>
                     <Text style={{fontWeight:'bold',fontSize:14,color:'#000000',textAlign:'right',}}><TimeAgo time={offer.created_at}/></Text>
                         
                     </View>
                    
                        </View>                      
                    </View>
                    </View>
                     )
             })
         }
          return (
                   <View>
                          {res}
                   </View>
          )
     }
 }
export default class singleTask extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);
        this.onChangeText = this.onChangeText.bind(this);
        this.typographyRef = this.updateRef.bind(this, 'typography');
        global.count = 0;
        this.state = {
            touchMsg : false,
            visible: false,
            LodingStatus : false,
            taskData: [],
            data : [],
            dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
            netInfoMsg : false,
          
            commentLoop : [],
            errorsData: [],
            replyId : null,
            makeOffer : false,
            assignUserId : null,
            taskOffer : [],
            date_in: '',
            date_out: '2046-10-01',
            estimatePrice : '',
            makeOfferComment : '',
            reportThisTask : false,
            user: '',
            taskBudgetOld : '',
            errorMsg :'',
            completionDateValiMsg : false,
            successImg : false,
            successTaskHeading : '',
            successTaskContent : '',
            taskOfferEmpty : false,
            makeAnOfferTitle : '',
            makeAnOfferDueDate : '',
            commentCount : false,
            profileView : true,
            reasone:[],
           // getReason : '',
         reasonLabel :  ' Plese select',
         makeOffervisible:false,
         replycommentDescription :'',
         numberOfComment : 0,
         isErrorResone : false,
         errorMsgResone : '',
         reasonComment : '',
         getReason :'',
        }
      //  var commentLoop = [];
    }
    onChangeText(text) {
        //console.log(text);
        this.setState({getReason:text,reasonLabel : ''});
       }
       updateRef(name, ref) {
         this[name] = ref;
       }
    closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
    componentDidMount(){
        this.fetchTaskDetail();
        this.fetchTaskComment();
        this.getTaskOffer();
        this.getReportTaskReason();
        BackHandler.addEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
			NetInfo.isConnected.fetch().done(
			  (isConnected) => { this.setState({ status: isConnected }); }
			);
    }
    componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
		BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
	  }
    fetchTaskDetail(){
        const { params } = this.props.navigation.state;
        const { navigate } = this.props.navigation;
        this.setState({visible: true}); 
  
       // fetch('http://api.yellotasker.com/api/v1/getPostTask?taskId='+params.id)
        fetch('http://api.yellotasker.com/api/v1/getPostTask?taskId='+params.id)
    .then((response) => response.json())
    .then((responseJson) =>{
       // alert(responseJson.data);
       this.setState({
        taskData:responseJson.data,
    })
    var item = responseJson.data[0];
    var userImage = 'http://yellotasker.com/assets/img/task-person.png';
    if(item.user_detail.profile_image){
        userImage = item.user_detail.profile_image;
    }else{
        userImage = 'http://yellotasker.com/assets/img/task-person.png';
    }
    console.log(userImage);
    console.log(item.user_detail.profile_image);
    console.log(item)
   this.setState({ userImage : userImage});
    this.setState({visible: false}); 
    })
    .catch((error) =>{
       // alert('fetchTaskDetailerror'+error);
    });
    }
    fetchTaskComment(){
        const { params } = this.props.navigation.state;
        const { navigate } = this.props.navigation;
       // this.setState({visible: true}); 
    fetch('http://api.yellotasker.com/api/v1/comment/post?getCommentBy=task&taskId='+params.id)
    .then((response) => response.json())
    .then((responseJson) =>{
      // alert(JSON.stringify(responseJson));
        data = responseJson.data; // here we have all products data
        data = data.reverse();
      console.log(data);
   // var stubComment = 
  // let commentLoop = this.state.commentLoop;
   let  commentLoop = this.prepareCommentLoop(data);
  //  console.log(JSON.stringify(data));
   // console.log(data.length);
    this.setState({numberOfComment:data.length});
    if(data.length>0){
    this.setState({
        dataSource: this.state.dataSource.cloneWithRows(data)
    })

}
    this.setState({visible: false}); 
    })
    .catch((error) =>{
       // alert('fetchTaskCommenterror'+error);
    });
    }
    prepareCommentLoop(resp){
        var commonIndexes = [];
        for(var i=0; i < resp.length;i++){
          for(var j=0;j < resp.length;j++){
            if(resp[i].commentId == resp[j].id){
              if(!resp[j].comments) resp[j].comments = [];
              resp[j].comments.push(resp[i]);
              commonIndexes.push(resp[i]);
              //resp.splice(j,1);
            }else{
              if(resp[j].comments){
                  this.prepareCommentLoop(resp[j].comments);
                }
            }
          }
        }
        for(var i=0;i<commonIndexes.length;i++){
        for(var j=0 ; j<resp.length; j++){
          if(resp[j].id == commonIndexes[i].id){
            resp.splice(j,1);
          }
        }
      }
      }
      getTaskOffer(){
        const { params } = this.props.navigation.state;
        this.setState({visible: true}); 
                fetch('http://api.yellotasker.com/api/v1/taskOffer/'+params.id)
                .then((response) => response.json())
                .then((responseJson) =>{
                    console.log('------------------------------------------');
                             var offerResult = responseJson.data[0].offer_details;
                             
                             console.log(offerResult);
                             //console.log('offer list');
                           // console.log('offer Length'+offerResult[0].interested_users.lenght);
                             if(offerResult.length>0){
                                 console.log('if..............');
                                 console.log(offerResult);
                            this.setState({
                                taskOffer:offerResult[0].interested_user
                            })
                            this.setState({
                                taskOfferEmpty:offerResult[0].interested_user.length
                            });
                            console.log(this.state.taskOfferEmpty);
                            console.log(this.state.taskOffer);
                        }else{
                           
                            console.log('else+++++offerResult.length'+offerResult.length);
                        }
                          // alert(JSON.stringify(this.state.taskOffer));
                            
                             this.setState({visible: false}); 
                })
                .catch((error) =>{
                // alert('fetchTaskDetailerror'+error);
                });
      }
    onBrowsePressed() {
        const { navigate } = this.props.navigation;
        navigate('Browse');
    }
    onBackPressed(){
        const { params } = this.props.navigation.state;
        const { navigate } = this.props.navigation;
        if(params.back=='MyTask'){
            navigate('MyTask');
        }else{
            navigate('Browse');
        }
    }
    onTaskPressed() {
        let userId = global.userId;
        // alert('userId'+userId);
        const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
    }
    onLoginPressed() {
        const { navigate } = this.props.navigation;
        navigate('Login');
    }
    onMyTaskPressed(){
        let userId = global.userId;
         // alert('userId'+userId);
        const { navigate } = this.props.navigation;
        if(userId){
        
         navigate('MyTask'); 
         }else{
        Alert.alert(
           'Message',
           'Please Login to go to My task',
           [
           {text : 'OK', onPress:()=>{this.onLoginPressed()}}
           ]
          );
         }
        }
        onMyProfilePressed(){
            let userId = global.userId;
            // alert('userId'+userId);
           const { navigate } = this.props.navigation;
           if(userId){
           
            navigate('Profile'); 
            }else{
           Alert.alert(
              'Message',
              'Please Login to go to Profile',
              [
              {text : 'OK', onPress:()=>{this.onLoginPressed()}}
              ]
             );
            }
          }
    onNextStep() {
       // alert('chgghdhg');
    }
    onBackFun(){

    }
    onCloseSuccess(){
		this.setState({successImg : false});
		
	}
    handleBack() {
		global.count = global.count+1;
		if(global.count>1){
		  Alert.alert(
			'Confirmation',
			'Are sure want to quit this App',
			[
			  {text: 'OK', onPress: () => BackHandler.exitApp()},
			]
		);
		}else{  
		  global.setTimeout(() => {
			global.count = 0;
		  }, 1000);
		}		  
		return true;		
		  }
	  handleConnectionChange = (isConnected) => {
		this.setState({ status: isConnected });
	   let networkInfo = this.state.status;
	   if(isConnected){
	//	this.setState({ visible: false });
		this.setState({ netInfoMsg: false });	   
	   }else{
	//	this.setState({ visible: true });
		this.setState({ netInfoMsg: true });		
	   }
    }
    getReportTaskReason = async() =>{
        const response = await fetch(
       'http://api.yellotasker.com/api/v1/getReason');
      const json = await response.json();
       console.log(json.data.taskReason.length);
       var res = [];
     
       for(var i=0;i<json.data.taskReason.length;i++){ 
         res.push({value : json.data.taskReason[i].id,label :json.data.taskReason[i].reasonDescription });
       }
       this.setState({ reasone: res });	
     }
    async postComment(){
        let userId = global.userId;
        if(userId){
        const { params } = this.props.navigation.state;
        let taskId = params.id;
        let commentDescription =this.state.commentDescription;
        commentDescription =  commentDescription.trim();
        if(commentDescription){
        try {
            this.setState({visible: true});
     
            let response  = await fetch('http://api.yellotasker.com/api/v1/comment/post', {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json',
               },
              body: JSON.stringify({
                
            userId:userId,
            taskId:taskId,
            commentDescription:commentDescription
           
               })
             });
              
             let res  = await response.text();
             this.setState({visible: false});
          console.log(response.status);
             if(response.status >=200){
               let formErrorsss = JSON.parse(res);
                this.setState({visible: false,commentDescription:''});
               console.log("res token: " + res);
               if(formErrorsss.code==500){
               
               }
               if(formErrorsss.status==1){
                this.setState({commentDescription : null});
                this.setState({replyId : null});
                
                const { navigate } = this.props.navigation;
                navigate('SingleTask',{id:taskId});
                this.fetchTaskComment();
              
                //alert(result.data.id);
               }
             }else{
                 let errors = res;
                 throw errors;
                 //alert(errors);
             }
          } catch (errors) {
                // alert("catch errors " + errors);
          }
        }
        }else{
            Alert.alert(
                'Message',
                'Please Login for post comments',
                [
                {text : 'OK', onPress:()=>{this.onLoginPressed()}}
                ]
               );
        }
    }
    async postReplyOnComment(){
        let userId = global.userId;
        if(userId){
        const { params } = this.props.navigation.state;
        let taskId = params.id;
        let commentDescription = this.state.replycommentDescription;
        commentDescription = commentDescription.trim();
        let commentId = this.state.replyId;
        if(commentDescription){
            this.setState({visible:true});
        try {
           // this.setState({visible: true});
     
            let response  = await fetch('http://api.yellotasker.com/api/v1/comment/post?commentReply=yes', {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json',
               },
              body: JSON.stringify({
                
            userId:userId,
            taskId:taskId,
            commentDescription:commentDescription,
            commentId:commentId
           
               })
             });
              
             let res  = await response.text();
             this.setState({visible:false});
          console.log(response.status);
             if(response.status >=200){
               let formErrorsss = JSON.parse(res);
                this.setState({visible: false});
              console.log("res token: " + formErrorsss);
               if(formErrorsss.code==500){
               
               }
               if(formErrorsss.status==1){
                this.setState({replyId : null});
                this.setState({replycommentDescription : null});
                
                const { navigate } = this.props.navigation;
               // navigate('SingleTask',{id:taskId});
               this.fetchTaskComment();
              
                //alert(result.data.id);
               }
             }else{
                 let errors = res;
                 throw errors;
                 //alert(errors);
             }
          } catch (errors) {
                // alert("catch errors " + errors);
          }
        }else{
            Alert.alert(
                'Warning',
                'Comment field must be required',
                [
                {text : 'OK', onPress:()=>{this.onLoginPressed()}}
                ]
               );
        }
        }else{
            Alert.alert(
                'Message',
                'Please Login for post comments',
                [
                {text : 'OK', onPress:()=>{this.onLoginPressed()}}
                ]
               );
        }
    }
    check(){
        Alert.alert(
            'Message',
            'Please Login for post comments',
            [
            {text : 'OK', onPress:()=>{this.onLoginPressed()}}
            ]
           );
    }
    singleTaskFun = (text,budget,title,duedate)=>{
       //alert('test');
       let idd = JSON.stringify(text);
       let taskbudget = JSON.stringify(budget);
       // title = JSON.stringify(title);
       let dueDate = JSON.stringify(duedate);
      // alert(taskbudget);
       this.setState({assignUserId: idd});
       this.setState({makeOffer : true});
       this.setState({taskBudgetOld : budget})
       this.setState({estimatePrice : budget})
       var serviceChargeOld =parseInt(taskbudget)*0.1;
       serviceChargeOld = serviceChargeOld.toFixed(2);
       console.log('servicecharge',serviceChargeOld);
       this.setState({serviceCharge:serviceChargeOld});
       this.setState({makeAnOfferTitle : title});
       this.setState({makeAnOfferDueDate : duedate});
       this.setState({date_in : duedate});

    }
    async saveTaskFun(){
        let interestedUsreId = global.userId;
        if(interestedUsreId){
       const { params } = this.props.navigation.state;
       let taskId  = params.id;

    // alert("assignUserId"+assignUserId+"interestedUsreId"+interestedUsreId+"taskId"+taskId);
    try {
      //  this.setState({visible: true});
        let response  = await fetch('http://api.yellotasker.com/api/v1/saveTask', {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
          body: JSON.stringify({  
            userId : interestedUsreId,
          taskId:taskId
         

           })
         });
         console.log(response);
         let res  = await response.text();
         let formErrorsss = JSON.parse(res);
         if(formErrorsss.code==500){
             Alert.alert(
                'Message',
                'This task already saved',
                [
                {text : 'OK', onPress:()=>{console.log('cliclllllll')}}
                ]
             )
         }
         if(response.status >=200){
           
  console.log(JSON.stringify(formErrorsss));
            //this.setState({visible: false});
          //  this.setState({successImg : true});
          this.setState({successTaskHeading : 'Saved Jobs'});
          this.setState({successTaskContent : 'Successfully added to Saved Jobs.'});
           if(formErrorsss.status==1){
          this.setState({successImg : true});
           }
         }else{
             let errors = res;
             throw errors;
         }
      } catch (errors) {
          console.log(JSON.stringify(errors));
      }
    }else{
        Alert.alert(
            'Message',
            'Please Login for make an offer',
            [
            {text : 'OK', onPress:()=>{this.onLoginPressed()}}
            ]
           );
        }
    
    }
    onClose(){
        this.setState({makeOffervisible:false});
       this.setState({makeOffer : false});
       
    }
    passwordValidate = (text)=> {
    this.setState({commentDescription: text});

    
    }
    resPress = (text)=>{
        //alert(JSON.stringify(text));
        let idd = JSON.stringify(text);
    this.setState({replyId: idd});
    
    }
    makeComments = (text) =>{
        let idd = JSON.stringify(text);
        this.setState({makeOfferComment: idd});
    }
    estimatePriceFun = (val)=>{
        this.setState({estimatePrice: val});
         if(val>0){
            this.setState({estimateValiMsg : false});
            this.setState({errorMsg : ''})
         }else if(val<0){
            this.setState({estimateValiMsg : true});
            this.setState({errorMsg : 'Please enter positive value'})
         }else{
            this.setState({estimateValiMsg : true});
            this.setState({errorMsg : 'Please enter bid price'})
         }
    }
    makeConfirmOffer(){
        console.log('Make Offer Btn Click');
        var estimatePrice= this.state.estimatePrice;
      var completionDate = this.state.date_in;
 if(estimatePrice==""){
    this.setState({estimateValiMsg : true});
    this.setState({errorMsg : 'Please enter bid price'})
 }else if(estimatePrice<0){
    this.setState({estimateValiMsg : true});
    this.setState({errorMsg : 'Please enter positve bid price'})
 }else{

    this.setState({completionDateValiMsg : false});
    this.setState({estimateValiMsg : false});
    
    this.setState({errorMsg : ''});
   console.log('submit');
 this.onContinue();
 }
    }
    async onContinue(){
        this.setState({makeOffervisible:true});
        let interestedUsreId = global.userId;
        if(interestedUsreId){
       var assignUserId =  this.state.assignUserId;
       const { params } = this.props.navigation.state;
       let taskId  = params.id;
      var estimatePrice= this.state.estimatePrice;
      var completionDate = this.state.date_in;
      var makeOfferComment = this.state.makeOfferComment;
      if(makeOfferComment){
    this.setState({ commentDescription : makeOfferComment})
        this.postComment()

      }
   // alert("assignUserId"+assignUserId+"interestedUsreId"+interestedUsreId+"taskId"+taskId);
    try {
        let response  = await fetch('http://api.yellotasker.com/api/v1/makeOffer', {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
          body: JSON.stringify({  
          assignUserId:assignUserId,
          interestedUserId:interestedUsreId,
          taskId:taskId,
          offerPrice: estimatePrice, 
          completionDate: completionDate

           })
         });
         let res  = await response.text();
      
         this.setState({makeOffervisible : false})
         let formErrorsss = JSON.parse(res);
         if(response.code==200){
          
           console.log(res);
            this.setState({visible: false});
       
           if(formErrorsss.status==1){
               this.getTaskOffer()
    console.log('status log++++++++++++++')
    this.setState({makeOffer : false});
        this.setState({successImg: true});
               
            this.setState({successTaskHeading : 'Successfully Posted Offer'});
            this.setState({successTaskContent : ' Offer has been posted for this task .'});
           }
         }else if(formErrorsss.code==500){
            global.setTimeout(() => {
                Alert.alert(
                    'Message',
                    'Offer already exists',
                    [
                    {text : 'OK', onPress:()=>{this.onClose()}}
                    ]
                   );
                }, 50);
    
               }else{
        this.setState({makeOffervisible:false});
             
             let errors = res;
             throw errors;
         }
      } catch (errors) {
          console.log('catch Error');
          console.log(JSON.stringify(errors));
      }
    }else{
        Alert.alert(
            'Message',
            'Please Login for make an offer',
            [
            {text : 'OK', onPress:()=>{this.onLoginPressed()}}
            ]
           );
    }
    }
    
    reportTaskClick = ()=>{
        //alert('test');
        //let idd = JSON.stringify(text);
       // this.setState({assignUserId: idd});
  this.setState({reportThisTask : true});
     }
     closeReportTask(){
        this.setState({assignUserId: null});
        this.setState({reportThisTask : false});
        
     }
     makeOfferSecond(){
         console.log(this.state.taskData);
         var d = this.state.taskData;
         var item = d[0];
         this.singleTaskFun(item.userId,item.totalAmount,item.title,item.dueDate);
     }
     reportTaskFunSubmit(){
        var reasonComment = this.state.reasonComment;
        var getReason = this.state.getReason;
        console.log(getReason);
        if(getReason==''){
            this.setState({isErrorResone:true,errorMsgResone:'Reason must be required!'})

        }else if(getReason==null){
            this.setState({isErrorResone:true,errorMsgResone:'Reason must be required!'})

        }else if(reasonComment==''){
            this.setState({isErrorResone:true,errorMsgResone:'Comment must be required!'})

        }else{
            this.setState({isErrorResone:false,errorMsgResone:'Reason must be required!'})
             this.reprtTaskFun();
        }

     }
     async reprtTaskFun(){
         
       console.log(this.state.getReason,
        this.state.reasonComment);
        let reportedUserId = global.userId;
        if(reportedUserId){
            var getReason = this.state.getReason;
          
       var assignUserId =  this.state.assignUserId;
       const { params } = this.props.navigation.state;
       let taskId  = params.id;
   
    try {
        let response  = await fetch('http://api.yellotasker.com/api/v1/report/user', {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
          body: JSON.stringify({  
            "reasonId": this.state.getReason,
            "comment": this.state.reasonComment,
            "reportedUserId": reportedUserId,
            "taskId": taskId

           })
         });
         let res  = await response.text();
         if(response.status >=200){
            this.setState({reportThisTask : false});
           let formErrorsss = JSON.parse(res);
           console.log('res'+formErrorsss);
           console.log(JSON.stringify(res));
            this.setState({visible: false});
           if(formErrorsss.code==500){
        
           }
           if(formErrorsss.status==1){
               
           this.setState({successImg: true});
              
            this.setState({successTaskHeading : 'Reported task successfully'});
            this.setState({successTaskContent : 'Successfully reported this task. Admin will take action and get back to you.'});
           }
         }else{
             let errors = res;
             throw errors;
         }
      } catch (errors) {
      }
   
    }else{
        Alert.alert(
            'Message',
            'Please Login for report task',
            [
            {text : 'OK', onPress:()=>{this.onLoginPressed()}}
            ]
           );
    }
     }
     DateChangeFormate(duedate){
        if(duedate){
       var date = duedate;
   //		var date = "2017-12-04"; 10-1-2018
         //var arr1 = date.split(' ');
         var arr2 = date.split('-');
         var month;
         if(arr2[1]=='01'){
             month = 'Jan';
         }else if(arr2[1]=='02'){
           month = 'Feb';
       }else if(arr2[1]=='03'){
           month = 'March';
       }else if(arr2[1]=='04'){
           month = 'April';
       }else if(arr2[1]=='05'){
           month = 'May';
       }else if(arr2[1]=='06'){
           month = 'June';
       }else if(arr2[1]=='07'){
           month = 'July';
       }else if(arr2[1]=='08'){
           month = 'Aug';
       }else if(arr2[1]=='09'){
           month = 'Sep';
       }else if(arr2[1]=='10'){
           month = 'Oct';
       }else if(arr2[1]=='11'){
           month = 'Nov';
       }else{
           month = 'Dec';
       } 
         var dt = month +' '+arr2[2]+','+arr2[0];
         return dt;
   }
}
indianDateFormat(dueDate){
	var arr = dueDate.split('-');
	var formattedDate = arr[2]+'-'+arr[1]+'-'+arr[0];
	return formattedDate;
  }
  bidAmountChangeFun(val){
      this.setState({taskBudgetOld : val});
      if(val){       
   this.setState({estimatePrice: val});
   var serviceChargeOld =parseInt(val)*0.1;
   serviceChargeOld = serviceChargeOld.toFixed(2);
       console.log('servicecharge',serviceChargeOld);
   this.setState({serviceCharge:serviceChargeOld});
    }else{
        this.setState({estimatePrice: ''});
        this.setState({serviceCharge:'0.00'});
        
    }
  }
    render() {
        const { params } = this.props.navigation.state;
        return (
<Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} 
            drawerPosition={'right'}
            >
            <View style={styles.mainviewStyle}>
            <Modal
    animationType="slide"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
     <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <Modal  animationType="slide"
      transparent={true} visible={this.state.successImg}>
			<View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#FFFFFF',
    height: 300,
    width: 300,
    borderRadius: 3,
   }}> 
		<View style={{backgroundColor:'#efeb10',height:50,flexDirection:'row'}} >
    <Text style={{color:'#000',textAlign:'center',paddingLeft:24,paddingVertical:10,flex:.9}}> {this.state.successTaskHeading} </Text>
		<Text style={{flex:.1,paddingVertical:10,color:'#000',textAlign:'center',fontWeight:'bold'}} onPress={this.onCloseSuccess.bind(this)} >X</Text>
		</View>
   <View style={{alignItems: 'center',}}>
		<Image source={require('../img/checkmark.gif')} style={{height : 200,width:100}}/>
	 </View>
	 <Text style={{textAlign:'center'}}> {this.state.successTaskContent}</Text>
	 </View>
	 </View>
   </Modal>
  
                <Modal
                    
                    transparent={false}
                    visible={this.state.netInfoMsg}>
                                <ActivityIndicator  style={styles.indicator} color="#000000" size = "large"></ActivityIndicator>

                    <Text style={{color:'#333333',textAlign:'center',marginTop:height/2+10}}>Please check your network connection</Text>
                </Modal>
               
                <Modal
                    
                    transparent={false}
                    visible={this.state.makeOffer}>
                     <ScrollView keyboardShouldPersistTaps="never">
                     <Modal
    animationType="slide"
      transparent={true}
      visible={this.state.makeOffervisible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
     <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
                            <View style={{flexDirection:'row',paddingVertical:10,backgroundColor:'#000000'}}>
                <View style={{flex:0.2,alignItems:'flex-start',paddingVertical:12,paddingLeft:10}}>
                 
                </View>
             <View  style={{flex:0.6,alignItems:'center'}}>
             <Text onPress={() => this.props.navigation.navigate("Dashboard")}>
                <Image source={require('../img/app-logo.png')}/>
                </Text>
                </View>
                <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:12,paddingRight:10}}>
                
                </View>             
             </View>
             <View style={{backgroundColor:'#efeb10', borderColor: '#ccc',paddingVertical: 12,flexDirection:'row'}}>
                   <Text style={{textAlign : 'center',color:'#000000',fontWeight:'bold',flex:0.9}}>Confirm Your Offer  </Text>
                  <Text style={{flex:0.1,textAlign : 'center',}} onPress={this.onClose.bind(this)}> <FontAwesome style={{fontSize: 14,color:'#000000',}}>{Icons.close}</FontAwesome> </Text>
             </View>
                    
                    <View style={styles.makeOfferContainer}>
                    
                    <View style={{paddingHorizontal:10,flexDirection:'row'}}>
                    <Text style={{textAlign:'left',color:'#dd4b39',paddingVertical:4}}>{this.state.errorMsg}</Text>
                    </View>
                    <Text style={{paddingHorizontal:10,flexDirection:'row',fontWeight:'bold',color:'#000',fontSize:17}}>{this.state.makeAnOfferTitle}</Text>
                    
                    <View style={{paddingHorizontal:10,paddingVertical:4,flexDirection:'row'}}>
                    <Text style={{flex:0.4,textAlign:'left',fontWeight:'bold'}}>Budget : <Text style={{fontWeight:'bold',color:'#7F878A'}}>{this.state.taskBudgetOld} MYR</Text></Text>
<Text style={{flex:0.6,textAlign:'right',fontWeight:'bold',flexDirection:'row'}}>Service Charge : <Text style={{fontWeight:'bold',color:'#7F878A'}}>{this.state.serviceCharge}MYR</Text></Text>
                    </View> 
                    <View style={{paddingHorizontal:10,paddingVertical:4,flexDirection:'row'}}>
                    <Text style={{flex:0.5,textAlign:'left',fontWeight:'bold'}}>Bid Price <Text style={{color:'#dd4b39'}}>*</Text></Text>
{/* <Text style={{flex:0.5,textAlign:'right'}}>Budget <Text>{this.state.taskBudgetOld}$</Text></Text>*/}
                    </View> 
    <View style={{paddingHorizontal:10,paddingVertical:4,}}>
      <TextInput
      keyboardType="numeric"
        style={[styles.InputText, this.state.estimateValiMsg && styles.textInputAlt]} 
        placeholder={`${this.state.taskBudgetOld} MYR`}
       underlineColorAndroid = "transparent" returnKeyType="next" 
     autoCapitalize="none"
      autoCorrect={false}
      onSubmitEditing={()=>this.commentNameInput.focus()}
      onChangeText={(val) => this.bidAmountChangeFun(val)}
      value={`${this.state.taskBudgetOld}`}
      blurOnSubmit={true}
      />
      </View> 
      <Text style={{paddingVertical:4,paddingHorizontal:10,fontWeight:'bold'}}>Comments</Text>
    <View style={{paddingHorizontal:10,paddingVertical:4,}}>
                      
                       <TextInput
       placeholder="comments"  
       underlineColorAndroid = "transparent" returnKeyType="done" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      blurOnSubmit={true}
      numberOfLines={3}
      onSubmitEditing={Keyboard.dismiss}
      ref={(input)=>this.commentNameInput=input}
      onChangeText={(text) => this.makeComments(text)}></TextInput>
    
                       </View>
                       <View style = {{paddingHorizontal:10,paddingVertical:4,flexDirection:'row'}}>
                  <Text style={[styles.extrasText,{flex:0.5,textAlign:'left',fontWeight:'bold'}]}>Completion Date</Text>
                  <Text style={{flex:0.5,textAlign:'right'}}><FontAwesome style={{fontSize: 12,color:'#000000',}}>{Icons.calendar}</FontAwesome> {this.indianDateFormat(this.state.makeAnOfferDueDate)}</Text>
          </View>
             <View style={{paddingHorizontal:10,paddingVertical:4,}}>     

</View>

                       
                       <View style={{paddingHorizontal:6,paddingVertical:10,marginTop:'27%'}}>
                       <Text style={{ borderRadius: 30, paddingVertical: 10, paddingHorizontal: 25, backgroundColor: '#efeb10', color: '#000000', textAlign: 'center' }} onPress={this.makeConfirmOffer.bind(this)}> Make An Offer</Text>
                       </View>
                       
                       
    <View style={{paddingHorizontal:6,paddingVertical:10,}}>
               <TouchableHighlight onPress={this.onClose.bind(this)} style={styles.skip}>
               <Text style={{textAlign :'center',fontSize: 20,color:'#ffffff'}}>Close</Text>
                </TouchableHighlight>
           </View>
           </View>
           </ScrollView>
                </Modal>        

              
                
                <Modal  transparent={false} visible={this.state.reportThisTask}>
                            <View style={{flexDirection:'row',paddingVertical:10,backgroundColor:'#000000'}}>
                <View style={{flex:0.2,alignItems:'flex-start',paddingVertical:12,paddingLeft:10}}>
                
                </View>
             <View  style={{flex:0.6,alignItems:'center'}}>
             <Text onPress={() => this.props.navigation.navigate("Dashboard")}>
                <Image source={require('../img/app-logo.png')}/>
                </Text>
                </View>
                <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:12,paddingRight:10}}>
                
                </View>             
             </View>
             <View style={{backgroundColor:'#efeb10', borderColor: '#ccc',paddingVertical: 12,}}>
                   <Text style={{textAlign : 'center',color:'#000000',fontWeight:'bold'}}>Report this task</Text>
             </View>       
                    <View style={styles.makeOfferContainer}>
                    
                    <View style={{paddingHorizontal:10,paddingVertical:4,}}>
                    <Text style={{paddingVertical:6,fontWeight:'500'}}>Please give us more information regarding this project</Text>
                    
      {this.state.isErrorResone&&<Text style={{paddingVertical:6,color:'red'}}>{this.state.errorMsgResone}</Text> }
                    {/* <Dropdown
              ref={this.typographyRef}
              onChangeText={this.onChangeText}
              label={this.state.reasonLabel}
              data={this.state.reasone}
              pickerStyle={styles.textContainer}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              dropdownOffset = {{top: 42, left: 0}}
              value={this.state.getReason}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',paddingHorizontal:5}}
             
            /> */}
                <RNPickerSelect           
                      items={this.state.reasone}
                      onValueChange={
                          (item) => {
                            this.onChangeText(item);
                            console.log(item);
                            this.setState({sendingCurrency : item});
                            console.log(this.state.sendingCurrency);
                          }
                      }
                      style={{ ...pickerSelectStyles }}
                      value={this.state.getReason}
                      
                    
                    />
      </View>
                       <View style={{paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Comment(required)"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}
      onChangeText={(text) => this.setState({reasonComment : text})}      
       ></TextInput>
    
                       </View>
                      
                       <View style={{paddingHorizontal:6,paddingVertical:10,}}>
                       <Text style={{ borderRadius: 30, paddingVertical: 10, paddingHorizontal: 25, backgroundColor: '#efeb10', color: '#000000', textAlign: 'center' }} onPress={this.reportTaskFunSubmit.bind(this)}> Continue</Text>
                       </View>
            <View style={{paddingHorizontal:6,paddingVertical:10,}}>
               <TouchableHighlight onPress={this.closeReportTask.bind(this)} style={styles.skip}>
               <Text style={{textAlign :'center',fontSize: 20,color:'#ffffff'}}>Cancel</Text>
                </TouchableHighlight>
            </View>
            </View>
                </Modal> 
                

                <View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
            <View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
                <TouchableHighlight onPress={this.onBackPressed.bind(this)} underlayColor={'transparent'}>
                    <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.arrowLeft}</FontAwesome>
                </TouchableHighlight> 
            </View>
         <View  style={{flex:0.6,alignItems:'center',}}>
         <Text onPress={() => this.props.navigation.navigate("Dashboard")}>
            <Image source={require('../img/app-logo.png')} />
            </Text>
            </View>
            <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
            <TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
           
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
           
            </TouchableHighlight>
            </View>
         
         </View>
                <ScrollView style={styles.scrollViewStyle}>
                    <View style={styles.formViewStyle} >
                    <FlatList 
                      data={this.state.taskData}
                      keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                       <View>
                           {/*
                       <View style={styles.mainBrowse}>
                       

                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4}}>
                       <View style={{flexDirection:'row'}}>
                       <View style={{flex:0.7,flexDirection:'column'}}>
                     <Text style={{fontSize:16,fontWeight:'bold',color:'#000000',textAlign:'left',}}>{item.title}</Text>
                     <Text style={{color:'#337ab7',paddingVertical:5}}  onPress={() => this.props.navigation.navigate("TaskerProfile",{id:params.id})}>{item.user_detail.first_name} {item.user_detail.last_name}</Text>
                     <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                    </View>
                    <Text style={{paddingVertical:5}}>
                           <FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.locationArrow}</FontAwesome>
                              <Text> {item.address ==null||item.address =='' ? item.locationType : item.address} </Text>
                             
                               </Text>
                     </View>
                     <View style={{flex:0.3,flexDirection:'column',paddingHorizontal:5 }}>
                    
                         <Text style={styles.followBtn }><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.heartO}</FontAwesome> Follow
                         </Text>
                        <Text style={styles.makeAnOfferBtn} onPress={this.singleTaskFun.bind(this,item.userId,item.totalAmount,item.title,item.dueDate)}>Make An Offer</Text>
                        <Text style={{color:'#337ab7',paddingVertical:8, fontSize:13,textAlign: 'center'}} onPress={this.saveTaskFun.bind(this)}> 
                        <Text style={{ fontSize:14,}}>+</Text>Save Job</Text>
                        <Text style={{color:'#337ab7',paddingVertical:8, fontSize:13,textAlign: 'center'}} onPress={this.reportTaskClick.bind(this)}> 
                       <FontAwesome>{Icons.flag}</FontAwesome> Report Task</Text>
                     </View>
                     </View>
                     
                  
                     <View style={{flexDirection:'row',paddingVertical:10,}}>
                       
                       <View style={{ flex: 0.3 }}>
                           <Text style={{ backgroundColor: '#000000', color: '#ffffff', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Open</Text>
                       </View>
                       <View style={{ flex: 0.3, marginLeft: 4 }}>
                           <Text style={{ backgroundColor: '#eee', color: '#000', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Assigned</Text>

                       </View>
                       <View style={{ flex: 0.4, marginLeft: 4 }}>
                           <Text style={{ backgroundColor: '#eee', color: '#000', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Completed</Text>

                       </View>
                   </View>
                  
                       </View>                      
                   </View>  */}

                   <View style = {[styles.browseContent]}>    
                   <View style={{flexDirection:'row',flex:1,paddingVertical:7,paddingHorizontal:5}}>
                   
			<Text style = {styles.browseContentHeading} >{item.title}</Text> 
           
            <View style={{ flex: 0.2 }}>
                           <Text style={{ backgroundColor: 'transparent', color: 'green', borderRadius: 6,borderColor:'green',borderWidth:1, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Open</Text>
                       </View>
                    </View>
                    <View style={{flexDirection:'row',paddingVertical:10,}}>
                       
                       
                     {/*  <View style={{ flex: 0.3, marginLeft: 4 }}>
                           <Text style={{ backgroundColor: '#eee', color: '#000', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Assigned</Text>

                       </View>
                       <View style={{ flex: 0.3, marginLeft: 4 }}>
                           <Text style={{ backgroundColor: '#eee', color: '#000', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Completed</Text>

                       </View>
                      {/* <View style={{flex:0.2,flexDirection:'column',paddingHorizontal:8, }}>
                    
                    <Text style={styles.followBtn }><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.heartO}</FontAwesome> Follow
                    </Text>
                    </View> */}
                   </View>
			<View style={{flexDirection:'row',flex:1,paddingVertical:5,paddingHorizontal:5,alignItems:'flex-start', }}>
        <View style={{flexDirection:'row',flex:0.5}}>
			<View style={{paddingHorizontal:10,}}>
			{!item.user_detail.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
		   {item.user_detail.profile_image && 
		    <Image source={{uri : item.user_detail.profile_image}} style={styles.userImage}/>
		  }
			</View>

			<View style={{flexDirection:'column',}}>
            <Text style={{paddingVertical:2,color:'#34495e',alignItems:'flex-start',flex:0.5}}>Posted By</Text>
			<Text style={{paddingVertical:2,color:'#337ab7',alignItems:'flex-start',flex:0.5}}>{item.user_detail.first_name} {item.user_detail.last_name}</Text>
		    <View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>

				</View>
		  
		  </View>
</View>
          <View style={{flexDirection:'column',flex:0.5,alignItems:'flex-end',paddingHorizontal:3}}>
                   <Text style={styles.makeAnOfferBtn} onPress={this.singleTaskFun.bind(this,item.userId,item.totalAmount,item.title,item.dueDate)}>Make An Offer</Text>
                   <Text style={{color:'#337ab7',paddingVertical:5, fontSize:13,textAlign: 'center'}} onPress={this.saveTaskFun.bind(this)}> 
                   <Text style={{ fontSize:14,textAlign: 'center'}}><FontAwesome>{Icons.plus}</FontAwesome></Text> Save this Job</Text>
                   <Text style={{color:'#337ab7', fontSize:13,textAlign: 'center'}} onPress={this.reportTaskClick.bind(this)}> 
                  <FontAwesome>{Icons.flag}</FontAwesome> Report Task</Text>
         </View>
		  </View>
	  </View>


                        <View style={{ flex: 2, paddingVertical: 10, paddingHorizontal: 10,minHeight:100, borderColor: '#ccc',borderBottomWidth: 1,borderTopWidth: 1 }}>
                            <Text style={{ paddingVertical: 5, fontSize: 16, fontWeight: 'bold',color:'#000000' }}>Description</Text>
                            <Text style={{color:'#333',paddingVertical:5,fontSize : 14,}}>{item.description}</Text>  
                            {/* <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                                <Text style={{flex:0.5,alignItems:'flex-start',}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',}}>Posted ON</Text> 
                                </Text>
                                <Text style={{flex:0.5,}}>{this.indianDateFormat(item.dueDate)}</Text>
                             </View> */}
                             <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                                <Text style={{flex:0.5,alignItems:'flex-start',}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',}}>Due Date</Text> 
                                </Text>
                                <Text style={{flex:0.5,}}>{this.indianDateFormat(item.dueDate)}</Text>
                             </View>
                           {/* <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>People Required</Text>
                            <Text style={{flex:0.5,}}>
                            5
                            </Text>
                            </View> */}
                            <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Budget Type</Text>
                            <Text style={{flex:0.5,}}>
                            {item.budgetType}
                            </Text>
                            </View>
                            <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Total Amount</Text>
                            <Text style={{flex:0.5,}}>
                            {item.totalAmount} MYR
                            </Text>
                            </View>
                        {/*    <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Location</Text>
                            <Text style={{flex:0.5,}}>
                          {item.address ==null||item.address =='' ? item.locationType : item.address} 
                            </Text>
                            </View>  */}
                        </View>
                      
                        </View>

}
/>
                        <View style={{ paddingVertical: 10, paddingHorizontal: 10,borderRadius: 4, }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>OFFER</Text>
                        </View>
                        {this.state.taskOfferEmpty==0 && <View style={{paddingHorizontal:10,paddingVertical:15,flexDirection:'row',backgroundColor:'#eee'}}>
                        <Text style={{ fontSize: 14, fontWeight: 'normal', color: '#333', flex:0.6,paddingVertical: 10 }}>Currently there is no offer </Text>
                        <Text style={{flex:0.4,overflow:"hidden", borderRadius: 5, paddingVertical: 10, paddingHorizontal: 25, backgroundColor: '#efeb10', color: '#000000', textAlign: 'center' }} onPress={this.makeOfferSecond.bind(this)}> Make An Offer</Text>
                        </View>
                    }
                  
                   <FlatList 
                      data={this.state.taskOffer}
                      keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                     <OfferComponent result={this.state.taskOffer}/>
                   }
                   />
  
                        <View style={{ paddingVertical: 10, paddingHorizontal: 10,borderRadius: 4, }}>
                        {this.state.numberOfComment>0&&  <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>{this.state.numberOfComment} COMMENTS ABOUT THIS TASK</Text>} 
                           {this.state.numberOfComment==0&&  <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>NO COMMENTS ABOUT THIS TASK</Text>} 
                            <Text style={{ paddingVertical: 10, fontSize: 12, fontWeight: 'normal', color: '#333', }}>Comment below for more details and remember that for your safety, not to share personal information e.g. email and phone numbers.</Text>
                        
                        </View>
                        <View style={styles.mainBrowses}>
                       <View style={styles.browseMenTwo}>
                           <Image source={{uri : global.profileImage}} style={styles.userImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Ask a question"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}
      onChangeText={(text) => this.passwordValidate(text)}      
      value={this.state.commentDescription}
       ></TextInput>
     
     <Text style={{textAlign:'right',color:'#000000'}} onPress={this.postComment.bind(this)}>Send</Text>
                       </View>
                       </View>
  
<View style={{  marginBottom: 80,}}>
<ListView
		dataSource={this.state.dataSource}
		renderRow={(comment)=>
                       <View>
                        <View style={styles.mainsBrowse}>
                       <View style={styles.browseMen}>
                       {comment.user_detail.profile_image &&  <Image source={{uri : comment.user_detail.profile_image}} style={styles.replymensImage} />}
                       {!comment.user_detail.profile_image &&  <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.replymensImage} />}
                           
                       </View>
                       <View style = {{flex:0.8,flexDirection:'column',paddingHorizontal:10,}}>
                       {comment.user_detail && 
                       <Text style={{color:'#337ab7'}}>
                       {comment.user_detail.first_name} {comment.user_detail.last_name}</Text>
                       }
                       <Text style = {styles.browseSubContent} >{comment.commentDescription}</Text> 
                       
                       <View style={{flexDirection:'row',}}>
                          <View>
                            <Text style={{fontWeight:'bold',fontSize:14,color:'#000000'}}><TimeAgo time={comment.created_at}/></Text>
                       </View>
                       <View>

                            <Text style={{fontWeight:'normal',fontSize:14,paddingLeft:25}} onPress={this.resPress.bind(this,comment.id)}> 
                            <FontAwesome style={{fontSize: 14,color:'#337ab7'}}>{Icons.reply}</FontAwesome>  Reply</Text>
                       </View>
                       </View>
                 </View>
                       </View>
                       {comment.id==this.state.replyId &&
                        <View style={styles.mainsBrowse}>
                        <View style={styles.browseMenTwo}>
                            <Image source={{uri : global.profileImage}} style={styles.replymenImage} />
                        </View>
                        <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                        <TextInput
        placeholder=""  
        underlineColorAndroid = "transparent" returnKeyType="next" 
              keyboardType="email-address"
       autoCapitalize="none"
       autoCorrect={false} style={styles.TextArea}
       multiline={true}
       numberOfLines={3}
       onChangeText={(text) => this.setState({replycommentDescription : text})}      
       
        ></TextInput>
     
      <Text style={{textAlign:'right',color:'#000000'}} onPress={this.postReplyOnComment.bind(this)}>Send</Text>
                        </View>
                        </View>

                       }
                       {comment.comments && 
                       <ChildComponent status={this.state.status} result={comment.comments}/>
                       }
                     <View>
                         <Text>
                                 
                         </Text>
                     </View>
                  
                           </View>

}
/>
                    </View>
                    </View>
                </ScrollView>

                <View style={styles.footer}>
                <View style={styles.browseButtons}>
                <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
                  </TouchableHighlight>
                    <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
                </View>
                <View style={styles.postButtons}>
       <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
                     
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
                    
                     </TouchableHighlight>
                    <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
                </View>
                <View style={styles.myTaskButtons}>
                <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
                    </TouchableHighlight>
                         <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
                     </View>
                     <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
                </View>
            </View>
            </Drawer>
        );
    }
}
const Errors = (props) => {
    return (
      <View>
         {props.errorsData.map((error,i) => <Text key={i}>{error}</Text>)}
      </View>
    );
    
  }

  import EStyleSheet from 'react-native-extended-stylesheet';
const pickerSelectStyles = EStyleSheet.create({
    inputIOS: {
      fontSize: 18,
      paddingTop: 10,
      paddingHorizontal: 8,
      paddingBottom: 10,
      borderWidth: 1,
      borderColor: 'gray',
      borderRadius: 4,
      color:'gray',
      backgroundColor:'transparent'
    },
    icon:{
      position: 'absolute',
      backgroundColor: 'transparent',
      borderTopWidth: 8,
      borderTopColor: 'gray',
      borderRightWidth: 7,
      borderRightColor: 'transparent',
      borderLeftWidth: 7,
      borderLeftColor: 'transparent',
      width: 0,
      height: 0,
      top: 15,
      right: 10,
    
    }
  });
//AppRegistry.registerComponent('singleTask', () => singleTask) //Entry Point    and Root Component of The App