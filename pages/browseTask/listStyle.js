import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
	mainviewStyle: {
	flex: 1,
	flexDirection: 'column',
  },
  footer: {
	position: 'absolute',
	flex:0.1,
	left: 0,
	right: 0,
	bottom: -10,
	backgroundColor:'#000000',
	flexDirection:'row',
	height:70,
	alignItems:'center',
  },
  postButtons: {
	alignItems:'center',
	justifyContent: 'center',
	flex:1,
	 flexDirection:'column',
	 borderColor: '#efeb10',
	 borderRightWidth: 1,
  
  },
  browseButtons: {
	alignItems:'center',
	justifyContent: 'center',
	flex:1,
	 flexDirection:'column',
	 borderColor: '#efeb10',
	 borderRightWidth: 1,
  
  },
  myTaskButtons: {
	alignItems:'center',
	justifyContent: 'center',
	flex:1,
	 flexDirection:'column',
	 borderColor: '#efeb10',
	 borderRightWidth: 1,
	 
  },
  messageButtons: {
	alignItems:'center',
	justifyContent: 'center',
	flex:1,
	 flexDirection:'column',
	 
  },
  footerText: {
	color:'white',
	alignItems:'center',
	fontSize:14,
   
  
  },
  textStyle: {
	alignSelf: 'center',
	color: 'orange'
  },
  scrollViewStyle: {
	borderTopWidth : 4,
		borderColor: '#efeb10',
  },
  secondHeader : {
  backgroundColor: '#efeb10',
  flexDirection : 'row',
  borderColor : '#ccc',
  borderBottomWidth : 1,
  paddingVertical : 12,
 
  },
  headerText : {
   color : '#000000',
   paddingLeft : 10,
   flex : 0.8,
   fontWeight :'bold',
  },
  settingDiv : {
	flex:0.1,
	backgroundColor: '#efeb10',
	borderRightWidth : 1,
	borderLeftWidth : 1,
	borderColor : '#d8d406',
	alignItems : 'center',
  },
  searchDiv : {
	 flex:0.1,
	backgroundColor: '#efeb10',
	alignItems : 'center',
  },
  mainBrowse : {
	flexDirection : 'column',
	borderColor : '#ccc',
	borderWidth : 1,
	paddingVertical : '3%',
	borderRadius:5,
   flex : 1,
   marginTop:3,
  },
  mainsBrowse :{
	flexDirection : 'row',
   borderColor : '#ccc',
   borderBottomWidth : 1,
   paddingVertical : '3%',
   marginBottom : 80,
  },
  browseMen : {
	flex : 0.2,
	marginLeft : '2%',
   width : '15%',
  },
  menImage : {
   height: 48,
	  width : 48,
	},
	userImage : {
		height: 49,
		width : 47,
		borderRadius:24,

	},
  browseContent : {
	flex : 0.8,
	marginLeft : '3%',
	flexDirection : 'column',
  },
  browseContentHeading : {
	fontSize : 16,
	fontWeight : 'bold',
  color : '#000000'
  },
  browseSubContent : {
	fontSize : 12,
	fontWeight : 'normal',
	color:'#333',
	flexDirection : 'row',
	paddingVertical:5,
  },
  browseOffer : {
	flex : 0.2,
   alignItems:'center',
	flexDirection : 'column',
  },
  browseOfferText : {
	fontSize : 15,
	fontWeight : 'bold',
	color:'#000000'
  },
  browseOfferView :{
   borderRadius : 4,
   backgroundColor: '#efeb10',
	alignItems : 'center',
	paddingVertical : 6,
	paddingHorizontal : 12,
	marginTop:10,
	color :'#000000',
	overflow:"hidden",
   },
   container:{
	display:'flex'
},
pageName:{
	margin:10,fontWeight:'bold',
	color:'#000', textAlign:'center'
},
LoginDiv : {
	backgroundColor : '#efeb10',
	paddingVertical : 11, 
	  borderRadius:5,
	  borderWidth: 1,
	  borderColor: '#efeb10',
  
  },
  extraText : {
	color : '#000000',
	textAlign: 'center',
		fontSize: 18,
	},
	InputText : {
	
	
		 
		 paddingLeft : 10,
   flex : 0.8,
	},
	textInputAlt : {
		borderColor: '#dd4b39',
	},
	itemTextStyle : {
		paddingHorizontal:5,
		 },
		 containerStyle :{
		borderColor:'gray',
		borderWidth:1,
		borderRadius: 5,
		 
		},
	'@media (max-width: 360)': {
		footerText: {
      color:'white',
      alignItems:'center',
      fontSize:13,
     
    
		},
	  menImage : {
			height: 64,
			 width : 64,
		 },
	},
	activeText : {
		color:'#efeb10',
	}
  });

  export default styles;
  