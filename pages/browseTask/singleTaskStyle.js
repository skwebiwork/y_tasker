import EStyleSheet from 'react-native-extended-stylesheet';
const pickerSelectStyles = EStyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingTop: 10,
    paddingHorizontal: 8,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 4,
    color:'#fff',
    backgroundColor:'#026d7f'
  },
  icon:{
    position: 'absolute',
    backgroundColor: 'transparent',
    borderTopWidth: 8,
    borderTopColor: '#F06124',
    borderRightWidth: 7,
    borderRightColor: 'transparent',
    borderLeftWidth: 7,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
    top: 15,
    right: 10,
  
  }
});
const styles = EStyleSheet.create({
    mainviewStyle: {
        flex: 1,
        flexDirection: 'column',
    },
    footer: {
        position: 'absolute',
        flex: 0.1,
        left: 0,
        right: 0,
        bottom: -10,
        backgroundColor: '#000000',
        flexDirection: 'row',
        height: 70,
        alignItems: 'center',
    },
    postButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        borderColor: '#efeb10',
        borderRightWidth: 1,

    },
    browseButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        borderColor: '#efeb10',
        borderRightWidth: 1,

    },
    myTaskButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        borderColor: '#efeb10',
        borderRightWidth: 1,

    },
    messageButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',

    },
    footerText: {
        color: 'white',
        alignItems: 'center',
        fontSize: 14,


    },
    textStyle: {
        alignSelf: 'center',
        color: 'orange'
    },
    scrollViewStyle: {
        borderTopWidth: 4,
        borderColor: '#efeb10',
    },
    secondHeader: {
        backgroundColor: '#efeb10',
        flexDirection: 'row',
        borderColor: '#ccc',
        borderBottomWidth: 1,
        paddingVertical: 12,

    },
    headerText: {
        color: '#000000',
        paddingLeft: 10,
        flex: 0.8,
        fontWeight: 'bold',
    },
    settingDiv: {
        flex: 0.1,
        backgroundColor: '#efeb10',
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderColor: '#d8d406',
        alignItems: 'center',
    },
    searchDiv: {
        flex: 0.1,
        backgroundColor: '#efeb10',
        alignItems: 'center',
    },
    mainBrowse: {
        flexDirection: 'row',
        borderColor: '#ccc',
        paddingHorizontal:'1%', 
        paddingVertical: '3%',
        borderBottomWidth: 1,
    },
    mainBrowses: {
        flexDirection: 'row',
        borderColor: '#ccc',
        borderTopWidth: 1,
      paddingHorizontal:'1%',
      paddingVertical:'3%',

        
    },
    makeOfferContainer: {
        flexDirection: 'column',
        borderColor: '#ccc',
        borderTopWidth: 1,
      paddingHorizontal:'1%',
      paddingVertical:'1%',

        
    },
    mainsBrowse: {
        flexDirection: 'row',
       
        paddingVertical: '3%',
        paddingHorizontal:'1%',
      
    },
    replymainsBrowse: {
        flexDirection: 'row',
       paddingLeft:'10%',
        paddingVertical: '3%',
        paddingHorizontal:'1%',
      
    },
    subreplymainsBrowse: {
        flexDirection: 'row',
       paddingLeft:'18%',
        paddingVertical: '3%',
        paddingHorizontal:'1%',
      
    },
    browseMen: {
        flex: 0.2,
        marginLeft: '2%',
        width: '15%',
    },
    browseMenTwo:  {flex: 0.2,
        marginLeft: '2%',
        width: '15%',paddingTop:10},
        replymensImage : {
            height: 48,
            width: 48,
            borderRadius:48/2
        },
    menImage: {
        height: 52,
        width: 52,
    },
    replymenImage: {
        height: 48,
        width: 48,
        borderRadius:48/2
    },
    subreplymenImage: {
        height: 42,
        width: 42,
        borderRadius:42/2
    },
    browseContent: {
        flex: 0.8,
        marginLeft: '3%',
        flexDirection: 'column',
    },
    browseContentHeading: {
        fontSize: 16,
        fontWeight: 'bold',
        color : '#000000',
       flex:0.8,
       
    },
    browseSubContent: {
        fontSize: 12,
        fontWeight: '500',
        color:'#333',

    },
    browseOffer: {
        flex: 0.2,
        flexDirection:'row',
       
    },
    browseOfferText: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    browseOfferView: {
        borderRadius: 6,
        backgroundColor: '#efeb10',
        alignItems: 'center',
        paddingVertical: 6,
        paddingHorizontal: 12,

    },
    followBtn:{
        borderRadius: 6,
        backgroundColor: '#efeb10',
        textAlign: 'center',
        paddingVertical: 6,
        paddingHorizontal: 12,
        color:'#000000',
        fontSize:10,
        overflow:"hidden",
    },
    makeAnOfferBtn:{
        borderRadius: 6,
        backgroundColor: '#efeb10',
        textAlign: 'center',
        paddingVertical: 7,
        paddingHorizontal: 12,
        color:'#000000',
        fontSize:10,
        overflow:"hidden",
       
    },
    PercentBtn:{
        borderRadius: 6,
        backgroundColor: '#efeb10',
        textAlign: 'center',
        paddingVertical: 6,
        paddingHorizontal: 6,
        color:'#000000',
        fontSize:10,
        overflow:"hidden",
    },
    InputText : {
        paddingVertical : 12,
        borderRadius:5,
        borderColor: 'gray',
         borderWidth: 1,
         paddingHorizontal: 5,
         marginBottom : 10,
        textAlignVertical: 'top',
      },
      TextArea : {
        paddingVertical : 12,
        borderRadius:5,
        borderColor: 'gray',
         borderWidth: 1,
         paddingHorizontal: 5,
         marginBottom : 10,
        textAlignVertical: 'top',
        height:100,
      },
      skip : {
        paddingHorizontal : 8,
        backgroundColor : '#000000',
      paddingVertical : 5,
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#000000',
    },
       skips : {
        paddingHorizontal : 8,
        backgroundColor : '#000000',
      paddingVertical : 8,
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#000000',
        marginLeft : 10,
    },
    userImage : {
		height: 48,
		width : 48,
		borderRadius:23,

	},
    textInputAlt : {
        borderColor: '#dd4b39',
      },
      datePickerCss : {
        width: '100%',
        borderRadius:5,
        borderColor: 'gray',
      },containerStyle :{
        borderColor:'gray',
        borderWidth:1,
        borderRadius: 5,
       
      },	
      itemTextStyle : {
       paddingHorizontal:5,
      },
      '@media (max-width: 360)': {
		footerText: {
      color:'white',
      alignItems:'center',
      fontSize:13,
     
    
		},
	  menImage : {
			height: 64,
			 width : 64,
		 },
	},
});
export default styles;