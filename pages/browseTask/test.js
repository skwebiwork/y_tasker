import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Alert,Image,AppRegistry,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView,BackHandler,NetInfo,ActivityIndicator,FlatList} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from '../sideBar';
//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./listStyle";
var {width, height} = Dimensions.get('window');
export default class list extends Component {		
	static navigationOptions = {
		header: null,
	};
	constructor(){
		super();
		global.count = 0;
		this.onEndReachedCalledDuringMomentum = true;
		this.state={
			touchMsg : false,
			taskPeopleRequired:"",
			taskBudget:"",
			 LodingStatus:false,
			 status : "",
			 error : "",
			 visible : false,
			 netInfoMsg : false,
			 data : [],
			dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			searchShow : false,
			page:1,
			isLoading : false,	
		//link: 'http://hardeepcoder.com/laravel/easyshop/api/products/' + params.id,
		}		
	}
	componentDidMount(){
		// const { params } = this.props.navigation.state;
		this.getTaskList();
		BackHandler.addEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
			NetInfo.isConnected.fetch().done(
			  (isConnected) => { this.setState({ status: isConnected }); }
			);
	   
	}
	componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
		BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
	  }
	  handleBack() {
		global.count = global.count+1;
		if(global.count>1){
		  Alert.alert(
			'Confirmation',
			'Are sure want to quit this App',
			[
			  {text: 'OK', onPress: () => BackHandler.exitApp()},
			]
		);
		}else{  
		  global.setTimeout(() => {
			global.count = 0;
		  }, 1000);
		}		  
		return true;		
		  }
	  handleConnectionChange = (isConnected) => {
		this.setState({ status: isConnected });
	   let networkInfo = this.state.status;
	   if(isConnected){
		this.setState({ visible: false });
		this.setState({ netInfoMsg: false });	   
	   }else{
		this.setState({ visible: true });
		this.setState({ netInfoMsg: true });		
	   }
	}

	getTaskList = async() =>{
		console.log('Call Function...'+this.state.page);
		this.setState({ visible: true });	
		let page_num = this.state.page;
     const response = await fetch(
		'http://api.yellotasker.com/api/v1/getPostTask?page_size=10&page_num='+page_num
	 );
	 const json = await response.json();
	// alert(json.data);
	this.setState(state =>({data :[ ...state.data,...json.data]}));
	//this.setState({data : [ ...state.data,...json.data]});
//	this.setState({data :json.data});
	 this.setState({ visible: false });	
	}
	handleEnd = ()=>{
	//	alert('test')
		 this.setState( state=> ({page : state.page + 1}), ()=>this.getTaskList());
	}
	onSeeMore(){
		//this.setState({page : this.state.page+1});
		//this.getTaskList();
		this.setState( state=> ({page : state.page + 1}), ()=>this.getTaskList());
		
	}
	closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
	  onDashboardRender(){
		const { navigate } = this.props.navigation;
		 navigate('Dashboard');
 }
onRegisterValidate(){
	if(this.state.taskBudget==""){
			this.setState({error: 'Please enter required fields'});
	}else if(this.state.taskPeopleRequired==""){
		   this.setState({error: 'Please enter required fields'});
	}else{
			this.onRegisterPressed();
	}
 }
  onBrowsePressed(){
	const { navigate } = this.props.navigation;
	navigate('Browse'); 
 }
  onTaskPressed(){
   let userId = global.userId;
  // alert('userId'+userId);
   const { navigate } = this.props.navigation;
   //const { navigate } = this.props.navigation;
   global.category_id = null;
   navigate('Mainview');
 }
   onLoginPressed(){
   const { navigate } = this.props.navigation;
navigate('Login');
 }
 onMyTaskPressed(){
	let userId = global.userId;
   // alert('userId'+userId);
	const { navigate } = this.props.navigation;
	if(userId){
	
   navigate('MyTask'); 
   }else{
	Alert.alert(
	   'Message',
	   'Please Login to go to My task',
	   [
		 {text : 'OK', onPress:()=>{this.onLoginPressed()}}
	   ]
	  );
   }
  }
	
 onNextStep(){
  // alert('chgghdhg');
 }
 readMoreCount(description){
     return description.length;
 }
 readMore(description){
	 if(description.length>24){
		 return description.substring(0,22)+'...';
	 }else{
		 return description;
	 }

 }

  onsearchShow(){
	  this.setState({searchShow : true});

  }
  onsearchHide(){
	this.setState({searchShow : false});

	}
	
	render(){
		 const { params } = this.props.navigation.state;
		 const { navigate } = this.props.navigation;
		return(
			<Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >	
		<View style={styles.mainviewStyle}>
   <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
   <View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
   <Text>
 
  
   </Text>
   
   </View>

  

</View>
        <ScrollView style = {styles.scrollViewStyle}>
               <View style = {styles.formViewStyle} >
                     
        <View style={{marginBottom:70}}>
			<FlatList  data={this.state.data} keyExtractor={(x,i)=>i}	
                      renderItem={({item})=>
                      <View style={{paddingHorizontal:5}}>
    <View style={{paddingHorizontal:5}}>
<TouchableHighlight onPress={()=>navigate('SingleTask',{id:item.id})}>
                    <View style = {styles.mainBrowse}>
		   <View style = {styles.browseMen}>
		   {!item.user_detail.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
		   {item.user_detail.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
				
			</View>
			
			

		   <View style = {styles.browseOffer}>
				<Text style = {styles.browseOfferText}> ${item.totalAmount}</Text>
				<Text style = {styles.browseOfferView} onPress={()=>navigate('SingleTask',{id:item.id})}>View</Text>

			</View>
			
   </View>
   </TouchableHighlight>
                      </View>
                      </View>
                   }
/>
{!this.state.visible && 
 <View style={{ alignItems:'flex-end',marginRight : 10}}><Text onPress={this.onSeeMore.bind(this)} style={styles.browseOfferView}>See More</Text></View>
} 
                     </View>
					               
                       
                </View>
        </ScrollView>

		<View style={styles.footer}>
	
		</View>
      </View>
		</Drawer>
			
		);
	}
	
		
	
}



//AppRegistry.registerComponent('list', () => list) //Entry Point    and Root Component of The App

