import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableHighlight, ScrollView, Alert, Image, AppRegistry, TextInput, Button, AsyncStorage, Modal, Dimensions, ListView, BackHandler, NetInfo, ActivityIndicator, FlatList } from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem, Drawer, Icon } from 'native-base';
import SideBar from '../sideBar';
import { Dropdown } from 'react-native-material-dropdown';
//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./listStyle";
var Slider = require('react-native-slider');
var { width, height } = Dimensions.get('window');
export class OfferComponent extends Component {

	render() {
		if (this.props.result) {
			var res = this.props.result.map((offer, i) => {
				return (
					<View style={styles.mainBrowse}>
						<View style={styles.browseMen}>
							{!item.user_detail.profile_image &&
								<Image source={{ uri: 'http://yellotasker.com/assets/img/task-person.png' }} style={styles.menImage} />
							}
							{rowData.user_detail.profile_image &&
								<Image source={{ uri: 'http://yellotasker.com/assets/img/task-person.png' }} style={styles.menImage} />
							}

						</View>
						<View style={styles.browseContent}>
							<Text style={styles.browseContentHeading} >{rowData.title}</Text>
							<Text style={styles.browseSubContent} >{rowData.description}</Text>
							<Text style={{ paddingVertical: 3, color: '#337ab7' }}>{rowData.user_detail.first_name} {rowData.user_detail.last_name}</Text>
							<View style={{ flexDirection: 'row' }}><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', paddingHorizontal: 2, }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', paddingHorizontal: 2, }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome>
							</View>
							<Text style={{ color: '#000' }}>
								<FontAwesome style={{ fontSize: 10, color: '#000000', flexDirection: 'row' }}>{Icons.locationArrow}</FontAwesome> <Text>{rowData.address == null ? rowData.locationType : rowData.address}</Text>
							</Text>
							<Text style={{ color: '#000' }}><FontAwesome style={{ fontSize: 10, color: '#000000', }}>{Icons.calendar}</FontAwesome> Due date <Text>{rowData.dueDate}</Text> </Text>

						</View>
						<View style={styles.browseOffer}>
							<Text style={styles.browseOfferText}>{rowData.totalAmount} MYR</Text>
							<Text style={styles.browseOfferView} onPress={() => navigate('SingleTask', { id: rowData.id })}>View</Text>

						</View>
					</View>
				)
			})
		}
		return (
			<View>
				{res}
			</View>
		)
	}
}
export default class list extends Component {

	static navigationOptions = {
		header: null,
	};

	constructor() {
		super();
		global.count = 0;
		this.onEndReachedCalledDuringMomentum = true;
		this.onChangeDefault = this.onChangeDefault.bind(this);
		this.onChangeDate = this.onChangeDate.bind(this);
		this.onChangebudget = this.onChangebudget.bind(this);
		this.onChangelocation = this.onChangelocation.bind(this);
		this.onChangeCategory = this.onChangeCategory.bind(this);
		this.onChangebookmark = this.onChangebookmark.bind(this);

		this.dateRef = this.dateRef.bind(this, 'dateRef');
		this.state = {
			touchMsg: false,
			taskPeopleRequired: "",
			taskBudget: "",
			LodingStatus: false,
			status: "",
			error: "",
			visible: false,
			netInfoMsg: false,
			data: [],
			dataSource: new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 != r2 }),
			searchShow: false,
			page: 1,
			isLoading: false,
			isViewSetting: false,
			shortByList: 'default',
			shortByValue: "",
			dueDateOption: [{ value: "due_today", label: "Due Today" }, { value: "due_tomorrow", label: "Due Tommorow" }, { value: "due_current_week", label: "Due within current week" }, { value: "due_current_month", label: "Due within current month" }],
			budgetOption: [{ value: "0-100", label: "under  100 MYR" }, { value: "100-500", label: "more then 100 and less then 500" }, { value: "500-1000", label: "more then 500 and less then 1000" }, { value: "1000-1500", label: "more 1000 MYR" }],
			locationOption: [{ value: "work remotely", label: "Work remotely" }, { value: "come to work place", label: "Come to work place" }],
			categoryOption: [],
			bookmarkOption: [{ value: "bookmark", label: "Browse by bookmark" }],
			latesttaskOption: [{ value: "default", label: "Latest task posted" }],
			latestTaskLabel: ' Latest task posted',
			dueDateLabel: ' Browse By Due Date',
			budgetLabel: ' Browse By Budget',
			locationLabel: ' Browse By location',
			categoryLabel: ' Browse By Category',
			bookmarkLabel: ' Browse by bookmark',
			searchCaption: 'Latest task posted',
			//link: 'http://hardeepcoder.com/laravel/easyshop/api/products/' + params.id,
		}


	}
	componentDidMount() {
		// const { params } = this.props.navigation.state;
		this.getTaskList();
		this.categoryList();
		BackHandler.addEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
		NetInfo.isConnected.fetch().done(
			(isConnected) => { this.setState({ status: isConnected }); }
		);

	}
	dateRef(name, ref) {
		this[name] = ref;
	}
	onChangeDate(text) {
		this.setState({ shortByList: 'browseByDate', shortByValue: text, dueDateLabel: '' });
	}
	onChangeDefault(text) {
		this.setState({ shortByList: 'default', shortByValue: text, latestTaskLabel: '' });

	}
	onChangebudget(text) {
		this.setState({ shortByList: 'browseByBudget', shortByValue: text, budgetLabel: '' });

	}
	onChangelocation(text) {
		this.setState({ shortByList: 'browseByLocation', shortByValue: text, locationLabel: '' });

	}
	onChangeCategory(text) {
		this.setState({ shortByList: 'browseByCategory', shortByValue: text, categoryLabel: '' });

	}
	onChangebookmark(text) {
		this.setState({ shortByList: 'browseByBookmark', shortByValue: text, bookmarkLabel: '' });

	}
	onPressApply() {
		this.setState({ isViewSetting: false })
		var shortByList = this.state.shortByList;
		var shortByValue = this.state.shortByValue;
		console.log('shortByList--------------' + shortByList);
		console.log('shortByValue..............' + shortByValue);
		if (shortByList == 'browseByDate') {
			this.getTaskListByDate(shortByValue);
			this.setState({ searchCaption: 'Browse by date' });
		} else if (shortByList == 'browseByBudget') {
			this.getTaskListByBudget(shortByValue);
			this.setState({ searchCaption: 'Browse by Budget' });

		} else if (shortByList == 'browseByLocation') {
			this.getTaskListByLocation(shortByValue);
			this.setState({ searchCaption: 'Browse by Location' });

		} else if (shortByList == 'browseByCategory') {
			//this.getTaskListByCategory(shortByValue);	
			this.setState({ searchCaption: 'Browse by Category' });

		} else if (shortByList == 'browseByBookmark') {
			this.setState({ searchCaption: 'Browse by Bookmark' });

			//this.getTaskListByBookmark(shortByValue);	
		} else {
			this.getTaskList();
		}

	}
	getTaskListByDate = async (shortByValue) => {
		this.setState({ visible: true });
		const response = await fetch(
			'http://api.yellotasker.com/api/v1/getPostTask?' + shortByValue + '=1'
		);
		const json = await response.json();
		// var data = json.data.reverse();
		//	 data = data.reverse();
		this.setState(state => ({ data: [json.data] }));
		this.setState({ visible: false });
	}
	getTaskListByLocation = async (shortByValue) => {
		this.setState({ visible: true });
		const response = await fetch(
			'http://api.yellotasker.com/api/v1/getPostTask?locationType=' + shortByValue
		);
		const json = await response.json();
		// var data = json.data.reverse();
		//	 data = data.reverse();
		this.setState(state => ({ data: [...state.data, ...json.data] }));
		this.setState({ visible: false });
	}
	async getTaskListByBudget(shortByValue) {

		// console.log('getPayeeCurrency ....'+authDataToken);
		try {
			//http://api.remitanywhere.com/restapi/lists/payee/countries

			let response = await fetch('http://api.yellotasker.com/api/v1/getPostTask',
				{
					method: 'POST',
					headers:
						{
							'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
					body: JSON.stringify({
						totalAmount: shortByValue
					})
				});
			let res = await response.text();
			let result = JSON.parse(res);
			console.log(result);

		} catch (errors) {
			console.log(JSON.stringify(errors));
		}
	}

	categoryList() {
		AsyncStorage.getItem('DynamicCategory', (err, result) => {

			let cat = JSON.parse(result);
			var catOption = [];
			for (var i = 0; i < cat.length; i++) {
				catOption.push({ value: cat[i].category_id, label: cat[i].category_name })
			}
			this.setState({
				// dataSource: this.state.dataSource.cloneWithRows(data)
				categoryOption: catOption
			})
		});
		let len = this.state.categoryOption;
		console.log(len.length);
	}
	componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
		BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
	}
	handleBack() {
		global.count = global.count + 1;
		if (global.count > 1) {
			Alert.alert(
				'Confirmation',
				'Are sure want to quit this App',
				[
					{ text: 'OK', onPress: () => BackHandler.exitApp() },
				]
			);
		} else {
			global.setTimeout(() => {
				global.count = 0;
			}, 1000);
		}
		return true;
	}
	handleConnectionChange = (isConnected) => {
		this.setState({ status: isConnected });
		let networkInfo = this.state.status;
		if (isConnected) {
			this.setState({ visible: false });
			this.setState({ netInfoMsg: false });
		} else {
			this.setState({ visible: true });
			this.setState({ netInfoMsg: true });
		}
	}

	getTaskList = async () => {
		console.log('Call Function...' + this.state.page);
		this.setState({ visible: true });
		let page_num = this.state.page;
		const response = await fetch(
			'http://api.yellotasker.com/api/v1/getPostTask?page_size=10&page_num='+page_num+'&taskStatus=open'
		);
		const json = await response.json();
		console.log(json.data.reverse())
		// alert(json.data);
		this.setState(state => ({ data: [...state.data, ...json.data] }));
		//this.setState({data : [ ...state.data,...json.data]});
		//	this.setState({data :json.data});
		this.setState({ visible: false });
	}
	handleEnd = () => {
		//	alert('test')
		this.setState(state => ({ page: state.page + 1 }), () => this.getTaskList());

		//	this.getTaskList();
		// alert(this.state.page);

	}
	onSeeMore() {
		//this.setState({page : this.state.page+1});
		//this.getTaskList();
		this.setState(state => ({ page: state.page + 1 }), () => this.getTaskList());

	}
	closeDrawer = () => {
		this.drawer._root.close()
	};
	openDrawer = () => {
		this.drawer._root.open()
	};
	onDashboardRender() {
		const { navigate } = this.props.navigation;
		navigate('Dashboard');
	}
	onRegisterValidate() {
		if (this.state.taskBudget == "") {
			this.setState({ error: 'Please enter required fields' });
		} else if (this.state.taskPeopleRequired == "") {
			this.setState({ error: 'Please enter required fields' });
		} else {
			this.onRegisterPressed();
		}


	}
	onBrowsePressed() {
		const { navigate } = this.props.navigation;
		navigate('Browse');
	}
	onTaskPressed() {
		let userId = global.userId;
		// alert('userId'+userId);
		const { navigate } = this.props.navigation;
		//const { navigate } = this.props.navigation;
		global.category_id = null;
		navigate('Mainview');
	}
	onLoginPressed() {
		const { navigate } = this.props.navigation;
		navigate('Login');
	}
	onMyTaskPressed() {
		let userId = global.userId;
		// alert('userId'+userId);
		const { navigate } = this.props.navigation;
		if (userId) {

			navigate('MyTask');
		} else {
			Alert.alert(
				'Message',
				'Please Login to go to My task',
				[
					{ text: 'OK', onPress: () => { this.onLoginPressed() } }
				]
			);
		}
	}
	onMyProfilePressed(){
		let userId = global.userId;
		// alert('userId'+userId);
	   const { navigate } = this.props.navigation;
	   if(userId){
	   
		navigate('Profile'); 
		}else{
	   Alert.alert(
		  'Message',
		  'Please Login to go to Profile',
		  [
		  {text : 'OK', onPress:()=>{this.onLoginPressed()}}
		  ]
		 );
		}
	  }

	onNextStep() {
		// alert('chgghdhg');
	}
	readMoreCount(description) {
		return description!==undefined && description.length;
	}
	readMore(description) {
		if (description!==undefined &&description.length > 50) {
			return description.substring(0, 48) + '...';
		} else {
			return description;
		}

	}
	readMoreTitle(description) {
		if (description!==undefined && description.length > 32) {
			return description.substring(0, 30) + '...';
		} else {
			return '';
		}

	}
	DateChangeFormate(duedate) {
		if (duedate) {
			var date = duedate;
			//		var date = "2017-12-04";
			//var arr1 = date.split(' ');
			var arr2 = date.split('-');
			var month;
			if (arr2[1] == '01') {
				month = 'Jan';
			} else if (arr2[1] == '02') {
				month = 'Feb';
			} else if (arr2[1] == '03') {
				month = 'Mar';
			} else if (arr2[1] == '04') {
				month = 'Apr';
			} else if (arr2[1] == '05') {
				month = 'May';
			} else if (arr2[1] == '06') {
				month = 'June';
			} else if (arr2[1] == '07') {
				month = 'July';
			} else if (arr2[1] == '08') {
				month = 'Aug';
			} else if (arr2[1] == '09') {
				month = 'Sep';
			} else if (arr2[1] == '10') {
				month = 'Oct';
			} else if (arr2[1] == '11') {
				month = 'Nov';
			} else {
				month = 'Dec';
			}
			var dt = month + ' ' + arr2[2] + ',' + arr2[0];
			return dt;
		}
		//alert(dt);

	}
	onsearchShow() {
		this.setState({ searchShow: true });

	}
	onsearchHide() {
		this.setState({ searchShow: false });

	}
	dynamicRating(rating) {
		// 		let optn = '<View>';
		// 		for(let i=0;i<rating;i++){
		// 			optn += "<FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>";
		// 		}
		// 		optn +='</View>';
		// return optn;
	}
	onSettingPress() {
		this.setState({ isViewSetting: true })
		this.setState({
			latestTaskLabel: ' Latest task posted',
			dueDateLabel: ' Browse By Due Date',
			budgetLabel: ' Browse By Budget',
			locationLabel: ' Browse By location',
			categoryLabel: ' Browse By Category',
			bookmarkLabel: ' Browse by bookmark'
		})
	}

	render() {
		const { params } = this.props.navigation.state;
		const { navigate } = this.props.navigation;
		return (
			<Drawer
				ref={(ref) => { this.drawer = ref; }}
				content={<SideBar navigator={this.navigator} />}
				onClose={() => this.closeDrawer()} >
				<View style={styles.mainviewStyle}>


					<Modal
						animationType="fade"
						transparent={true}
						visible={this.state.visible}>
						<View style={{
							flex: 1,
							alignItems: 'center',
							flexDirection: 'column',
							justifyContent: 'space-around',
							backgroundColor: '#00000040'
						}} >
							<View style={{
								backgroundColor: 'transparent',
								height: 100,
								width: 200,
								borderRadius: 3,
								alignItems: 'center',
								justifyContent: 'space-around'
							}}>
								<Image source={require('../img/loading.png')} />
							</View>
						</View>
					</Modal>

					<Modal
						animationType="fade"
						transparent={true}
						visible={this.state.isViewSetting}>
						<View style={{
							flex: 1,

							flexDirection: 'column',
							paddingTop: 20,
							backgroundColor: '#fff'
						}} >

							<View style={{ flexDirection: 'row', backgroundColor: '#000000', alignItems: 'center', paddingVertical: 10 }}>
								<View style={{ flex: 0.2, alignItems: 'flex-start', paddingLeft: 10 }}>
									<TouchableHighlight style={{ backgroundColor: '#000', padding: 5, }} underlayColor={'transparent'} onPress={() => this.setState({ isViewSetting: false })}><FontAwesome style={{ fontSize: 18, color: '#fff' }}>{Icons.arrowLeft}</FontAwesome></TouchableHighlight>
								</View>
								<View style={{ flex: 0.6, alignItems: 'center', }}>
									<Text> <Image source={require('../img/app-logo.png')} /> </Text>
								</View>
								<View style={{ flex: 0.2, alignItems: 'flex-end', paddingVertical: 8, paddingRight: 4 }}>
									<TouchableHighlight style={{ backgroundColor: '#000', padding: 5, }} underlayColor={'transparent'} onPress={() => this.setState({ isViewSetting: false })}>

										<FontAwesome style={{ fontSize: 18, color: '#fff' }}>{Icons.close}</FontAwesome>

									</TouchableHighlight>
								</View>

							</View>
							<View style={styles.secondHeader}>
								<Text style={styles.headerText}>Filters</Text>
							</View>
							<View style={{ flexDirection: 'column', backgroundColor: '#eee', borderRadius: 4, paddingVertical: 10, paddingHorizontal: 15 }}>
								<View style={{ paddingVertical: 10 }}>
									<Dropdown
										ref={this.dateRef}
										onChangeText={this.onChangeDefault}
										label={this.state.latestTaskLabel}
										data={this.state.latesttaskOption}
										pickerStyle={styles.textContainer}
										containerStyle={styles.containerStyle}
										itemPadding={4}
										dropdownOffset={{ top: 42, left: 0 }}

										itemTextStyle={styles.itemTextStyle}
										labelHeight={10}
										fontSize={14}

										inputContainerStyle={{ borderBottomColor: 'transparent', paddingHorizontal: 5 }}


									/>
								</View>

								<View style={{ paddingVertical: 10 }}>
									<Dropdown
										ref={this.dateRef}
										onChangeText={this.onChangeDate}
										label={this.state.dueDateLabel}
										data={this.state.dueDateOption}
										pickerStyle={styles.textContainer}
										containerStyle={styles.containerStyle}
										itemPadding={4}
										dropdownOffset={{ top: 42, left: 0 }}

										itemTextStyle={styles.itemTextStyle}
										labelHeight={10}
										fontSize={14}
										inputContainerStyle={{ borderBottomColor: 'transparent', paddingHorizontal: 5 }}

									/>
								</View>
								<View style={{ paddingVertical: 10 }}>
									<Dropdown
										ref={this.dateRef}
										onChangeText={this.onChangebudget}
										label={this.state.budgetLabel}
										data={this.state.budgetOption}
										pickerStyle={styles.textContainer}
										containerStyle={styles.containerStyle}
										itemPadding={4}
										dropdownOffset={{ top: 42, left: 0 }}

										itemTextStyle={styles.itemTextStyle}
										labelHeight={10}
										fontSize={14}
										inputContainerStyle={{ borderBottomColor: 'transparent', paddingHorizontal: 5 }}

									/>
								</View>
								<View style={{ paddingVertical: 10 }}>
									<Dropdown
										ref={this.dateRef}
										onChangeText={this.onChangelocation}
										label={this.state.locationLabel}
										data={this.state.locationOption}
										pickerStyle={styles.textContainer}
										containerStyle={styles.containerStyle}
										itemPadding={4}
										dropdownOffset={{ top: 42, left: 0 }}

										itemTextStyle={styles.itemTextStyle}
										labelHeight={10}
										fontSize={14}
										inputContainerStyle={{ borderBottomColor: 'transparent', paddingHorizontal: 5 }}

									/>

								</View>
								<View style={{ paddingVertical: 10 }}>
									<Dropdown
										ref={this.dateRef}
										onChangeText={this.onChangeCategory}
										label={this.state.categoryLabel}
										data={this.state.categoryOption}
										pickerStyle={styles.textContainer}
										containerStyle={styles.containerStyle}
										itemPadding={4}
										dropdownOffset={{ top: 42, left: 0 }}

										itemTextStyle={styles.itemTextStyle}
										labelHeight={10}
										fontSize={14}
										inputContainerStyle={{ borderBottomColor: 'transparent', paddingHorizontal: 5 }}


									/>
								</View>
								<View style={{ paddingVertical: 10 }}>
									<Dropdown
										ref={this.dateRef}
										onChangeText={this.onChangebookmark}
										label={this.state.bookmarkLabel}
										data={this.state.bookmarkOption}
										pickerStyle={styles.textContainer}
										containerStyle={styles.containerStyle}
										itemPadding={4}
										dropdownOffset={{ top: 42, left: 0 }}

										itemTextStyle={styles.itemTextStyle}
										labelHeight={10}
										fontSize={14}
										inputContainerStyle={{ borderBottomColor: 'transparent', paddingHorizontal: 5 }}


									/>
								</View>
								<View style={{ paddingVertical: 10 }}>
									<Text style={{ textAlign: 'center', paddingVertical: 10, borderColor: '#efeb10', borderRadius: 6, backgroundColor: '#efeb10', overflow: "hidden", color: "#000", fontWeight: 'bold' }} onPress={() => this.onPressApply()}> Apply </Text>
								</View>

							</View>
						</View>

					</Modal>

					<View style={{ flexDirection: 'row', backgroundColor: '#000000', alignItems: 'center', paddingVertical: 10 }}>
						<View style={{ flex: 0.2, alignItems: 'flex-start', paddingLeft: 10 }}>
							<Text>


							</Text>

						</View>
						<View style={{ flex: 0.6, alignItems: 'center', }}>
							<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
								<Image source={require('../img/app-logo.png')} />
							</Text>
						</View>
						<View style={{ flex: 0.2, alignItems: 'flex-end', paddingVertical: 8, paddingRight: 4 }}>
							<TouchableHighlight style={{ backgroundColor: '#000', padding: 5, }} onPress={() => this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>

								<FontAwesome style={{ fontSize: 18, color: '#fff' }}>{Icons.navicon}</FontAwesome>

							</TouchableHighlight>
						</View>

					</View>
					<ScrollView contentContainerStyle={styles.scrollViewStyle}>
						<View style={styles.formViewStyle} >
							<View style={styles.secondHeader}>
								{this.state.searchShow &&
									<TextInput
										placeholder="Search" style={[styles.InputText, this.state.taskValiMsg && styles.textInputAlt]}
										underlineColorAndroid="transparent" returnKeyType="next"

										keyboardType="email-address"
										autoCapitalize="none"
										autoCorrect={false}

									></TextInput>
								}
								{!this.state.searchShow &&
									<Text style={styles.headerText}>{this.state.searchCaption}
									</Text>
								}
								<View style={styles.settingDiv}>
									<TouchableHighlight onPress={this.onSettingPress.bind(this)} underlayColor={'transparent'}>
										<FontAwesome style={{ fontSize: 18, color: '#000000', }}>{Icons.gear}</FontAwesome>
									</TouchableHighlight>
								</View>
								<View style={styles.searchDiv}>
									{!this.state.searchShow &&
										<TouchableHighlight onPress={this.onsearchShow.bind(this)} underlayColor={'transparent'}>
											<FontAwesome style={{ fontSize: 18, color: '#000000', }}>{Icons.search}</FontAwesome>
										</TouchableHighlight>
									}
									{this.state.searchShow &&
										<TouchableHighlight onPress={this.onsearchHide.bind(this)} underlayColor={'transparent'}>
											<FontAwesome style={{ fontSize: 18, color: '#000000', }}>{Icons.search}</FontAwesome>
										</TouchableHighlight>
									}
								</View>
							</View>

							<View style={{ marginBottom: 70, backgroundColor: '#ffffff' }}>
								<FlatList
									data={this.state.data}
									keyExtractor={(x, i) => i}
									renderItem={({ item }) =>
										<View style={{ paddingHorizontal: 5 }}>
											<TouchableHighlight onPress={() => navigate('SingleTask', { id: item.id })}>
												<View style={styles.mainBrowse}>
													{/* <View style = {styles.browseMen}>
		   {!item.user_detail.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
		   {item.user_detail.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
				
			</View> */}
													<View style={{ flex: 1, flexDirection: 'row' }}>
														<View style={styles.browseContent}>
															<Text style={styles.browseContentHeading} onPress={() => navigate('SingleTask', { id: item.id, back: 'Browse' })}>{this.readMoreTitle(item.title)}</Text>

															<Text style={styles.browseSubContent}>{this.readMore(item.description)}
																{this.readMoreCount(item.description) > 24 && <Text style={{ color: '#337ab7', fontWeight: 'bold' }} onPress={() => navigate('SingleTask', { id: item.id, back: 'Browse' })}> Read More</Text>}
															</Text>
															<View style={{ flexDirection: 'row', flex: 1, paddingVertical: 5, }}>

																<View style={{ paddingHorizontal: 10, }}>
																	{!item.user_detail.profile_image &&
																		<Image source={{ uri: 'http://yellotasker.com/assets/img/task-person.png' }} style={styles.menImage} />
																	}
																	{item.user_detail.profile_image &&
																		<Image source={{ uri: item.user_detail.profile_image }} style={styles.userImage} />
																	}
																</View>

																<View style={{ flexDirection: 'column' }}>
																	<Text style={{ paddingVertical: 3, color: '#337ab7', alignItems: 'flex-start', flex: 0.5 }}>{item.user_detail.first_name} {item.user_detail.last_name}</Text>
																	{item.user_detail.rating == '1' && <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10, flex: 0.5 }}><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome></View>}
																	{item.user_detail.rating == '2' && <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10, flex: 0.5 }}> <FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', paddingHorizontal: 2, }}>{Icons.star}</FontAwesome></View>}
																	{item.user_detail.rating == '3' && <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10, flex: 0.5 }}><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', paddingHorizontal: 2, }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome></View>}
																	{item.user_detail.rating == '4' && <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10, flex: 0.5 }}><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', paddingHorizontal: 2, }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', paddingHorizontal: 2, }}>{Icons.star}</FontAwesome></View>}
																	{item.user_detail.rating == '5' && <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 10, flex: 0.5 }}><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', paddingHorizontal: 2, }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', paddingHorizontal: 2, }}>{Icons.star}</FontAwesome><FontAwesome style={{ fontSize: 14, color: '#FFA500', }}>{Icons.star}</FontAwesome> </View>}
																</View>
															</View>
															{/*
		  <Text style={{color:'#000',paddingVertical:5,}}>
		  <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome> <Text>{item.address ==null ||item.address =='' ? item.locationType : item.address}</Text>
		  </Text>
		  <Text style={{color:'#000',fontWeight:'bold',backgroundColor:'red'}}><FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center',}}>{Icons.calendar}</FontAwesome> Due Date <Text>{this.DateChangeFormate(item.dueDate)}</Text> </Text>
		*/}
														</View>

														<View style={styles.browseOffer}>
															<Text style={styles.browseOfferText}> {item.totalAmount} MYR</Text>
															<Text style={styles.browseOfferView} onPress={() => navigate('SingleTask', { id: item.id })}>View</Text>

														</View>
													</View>
													<View style={{ paddingHorizontal: 10 }}>
														{/* <Text style={{color:'#000',paddingVertical:5}}>
		  <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome> <Text>{item.address ==null ||item.address =='' ? item.locationType : item.address}</Text>
		  </Text>*/}

														{/*  <Text style={{color:'#000000',fontWeight:'bold',flexDirection:'row',}}>
      
    <FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> Due Date <Text style={{color:'#666'}}>{this.DateChangeFormate(item.dueDate)}</Text>
    
    
    </Text>*/}

														<View style={{ flexDirection: 'row', flex: 1, paddingVertical: 4 }}>
															<Text style={{ flex: 0.5, textAlign: 'left' }}>
																<Text style={{ color: '#000000', fontWeight: 'bold', flexDirection: 'row', }}>
																	<FontAwesome style={{ fontSize: 10, color: '#000000', }}>{Icons.calendar}</FontAwesome> Due Date <Text style={{ color: '#666', fontSize: 13 }}>{this.DateChangeFormate(item.dueDate)}</Text>
																</Text>
															</Text>
															<Text style={{ flex: 0.5, fontWeight: 'bold', flexDirection: 'row', textAlign: 'right', color: '#484848' }}><Text>{item.offer_count} Offer</Text> <Text>{item.comment_count} Comments</Text></Text>
														</View>
													</View>
												</View>
											</TouchableHighlight>
										</View>
									}
								/>
								{!this.state.visible &&
									<View style={{ alignItems: 'flex-end', marginRight: 10 }}><Text onPress={this.onSeeMore.bind(this)} style={styles.browseOfferView}>See More</Text></View>
								}
							</View>


						</View>
					</ScrollView>

					<View style={styles.footer}>
						<View style={styles.browseButtons}>
							<TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
								<FontAwesome style={{ fontSize: 15, color: '#efeb10' }}>{Icons.search}</FontAwesome>
							</TouchableHighlight>
							<Text style={[styles.footerText, { color: '#efeb10' }]} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
						</View>
						<View style={styles.postButtons}>
							<TouchableHighlight onPress={this.onTaskPressed.bind(this)}>

								<FontAwesome style={{ fontSize: 15, color: '#fff' }}>{Icons.plusSquare}</FontAwesome>

							</TouchableHighlight>
							<Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
						</View>
						<View style={styles.myTaskButtons}>
							<TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
								<FontAwesome style={{ fontSize: 15, color: '#fff' }}>{Icons.clipboard}</FontAwesome>
							</TouchableHighlight>
							<Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
						</View>
						<TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
					</View>
				</View>
			</Drawer>

		);
	}



}



//AppRegistry.registerComponent('list', () => list) //Entry Point    and Root Component of The App

