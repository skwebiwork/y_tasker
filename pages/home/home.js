import React, { Component } from 'react';
import {
  AppRegistry,NetInfo,
  StyleSheet,
  Text,BackHandler,
  View,Image,Button,TouchableOpacity,TouchableHighlight,ScrollView,Alert,AsyncStorage,BackAndroid,Modal,ActivityIndicator,Dimensions 
} from 'react-native';
//import Spinner from 'react-native-loading-spinner-overlay';
import styles from "./homeStyle";
import SplashScreen from "rn-splash-screen";
var {width, height} = Dimensions.get('window');
export default class home extends Component {
  static navigationOptions = {
     header: null ,
  };
  constructor() {
    super();
    global.count = 0;
   global.isLaunch = 0;
   global.isLoading = 0;

    this.state = {
         LodingStatus:false,
          visible : false,   
          isLoading: false,
          isFinishedloading : true,    
          touchMsg : false,   
          status :'', 
    }
  }
  componentDidMount(){
    if(global.isLaunch==0){
    //this.startUpFun();
    global.isLaunch = 1;
  }
  BackHandler.addEventListener('hardwareBackPress', this.handleBack);
 NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
     NetInfo.isConnected.fetch().done(
       (isConnected) => { this.setState({ status: isConnected }); }
     );
     
    this.categoryList();
 
  }
  componentWillUnmount() {
    //Forgetting to remove the listener will cause pop executes multiple times
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  }
  categoryList(){

   
	   fetch('http://api.yellotasker.com/api/v1/category')
	   .then((response) => response.json())
	   .then((responseJson) =>{
		   data = responseJson.data; // here we have all products data
		   console.log('result category..........');
    //   console.log(data);
      AsyncStorage.setItem('DynamicCategory', JSON.stringify(data), () => {
     
        AsyncStorage.getItem('DynamicCategory', (err, result) => {
        // console.log(result);
        });
     
    });
		   
  //  AsyncStorage.setItem('taskData','null');
	   })
	   .catch((error) =>{
		   console.error(error);
	   });
  }
  startUpFun(){
    global.setTimeout(() => {
      // this.setState({isLoading: true});
       global.isLaunch = 1;
     // this.setState({isLoading: true});
     }, 3000);
  }
  handleBack() {
    global.count = global.count+1;
    //alert('count'+global.count);
    if(global.count>1){
      Alert.alert(
        'Confirmation',
        'Are sure want to quit this App',
        [
          {text: 'OK', onPress: () => BackHandler.exitApp()},
        ]
    );
    }else{
      //this.setState({touchMsg : true});
      
      global.setTimeout(() => {
        global.count = 0;
      }, 1000);
    }
      
    return true;
    
      }
  handleConnectionChange = (isConnected) => {
    this.setState({ status: isConnected });
   let networkInfo = this.state.status;
   if(isConnected){
   
   AsyncStorage.getItem("FirstName").then((value) => {
    if(value){
      global.FirstName = value;}
 }).done();
 AsyncStorage.getItem("LastName").then((value) => {
  if(value){
    global.LastName = value;
   
  }
}).done();
AsyncStorage.getItem("profileImage").then((value) => {
  if(value){
    global.profileImage = value;
   
  }
}).done();
AsyncStorage.getItem("loginId").then((value) => {
  if(value){
    global.userId = value;
    const { navigate } = this.props.navigation;
    //  this.UserDetail();
    
       navigate('Dashboard');
  }
}).done();

 //  this.setState({ visible: false });
   }else{
   // this.setState({ visible: true });
   }
}
UserDetail(){
  // this.setState({visible: true}); 
    fetch('http://api.yellotasker.com/api/v1/userDetail'+global.userId)
    .then((response) => response.json())
    .then((responseJson) =>{
     var data = responseJson.data; // here we have all products data
     global.firstName = data.first_name;
     global.lastName = data.last_name;
      if(data.profile_image){
     global.profileImage = data.first_name;
     }else{
         // dataSource: this.state.dataSource.cloneWithRows(data)
         global.profileImage = 'http://yellotasker.com/assets/img/task-person.png';
       
     }
      //this.setState({visible: false}); 
    })
    .catch((error) =>{
      console.error(error);
    });
 }
render() {
  //SplashScreen.hide();
     //const { navigate } = this.props.navigation;
    
    return (
      
      <ScrollView style={styles.container}>
       <Modal
         
          transparent={false}
          visible={this.state.visible}>
       <ActivityIndicator  style={styles.indicator} color="#000000" size = "large"></ActivityIndicator>
       <Text style={{color:'#333333',textAlign:'center',marginTop:height/2+10}}>Please check your network connection</Text>
   </Modal>
      <View style={styles.column}>
        <Image 
       style={styles.logo}
      source ={require('../img/logo1.png')} />
      <Text style={styles.welcome}>
       Get More Done
      </Text>
      <Text style={styles.instructions}>
      Sign up /Login to get most out of yellotasker
       </Text>
      </View>
       <View style={[styles.buttonsDiv,{paddingTop:40}]}>
              
                    <TouchableHighlight style={styles.buttontouchLogin} onPress={()=> this.props.navigation.navigate('SignUp')}>
                    <Text style={styles.buttonSkipText}>
                      Sign Up
                    </Text>
                    </TouchableHighlight>
             
      </View>
      <View style={styles.buttonsDiv}>
      <TouchableHighlight style={styles.buttontouchLogin} onPress={()=>this.props.navigation.navigate('Login')}>
                      <Text style={styles.buttonTextLogin}>
                        Login
                      </Text>
                  </TouchableHighlight>
      </View>
      <View style={styles.buttonsDiv}>
          <TouchableHighlight style={styles.skipDiv} onPress={()=>this.props.navigation.navigate('Dashboard')}>
            <Text style={styles.buttonSkipText}>
                  Skip & Explore
                            </Text>
          </TouchableHighlight>
      </View>          
      </ScrollView>
    );
  
 

  }
}
//AppRegistry.registerComponent('home', () => home);
