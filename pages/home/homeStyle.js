import EStyleSheet from 'react-native-extended-stylesheet';
const styles = EStyleSheet.create({
    column: {
      alignItems: 'center',
        },
        
    container: {
      flex: 1,
      backgroundColor: '#000',
       flexDirection:'column', 
        padding : 20,
    },
    logo : {
      marginTop: '10%',
      
        },
        welcome : {
          marginTop: '30%',
          fontSize: 18,
      fontWeight: '700',
      color:'#fff',
    },
     instructions: {
      marginTop: 7,
      textAlign: 'center',
      color: '#fff',
      marginBottom: 5,
      fontSize: 16,
    },
    buttonConatiner : {
    flexDirection:'row'
    },
    textContainer : {
  backgroundColor : '#000000',
  paddingVertical : 10,
    },
    textSignUp : {
   textAlign: 'center',
    },
    buttonDiv : {
   flexDirection: 'row',
   alignItems: 'center',
   paddingVertical : '2%',
   paddingHorizontal : '9%',
  
    },
   buttonsDiv : {
       paddingVertical : '3%',
       paddingHorizontal : '9%',
      },
     buttontouch:{
  
      paddingHorizontal : 8,
      paddingVertical : 12,
   
      backgroundColor : '#e8e8e8',
     
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#e8e8e8',
     
    },
      buttonText: {
      fontSize: 16,
      color: '#fff',
      textAlign: 'center',
  },
  buttonSkipText : {
    fontSize: 16,
    color: '#000',
    textAlign: 'center',
  },
  '@media (max-width: 360)': {
    buttonText: {
      color:'white',
      alignItems:'center',
      fontSize:13,
     
    
    },
    buttonTextLogin: {
      fontSize: 16,
      color: '#000000',
      textAlign: 'center',
  },
  },
  buttontouchLogin:{
   
    paddingHorizontal : 8,
    paddingVertical : 12,
      backgroundColor : '#fff',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#fff',
     
       marginRight :'1%',
    },
      buttonTextLogin: {
      fontSize: 16,
      color: '#000000',
      textAlign: 'center',
  },skipDiv : {
      
      backgroundColor : '#efeb10',
      paddingHorizontal : 8,
      paddingVertical : 12,
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#efeb10',
  },
  splashContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:'#ffffff',
  },
  });

  export default styles;