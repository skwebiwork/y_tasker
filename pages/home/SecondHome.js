import React, { Component } from 'react';
import {
  AppRegistry,NetInfo,
  StyleSheet,
  Text,
  View,Image,Button,TouchableOpacity,TouchableHighlight,ScrollView,Alert,AsyncStorage,BackHandler,Modal,ActivityIndicator,Dimensions 
} from 'react-native';
//import Spinner from 'react-native-loading-spinner-overlay';
import styles from "./homeStyle";
import SplashScreen from "rn-splash-screen";
var {width, height} = Dimensions.get('window');
export default class SecondHome extends Component {
  static navigationOptions = {
     header: null ,
  };
  constructor() {
    super();
    global.count = 0;
   global.isLaunch = 0;
    this.state = {
         LodingStatus:false,
          visible : false,   
          isLoading: false,
          isFinishedloading : true,    
          touchMsg : false,    
    }
  }
  componentDidMount(){

    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
 NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
     NetInfo.isConnected.fetch().done(
       (isConnected) => { this.setState({ status: isConnected }); }
     );
  }
  componentWillUnmount() {
    //Forgetting to remove the listener will cause pop executes multiple times
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  }
  handleBack() {
    global.count = global.count+1;
    //alert('count'+global.count);
    if(global.count>1){
      Alert.alert(
        'Confirmation',
        'Are sure want to quit this App',
        [
          {text: 'OK', onPress: () => BackHandler.exitApp()},
        ]
    );
    }else{
      //this.setState({touchMsg : true});
      
      global.setTimeout(() => {
        global.count = 0;
      }, 1000);
    }
      
    return true;
    
      }
  handleConnectionChange = (isConnected) => {
    this.setState({ status: isConnected });
   let networkInfo = this.state.status;
   if(isConnected){
    AsyncStorage.getItem("loginId").then((value) => {
      if(value){
        global.userId = value;
       const { navigate } = this.props.navigation;
        navigate('Dashboard');
      }else{
        
      }
   }).done();
   AsyncStorage.getItem("profileImage").then((value) => {
    if(value){
      global.profileImage = value;
     
    }
  }).done();
  AsyncStorage.getItem("FirstName").then((value) => {
    if(value){
      global.FirstName = value;}
 }).done();
 AsyncStorage.getItem("LastName").then((value) => {
  if(value){
    global.LastName = value;
   
  }
}).done();
   this.setState({ visible: false });
   }else{
    this.setState({ visible: true });
   }
}
render() {

    
    return (
      
      <ScrollView style={styles.container}>
       <Modal
          animationType ={'fade'}
          transparent={false}
          visible={this.state.visible}>
                    <ActivityIndicator  style={styles.indicator} color="#000000" size = "large"></ActivityIndicator>

       <Text style={{color:'#333333',textAlign:'center',marginTop:height/2+10}}>Please check your network connection</Text>
   </Modal>
      <View style={styles.column}>
        <Image 
       style={styles.logo}
      source ={require('../img/logo1.png')} />
      <Text style={styles.welcome}>
       Get More Done
      </Text>
      <Text style={styles.instructions}>
      Sign up /Login to get most out of yellotasker
       </Text>
      </View>
      <View style={[styles.buttonsDiv,{paddingTop:40}]}>
              
              <TouchableHighlight style={styles.buttontouch} onPress={()=> this.props.navigation.navigate('SignUp')}>
              <Text style={styles.buttonSkipText}>
                Sign Up
              </Text>
              </TouchableHighlight>
       
</View>
<View style={styles.buttonsDiv}>
<TouchableHighlight style={styles.buttontouchLogin} onPress={()=>this.props.navigation.navigate('Login')}>
                <Text style={styles.buttonTextLogin}>
                  Login
                </Text>
            </TouchableHighlight>
</View>
<View style={styles.buttonsDiv}>
    <TouchableHighlight style={styles.skipDiv} onPress={()=>this.props.navigation.navigate('Dashboard')}>
      <Text style={styles.buttonSkipText}>
            Skip & Explore
                      </Text>
    </TouchableHighlight>
</View> 
      </ScrollView>
    );
 

  }
}
AppRegistry.registerComponent('SecondHome', () => SecondHome);
