import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,Alert,
  View,
  Image,Button,TouchableOpacity,ScrollView,TouchableHighlight,TextInput,KeyboardAvoidingView,Modal,AsyncStorage,ProgressBar,ActivityIndicator,BackHandler
} from 'react-native';
//import Spinner from 'react-native-loading-spinner-overlay';
import styles from "./loginStyle";
export default class login extends Component {
  
 
   static navigationOptions = {
     header: null ,
  };
   constructor(){
    super()

    this.state = {
      email:"",
      password:"",
      error:"",
    LodingStatus:false,
     modalVisible: false,
     loading : true,
     visible : false,
     emailValid : false,
     emailValidMsg: false,
     passwordValidMsg: false
    }
  }
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    }
    componentWillUnmount() {
      //Forgetting to remove the listener will cause pop executes multiple times
      BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
    }
    handleBack() {
  global.count = global.count+1;
  //alert('count'+global.count);
  if(global.count>1){
    Alert.alert(
      'Confirmation',
      'Are sure want to quit this App',
      [
        {text: 'OK', onPress: () => BackHandler.exitApp()},
      ]
  );
  }else{
    //this.setState({touchMsg : true});
    
    global.setTimeout(() => {
      global.count = 0;
    }, 1000);
  }
    
  return true;
  
    }
 setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  validate = (text) => {
//console.log(text);
let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
if(reg.test(text) === false)
{
console.log("Email is Not Correct");
//this.setState({error: 'Please enter valid email'});
this.setState({email:text})
this.setState({emailValid: false});
//this.setState({emailValidMsg: true});

return false;
  }
else {
  this.setState({email:text})
  console.log("Email is Correct");
  this.setState({error: ''});
  this.setState({emailValid: true});
  this.setState({emailValidMsg: false});
}
}

  async onloginPressed() {
    try {
      global.userId = "";
      global.firstName = "";
      global.lastName = "";
      global.profileImage = 'http://yellotasker.com/assets/img/task-person.png';
      
      const { navigate } = this.props.navigation;
      this.setState({visible: true});
    
      let response = await fetch('http://api.yellotasker.com/api/v1/user/login', {
                            method: 'POST',
                            headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({

                                 email: this.state.email,
                                 password: this.state.password,

                            })
                          });
      let res = await response.text();
      if(response.status >= 200){
        this.setState({visible: false});
       // this.setState({LodingStatus: false});
         let result= JSON.parse(res); 
        // this.setState({error: result.message});
        //alert(JSON.stringify(result.data));
        console.log(result);
         if(result.status==1){
          // alert(result.data.id);
          console.log(res);
              let _id =  result.data.id.toString();
          global.userId = result.data.id;
          global.FirstName = result.data.first_name;
          global.LastName = result.data.last_name;
          if(result.data.profile_image){
            global.profileImage = result.data.profile_image;
            }else{
                // dataSource: this.state.dataSource.cloneWithRows(data)
                global.profileImage = 'http://yellotasker.com/assets/img/task-person.png';
              
            }
            let _firstName = result.data.first_name.toString();
            let _lastName = result.data.last_name.toString();
          AsyncStorage.setItem("loginId", _id);
          AsyncStorage.setItem("FirstName", _firstName);
          AsyncStorage.setItem("LastName", _lastName);
          AsyncStorage.setItem("profileImage", global.profileImage);
          
          //this.setState({error: result.message});
          
         
         AsyncStorage.getItem("postDataJson").then((value) => {
          if(value){
            alert(JSON.stringify(value));
            navigate('ThirdStep');
          }else{
            navigate('Dashboard');
          }
       }).done();
          
         }else if(result.status==0){
          this.setState({error: result.message});
         }
       


      }else{
        let error = res;
        throw error;
      }
    } catch (error) {
      alert(JSON.stringify(error));   
      //this.setState({error: error});
    }
  }
  UserDetail(){
      // this.setState({visible: true}); 
        fetch('http://api.yellotasker.com/api/v1/userDetail'+global.userId)
        .then((response) => response.json())
        .then((responseJson) =>{
         var data = responseJson.data; // here we have all products data
         global.firstName = data.first_name;
         global.lastName = data.last_name;
         console.log(data.profile_image);
        
          //this.setState({visible: false}); 
        })
        .catch((error) =>{
          console.error(error);
        });
     }
  
  onloginPresseds(){
     if(this.state.email==""){
             this.setState({error: 'Please enter required fields'});
             this.setState({emailValidMsg: true});
     }else if(this.state.emailValid==false){
        this.setState({error: 'Please enter valid email'});
        this.setState({emailValidMsg: true});
     }else if(this.state.password==""){
      this.setState({passwordValidMsg: true});
            this.setState({error: 'Please enter required fields'});
     }else{
      this.setState({emailValidMsg: false});
      this.setState({passwordValidMsg: false});
             this.onloginPressed();

     }

    
  }
  onsignupPressed(){
    const { navigate } = this.props.navigation;
    navigate('SignUp');
  }
  onskipPressed(){
    const { navigate } = this.props.navigation;
   navigate('Dashboard'); 
  }
 onForgotPressed(){
  const { navigate } = this.props.navigation;
  navigate('ForgotPassword'); 

 }
  render() {
    return ( 

      <View style={styles.LoadingView}>
            <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
    
   </View>
   </View>
   </Modal>

       <View style={{alignItems:'center',backgroundColor:'#000000',paddingVertical:10,}}>
               <Image source={require('../img/app-logo.png')} />
      </View>
       
      <ScrollView style={styles.contentContainer}>
     
        <View style={styles.mainContainer}>
             <Image source={require('../img/banner-login.jpg')} style={styles.bannerImg}/>
              <View style={styles.container}>
              <Text style={styles.errorText}>{this.state.error}</Text>
              <View style={{paddingHorizontal : 20,paddingBottom:10}}>
      <TextInput
       placeholder="Email" style={[styles.InputText,this.state.emailValidMsg&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
      onSubmitEditing={()=>this.passwordInput.focus()}
      keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false}
     onChangeText={(text) => this.validate(text)}
      value={this.state.email}
       ></TextInput>
       </View>
       <View style={styles.buttonDiv}>
      <TextInput
       placeholder="Password"
        style={[styles.InputText,this.state.passwordValidMsg&& styles.errorInput]}  
        underlineColorAndroid = "transparent" secureTextEntry 
        ref={(input)=>this.passwordInput=input}
        onChangeText={(text)=>this.setState({password:text})}
        >
      </TextInput>
       </View>
       <View style={styles.buttonDiv} keyboardShouldPersistTaps='always'>
      <TouchableHighlight style={styles.LoginDiv} underlayColor='#efeb10' onPress={this.onloginPresseds.bind(this)}>
         <Text style={styles.extraText}>
              Login  
                         </Text>
      </TouchableHighlight>
       </View>
       <View style={styles.forgateDiv}>
       <View style={{ flex: 1 }}>
         <Text style={styles.smText} onPress={this.onForgotPressed.bind(this)}>
              Forgot Password?
            </Text>
            </View>
            <View style={{ flex:1 }}>
            <Text style={styles.smTexts} onPress={this.onsignupPressed.bind(this)}>
              Not a member? Register
            </Text>
       </View>
       </View>
              <View style={styles.buttonSec}>
      
      <Text style={styles.extrasText}>    
           Or 
         </Text>
         </View>
                    <View style={styles.buttonsDiv}>
      <TouchableHighlight style={styles.facebookDiv}>
         <Text style={styles.facebokText}>
              Login with Facebook
            </Text>
      </TouchableHighlight>
       </View>
             <View style={styles.buttonDiv}>
      <TouchableHighlight style={styles.googleDiv} onPress={() => {
          this.setModalVisible(true)
        }}>
         <Text style={styles.facebokText}>
              Login with G+
            </Text>
      </TouchableHighlight>
       </View>
      
           
          
            
             
 
       <View style={styles.buttonssDiv} keyboardShouldPersistTaps='always'>
      <TouchableHighlight style={styles.skipDiv} onPress={this.onskipPressed.bind(this)}>
         <Text style={styles.facebokText}>
              Skip & Explore
                         </Text>
      </TouchableHighlight>
       </View>
      </View>
              </View> 
      
        

  </ScrollView>
  </View>
    );
  }
}


//AppRegistry.registerComponent('login', () => login);
