import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    contentContainer: {
     
    },
    LoadingView : {
      
    },
    mainContainer:{
          borderTopWidth : 4,
        borderColor: '#efeb10',
    },
  bannerImg : {
    width:'100%',
    height : 115,
  },
  container : {
    flex: 1,
      backgroundColor: '#ffffff',
       flexDirection:'column', 
        paddingHorizontal : 20,
       paddingVertical : 12,
      },
      buttonsDiv : {
       paddingVertical : 12,
       paddingHorizontal : 20,
       
      },
      buttonDiv : {
       paddingVertical : 10,
       paddingHorizontal : 20,
      },
      buttonssDiv : {
        paddingVertical : 10,
       paddingHorizontal : 20,
         marginBottom : 60,
      },
      buttonSec : {
      
        paddingHorizontal : 20,
      },
      errorInput : {
        borderColor: '#dd4b39',
        borderWidth: 1,
      },
    facebookDiv : {
      paddingVertical : 11,
      backgroundColor : '#3b5998',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#3b5998',
   
    },
    googleDiv : {
      paddingVertical : 11,
      backgroundColor : '#dd4b39',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#dd4b39',
   
    },
    errorText : {
  color : '#dd4b39',
  textAlign : 'left',
  paddingHorizontal : 20,
  paddingVertical : 4,
    },
    facebokText : {
       color : '#fff',
        textAlign: 'center',
        fontSize: 18,
    },
  LoginDiv : {
    backgroundColor : '#efeb10',
    paddingVertical : 11, 
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#efeb10',
  
  },
  extraText : {
    color : '#000000',
    textAlign: 'center',
        fontSize: 18,
  },
  extrasText : {
    color : '#000000',
    textAlign: 'center',
        fontSize: 17,
  },
  forgateDiv : {
    paddingVertical : 6, 
    paddingHorizontal : 20,
    flex:1,
      flexDirection:'row',
  },
  smText : {
  color : '#666',
  fontSize: 12,  
  },
  smTexts : {
  color : '#666',
  fontSize: 12,
  textAlign: 'right',
  },
  skipDiv : {
      paddingHorizontal : 8,
      backgroundColor : '#000000',
    paddingVertical : 10,
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#000000',
  },
  InputText : {
    paddingVertical : 10,
    borderRadius:5,
    borderColor: '#000',
     borderWidth: 1,
     paddingHorizontal: 5,
    
  }
  });

  export default styles;