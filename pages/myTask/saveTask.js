import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Alert,Image,AppRegistry,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView,BackHandler,NetInfo,ActivityIndicator,FlatList} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';

import TimeAgo from 'react-native-timeago';

//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./myTaskStyle";

var {width, height} = Dimensions.get('window');
export class OfferComponent extends Component{
    
	render(){
		console.log(this.props.result);
		if(this.props.result){
			var res = this.props.result.map((offer,i)=>{
				return (
   <View key={i}>
						 <View style={styles.mainBrowses}>
					   <View style={styles.browseMen}>
						   <Image source={require('../img/browse-man.png')} style={styles.replymenImage} />
					   </View>
					   <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4}}>
					   <View style={{flexDirection:'row',marginTop:6}}>
					 <View style={{flex:0.7,flexDirection:'row'}}><Text style={{marginTop:5,textAlign:'center',color:'#337ab7'}}> {offer.first_name} {offer.last_name}</Text>
					 <View style={{flexDirection:'row',marginTop:6,marginLeft:7}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
					</View>
					 </View>
					 <View style={{flex:0.3,}}>
					 <TouchableHighlight>
						 <Text style={styles.PercentBtn }>
						80%
						 </Text>
						 </TouchableHighlight>
					 </View>
					 </View>
					 
					
					
				   
					   </View>                      
				   </View>
				   </View>
					)
			})
		}
		 return (
				  <View>
						 {res}
				  </View>
		 )
	}
}
export default class SaveTask extends Component {
			
	static navigationOptions = {
		header: null,
	};
	
	constructor(props){
		super(props);
		global.count = 0;
		this.onEndReachedCalledDuringMomentum = true;
	//	this.navigate = this.props.navigation.navigate;      
		this.state={
			touchMsg : false,
			taskPeopleRequired:"",
			taskBudget:"",
			 LodingStatus:false,
			 status : "",
			 error : "",
			 visible : false,
			 netInfoMsg : false,
			 data : [],
			dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			dataSources: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			searchShow : false,
			page:1,
			isLoading : false,
			saveTaskTotal : 0,
			taskDetailPopup:false,
			makeOffer:false,
			taskOffer : [],
			
		//link: 'http://hardeepcoder.com/laravel/easyshop/api/products/' + params.id,
		}
			
      //  this.navigate = this.props.navigation.navigate;
      //const navigation = this.props.navigation;
	}
	componentDidMount(){
		// const { params } = this.props.navigation.state;
		this.getTaskList();
	
	   
	}
	componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
	
	  }
    assignTextFun(status){
      var st = '';
      if(status=='assigned'){
    st = "Assigned to somebody";
      }else{
        st = status;
      }
      return st;
    }
	getTaskList = async() =>{
		//console.log('Call Function...'+this.state.page);
		
       this.setState({ visible: true });	
     const response = await fetch(
		'http://api.yellotasker.com/api/v1/getUserTask/'+global.userId+'?action=saveTask');
	 const json = await response.json();
    console.log(json.data);
    var jData = json.data.save_task[0].save_task;
	this.setState({saveTaskTotal : jData.length})
	global.saveTaskTotal = jData.length;
    console.log(jData);
	//this.setState(state =>({data :[ ...state.data,...jData]}));
	//this.setState({data :jData});
	//this.setState({	data: this.state.data.cloneWithRows(jData)  })
	this.setState({	dataSource: this.state.dataSource.cloneWithRows(jData)  })
    //this.setState({data : [ ...state.data,...json.data]});
    
//	this.setState({data :json.data});
	 this.setState({ visible: false });	
	}


    readMoreTitle(description){
        if(description.length>32){
            return description.substring(0,30)+'...';
        }else{
            return description;
        }
   
    }
 readMoreCount(description){
     return description.length;
 }
 readMore(description){
	 if(description.length>24){
		 return description.substring(0,22)+'...';
	 }else{
		 return description;
	 }

 }
 DateChangeFormate(duedate){
	 if(duedate){
	var date = duedate;
//		var date = "2017-12-04";
	  //var arr1 = date.split(' ');
	  var arr2 = date.split('-');
	  var month;
	  if(arr2[1]=='01'){
		  month = 'Jan';
	  }else if(arr2[1]=='02'){
		month = 'Feb';
	}else if(arr2[1]=='03'){
		month = 'March';
	}else if(arr2[1]=='04'){
		month = 'April';
	}else if(arr2[1]=='05'){
		month = 'May';
	}else if(arr2[1]=='06'){
		month = 'June';
	}else if(arr2[1]=='07'){
		month = 'July';
	}else if(arr2[1]=='08'){
		month = 'Aug';
	}else if(arr2[1]=='09'){
		month = 'Sep';
	}else if(arr2[1]=='10'){
		month = 'Oct';
	}else if(arr2[1]=='11'){
		month = 'Nov';
	}else{
		month = 'Dec';
	} 
	  var dt = month +' '+arr2[2]+','+arr2[0];
	  return dt;
}
	 //alert(dt);
	  
  }		
  indianDateFormat(dueDate){
    var arr = dueDate.split('-');
    var formattedDate = arr[2]+'-'+arr[1]+'-'+arr[0];
    return formattedDate;
  }

  onDeletePress(taskId){
      console.log(taskId);
      Alert.alert(
        'Confirmation',
        'Are sure want to delete task?',
        [
          {text: 'Yes', onPress: () => this.deleteConfirm(taskId)},
          {text: 'No', onPress: () => console.log('cancel press!!!!!!')},
        ]
    );
  }
  deleteConfirm = async(taskId) =>{
    if(global.userId){
       // console.log(taskId);
       try
       { 
       // this.setState({visible: true});
         let response = await fetch('http://api.yellotasker.com/api/v1/saveTask/delete', 
           { 
             method: 'POST',
                 headers: 
                   { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                     
                     },
                     body: JSON.stringify({
                       "taskId": taskId
                          ,"userId": global.userId
                     })
                }); 
                let res  = await response.text();
                this.setState({visible: false});
                if(response.status >=200){
                       this.getTaskList();
                  }else if(response.code==500){
                   
                  }else{ 
                   let error = res;
                   throw error;
                   } 
                 } catch (error) { 
         //this.setState({error: error});
        
         console.log(JSON.stringify(error));
        // console.log(JSON.parse(error));
        }
    }else{

    }
      
  }
  readMoreClick(id,status){
	if(status!='expired'){
		this.setState({makeOffer:true});
		console.log(status,id);
		this.fetchTaskDetail(id);
		this.getTaskOffer(id);
		this.fetchTaskComment(id);
	}else{
		console.log('task is expired');
	}

}
  fetchTaskDetail(taskId){
	
   // fetch('http://api.yellotasker.com/api/v1/getPostTask?taskId='+params.id)
	fetch('http://api.yellotasker.com/api/v1/getPostTask?taskId='+taskId)
.then((response) => response.json())
.then((responseJson) =>{
  console.log(responseJson.data);
   this.setState({
	taskData:responseJson.data,
})
//this.setState({visible: false}); 
})
.catch((error) =>{
   // alert('fetchTaskDetailerror'+error);
});
}
getTaskOffer(taskId){
	
			fetch('http://api.yellotasker.com/api/v1/taskOffer/'+taskId)
			.then((response) => response.json())
			.then((responseJson) =>{
				console.log('------------------------------------------');
				console.log(responseJson.data[0]);
						 var offerResult = responseJson.data[0].offer_details;
						 
						 console.log(offerResult);
						 //console.log('offer list');
					   // console.log('offer Length'+offerResult[0].interested_users.lenght);
						 if(offerResult.length>0){
							 console.log('if..............');
							// console.log(offerResult);
						this.setState({
							taskOffers:offerResult
						})
						
						//console.log(this.state.taskOfferEmpty);
						console.log(this.state.taskOffers);
					}else{
					   
						console.log('else+++++offerResult.length'+offerResult.length);
					}
					  // alert(JSON.stringify(this.state.taskOffer));
						
						 this.setState({visible: false}); 
			})
			.catch((error) =>{
			// alert('fetchTaskDetailerror'+error);
			});
  }
  fetchTaskComment(taskId){
	
   // this.setState({visible: true}); 
fetch('http://api.yellotasker.com/api/v1/comment/post?getCommentBy=task&taskId='+taskId)
.then((response) => response.json())
.then((responseJson) =>{
  // alert(JSON.stringify(responseJson));
	data = responseJson.data; // here we have all products data
	data = data.reverse();
  
// var stubComment = 
// let commentLoop = this.state.commentLoop;
let  commentLoop = this.prepareCommentLoop(data);
console.log(JSON.stringify(data));
console.log(data.length);
if(data.length>0){
this.setState({
	dataSources: this.state.dataSources.cloneWithRows(data)
})
}
this.setState({visible: false}); 
})
.catch((error) =>{
   // alert('fetchTaskCommenterror'+error);
});
}
prepareCommentLoop(resp){
	var commonIndexes = [];
	for(var i=0; i < resp.length;i++){
	  for(var j=0;j < resp.length;j++){
		if(resp[i].commentId == resp[j].id){
		  if(!resp[j].comments) resp[j].comments = [];
		  resp[j].comments.push(resp[i]);
		  commonIndexes.push(resp[i]);
		  //resp.splice(j,1);
		}else{
		  if(resp[j].comments){
			  this.prepareCommentLoop(resp[j].comments);
			}
		}
	  }
	}
	for(var i=0;i<commonIndexes.length;i++){
	for(var j=0 ; j<resp.length; j++){
	  if(resp[j].id == commonIndexes[i].id){
		resp.splice(j,1);
	  }
	}
  }
  }
  resPress = (text)=>{
	//alert(JSON.stringify(text));
	let idd = JSON.stringify(text);
this.setState({replyId: idd});

}
	render(){
        var self = this;
        console.log(self);
		//const { params } = self.props.navigation.state;
		 const { navigate } = self.props.result.navigation;
		return(
			<View>
				<Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <Modal
                    
                    transparent={false}
                    visible={this.state.makeOffer}>
                            <View style={{flexDirection:'row',paddingVertical:10,backgroundColor:'#000000'}}>
                <View style={{flex:0.2,alignItems:'flex-start',paddingVertical:12,paddingLeft:10}}>
				<TouchableHighlight onPress={()=>this.setState({makeOffer:false})} underlayColor={'transparent'}>
                <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.arrowLeft}</FontAwesome>
                </TouchableHighlight> 
                </View>
             <View  style={{flex:0.6,alignItems:'center'}}>
             <Text>
                <Image source={require('../img/app-logo.png')}/>
                </Text>
                </View>
                <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:12,paddingRight:10}}>
                
                </View>             
             </View>
			 <ScrollView style={styles.scrollViewStyle}>
                    <View style={styles.formViewStyle} >
                    <FlatList 
                      data={this.state.taskData}
                      keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                       <View>
                               <View style = {[styles.browseContent]}>    
                   <View style={{flexDirection:'row',flex:1,paddingVertical:7,paddingHorizontal:5}}>
                   
			<Text style = {styles.browseContentHeading} >{item.title}</Text> 
           
          
                    </View>
                    <View style={{flexDirection:'row',paddingVertical:10,}}>
                       
                       <View style={{ flex: 0.2 }}>
                           <Text style={{ backgroundColor: '#000000', color: '#ffffff', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Open</Text>
                       </View>
                       <View style={{ flex: 0.3, marginLeft: 4 }}>
                           <Text style={{ backgroundColor: '#eee', color: '#000', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Assigned</Text>

                       </View>
                       <View style={{ flex: 0.3, marginLeft: 4 }}>
                           <Text style={{ backgroundColor: '#eee', color: '#000', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Completed</Text>

                       </View>
                       <View style={{flex:0.2,flexDirection:'column',paddingHorizontal:8, }}>
                    
                    <Text style={styles.followBtn }><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.heartO}</FontAwesome> Follow
                    </Text>
                    </View>
                   </View>
			<View style={{flexDirection:'row',flex:1,paddingVertical:5,paddingHorizontal:5,alignItems:'flex-start', }}>
        <View style={{flexDirection:'row',flex:0.5}}>
			<View style={{paddingHorizontal:10,}}>
			{!item.user_detail.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
		   {item.user_detail.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
			</View>

			<View style={{flexDirection:'column',}}>
            <Text style={{paddingVertical:2,color:'#34495e',alignItems:'flex-start',flex:0.5}}>Posted By</Text>
			<Text style={{paddingVertical:2,color:'#337ab7',alignItems:'flex-start',flex:0.5}}>{item.user_detail.first_name} {item.user_detail.last_name}</Text>
		    <View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>

				</View>
		  
		  </View>
</View>
          <View style={{flexDirection:'column',flex:0.5,alignItems:'flex-end',paddingHorizontal:3}}>
                   <Text style={styles.makeAnOfferBtn} >Make An Offer</Text>
                   <Text style={{color:'#337ab7',paddingVertical:5, fontSize:13,textAlign: 'center'}} > 
                   <Text style={{ fontSize:14,textAlign: 'center'}}><FontAwesome>{Icons.plus}</FontAwesome></Text> Save Job</Text>
                   <Text style={{color:'#337ab7', fontSize:13,textAlign: 'center'}} > 
                  <FontAwesome>{Icons.flag}</FontAwesome> Report Task</Text>
         </View>
		  </View>
	  </View>
                       {/*<View style={styles.mainBrowse}>
                       

                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4}}>
                       <View style={{flexDirection:'row'}}>
                       <View style={{flex:0.7,flexDirection:'column'}}>
                     <Text style={{fontSize:16,fontWeight:'bold',color:'#000000',textAlign:'left',}}>{item.title}</Text>
                     <Text style={{color:'#337ab7',paddingVertical:5}}  onPress={() => this.props.navigation.navigate("TaskerProfile",{id:params.id})}>{item.user_detail.first_name} {item.user_detail.last_name}</Text>
                     <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                    </View>
                    <Text style={{paddingVertical:5}}>
                           <FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.locationArrow}</FontAwesome>
                              <Text> {item.address ==null||item.address =='' ? item.locationType : item.address} </Text>
                             
                               </Text>
                     </View>
                     <View style={{flex:0.3,flexDirection:'column',paddingHorizontal:5 }}>
                    
                         <Text style={styles.followBtn }><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.heartO}</FontAwesome> Follow
                         </Text>
                        <Text style={styles.makeAnOfferBtn} >Make An Offer</Text>
                        <Text style={{color:'#337ab7',paddingVertical:8, fontSize:13,textAlign: 'center'}} > 
                        <Text style={{ fontSize:14,}}>+</Text>Save Job</Text>
                        <Text style={{color:'#337ab7',paddingVertical:8, fontSize:13,textAlign: 'center'}} > 
                       <FontAwesome>{Icons.flag}</FontAwesome> Report Task</Text>
                     </View>
                     </View>
                     
                  
                     <View style={{flexDirection:'row',paddingVertical:10,}}>
                       
                       <View style={{ flex: 0.3 }}>
                           <Text style={{ backgroundColor: '#000000', color: '#ffffff', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Open</Text>
                       </View>
                       <View style={{ flex: 0.3, marginLeft: 4 }}>
                           <Text style={{ backgroundColor: '#eee', color: '#000', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Assigned</Text>

                       </View>
                       <View style={{ flex: 0.4, marginLeft: 4 }}>
                           <Text style={{ backgroundColor: '#eee', color: '#000', borderRadius: 8, paddingHorizontal: 10, paddingVertical: 3, fontSize: 12, textAlign: 'center',overflow:"hidden", }}>Completed</Text>

                       </View>
                   </View>
                  
                       </View>                      
                   </View>  */}
                        <View style={{ flex: 2, paddingVertical: 10, paddingHorizontal: 10,minHeight:100, borderColor: '#ccc',borderBottomWidth: 1, borderTopWidth: 1}}>
                            <Text style={{ paddingVertical: 5, fontSize: 16, fontWeight: 'bold',color:'#000000' }}>Description</Text>
                            <Text style={{color:'#333',paddingVertical:5,fontSize : 12,}}>{item.description}</Text>  
                            <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                   
                   <Text style={{flex:0.5,alignItems:'flex-start',}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',}}>Due Date</Text> 
                    </Text>
                    <Text style={{flex:0.5,color:'#666'}}>{this.indianDateFormat(item.dueDate)}</Text>
            
        </View>
                            <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>People Required</Text>
                            <Text style={{flex:0.5,}}>
                            5
                            </Text>
                            </View>
                            <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Budget Type</Text>
                            <Text style={{flex:0.5,}}>
                            {item.budgetType}
                            </Text>
                            </View>
                            <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Total Amount</Text>
                            <Text style={{flex:0.5,}}>
                            {item.totalAmount} MYR
                            </Text>
                            </View>
                        </View>
                      
                        </View>

}
/>
                        <View style={{ paddingVertical: 10, paddingHorizontal: 10,borderRadius: 4, }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>OFFER</Text>
                        </View>
						
  <FlatList 
                      data={this.state.taskOffers}
                      keyExtractor={(x,i)=>i}
                      renderItem={({item})=>
                     <OfferComponent result={item.interested_user}/>
                   }
/>
                        <View style={{ paddingVertical: 10, paddingHorizontal: 10,borderRadius: 4, }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>COMMENTS ABOUT THIS TASK</Text>
                            <Text style={{ paddingVertical: 10, fontSize: 12, fontWeight: 'normal', color: '#333', }}>Comment below for more details and remember that for your safety, not to share personal information e.g. email and phone numbers.</Text>
                        
                        </View>
                        <View style={styles.mainBrowses}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Ask a question"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}
         
      value={this.state.commentDescription}
       ></TextInput>
     
     <Text style={{textAlign:'right',color:'#000000'}} >Send</Text>
                       </View>
                       </View>
  
					   <View style={{  marginBottom: 80,}}>
<ListView
		dataSource={this.state.dataSources}
		renderRow={(comment)=>
                       <View>
                        <View style={styles.mainsBrowse}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style = {{flex:0.8,flexDirection:'column',paddingHorizontal:10,}}>
                       {comment.user_detail && 
                       <Text style={{color:'#337ab7'}}>
                       {comment.user_detail.first_name} {comment.user_detail.last_name}</Text>
                       }
                       <Text style = {[{fontSize : 16,},styles.browseContentHeading]} >{comment.commentDescription}</Text> 
                       
                       <View style={{flexDirection:'row',}}>
                          <View>
                            <Text style={{fontWeight:'bold',fontSize:14,color:'#000000'}}><TimeAgo time={comment.created_at}/></Text>
                       </View>
                       <View>

                            <Text style={{fontWeight:'normal',fontSize:14,paddingLeft:25}} onPress={this.resPress.bind(this,comment.id)}> 
                            <FontAwesome style={{fontSize: 14,color:'#337ab7'}}>{Icons.reply}</FontAwesome>  Reply</Text>
                       </View>
                       </View>
                 </View>
                       </View>
                       {comment.id==this.state.replyId &&
                        <View style={styles.mainsBrowse}>
                        <View style={styles.browseMen}>
                            <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                        </View>
                        <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                        <TextInput
        placeholder=""  
        underlineColorAndroid = "transparent" returnKeyType="next" 
              keyboardType="email-address"
       autoCapitalize="none"
       autoCorrect={false} style={styles.TextArea}
       multiline={true}
       numberOfLines={3}    
      
        ></TextInput>
     
	  <Text style={{textAlign:'right',color:'#000000'}} 
	>Send</Text>
                        </View>
                        </View>

                       }
                       
                     <View>
                         <Text>
                                 
                         </Text>
                     </View>
                  
                           </View>

}
/>
                    </View>
                    </View>
                </ScrollView>
                    
                    
                </Modal>
   <View style={{paddingHorizontal:20,paddingVertical:10,flexDirection:'row',borderBottomColor:'gray',borderBottomWidth:1}}>
		  <FontAwesome style={{fontSize: 18,color:'#34495e',flexDirection:'row',alignItems:'center'}}>{Icons.bitbucket}</FontAwesome> 
                        
                        <Text style={{paddingLeft:5}}>Total Saved Task - {this.state.saveTaskTotal}</Text></View>
					<View style={{marginBottom:200,paddingHorizontal:5}}>
					<ListView
					  dataSource={this.state.dataSource}
					  renderRow={(item)=> 
                    <View style = {[styles.mainBrowse,item.status=='open' && styles.open,item.status=='assigned' && styles.open,item.status=='expired' && styles.expire,]}>
		 <View style={{flexDirection:'row'}}>
    	<View style = {styles.browseContent}>    
			<Text style = {styles.browseContentHeading} >{this.readMoreTitle(item.title)}</Text> 
			{/*
			<Text style = {styles.browseSubContent}>{this.readMore(item.description)}
			{
        this.readMoreCount(item.description)>24&& <Text style={{color:'#337ab7',fontWeight:'bold'}} onPress={()=>navigate('SingleTask',{id:item.taskId,back:'MyTask'})} > Read More</Text>
        }
      </Text> */}
      <Text style = {styles.browseSubContent}>{this.readMore(item.description)}
			{this.readMoreCount(item.description)>24&& 
      <Text> 
			{item.status=='expired' && <Text style={{color:'#337ab7',fontWeight:'bold'}} >Read More</Text>}
				{item.status=='open'  && <Text style={{color:'#337ab7',fontWeight:'bold'}} onPress={()=>navigate('SingleTask',{id:item.taskId,back:'MyTask'})} >Read More</Text>}
				
				{item.status=='assigned' && <Text style={{color:'#337ab7',fontWeight:'bold'}} >Read More</Text>}
				</Text> 
      }
			</Text> 
			<View style={{flexDirection:'row',flex:1,paddingVertical:5}}>
        
            <View style={{paddingHorizontal:10,}}>
		   {!item.task_posted_user.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
		   {item.task_posted_user.profile_image && 
		    <Image source={{uri : item.task_posted_user.profile_image}} style={styles.userImage}/>
		  }
				
                </View>
            <View style={{flexDirection:'column'}}>
			<Text style={{paddingVertical:3,color:'#337ab7',alignItems:'flex-start',flex:0.5}}>{item.task_posted_user.first_name} {item.task_posted_user.last_name}</Text>
		  <View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
		  </View>
		  </View>
          
          </View>
		 
    </View>

		   <View style = {styles.browseOffer}>
          <Text onPress={this.onDeletePress.bind(this,item.taskId)} style={{marginBottom:5}}> <FontAwesome style={{fontSize: 18,color:'#337ab7',flexDirection:'row',alignItems:'center'}} >{Icons.trash}</FontAwesome></Text>
				<Text style = {styles.browseOfferText}>{item.totalAmount} MYR</Text>
			{/*	<Text style = {styles.browseOfferView} onPress={()=>navigate('SingleTask',{id:item.taskId,back:'MyTask'})} >View</Text>*/}

        {item.status=='expired' && <Text style = {styles.browseOfferView} >View</Text>}
				{item.status=='open'  && <Text style = {styles.browseOfferView} onPress={()=>navigate('SingleTask',{id:item.taskId,back:'MyTask'})} >View</Text>}
        {item.status=='assigned' && <Text style = {styles.browseOfferView} >View</Text>}
        

			</View>
			</View>
      <View style={{paddingHorizontal:10}}>
      {/*<Text style={{color:'#000',paddingVertical:5}}>
		  <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome> <Text>{item.address ==null ||item.address =='' ? item.locationType : item.address}</Text>
		  </Text> */}
		 
      <Text style={{color:'#000000',fontWeight:'bold',flexDirection:'row',}}>
      
    <FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> Due Date <Text style={{color:'#666'}}>{this.DateChangeFormate(item.dueDate)}</Text>
    
    
    </Text>
		 
    <View style={{flexDirection:'row',flex:1,paddingVertical:4}}>
    <Text style={{flex:0.5}}>
    <Text style={[item.status=='open' && styles.openText,item.status=='assigned' && styles.assignText,item.status=='expired' && styles.expireText,]}> {this.assignTextFun(item.status)}</Text>
   </Text>
   
   <Text style={{flex:0.5,fontWeight:'bold',flexDirection:'row',textAlign:'right',color:'#484848'}}><Text>{item.offer_count} Offer</Text> <Text>{item.comment_count} Comments</Text></Text>
    </View>
      </View>
   </View>
  
					    
                      
                   }
/>
</View>
</View>
       
        )
	}
	
		
	
}



//AppRegistry.registerComponent('list', () => list) //Entry Point    and Root Component of The App

