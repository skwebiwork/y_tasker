import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    mainviewStyle: {
        flex: 1,
        flexDirection: 'column',
    },
    footer: {
        position: 'absolute',
        flex: 0.1,
        left: 0,
        right: 0,
        bottom: -10,
        backgroundColor: '#000000',
        flexDirection: 'row',
        height: 70,
        alignItems: 'center',
    },
    postButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        borderColor: '#efeb10',
        borderRightWidth: 1,

    },
    browseButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        borderColor: '#efeb10',
        borderRightWidth: 1,

    },
    myTaskButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        borderColor: '#efeb10',
        borderRightWidth: 1,

    },
    messageButtons: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',

    },
    footerText: {
        color: 'white',
        alignItems: 'center',
        fontSize: 14,


    },
    textStyle: {
        alignSelf: 'center',
        color: 'orange'
    },
    scrollViewStyle: {
        borderTopWidth: 4,
        borderColor: '#efeb10',
    },
    secondHeader: {
        backgroundColor: '#efeb10',
        flexDirection: 'row',
        borderColor: '#ccc',
        borderBottomWidth: 1,
        paddingVertical: 12,

    },
    headerText: {
        color: '#000000',
        paddingLeft: 10,
        flex: 0.8,
        fontWeight: 'bold',
    },
    settingDiv: {
        flex: 0.1,
        backgroundColor: '#efeb10',
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderColor: '#d8d406',
        alignItems: 'center',
    },
    searchDiv: {
        flex: 0.1,
        backgroundColor: '#efeb10',
        alignItems: 'center',
    },
    mainBrowse: {
        flexDirection: 'row',
        borderColor: '#ccc',
        paddingHorizontal:'1%', 
        paddingVertical: '3%',
        borderBottomWidth: 1,
    },
    mainBrowses: {
        flexDirection: 'row',
        borderColor: '#ccc',
        borderBottomWidth: 1,
        paddingBottom: '4%',
        alignItems: 'center',
    },
    postedBy :{
        flex:1,
        flexDirection: 'row',
        paddingBottom: '4%',
        alignItems: 'center',
    },
    mainsBrowse: {
        flexDirection: 'row',
        borderColor: '#ccc',
        borderBottomWidth: 1,
        paddingVertical: '3%',
        marginBottom: 80,
    },
    browseMen: {
        flex: 0.2,
        marginLeft: '2%',
        width: '15%',
    },
    menImage: {
        height: 72,
        width: 72,
    },
    browseContent: {
        flex: 0.6,
        marginLeft: '3%',
        flexDirection: 'column',
    },
    browseContentHeading: {
        fontSize: 16,
        fontWeight: 'bold',
        color : '#000000'
    },
    browseSubContent: {
        fontSize: 12,
        fontWeight: 'bold',
        color:'#333',

    },
    browseOffer: {
        flex: 0.2,
        flexDirection:'row',
       
    },
    browseOfferText: {
        fontSize: 15,
        fontWeight: 'bold',
    },
    browseOfferView: {
        borderRadius: 6,
        backgroundColor: '#efeb10',
        alignItems: 'center',
        paddingVertical: 6,
        paddingHorizontal: 12,

    },
    followBtn:{
        borderRadius: 6,
        backgroundColor: '#efeb10',
        
        alignItems: 'center',
        paddingVertical: 6,
        paddingHorizontal: 12,
        color:'#000000',
        fontSize:10
    }

});

export default styles;
