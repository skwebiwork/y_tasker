import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Alert,Image,AppRegistry,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView,BackHandler,ActivityIndicator} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem,Drawer,Icon ,Tab, Tabs, ScrollableTab} from 'native-base';
import SideBar from '../sideBar';
import EStyleSheet from 'react-native-extended-stylesheet';
//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./myTaskStyle";
var dateFormat = require('dateformat');
var now = new Date();
var {width, height} = Dimensions.get('window');
export default class myTask extends Component {
			
	static navigationOptions = {
		header: null,
	};
	
	constructor(){
		super();
		global.count = 0;
		this.state={
		
			taskPeopleRequired:"",
			taskBudget:"",
			 LodingStatus:false,
			 error : "",
			 visible : false,
			 data : [],
			 dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			 dataPosted: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			 dataAsigned: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			 dataCompleted: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			emptyRow : false,
			emptyArray : [],
	
		}
			
		
	}
	componentDidMount(){
		this.getMyTask();
		//this.DateChangeFormate();
		BackHandler.addEventListener('hardwareBackPress', this.handleBack);
		
	}
	
	componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
		BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
	  }
	handleBack() {
		global.count = global.count+1;
		//alert('count'+global.count);
		if(global.count>1){
		  Alert.alert(
			'Confirmation',
			'Are sure want to quit this App',
			[
			  {text: 'OK', onPress: () => BackHandler.exitApp()},
			]
		);
		}else{
		  //this.setState({touchMsg : true});
		  
		  global.setTimeout(() => {
			global.count = 0;
		  }, 1000);
		}
		  
		return true;
		
		  }
	closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
	  DateChangeFormate(duedate){
		var date = duedate;
		if(date){
//		var date = "2017-12-04";
		  //var arr1 = date.split(' ');
		  var arr2 = date.split('-');
		  var month;
		  if(arr2[1]=='01'){
			  month = 'Jan';
		  }else if(arr2[1]=='02'){
			month = 'Feb';
		}else if(arr2[1]=='03'){
			month = 'March';
		}else if(arr2[1]=='04'){
			month = 'April';
		}else if(arr2[1]=='05'){
			month = 'May';
		}else if(arr2[1]=='06'){
			month = 'June';
		}else if(arr2[1]=='07'){
			month = 'July';
		}else if(arr2[1]=='08'){
			month = 'Aug';
		}else if(arr2[1]=='09'){
			month = 'Sep';
		}else if(arr2[1]=='10'){
			month = 'Oct';
		}else if(arr2[1]=='11'){
			month = 'Nov';
		}else{
			month = 'Dec';
		} 
		  var dt = month +' '+arr2[2]+','+arr2[0];
		  return dt;
		 //alert(dt);
		}
		  
	  }
	  onDashboardRender(){
		const { navigate } = this.props.navigation;
		 navigate('Dashboard');
 }
 getMyTask(){
	const { navigate } = this.props.navigation;
	this.setState({visible: true}); 
   fetch('http://api.yellotasker.com/api/v1/getUserTask/'+global.userId)
   .then((response) => response.json())
   .then((responseJson) =>{
	this.setState({visible: false}); 
	   if(responseJson.code==404){
	   //	alert(responseJson.code);
		   this.setState({ emptyRow : true});
	   }
	   data = responseJson.data; // here we have all products data
	 if(data[0].save_task.length>0){
		this.setState({
			dataSource: this.state.dataSource.cloneWithRows(data[0].save_task)  })
	   
	 }else{
		//this.setState({dataSource: this.state.dataSource.cloneWithRows(this.state.emptyArray)})
	   
		
	 }
	 if(data[0].posted_task.length>0){
	   	 this.setState({ dataPosted : this.state.dataPosted.cloneWithRows(data[0].posted_task)  })
	 }
	 if(data[0].offers_pending.length>0){
	 this.setState({ dataAsigned : this.state.dataAsigned.cloneWithRows(data[0].offers_pending)  })
	 }
	 if(data[0].offers_accepting.length>0){		 
	 this.setState({ dataCompleted: this.state.dataCompleted.cloneWithRows(data[0].offers_accepting)  })
	 }		
	 // this.setState({ dataOffer: this.state.dataOffer.cloneWithRows(data[0].offer_task) })
	   

   })
   .catch((error) =>{
	   console.error(error);
   });
 }
onRegisterValidate(){
	if(this.state.taskBudget==""){
			this.setState({error: 'Please enter required fields'});
	}else if(this.state.taskPeopleRequired==""){
		   this.setState({error: 'Please enter required fields'});
	}else{
			this.onRegisterPressed();
	}

   
 }
  onBrowsePressed(){
	const { navigate } = this.props.navigation;
	navigate('Browse'); 
 }
  onTaskPressed(){
   let userId = global.userId;
  // alert('userId'+userId);
  const { navigate } = this.props.navigation;
  global.category_id = null;
  navigate('Mainview');
 }
   onLoginPressed(){
   const { navigate } = this.props.navigation;
navigate('Login');
 }
 onNextStep(){
   alert('chgghdhg');
 }
 onMyTaskPressed(){
	let userId = global.userId;
   // alert('userId'+userId);
	const { navigate } = this.props.navigation;
	if(userId){
	
   navigate('MyTask'); 
   }else{
	Alert.alert(
	   'Message',
	   'Please Login to go to My task',
	   [
		 {text : 'OK', onPress:()=>{this.onLoginPressed()}}
	   ]
	  );
   }
  }

		
	render(){
		 const { params } = this.props.navigation.state;
		 const { navigate } = this.props.navigation;
		return(
			<Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >	
		<View style={styles.mainviewStyle}>
        <Modal
    animationType="slide"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
   <View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
   <Text>
 
  
   </Text>
   
   </View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
   <Image source={require('../img/app-logo.png')} />
   </Text>
   </View>
   <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
   <TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
  
   <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
  
   </TouchableHighlight>
   </View>

</View>
        <ScrollView contentContainerStyle={styles.scrollViewStyle}>
               <View style = {styles.formViewStyle} >
                      
		                   <View>
											 <Tabs tabBarUnderlineStyle= {{ backgroundColor: '#efeb10' }}renderTabBar={()=> <ScrollableTab />} tabContainerStyle={{height:30}}>
       <Tab heading="My Saved Task" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', height:10,}} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>
	   <View style={{marginBottom:70}}>
	   <ListView
dataSource={this.state.dataSource}
renderRow={(rowData)=>


<View style = {styles.mainBrowse}>
<View style = {styles.browseMen}>
   <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
</View>
<View style = {styles.browseContent}>
	 <Text style = {styles.browseContentHeading} >{rowData.title}</Text> 
	 <Text style = {styles.browseSubContent} >{rowData.description}</Text> 
	 <View style={{flexDirection:'row',flex:1,}}>
	 <Text style={{paddingVertical:3,color:'#337ab7',alignItems:'flex-start',flex:0.5}}>{rowData.task_posted_user.first_name} {rowData.task_posted_user.last_name}</Text>
   <View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
   </View>
   </View> 
   <Text style={{color:'#000'}}>
   <FontAwesome style={{fontSize: 10,color:'#000000',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome><Text> {rowData.address ==null ? rowData.locationType : rowData.address}</Text></Text>
   <Text style={{color:'#000'}}><Text><FontAwesome style={{fontSize: 10,color:'#000000',alignItems:'center'}}>{Icons.calendar}</FontAwesome> Due date </Text><Text>{rowData.dueDate}</Text> </Text>
	  
</View>
<View style = {styles.browseOffer}>
   <Text style = {styles.browseOfferText}>{rowData.totalAmount} MYR</Text>
   <Text style = {styles.browseOfferView}> View</Text>
   {rowData.status=='open' && 
   <Text style = {styles.offerOpen}>Open</Text>
   }
   {rowData.status=='assigned' && 
   <Text style = {styles.offerAssign}>Assigned</Text>
   }
   {rowData.status=='expired' && 
   <Text style = {styles.offerExpired}>Expired</Text>
   }
</View>
</View>

}
/>
{this.state.emptyRow && 
   <View style = {styles.mainBrowse}>
	  <Text style={{paddingHorizontal:15, paddingVertical:8}}>
		 Looks like you haven't posted or completed a task just yet. Post a task or browse tasks to get started
	  </Text>
   </View>
}
		</View>
       </Tab>
       <Tab heading="Posted Task" tabStyle={styles.tabcontain} textStyle={{color:'#000'}} activeTabStyle={{backgroundColor : '#000000',height:10,}} activeTextStyle={{color:'#fff'}}>
	   <View style={{marginBottom:70}}>
	   <ListView
dataSource={this.state.dataPosted}
renderRow={(rowData)=>


<View style = {styles.mainBrowse}>
<View style = {styles.browseMen}>
   <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
</View>
<View style = {styles.browseContent}>
	 <Text style = {styles.browseContentHeading} >{rowData.title}</Text> 
	 <Text style = {styles.browseSubContent} >{rowData.description}</Text>
	 <View style={{flexDirection:'row',flex:1,}}>
				  <Text style={{paddingVertical:3,color:'#337ab7',alignItems:'flex-start',flex:0.5}}>{rowData.task_posted_user.first_name} {rowData.task_posted_user.last_name}</Text>
				<View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
				</View>
				</View> 
	 
   <Text style={{color:'#000'}}>
   <FontAwesome style={{fontSize: 10,color:'#000000',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome><Text> {rowData.address ==null ? rowData.locationType : rowData.address}</Text></Text>
   <Text style={{color:'#000'}}><Text><FontAwesome style={{fontSize: 10,color:'#000000',alignItems:'center'}}>{Icons.calendar}</FontAwesome> Due date </Text><Text>{rowData.dueDate}</Text> </Text>
	  
</View>
<View style = {styles.browseOffer}>
   <Text style = {styles.browseOfferText}>{rowData.totalAmount} MYR</Text>
   <Text style = {styles.browseOfferView}> View</Text>
   {rowData.status=='open' && 
   <Text style = {styles.offerOpen}>Open</Text>
   }
   {rowData.status=='assigned' && 
   <Text style = {styles.offerAssign}>Assigned</Text>
   }
   {rowData.status=='expired' && 
   <Text style = {styles.offerExpired}>Expired</Text>
   }
</View>
</View>
}
/>
{this.state.emptyRow && 
   <View style = {styles.mainBrowse}>
	  <Text style={{paddingHorizontal:15, paddingVertical:8}}>
		 Looks like you haven't posted or completed a task just yet. Post a task or browse tasks to get started
	  </Text>
   </View>
}
		</View>
       </Tab>
       <Tab heading="Offer Pending" tabStyle={styles.tabcontain} textStyle={{color:'#000'}} activeTabStyle={{backgroundColor : '#000000',height:10,}} activeTextStyle={{color:'#fff'}}>
	   <View style={{marginBottom:70}}>
	   <ListView
dataSource={this.state.dataAsigned}
renderRow={(rowData)=>


<View style = {styles.mainBrowse}>
<View style = {styles.browseMen}>
   <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
</View>
<View style = {styles.browseContent}>
	 <Text style = {styles.browseContentHeading} >{rowData.title}</Text> 
	 <Text style = {styles.browseSubContent} >{rowData.description}</Text> 
	 <View style={{flexDirection:'row',flex:1,}}>
				  <Text style={{paddingVertical:3,color:'#337ab7',alignItems:'center',flex:0.5}}>{rowData.task_posted_user.first_name} {rowData.task_posted_user.last_name}</Text>
				<View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
				</View>
				</View> 
   <Text style={{color:'#000'}}>
   <FontAwesome style={{fontSize: 10,color:'#000000',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome><Text> {rowData.address ==null ? rowData.locationType : rowData.address}</Text></Text>
   <Text style={{color:'#000'}}><Text><FontAwesome style={{fontSize: 10,color:'#000000',alignItems:'center'}}>{Icons.calendar}</FontAwesome> Due date </Text><Text>{rowData.dueDate}</Text> </Text>
	  
</View>
<View style = {styles.browseOffer}>
   <Text style = {styles.browseOfferText}>{rowData.totalAmount} MYR</Text>
   <Text style = {styles.browseOfferView}> View</Text>
   {rowData.status=='open' && 
   <Text style = {styles.offerOpen}>Open</Text>
   }
   {rowData.status=='assigned' && 
   <Text style = {styles.offerAssign}>Assigned</Text>
   }
   {rowData.status=='expired' && 
   <Text style = {styles.offerExpired}>Expired</Text>
   }
</View>
</View>
}
/>
{this.state.emptyRow && 
   <View style = {styles.mainBrowse}>
	  <Text style={{paddingHorizontal:15, paddingVertical:8}}>
		 Looks like you haven't posted or completed a task just yet. Post a task or browse tasks to get started
	  </Text>
   </View>
}
		</View>
       </Tab>
       
       <Tab heading="Offer assigned" tabStyle={styles.tabcontain} textStyle={{color:'#000'}} activeTabStyle={{backgroundColor : '#000000',height:10,}} activeTextStyle={{color:'#fff'}}>
	   <View style={{marginBottom:70}}>
	   <ListView
dataSource={this.state.dataAsigned}
renderRow={(rowData)=>


<View style = {styles.mainBrowse}>
<View style = {styles.browseMen}>
   <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
</View>
<View style = {styles.browseContent}>
	 <Text style = {styles.browseContentHeading} >{rowData.title}</Text> 
	 <Text style = {styles.browseSubContent} >{rowData.description}</Text> 
	 <View style={{flexDirection:'row',flex:1,}}>
	 <Text style={{paddingVertical:3,color:'#337ab7',alignItems:'flex-start',flex:0.5}}>{rowData.task_posted_user.first_name} {rowData.task_posted_user.last_name}</Text>
   <View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5,}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
   </View>
   </View> 
   <Text style={{color:'#000'}}>
   <FontAwesome style={{fontSize: 10,color:'#000000',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome><Text> {rowData.address ==null ? rowData.locationType : rowData.address}</Text></Text>
   <Text style={{color:'#000'}}><Text><FontAwesome style={{fontSize: 10,color:'#000000',alignItems:'center'}}>{Icons.calendar}</FontAwesome> Due date </Text><Text>{rowData.dueDate}</Text> </Text>
	  
</View>
<View style = {styles.browseOffer}>
   <Text style = {styles.browseOfferText}>{rowData.totalAmount} MYR</Text>
   <Text style = {styles.browseOfferView} > View</Text>
   {rowData.status=='open' && 
   <Text style = {styles.offerOpen}>Open</Text>
   }
   {rowData.status=='assigned' && 
   <Text style = {styles.offerAssign}>Assigned</Text>
   }
   {rowData.status=='expired' && 
   <Text style = {styles.offerExpired}>Expired</Text>
   }
</View>
</View>
}
/>
{this.state.emptyRow && 
   <View style = {styles.mainBrowse}>
	  <Text style={{paddingHorizontal:15, paddingVertical:8}}>
		 Looks like you haven't posted or completed a task just yet. Post a task or browse tasks to get started
	  </Text>
   </View>
}
		</View>
       </Tab>
     </Tabs>
											 </View>
			              
               
                        
                       
                </View>
        </ScrollView>

		<View style={styles.footer}>
		<View style={styles.browseButtons}>
		<TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
		  </TouchableHighlight>
			<Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
		</View>
		<View style={styles.postButtons}>
<TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
			 
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
			
			 </TouchableHighlight>
			<Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
		</View>
		<View style={styles.myTaskButtons}>
		<TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
		  <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.clipboard}</FontAwesome>
			</TouchableHighlight>
				 <Text style={[styles.footerText,{color:'#efeb10'}]} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
			 </View>
		 <View style={styles.messageButtons}>
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>
			 
			<Text style={styles.footerText}>Message</Text>
		</View>
		</View>
      </View>
		</Drawer>
			
		);
	}
	
		
	
}


//AppRegistry.registerComponent('myTask', () => myTask) //Entry Point    and Root Component of The App

