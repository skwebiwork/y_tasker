import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
	mainviewStyle: {
	flex: 1,
	flexDirection: 'column',
	backgroundColor:'#ffffff',
  },
  footer: {
	position: 'absolute',
	flex:0.1,
	left: 0,
	right: 0,
	bottom: -10,
	backgroundColor:'#000000',
	flexDirection:'row',
	height:70,
	alignItems:'center',
  },
  postButtons: {
	alignItems:'center',
	justifyContent: 'center',
	flex:1,
	 flexDirection:'column',
	 borderColor: '#efeb10',
	 borderRightWidth: 1,
  
  },
  browseButtons: {
	alignItems:'center',
	justifyContent: 'center',
	flex:1,
	 flexDirection:'column',
	 borderColor: '#efeb10',
	 borderRightWidth: 1,
  
  },
  myTaskButtons: {
	alignItems:'center',
	justifyContent: 'center',
	flex:1,
	 flexDirection:'column',
	 borderColor: '#efeb10',
	 borderRightWidth: 1,
	 
  },
  messageButtons: {
	alignItems:'center',
	justifyContent: 'center',
	flex:1,
	 flexDirection:'column',
	 
  },
  footerText: {
	color:'white',
	alignItems:'center',
	fontSize:14,
   
  
  },
  textStyle: {
	alignSelf: 'center',
	color: 'orange'
  },
  scrollViewStyle: {
	borderTopWidth : 4,
		borderColor: '#efeb10',
  },
  secondHeader : {
  backgroundColor: '#efeb10',
  flexDirection : 'row',
  borderColor : '#ccc',
  borderBottomWidth : 1,
  paddingVertical : 12,
  color:'#000000',
  },
  headerText : {
   color : '#000000',
   paddingLeft : 10,
   flex : 0.8,
   fontWeight :'bold',
  },
  settingDiv : {
	flex:0.1,
	backgroundColor: '#efeb10',
	borderRightWidth : 1,
	borderLeftWidth : 1,
	borderColor : '#d8d406',
	alignItems : 'center',
  },
  searchDiv : {
	 flex:0.1,
	backgroundColor: '#efeb10',
	alignItems : 'center',
  },
  mainBrowse : {
		flexDirection : 'column',
		borderColor : '#ccc',
		borderWidth : 1,
		paddingVertical : '3%',
		borderRadius:5,
		 flex : 1,
		 marginTop:3,
		
   
	},

  mainsBrowse :{
	flexDirection : 'row',
   borderColor : '#ccc',
   borderBottomWidth : 1,
   paddingVertical : '3%',
   marginBottom : 80,
  },
  postedBy :{
	flex:1,
	flexDirection: 'row',
	paddingBottom: '4%',
	alignItems: 'center',
},
  browseMen : {
	flex : 0.2,
	marginLeft : '2%',
   width : '15%',
  },
  menImage : {
   height: 48,
	  width : 48,
	  borderRadius:24
  },
  browseContent : {
	flex : 0.8,
	marginLeft : '3%',
	flexDirection : 'column',
  },
  browseContentHeading : {
	fontSize : 16,
	fontWeight : 'bold',
	color : '#000000',
	paddingVertical:5,
  },
  browseSubContent : {
	fontSize : 12,
	fontWeight : 'bold',
	color:'#333',
	paddingVertical:5,
  },
  browseOffer : {
	flex : 0.2,
   alignItems:'center',
	flexDirection : 'column',
  },
  browseOfferText : {
	fontSize : 15,
	fontWeight : 'bold',
	color:'#000000'
  },
  browseOfferView :{
   borderRadius : 4,
   backgroundColor: '#efeb10',
	alignItems : 'center',
	paddingVertical : 6,
	paddingHorizontal : 12,
	color :'#000000',
	marginTop:10,
	overflow:"hidden",
  
	 },
	 offerExpired :{
		borderRadius : 4,
		backgroundColor: 'gray',
	 alignItems : 'center',
	 paddingVertical : 6,
	 paddingHorizontal : 12,
	 color :'#fff',
	 marginTop:10,
	 overflow:"hidden",
	 fontSize:10,
	 
		},
		expire : {
			borderLeftWidth:5,
			borderLeftColor:'gray',
		},
		open : {
			borderLeftWidth:5,
			borderLeftColor:'#27ae60',
		},
		assign:{
			borderLeftWidth:5,
			borderLeftColor:'#298992',
		},
		expireText:{
color:'gray',
paddingVertical:6,
fontWeight:'bold'
		},

		openText:{
color:'#27ae60',
paddingVertical:6,
fontWeight:'bold'
		},
		assignText:{
color:'#298992',
paddingVertical:6,
fontWeight:'bold'
		},
		userImage : {
			height: 48,
			width : 48,
			borderRadius:23,
	
		},
		offerOpen :{
			borderRadius : 4,
			backgroundColor: '#27ae60',
		 alignItems : 'center',
		 paddingVertical : 6,
		 paddingHorizontal : 12,
		 color :'#fff',
		 marginTop:10,
		 overflow:"hidden",
		 fontSize:10,
			},
			offerAssign : {
				borderRadius : 4,
			backgroundColor: '#298992',
		 alignItems : 'center',
		 paddingVertical : 6,
		 paddingHorizontal : 10,
		 color :'#fff',
		 marginTop:10,
		 overflow:"hidden",
		 fontSize:11,
			},
   container:{
	display:'flex'
},
pageName:{
	margin:10,fontWeight:'bold',
	color:'#000', textAlign:'center'
},
LoginDiv : {
	backgroundColor : '#efeb10',
	paddingVertical : 11, 
	  borderRadius:5,
	  borderWidth: 1,
	  borderColor: '#efeb10',
  
  },
  extraText : {
	color : '#000000',
	textAlign: 'center',
		fontSize: 18,
	},
	tabcontain : {
		backgroundColor : '#efeb10',
		height:10,
	},
	mainBrowses: {
		flexDirection: 'row',
		borderColor: '#ccc',
		borderTopWidth: 1,
	paddingHorizontal:'1%',
	paddingVertical:'3%',

		
},
mainBrowsess: {
	flexDirection: 'row',
	
paddingHorizontal:'1%',
paddingVertical:'3%',

	
},
streck : {
color:'red',
},
InputTexts : {
	paddingVertical : 12,
	borderRadius:5,
	borderColor: 'gray',
	 borderWidth: 1,
	 paddingHorizontal: 5,
	 marginBottom : 10,
	 fontSize:14
	 
  },
  inputDiv : {
	  paddingVertical:10,
	  paddingHorizontal:15,
	  borderRadius:4,
	  backgroundColor:'#fff',
	  flexDirection:'column',
	  borderColor:'gray',
	  borderWidth:1,
	  marginBottom:10
	},
	taskLabel : {color:'#555',paddingBottom:3},
browseMenTwo:  {flex: 0.2,
	marginLeft: '2%',
	width: '15%',paddingTop:10},
replymenImage: {
	height: 52,
	width: 52,
	borderRadius:26,
},  replymensImage : {
	height: 48,
	width: 48,
	borderRadius:48/2
},
mainsBrowse: {
	flexDirection: 'row',
 
	paddingVertical: '3%',
	paddingHorizontal:'1%',

},
replymainsBrowse: {
	flexDirection: 'row',
 paddingLeft:'10%',
	paddingVertical: '3%',
	paddingHorizontal:'1%',

},
subreplymainsBrowse: {
	flexDirection: 'row',
 paddingLeft:'18%',
	paddingVertical: '3%',
	paddingHorizontal:'1%',

},
TextArea : {
	paddingVertical : 12,
	borderRadius:5,
	borderColor: 'gray',
	 borderWidth: 1,
	 paddingHorizontal: 5,
	 marginBottom : 10,
	textAlignVertical: 'top',
	height:100,
},
followBtn:{
	borderRadius: 6,
	backgroundColor: '#efeb10',
	textAlign: 'center',
	paddingVertical: 6,
	paddingHorizontal: 12,
	color:'#000000',
	fontSize:10,
	overflow:"hidden",
},
openBtn:{
	borderRadius: 6,
	backgroundColor: '#34495e',
	textAlign: 'center',
	paddingVertical: 6,
	paddingHorizontal: 12,
	color:'#fff',
	fontSize:12,
	overflow:"hidden",
	fontWeight:'bold',
},
makeAnOfferBtn:{
	borderRadius: 6,
	backgroundColor: '#efeb10',
	textAlign: 'center',
	paddingVertical: 6,
	paddingHorizontal: 12,
	color:'#000000',
	fontSize:10,
	overflow:"hidden",
	marginTop : 15,
},
PercentBtn:{
	borderRadius: 6,
	backgroundColor: '#efeb10',
	textAlign: 'center',
	paddingVertical: 6,
	paddingHorizontal: 6,
	color:'#000000',
	fontSize:10,
	overflow:"hidden",
},
itemTextStyle : {
	paddingHorizontal:5,
   },
   containerStyle :{
	borderColor:'gray',
	borderWidth:1,
	borderRadius: 5,
   
  },
  });

export default styles;
  
