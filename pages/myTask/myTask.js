import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Alert,Image,AppRegistry,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView,BackHandler,ActivityIndicator} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem,Drawer,Icon ,Tab, Tabs, ScrollableTab,Fab} from 'native-base';
import SideBar from '../sideBar';
import EStyleSheet from 'react-native-extended-stylesheet';
//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./myTaskStyle";
import SaveTask from "./saveTask";
import PostTask from "./postTast";
import OfferPending from "./offerPending";
import OffersAccepting from "./offerAccepting";
var dateFormat = require('dateformat');
var now = new Date();
var {width, height} = Dimensions.get('window');
export default class myTask extends Component {
	static navigationOptions = {
		header: null,
	};
	
	constructor(){
		super();
		global.count = 0;
		this.state={
					taskPeopleRequired:"",
					taskBudget:"",
					LodingStatus:false,
					error : "",
					visible : false,
					data : [],
					dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
					dataPosted: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
					dataAsigned: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
					dataCompleted: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
					emptyRow : false,
					emptyArray : [],
					active: 'true',
		}
			
		
	}
	componentDidMount(){
		BackHandler.addEventListener('hardwareBackPress', this.handleBack);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
	  }
	handleBack() {
		global.count = global.count+1;
		if(global.count>1){
		  Alert.alert(
			'Confirmation',
			'Are sure want to quit this App',
			[
			  {text: 'OK', onPress: () => BackHandler.exitApp()},
			]
		);
		}else{
		  global.setTimeout(() => {
			global.count = 0;
		  }, 1000);
		}
		return true;
		  }
	  closeDrawer = () => {
	 			this.drawer._root.close()
	  };
	  openDrawer = () => {
				this.drawer._root.open()
	  };
	  DateChangeFormate(duedate){
		var date = duedate;
		if(date){

		  var arr2 = date.split('-');
		  var month;
					if(arr2[1]=='01'){
						month = 'Jan';
					}else if(arr2[1]=='02'){
					month = 'Feb';
				}else if(arr2[1]=='03'){
					month = 'March';
				}else if(arr2[1]=='04'){
					month = 'April';
				}else if(arr2[1]=='05'){
					month = 'May';
				}else if(arr2[1]=='06'){
					month = 'June';
				}else if(arr2[1]=='07'){
					month = 'July';
				}else if(arr2[1]=='08'){
					month = 'Aug';
				}else if(arr2[1]=='09'){
					month = 'Sep';
				}else if(arr2[1]=='10'){
					month = 'Oct';
				}else if(arr2[1]=='11'){
					month = 'Nov';
				}else{
					month = 'Dec';
		} 
		  var dt = month +' '+arr2[2]+','+arr2[0];
		  return dt;
		 //alert(dt);
		}
		  
	  }
	  onDashboardRender(){
		const { navigate } = this.props.navigation;
		 navigate('Dashboard');
 }
 onBrowsePressed(){
	const { navigate } = this.props.navigation;
	navigate('Browse'); 
 }
  onTaskPressed(){
   let userId = global.userId;
  // alert('userId'+userId);
  const { navigate } = this.props.navigation;
  global.category_id = null;
  navigate('Mainview');
 }
   onLoginPressed(){
   const { navigate } = this.props.navigation;
navigate('Login');
 }
 onNextStep(){
   alert('chgghdhg');
 }
 onMyTaskPressed(){
	let userId = global.userId;
   // alert('userId'+userId);
	const { navigate } = this.props.navigation;
	if(userId){
   navigate('MyTask'); 
   }else{
	Alert.alert(
	   'Message',
	   'Please Login to go to My task',
	   [
		 {text : 'OK', onPress:()=>{this.onLoginPressed()}}
	   ]
	  );
   }
	}
	onMyProfilePressed(){
    let userId = global.userId;
    // alert('userId'+userId);
   const { navigate } = this.props.navigation;
   if(userId){
   
    navigate('Profile'); 
    }else{
   Alert.alert(
      'Message',
      'Please Login to go to Profile',
      [
      {text : 'OK', onPress:()=>{this.onLoginPressed()}}
      ]
     );
    }
  }
	render(){
		 const { params } = this.props.navigation.state;
		 const { navigate } = this.props.navigation;
		return(
			<Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >	
		<View style={styles.mainviewStyle}>
        <Modal
    animationType="slide"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
   <View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
   <Text>
 
  
   </Text>
   
   </View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
   <Image source={require('../img/app-logo.png')} />
   </Text>
   </View>
   <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
   <TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
  
   <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
  
   </TouchableHighlight>
   </View>

</View>
<View style={{flex:1,}}>
   
			<Tabs  tabBarUnderlineStyle= {{ backgroundColor: '#efeb10' }}renderTabBar={()=> <ScrollableTab />} tabContainerStyle={{height:30}}>
					<Tab heading="My Saved Task" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', height:10,}} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>
                        <View>
												
												 <SaveTask result={this.props}/>
												 </View>
					</Tab>
					<Tab heading="Posted Task" tabStyle={styles.tabcontain} textStyle={{color:'#000'}} activeTabStyle={{backgroundColor : '#000000',height:10,}} activeTextStyle={{color:'#fff'}}>
					<PostTask result={this.props}/>
					</Tab>
					<Tab heading="Offer Pending" tabStyle={styles.tabcontain} textStyle={{color:'#000'}} activeTabStyle={{backgroundColor : '#000000',height:10,}} activeTextStyle={{color:'#fff'}}>
                    <OfferPending result={this.props}/>
					</Tab>
					
					<Tab heading="Offer assigned" tabStyle={styles.tabcontain} textStyle={{color:'#000'}} activeTabStyle={{backgroundColor : '#000000',height:10,}} activeTextStyle={{color:'#fff'}}>
				      <OffersAccepting result={this.props}/>
					</Tab>
			</Tabs>
		
</View>
	
	<View style={styles.footer}>
		<View style={styles.browseButtons}>
		  <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
		    <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
		  </TouchableHighlight>
			<Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
		</View>
	  <View style={styles.postButtons}>
        <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>	 
		     <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
			 </TouchableHighlight>
		 <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
		</View>
		<View style={styles.myTaskButtons}>
		<TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
		  <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.clipboard}</FontAwesome>
			</TouchableHighlight>
				 <Text style={[styles.footerText,{color:'#efeb10'}]} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
			 </View>
		 {/* <View style={styles.messageButtons}>
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>	 
			<Text style={styles.footerText}>Message</Text>
		</View> */}
		<TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
		</View>
      </View>
		</Drawer>
			
		);
	}
	
		
	
}


//AppRegistry.registerComponent('myTask', () => myTask) //Entry Point    and Root Component of The App

