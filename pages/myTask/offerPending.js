import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Alert,Image,AppRegistry,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView,BackHandler,NetInfo,ActivityIndicator,FlatList} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';

import TimeAgo from 'react-native-timeago';
//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./myTaskStyle";

var {width, height} = Dimensions.get('window');
export default class OfferPending extends Component {
			
	static navigationOptions = {
		header: null,
	};
	
	constructor(props){
		super(props);
		this.state={
			 status : "",
			 error : "",
			 visible : false,
			 netInfoMsg : false,
			 data : [],
			saveTaskTotal : 0,
		}
	}
	componentDidMount(){
		this.getTaskList();   
	}
	componentWillUnmount() {
	  }

	getTaskList = async() =>{
		console.log('Call Function...'+this.state.page);
    this.setState({ visible: true });	
     const response = await fetch(
		'http://api.yellotasker.com/api/v1/getUserTask/'+global.userId+'?action=offerPending');
   const json = await response.json();
    console.log(json.data);
    var jData = json.data.offers_pending[0].offers_pending;
    this.setState({saveTaskTotal : jData.length})
    console.log(jData);
  	this.setState(state =>({data :[ ...state.data,...jData]}));
    //this.setState({data : [ ...state.data,...json.data]});
    
//	this.setState({data :json.data});
	 this.setState({ visible: false });	
	}

    readMoreTitle(description){
        if(description.length>32){
            return description.substring(0,30)+'...';
        }else{
            return description;
        }
   
    }

 readMoreCount(description){
     return description.length;
 }
 readMore(description){
	 if(description.length>24){
		 return description.substring(0,22)+'...';
	 }else{
		 return description;
	 }

 }
 DateChangeFormate(duedate){
	 if(duedate){
	var date = duedate;
//		var date = "2017-12-04";
	  //var arr1 = date.split(' ');
	  var arr2 = date.split('-');
	  var month;
	  if(arr2[1]=='01'){
		  month = 'Jan';
	  }else if(arr2[1]=='02'){
		month = 'Feb';
	}else if(arr2[1]=='03'){
		month = 'March';
	}else if(arr2[1]=='04'){
		month = 'April';
	}else if(arr2[1]=='05'){
		month = 'May';
	}else if(arr2[1]=='06'){
		month = 'June';
	}else if(arr2[1]=='07'){
		month = 'July';
	}else if(arr2[1]=='08'){
		month = 'Aug';
	}else if(arr2[1]=='09'){
		month = 'Sep';
	}else if(arr2[1]=='10'){
		month = 'Oct';
	}else if(arr2[1]=='11'){
		month = 'Nov';
	}else{
		month = 'Dec';
	} 
	  var dt = month +' '+arr2[2]+','+arr2[0];
	  return dt;
}
	 //alert(dt);
	  
  }		
  onDeletePress(offerId,taskId){
    console.log(offerId,taskId);
    Alert.alert(
      'Confirmation',
      'do you want to delete your posted offer for this task',
      [
        {text: 'Yes', onPress: () => this.deleteConfirm(offerId,taskId)},
        {text: 'No', onPress: () => console.log('cancel press!!!!!!')},
      ]
  );
}
deleteConfirm = async(offerId,taskId) =>{
  if(global.userId){
     console.log(offerId,taskId);
     try
     { 
     this.setState({visible: true});
       let response = await fetch('http://api.yellotasker.com/api/v1/deleteOffer', 
         { 
           method: 'POST',
               headers: 
                 { 
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                   
                   },
                   body: JSON.stringify({
                    "offerId" : offerId     
                    ,"taskId": taskId
                        ,"userId": global.userId
                   })
              }); 
              let res  = await response.text();
              this.setState({visible: false});
              console.log(response.status);
       
              console.log(JSON.stringify(res));
              if(response.status >=200){
                     this.getTaskList();
                }else{ 
                 let error = res;
                 throw error;
                 } 
               } catch (error) { 
       //this.setState({error: error});
      
       console.log(JSON.stringify(error));
      // console.log(JSON.parse(error));
      }
  }else{

  }
    
}


assignTextFun(status){
  var st = '';
  if(status=='assigned'){
st = "Assigned to somebody";
  }else{
    st = status;
  }
  return st;
}
	render(){
        var self = this;
       const { navigate } = self.props.result.navigation;
	    	return(
        <View>
          <Modal
              animationType="fade"
              transparent={true}
              visible={this.state.visible}>
              <View style={{flex: 1,
                  alignItems: 'center',
                  flexDirection: 'column',
                  justifyContent: 'space-around',
                  backgroundColor: '#00000040'}} >
                    <View style={{backgroundColor: 'transparent',
                          height: 100,
                          width: 200,
                          borderRadius: 3,
                          alignItems: 'center',
                          justifyContent: 'space-around'}}> 
                        <Image source={require('../img/loading.png')}/>
                    </View>
            </View>
          </Modal>
            <View style={{paddingHorizontal:20,paddingVertical:10,flexDirection:'row',borderBottomColor:'gray',borderBottomWidth:1}}>
                                <FontAwesome style={{fontSize: 18,color:'#34495e',flexDirection:'row',alignItems:'center'}}>{Icons.bitbucket}</FontAwesome>  
                                <Text style={{paddingLeft:5}}>Total Pending Task - {this.state.saveTaskTotal}</Text>
            </View>
        <View style={{marginBottom:120}}>
                {this.state.data &&
                  <FlatList 
                              data={this.state.data}
                              keyExtractor={(x,i)=>i}
                              renderItem={({item})=>

                      <View style={{paddingHorizontal:5}}>
                          <TouchableHighlight >
                            <View style = {[styles.mainBrowse,item.status=='open' && styles.open,item.status=='assigned' && styles.open,item.status=='expired' && styles.expire]}>
              
                                    <View style={{flexDirection:'row'}}>
                                        <View style = {styles.browseContent}>    
                                            <Text style = {styles.browseContentHeading} >{this.readMoreTitle(item.title)}</Text> 
                                        
                                            <Text style = {styles.browseSubContent}>{this.readMore(item.description)}
                                            {this.readMoreCount(item.description)>24&& 
                                            <Text> 
                                            {item.status=='expired' && <Text style={{color:'#337ab7',fontWeight:'bold'}} >Read More</Text>}
                                              {item.status=='open'  && <Text style={{color:'#337ab7',fontWeight:'bold'}} onPress={()=>navigate('singlePendingTask',{id:item.taskId,back:'MyTask'})} >Read More</Text>}
                                              
                                              {item.status=='assigned' && <Text style={{color:'#337ab7',fontWeight:'bold'}} onPress={()=>navigate('singlePendingTask',{id:item.taskId,back:'MyTask'})} >Read More</Text>}
                                              </Text> 
                                            }
                                            </Text> 
                                            <View style={{flexDirection:'row',flex:1,paddingVertical:5}}>
                                              
                                                  <View style={{paddingHorizontal:10,}}>
                                                        {!item.task_posted_user.profile_image && 
                                                          <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
                                                        }
                                                        {item.task_posted_user.profile_image && 
                                                          <Image source={{uri : item.task_posted_user.profile_image}} style={styles.userImage}/>
                                                        }
                                              
                                                  </View>
                                                  <View style={{flexDirection:'column'}}>
                                                        <Text style={{paddingVertical:3,color:'#337ab7',alignItems:'flex-start',flex:0.5}}>{item.task_posted_user.first_name} {item.task_posted_user.last_name}</Text>
                                                        <View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                                                        </View>
                                                  </View>
                                            </View>
                                      </View>

                                    <View style = {styles.browseOffer}>
                                          <Text onPress={this.onDeletePress.bind(this,item.id,item.taskId)}> <FontAwesome style={{fontSize: 18,color:'#337ab7',flexDirection:'row',alignItems:'center'}} >{Icons.trash}</FontAwesome></Text>
                                          <Text style = {styles.browseOfferText}>{item.totalAmount} MYR</Text>
                                          {item.status=='expired' && <Text style = {styles.browseOfferView} >View</Text>}
                                          {item.status=='open'  && <Text style = {styles.browseOfferView} onPress={()=>navigate('singlePendingTask',{id:item.taskId,back:'MyTask'})} >View</Text>}
                                          {item.status=='assigned' && <Text style = {styles.browseOfferView} onPress={()=>navigate('singlePendingTask',{id:item.taskId,back:'MyTask'})} >View</Text>}
                                    </View>
                                </View>
                                <View style={{paddingHorizontal:10}}>
                                        <Text style={{color:'#000000',fontWeight:'bold',flexDirection:'row',}}>
                                          <FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> Due Date <Text style={{color:'#666'}}>{this.DateChangeFormate(item.dueDate)}</Text>
                                        </Text>
                                      <View style={{flexDirection:'row',flex:1,paddingVertical:4}}>
                                            <Text style={{flex:0.5}}>
                                                <Text style={[item.status=='open' && styles.openText,item.status=='assigned' && styles.assignText,item.status=='expired' && styles.expireText,]}> {this.assignTextFun(item.status)}</Text>
                                          </Text>
                                          <Text style={{flex:0.5,fontWeight:'bold',flexDirection:'row',textAlign:'right',color:'#484848'}}><Text>{item.offer_count} Offer</Text> <Text>{item.comment_count} Comments</Text></Text>
                                      </View>
                              </View>
                          </View>
                      </TouchableHighlight>
                    </View>
                              
            }
          />
        }
        </View>	
      </View>
        )
	}	
}


