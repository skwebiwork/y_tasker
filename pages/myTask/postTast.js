import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Alert,Image,AppRegistry,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView,BackHandler,NetInfo,ActivityIndicator,FlatList} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import TimeAgo from 'react-native-timeago';
//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./myTaskStyle";

var {width, height} = Dimensions.get('window');
export class OfferComponent extends Component{
    
	render(){
		console.log(this.props.result);
		if(this.props.result){
			var res = this.props.result.map((offer,i)=>{
				return (
   <View key={i}>
						 <View style={styles.mainBrowses}>
					   <View style={styles.browseMen}>
						   <Image source={require('../img/browse-man.png')} style={styles.replymenImage} />
					   </View>
					   <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4}}>
					   <View style={{flexDirection:'row',marginTop:6}}>
					 <View style={{flex:0.7,flexDirection:'row'}}><Text style={{marginTop:5,textAlign:'center',color:'#337ab7'}}> {offer.first_name} {offer.last_name}</Text>
					 <View style={{flexDirection:'row',marginTop:6,marginLeft:7}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
					</View>
					 </View>
					 <View style={{flex:0.3,}}>
					 <TouchableHighlight>
						 <Text style={styles.PercentBtn }>
						80%
						 </Text>
						 </TouchableHighlight>
					 </View>
					 </View>
					 
					
					
				   
					   </View>                      
				   </View>
				   </View>
					)
			})
		}
		 return (
				  <View>
						 {res}
				  </View>
		 )
	}
}
export default class PostTask extends Component {
			
	static navigationOptions = {
		header: null,
	};
	
	constructor(props){
		super(props);
		global.count = 0;
		this.onEndReachedCalledDuringMomentum = true;
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.state={
			touchMsg : false,
			taskPeopleRequired:"",
			taskBudget:"",
			 LodingStatus:false,
			 status : "",
			 error : "",
			 visible : false,
			 netInfoMsg : false,
			 data : [],
			dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			dataSources: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			searchShow : false,
			page:1,
			isLoading : false,
			saveTaskTotal : 0,
			makeOffer:false,
            taskOffers : [],
            catefory :[],
            activeSection: false,
            collapsed: true,
            activeSection1: false,
            activeSection2: false,
            activeSection3: false,
            taskData : [],
            activeSection4:false,
            activeSection5:false,
            taskPosted_details :0,

			
		//link: 'http://hardeepcoder.com/laravel/easyshop/api/products/' + params.id,
        }
        var st = this.state;
			
      //  this.navigate = this.props.navigation.navigate;
      const navigation = this.props.navigation;
	}
	componentDidMount(){
		// const { params } = this.props.navigation.state;
		this.getTaskList();
	
	   
	}
	componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
	
	  }

	getTaskList = async() =>{
		console.log('Call Function...'+this.state.page);
		
        this.setState({ visible: true });	
     const response = await fetch(
		'http://api.yellotasker.com/api/v1/getUserTask/'+global.userId+'?action=postedTask');
	 const json = await response.json();
    console.log(json.data);
    var jData = json.data.postedTask[0].posted_task;
    this.setState({saveTaskTotal : jData.length})
    console.log(jData);
	this.setState(state =>({data :[ ...state.data,...jData]}));
    //this.setState({data : [ ...state.data,...json.data]});
    
//	this.setState({data :json.data});
	 this.setState({ visible: false });	
    }
    

 readMoreCount(description){
     return description.length;
 }
 readMore(description){
	 if(description.length>24){
		 return description.substring(0,22)+'...';
	 }else{
		 return description;
	 }

 }
 DateChangeFormate(duedate){
	 if(duedate){
	var date = duedate;
//		var date = "2017-12-04";
	  //var arr1 = date.split(' ');
	  var arr2 = date.split('-');
	  var month;
	  if(arr2[1]=='01'){
		  month = 'Jan';
	  }else if(arr2[1]=='02'){
		month = 'Feb';
	}else if(arr2[1]=='03'){
		month = 'March';
	}else if(arr2[1]=='04'){
		month = 'April';
	}else if(arr2[1]=='05'){
		month = 'May';
	}else if(arr2[1]=='06'){
		month = 'June';
	}else if(arr2[1]=='07'){
		month = 'July';
	}else if(arr2[1]=='08'){
		month = 'Aug';
	}else if(arr2[1]=='09'){
		month = 'Sep';
	}else if(arr2[1]=='10'){
		month = 'Oct';
	}else if(arr2[1]=='11'){
		month = 'Nov';
	}else{
		month = 'Dec';
	} 
	  var dt = month +' '+arr2[2]+','+arr2[0];
	  return dt;
}
	 //alert(dt);
	  
  }	
  indianDateFormat(dueDate){
    var arr = dueDate.split('-');
    var formattedDate = arr[2]+'-'+arr[1]+'-'+arr[0];
    return formattedDate;
  }	

  readMoreTitle(description){
    if(description.length>32){
        return description.substring(0,30)+'...';
    }else{
        return description;
    }

}
readMoreClick(id,status){
	if(status!='expired'){
		this.setState({makeOffer:true});
		console.log(status,id);
		this.fetchTaskDetail(id);
		this.getTaskOffer(id);
		this.fetchTaskComment(id);
	}else{
		console.log('task is expired');
	}


}
fetchTaskDetail(taskId){
	
	// fetch('http://api.yellotasker.com/api/v1/getPostTask?taskId='+params.id)
	 fetch('http://api.yellotasker.com/api/v1/getPostTask?taskId='+taskId)
 .then((response) => response.json())
 .then((responseJson) =>{
   console.log(responseJson.data);
	this.setState({
	 taskData:responseJson.data,
 })
 console.log(this.state.taskData);
 this.setState({singleTask : responseJson.data[0].status})
 //this.setState({visible: false}); 
 })
 .catch((error) =>{
	// alert('fetchTaskDetailerror'+error);
 });
 }
 getTaskOffer(taskId){
	 
			 fetch('http://api.yellotasker.com/api/v1/taskOffer/'+taskId)
			 .then((response) => response.json())
			 .then((responseJson) =>{
				 console.log('------------------------------------------');
                 console.log(responseJson.data);
                 this.setState({taskPosted_details:responseJson.data})
						  var offerResult = responseJson.data[0];
						 // offerResult = offerResult[0].offer_details;
						  console.log('offerResult'+offerResult);
						  //console.log('offer list');
						// console.log('offer Length'+offerResult[0].interested_users.lenght);
						  if(responseJson.data.length>0){
							  console.log('if..............');
							  console.log(offerResult);
						 this.setState({
							 taskOffers:offerResult.offer_details
						 })
						 
						 //console.log(this.state.taskOfferEmpty);
						 console.log(this.state.taskOffers);
					 }else{
						
						 console.log('else+++++offerResult.length'+offerResult.length);
					 }
					   // alert(JSON.stringify(this.state.taskOffer));
						 
						  this.setState({visible: false}); 
			 })
			 .catch((error) =>{
			 // alert('fetchTaskDetailerror'+error);
			 });
   }
   fetchTaskComment(taskId){
	 
	// this.setState({visible: true}); 
 fetch('http://api.yellotasker.com/api/v1/comment/post?getCommentBy=task&taskId='+taskId)
 .then((response) => response.json())
 .then((responseJson) =>{
   // alert(JSON.stringify(responseJson));
	 data = responseJson.data; // here we have all products data
	 data = data.reverse();
   
 // var stubComment = 
 // let commentLoop = this.state.commentLoop;
 let  commentLoop = this.prepareCommentLoop(data);
 console.log(JSON.stringify(data));
 console.log(data.length);
 if(data.length>0){
     console.log('if------------------');
 this.setState({
	 dataSources: this.state.dataSources.cloneWithRows(data)
 })
 console.log(data);
 console.log(this.state.dataSources);
 }else{
     console.log('else***********************');
 }
 this.setState({visible: false}); 
 })
 .catch((error) =>{
	// alert('fetchTaskCommenterror'+error);
 });
 }
 prepareCommentLoop(resp){
	 var commonIndexes = [];
	 for(var i=0; i < resp.length;i++){
	   for(var j=0;j < resp.length;j++){
		 if(resp[i].commentId == resp[j].id){
		   if(!resp[j].comments) resp[j].comments = [];
		   resp[j].comments.push(resp[i]);
		   commonIndexes.push(resp[i]);
		   //resp.splice(j,1);
		 }else{
		   if(resp[j].comments){
			   this.prepareCommentLoop(resp[j].comments);
			 }
		 }
	   }
	 }
	 for(var i=0;i<commonIndexes.length;i++){
	 for(var j=0 ; j<resp.length; j++){
	   if(resp[j].id == commonIndexes[i].id){
		 resp.splice(j,1);
	   }
	 }
   }
   }
   resPress = (text)=>{
	 //alert(JSON.stringify(text));
	 let idd = JSON.stringify(text);
 this.setState({replyId: idd});
 
 }

 _renderHender(headerData){
     return (<View>
     <Animatable.View duration={400} style={{ flexDirection:'row',
    backgroundColor: '#efeb10',
    padding: 10,
   marginTop : 10,
   alignItems:'center',}} transition="backgroundColor">
     
       <Text style={styles.headerText}>Comment</Text>
     </Animatable.View></View>)
 }
 _content(section, i, isActive){
     
     return(
        
        <Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
        <View style={{flex:1,flexDirection:'column',}} >
        <Text style={{color:'#333',paddingVertical:5,fontSize : 14,}}>{section.description}</Text>  
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                   
                   <Text style={{flex:0.5,alignItems:'flex-start',}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',}}>Due Date</Text> 
                    </Text>
                    <Text style={{flex:0.5,}}>{section.dueDate}</Text>
            
        </View>
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>People Required</Text>
                            <Text style={{flex:0.5,}}>
                            5
                            </Text>
        </View>
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Budget Type</Text>
                            <Text style={{flex:0.5,}}>
                            {section.budgetType}
                            </Text>
        </View>
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Total Amount</Text>
                            <Text style={{flex:0.5,}}>
                            {section.totalAmount} MYR
                            </Text>
                            </View>
                         {/*   <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}> 
		  <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center'}}>{Icons.locationArrow} </FontAwesome>Location 
		  </Text>
                            <Text style={{flex:0.5,}}>
                            <Text>{section.address ==null ||section.address =='' ? section.locationType : section.address}</Text>
                            </Text>
                            </View> */}
      </View>
      </Animatable.View>
     
     )
 }
 _content2(section, i, isActive){
   // console.log(this.state.dataSources);
     return(
           
        <Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
           <View style={{ paddingVertical: 10, paddingHorizontal: 10,borderRadius: 4, }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>COMMENTS ABOUT THIS TASK</Text>
                            <Text style={{ paddingVertical: 10, fontSize: 12, fontWeight: 'normal', color: '#333', }}>Comment below for more details and remember that for your safety, not to share personal information e.g. email and phone numbers.</Text>
                        
                        </View>
                        <View style={styles.mainBrowses}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Ask a question"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}

       ></TextInput>
     
     <Text style={{textAlign:'right',color:'#000000'}} >Send</Text>
                       </View>
                       </View>
                                          </Animatable.View>
     )
 }
 
 _renderHeader22(section, i, isActive) {
    // console.log(section);
     return (
       <Animatable.View duration={400} style={[styles.header]} transition="backgroundColor">
       <Image style={{width: 30, height: 30}} source={{
         uri:section.category_group_image}} />
         <Text style={styles.headerText}>text</Text>
       </Animatable.View>
     );
   }
 
   _renderContent22(section, i, isActive) {
     return (
       <Animatable.View duration={400}  style={[styles.content]} transition="backgroundColor">
        
         
         <Text>asdfffsfsdf</Text>
 
       </Animatable.View>
     );
   }
   _setSection5(section) {
    this.setState({ activeSection5: section });
  }
 _content4(section, i, isActive){
    var _this =this;
    // console.log(this.state.dataSources);
  // console.log(st.taskOffers);
  console.log(section.interested_user);
      return(
        <View style={{paddingVertical:8}}>
                    
             <Text>fgfd</Text>
             
      </View>
      
      )
  }
 _descriptionContent(){
     return(
        <FlatList 
        data={this.state.taskData}
        keyExtractor={(x,i)=>i}
        renderItem={({item})=>
        <View style={{flex:1,flexDirection:'column',paddingHorizontal:'1%',paddingVertical:'3%',borderBottomWidth:1}}>
        <Text style={{color:'#333',paddingVertical:5,fontSize : 12,}}>{item.description}</Text>  
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                   
                   <Text style={{flex:0.5,alignItems:'flex-start',}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',fontSize : 12}}>Due Date</Text> 
                    </Text>
                    <Text style={{flex:0.5,}}>{this.indianDateFormat(item.dueDate)}</Text>
            
        </View>
        </View>
}
/>
     )
 }
 _feedBackContent(){
     return(
        <Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
        <View style={{flex:1,flexDirection:'column',}} >
              <Text>Thanks for choosing this task lorem ipsum dolor sit amet is the dummy text for describing.</Text>
        </View>
        <View style={{flexDirection:'row',paddingVertical:10}}>
			<Text style={{paddingVertical:3,color:'#000',alignItems:'flex-start',fontWeight:'400'}}>Rating</Text>
		  <View style={{flexDirection:'row',alignItems:'center',paddingLeft:15,}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
		  </View>
		  </View>
          <Text style={{flexDirection:'row',color:'#000',paddingVertical:10,fontWeight:'400'}}> Review</Text>
          <TextInput
       placeholder=""  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}

       ></TextInput>
       <Text style={styles.followBtn}> Submit</Text>
        </Animatable.View>
     )
 }
_msgContent(){
    return(
        <Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
           <View style={{ paddingVertical: 10, paddingHorizontal: 8,borderRadius: 4, }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>5 MESSAGES FROM DOER</Text>
                            <Text style={{ paddingVertical: 10, fontSize: 12, fontWeight: 'normal', color: '#333', }}>Comment below for more details and remember that for your safety, not to share personal information e.g. email and phone numbers.</Text>
                        
                        </View>
                        <View style={styles.mainBrowsess}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Ask a question"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}

       ></TextInput>
     
     <Text style={{textAlign:'right',color:'#000000'}} >Send</Text>
                       </View>
                       </View>
                       <View style={styles.mainBrowsess}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                   <Text style={{paddingVertical:4,}}>I am in parramatta would there in 10 minuets Offer includes 15% Airtasker fees and a man with van</Text>
     <View stylee={{flexDirection:'row',flex:1,paddingVertical:4,}}>
     <Text style={{fontSize: 14,color:'#337ab7',paddingVertical:4,}} >1 min ago</Text>
 
     </View>
                       </View>
                       </View>

                                          </Animatable.View>
     )
}
 
 _setSection(section) {
     console.log(section);
    this.setState({ activeSection: section });
  }
  _setSection1(section) {
    this.setState({ activeSection1: section });
  }
  _setSection2(section) {
    this.setState({ activeSection2: section });
  }
  _setSection3(section) {
    this.setState({ activeSection3: section });
  }
  _setSection4(section) {
    this.setState({ activeSection4: section });
  }
 
	render(){
        var self = this;
		//const { params } = self.props.navigation.state;
         //const { navigate } = self.props.navigation;
         const { navigate } = self.props.result.navigation;
		return(
				
		<View >
      
		
   <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <Modal
                    
                    transparent={false}
                    visible={this.state.makeOffer}>
                            <View style={{flexDirection:'row',paddingVertical:10,backgroundColor:'#000000'}}>
                <View style={{flex:0.2,alignItems:'flex-start',paddingVertical:12,paddingLeft:10}}>
				<TouchableHighlight onPress={()=>this.setState({makeOffer:false})} underlayColor={'transparent'}>
                <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.arrowLeft}</FontAwesome>
                </TouchableHighlight> 
                </View>
             <View  style={{flex:0.6,alignItems:'center'}}>
             <Text >
                <Image source={require('../img/app-logo.png')}/>
                </Text>
                </View>
                <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:12,paddingRight:10}}>
                
                </View>             
             </View>

             <ScrollView style={styles.scrollViewStyle}>
             <View style={{paddingHorizontal:'2%'}} >
             <FlatList 
              data={this.state.taskData}
              keyExtractor={(x,i)=>i}
              renderItem={({item})=>
              <View style={{flex:1,flexDirection:'column',paddingHorizontal:'1%',paddingVertical:'3%',borderBottomWidth:1}}>
                <View style={{flexDirection:'row'}}>
                       <View style={{flex:0.6}}></View>
                       <View style={{flex:0.3,flexDirection:'row'}}>
                       <Text style={styles.openBtn}>{item.status}</Text>
                       <Text style={{paddingHorizontal:15}}> <FontAwesome style={{fontSize: 18,color:'#34495e',flexDirection:'row',paddingTop:5}} >{Icons.trash}</FontAwesome></Text>
                       <Text > <FontAwesome style={{fontSize: 18,color:'#34495e',flexDirection:'row',paddingTop:3}} >{Icons.pencil}</FontAwesome></Text>
                       </View>
                </View>
                 <Text style={{color:'#34495e',fontSize:14,paddingVertical:8,fontWeight:'600'}}>{item.title}
                  </Text>
              </View>
      }
      />

         <View style={{paddingVertical:8}}>
                    
             <Accordion
        activeSection={this.state.activeSection}
        sections={this.state.taskData}
        initiallyActiveSection	={0}
        renderHeader={()=>{return(<View>
         <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16}}>Description</Text>
            </Animatable.View></View>)}}
        renderContent={this._content}
        duration={400}
        onChange={this._setSection.bind(this)}
        underlayColor={'transparent'}
      />
      </View>
      <View style={{paddingVertical:6}}>            
        <Accordion
           activeSection={this.state.activeSection1}
           sections={['ad']}
           renderHeader={()=>{return(<View>
              <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16}}>Comment</Text>
           </Animatable.View></View>)}}
          renderContent={this._content2}
          duration={400}
          onChange={this._setSection1.bind(this)}
          underlayColor={'transparent'}
      />
      </View>
       <View style={{paddingVertical:6}}>            
        <Accordion
           activeSection={this.state.activeSection4}
           sections={this.state.taskOffers}
           renderHeader={()=>{return(<View>
            <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
              marginTop : 10,overflow:"hidden",borderRadius: 6,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16}}>Offer</Text>
              </Animatable.View></View>)}}
          renderContent={this._content4}
          duration={400}
          onChange={this._setSection4.bind(this)}
          underlayColor={'transparent'}
      />
      </View>
      {!this.state.singleTask=='open' && 
      <View style={{paddingVertical:6}}>            
        <Accordion
           activeSection={this.state.activeSection3}
           sections={['FeedBack']}
           renderHeader={()=>{return(<View>
              <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16}}>View Message History</Text>
           </Animatable.View></View>)}}
          renderContent={this._msgContent}
          duration={400}
          onChange={this._setSection3.bind(this)}
          underlayColor={'transparent'}
      />
      </View>
      }
       {!this.state.singleTask=='open' && 
      <View style={{paddingVertical:6}}>            
        <Accordion
           activeSection={this.state.activeSection2}
           sections={['FeedBack']}
           renderHeader={()=>{return(<View>
              <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16}}>FeedBack</Text>
           </Animatable.View></View>)}}
          renderContent={this._feedBackContent}
          duration={400}
          onChange={this._setSection2.bind(this)}
          underlayColor={'transparent'}
      />
      </View>
       }
      </View>
      </ScrollView>
       
                </Modal>
        
                    <View style={{paddingHorizontal:20,paddingVertical:10,flexDirection:'row',borderBottomColor:'gray',borderBottomWidth:1}}>
		  <FontAwesome style={{fontSize: 18,color:'#34495e',flexDirection:'row',alignItems:'center'}}>{Icons.bitbucket}</FontAwesome> 
                        
                        <Text style={{paddingLeft:5}}>Total Posted Task - {this.state.saveTaskTotal}</Text></View>
						<View style={{marginBottom:120}}>
{this.state.data &&
					<FlatList 
                      data={this.state.data}
					  keyExtractor={(x,i)=>i}
						
                      renderItem={({item})=>
                      <View style={{paddingHorizontal:5}}>
<TouchableHighlight >
<View style = {[styles.mainBrowse,item.status=='open' && styles.open,item.status=='assigned' && styles.open,item.status=='expired' && styles.expire]}>

<View style={{flexDirection:'row'}}>
			
			<View style = {styles.browseContent}>    
			<Text style = {styles.browseContentHeading} >{this.readMoreTitle(item.title)}</Text> 
			
			<Text style = {styles.browseSubContent}>{this.readMore(item.description)}
			{this.readMoreCount(item.description)>24&& 
            <Text style={{color:'#337ab7',fontWeight:'bold'}}> 
            {item.status=='expired' && <Text > Read More</Text>}
				{item.status=='open'  && <Text onPress={()=>navigate('singlePostedTask',{id:item.id,back:'MyTask'})} > Read More</Text>}
        {item.status=='assigned' && <Text  onPress={()=>navigate('singlePostedTask',{id:item.id,back:'MyTask'})} > Read More</Text>}
        
           
            
            </Text>}
			</Text> 
			<View style={{flexDirection:'row',flex:1,paddingVertical:5}}>
        
            <View style={{paddingHorizontal:10,}}>
		   {!item.task_posted_user.profile_image && 
		    <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
		  }
		   {item.task_posted_user.profile_image && 
		    <Image source={{uri : item.task_posted_user.profile_image}} style={styles.userImage}/>
		  }
				
                </View>
            <View style={{flexDirection:'column'}}>
			<Text style={{paddingVertical:3,color:'#337ab7',alignItems:'flex-start',flex:0.5}}>{item.task_posted_user.first_name} {item.task_posted_user.last_name}</Text>
		  <View style={{flexDirection:'row',alignItems:'center',paddingRight:10,flex:0.5}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
		  </View>
		  </View>
          
          </View>
		{/*  <Text style={{color:'#000',paddingVertical:5}}>
		  <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome> <Text>{item.address ==null ||item.address =='' ? item.locationType : item.address}</Text>
		  </Text> */}
       {/*   <Text style={{color:'#000000',fontWeight:'bold'}}><FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center',}}>{Icons.calendar}</FontAwesome> Due Date <Text style={{color:'#666'}}>{this.indianDateFormat(item.dueDate)}</Text> </Text>*/}
{/*
          <Text style={{color:'#337ab7',fontWeight:'bold'}}><FontAwesome style={{fontSize: 10,color:'#337ab7',flexDirection:'row',alignItems:'center',}}>{Icons.calendar}</FontAwesome> Due Date <Text>{this.indianDateFormat(item.dueDate)}</Text> </Text>
        
        */} 
     {/*    <Text style={[item.status=='open' && styles.openText,item.status=='assigned' && styles.assignText,item.status=='expired' && styles.expireText]}> {item.status}</Text>
 */}
	  </View>

		   <View style = {styles.browseOffer}>
        
				<Text style = {styles.browseOfferText}>{item.totalAmount} MYR</Text>
				{/*  <Text style = {styles.browseOfferView} onPress={()=>navigate('singlePostedTask',{id:item.id,back:'MyTask'})}> View</Text>*/}
                 {item.status=='expired' && <Text style = {styles.browseOfferView} >View</Text>}
				{item.status=='open'  && <Text style = {styles.browseOfferView} onPress={()=>navigate('singlePostedTask',{id:item.id,back:'MyTask'})} >View</Text>}
        {item.status=='assigned' && <Text style = {styles.browseOfferView} onPress={()=>navigate('singlePostedTask',{id:item.id,back:'MyTask'})} >View</Text>}
        


                {/*<Text style = {styles.browseOfferView} onPress={()=>this.readMoreClick(item.id,item.status)}>View</Text>*/}
			</View>
            </View>
            <View style={{paddingHorizontal:10}}>
     {/* <Text style={{color:'#000',paddingVertical:5}}>
		  <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center'}}>{Icons.locationArrow}</FontAwesome> <Text>{item.address ==null ||item.address =='' ? item.locationType : item.address}</Text>
		  </Text>*/}
		 
      <Text style={{color:'#000000',fontWeight:'bold',flexDirection:'row',}}>
      
    <FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> Due Date <Text style={{color:'#666'}}>{this.DateChangeFormate(item.dueDate)}</Text>
    
    
    </Text>
		 
    <View style={{flexDirection:'row',flex:1,paddingVertical:4}}>
    <Text style={{flex:0.4}}>
    <Text style={[item.status=='open' && styles.openText,item.status=='assigned' && styles.assignText,item.status=='expired' && styles.expireText,]}> {item.status}</Text>
   </Text>
   
   <Text style={{flex:0.6,fontWeight:'bold',flexDirection:'row',textAlign:'right',color:'#484848'}}><Text>{item.offer_count} Offer</Text> <Text>{item.comment_count} Comments</Text></Text>
    </View>
      </View>
			
   </View>
   </TouchableHighlight>
                      </View>
                   }
/>
}
</View>
                     

		
      </View>
        )
	}
	
		
	
}



//AppRegistry.registerComponent('list', () => list) //Entry Point    and Root Component of The App

