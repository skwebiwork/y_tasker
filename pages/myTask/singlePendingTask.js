import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Alert,Image,AppRegistry,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView,BackHandler,NetInfo,ActivityIndicator,FlatList} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import { Container, Content, List, ListItem,Drawer,Icon, Fab  } from 'native-base';
import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import TimeAgo from 'react-native-timeago';
//import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./myTaskStyle";
import DatePicker from 'react-native-datepicker';
var {width, height} = Dimensions.get('window');
import { Dropdown } from 'react-native-material-dropdown';
export class SubChildComponent extends Component{
  render(){
      if(this.props.result){
          console.log(this.props.result);
          var res = this.props.result.map((item,i)=>{
             
              return (
                  <View style={styles.subreplymainsBrowse}>
                  <View style={styles.browseMen}>
                      <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.subreplymenImage} />
                  </View>
                  <View style = {{flex:0.8,flexDirection:'column',paddingHorizontal:10,}}>
                  {item.user_detail && 
                  <Text style={{color:'#337ab7'}}>
                  {item.user_detail.first_name} {item.user_detail.last_name}</Text>
                  }
                  <Text style = {styles.browseSubContent} >{item.commentDescription}</Text> 
                  <View style={{flexDirection:'row',paddingVertical:5,}}>
                     <View>
                       <Text style={{fontWeight:'bold',fontSize:14,color:'#000000'}}><TimeAgo time={item.created_at}/></Text>
                  </View>
                  <View>

                       <Text style={{fontWeight:'normal',fontSize:14,paddingLeft:25}} > 
                       <FontAwesome style={{fontSize: 14,color:'#337ab7'}}>{Icons.reply}</FontAwesome>  Reply</Text>
                  </View>
                  </View>
            </View>
                  </View>
              )
          })
      }
       return (
                <View>
                       {res}
                </View>
       )
  }
}
export class ChildComponent extends Component{
   
  render(){
      if(this.props.result){
          var res = this.props.result.reverse().map((item,i)=>{
              if(item){
              return (
                  <View key={i}>
                  <View style={styles.replymainsBrowse}>
                  <View style={styles.browseMen}>
                  {item.user_detail.profile_image &&  <Image source={{uri : item.user_detail.profile_image}} style={styles.subreplymenImage} />}
                     {!item.user_detail.profile_image &&  <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.subreplymenImage} />}
                        
                  
                  </View>
                  <View style = {{flex:0.8,flexDirection:'column',paddingHorizontal:10,}}>
                  {item.user_detail && 
                  <Text style={{color:'#337ab7'}}>
                  {item.user_detail.first_name} {item.user_detail.last_name}</Text>
                  }
                  <Text style = {styles.browseSubContent} >{item.commentDescription}</Text> 
                  <View style={{flexDirection:'row',paddingVertical:6,}}>
                     <View>
                       <Text style={{fontWeight:'bold',fontSize:14,color:'#000000'}}><TimeAgo time={item.created_at}/></Text>
                  </View>
                  <View>

                       <Text style={{fontWeight:'normal',fontSize:14,paddingLeft:25}}> 
                       <FontAwesome style={{fontSize: 14,color:'#337ab7'}}>{Icons.reply}</FontAwesome>  Reply</Text>
                  </View>
                  </View>
            </View>
                  </View>
                  {item.comments && 
                   <SubChildComponent  result={item.comments} />
                  }
                   </View>
              )
          }
          })
      }
       return (
                <View>
                       {res}
                </View>
       )
  }
}
export default class SinglePendingTask extends Component {
			
	static navigationOptions = {
		header: null,
	};
	
	constructor(props){
		super(props);
    global.count = 0;
    global.singleTask = "";
    global.updatedlocationType = "";
        this.onEndReachedCalledDuringMomentum = true;
        this._deleteInvoice = this._deleteInvoice.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);
        this.typographyRef = this.updateRef.bind(this, 'typography');
        this.typographyRefLocation = this.updateRef.bind(this, 'typography');
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.state={
			touchMsg : false,
			taskPeopleRequired:"",
			taskBudget:"",
			 LodingStatus:false,
			 status : "",
			 error : "",
			 visible : false,
			 netInfoMsg : false,
			 data : [],
			dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			dataSources: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
			searchShow : false,
			page:1,
			isLoading : false,
			saveTaskTotal : 0,
			makeOffer:false,
            taskOffers : [],
            catefory :[],
            activeSection: 0,
            collapsed: true,
            activeSection1: false,
            activeSection2: false,
            activeSection3: false,
            taskData : [],
            activeSection4:false,
            activeSection5:false,
            taskPosted_details :0,
            isLoad : false,
            clickViewDetails : null,
            isNoLoad : false,
            isCommentView : false,
            userProfileImage : 'http://yellotasker.com/assets/img/task-person.png',
            commentDescription : '',
            replyId : null,
            replycommentDescription : '',
            isMsgView : false,
            msgPosted:'',
            totalMsg : 0,
            totalComment : 0,
            isEdit : false,
            isDescriptionShow : true,
            updatedTaskBudget : '',
            updatedTaskaddress : '',
            budgetLabel : ' ',
            locationLabel : ' ',
            updatedlocationType : '',
            updateTaskTitle : '',
            updatedTaskDescription : '',
            updatedTaskAmount : '',
            date_in : '',
            isErrorView : false,
            errorMsg : '',
            isOfferLoad : false,
            isOfferEdit : false,
            active: 'true',
            budgetTypeOption : [{value : "Materials & workmanship",label : "Materials & workmanship"},{value : "Workmanship only - i will supply materials",label : "Workmanship only - i will supply materials"}],
            locationTypeOption : [{value : 'Physical task',label : 'Physical task'},{value : 'Digital task',label : 'Digital task'}],
        }
        var st = this.state;

        
			
      //  this.navigate = this.props.navigation.navigate;
      const navigation = this.props.navigation;
	}
	componentDidMount(){
        const { params } = this.props.navigation.state;
        var id =params.id;
        console.log(id);
		this.fetchTaskDetail(id);
		this.getTaskOffer(id);
    this.fetchTaskComment(id);
    this.fetchPostMsg(id);
	//fetchTaskComment
	   
	}
	componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
	
	  }
    onChangeText(text) {
      //console.log(text);
      this.setState({updatedTaskBudget:text,budgetLabel : ''});
     }
     onChangeLocation(text){
       console.log('onChangeLocation'+text)
       global.updatedlocationType = text;
      this.setState({locationLabel : ''});
      //this.setState({updatedlocationType:text,locationLabel : ''});
      // console.log(this.state.updatedlocationType);
     }
     updateRef(name, ref) {
      this[name] = ref;
    }
	getTaskList = async() =>{
		console.log('Call Function...'+this.state.page);
		
        this.setState({ visible: true });	
     const response = await fetch(
		'http://api.yellotasker.com/api/v1/getUserTask/'+global.userId+'?action=postedTask');
	 const json = await response.json();
    console.log(json.data);
    var jData = json.data.postedTask[0].posted_task;
    this.setState({saveTaskTotal : jData.length})
    console.log(jData);
	this.setState(state =>({data :[ ...state.data,...jData]}));
    //this.setState({data : [ ...state.data,...json.data]});
    
//	this.setState({data :json.data});
	 this.setState({ visible: false });	
	}
    

    onDeletePress(){
        console.log();
        Alert.alert(
          'Confirmation',
          'Are sure want to delete offer?',
          [
            {text: 'Yes', onPress: () => this.deleteConfirm()},
            {text: 'No', onPress: () => console.log('cancel press!!!!!!')},
          ]
      );
    }
    deleteConfirm = async(taskId) =>{
        if(global.userId){
            console.log('call delete function'+taskId);
           this.setState({visible: true});
           try
           { 
           // this.setState({visible: true}); http://api.yellotasker.com/api/v1/postTask/delete/
             let response = await fetch('http://api.yellotasker.com/api/v1/deleteOffer/'+this.state.offerId, 
               { 
                 method: 'GET',
                     headers: 
                       { 
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                         
                         }
                    }); 
                    let res  = await response.json();
                    console.log(res);
                    console.log(res.code);
                    this.setState({visible: false});
                    if(res.code >=200){
                          // this.getTaskList();
                          const { navigate } = this.props.navigation;
                          navigate('MyTask')
                      }else{ 
                       let error = await response;
                       throw error;
                       } 
                     } catch (error) { 
             //this.setState({error: error});
            
             console.log(JSON.stringify(error));
            // console.log(JSON.parse(error));
            }
        }else{
    
        }
          
      }
      assignTaskFun(data){
        const { params } = this.props.navigation.state;
        var taskID =params.id;
        console.log(data);
        var taskDoerID = data.id;
        this.assignTask(taskID,taskDoerID);
    }
    onChangePayMethod(data){
        const { params } = this.props.navigation.state;
        var taskID =params.id;
        console.log(data);
        var taskDoerID = data.id;
        this.assignTask(taskID,taskDoerID);
    }
    onViewOfferdUser(data){
      console.log(data);
      this.setState({clickViewDetails : data.interestedUserId});
      console.log(this.state.clickViewDetails);
    }

      async assignTask(taskID,taskDoerID){
        if(global.userId){
           this.setState({visible: true});
           // console.log(taskId);
           try
           { 
           // this.setState({visible: true});
             let response = await fetch('http://api.yellotasker.com/api/v1/assignTask', 
               { 
                 method: 'POST',
                     headers: 
                       { 
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                         
                         },
                         body: JSON.stringify({
                            "taskOwnerID" : global.userId,
                            "taskDoerID" : taskDoerID,
                            "taskId" : taskID,
                            "status" : "assigned"
                         })
                    }); 
                    let res  = await response.text();
                    console.log(response);
                    res =  JSON.parse(res);
                   console.log(res);
                    this.setState({visible: false});
                    if(res.code ==200){
                        console.log('if');
                          // this.getTaskList();
                          global.setTimeout(() => {
                          Alert.alert(
                            'Success',
                            'Task Assign Successfully!',
                            [
                              {text: 'Ok', onPress: () => this.backMyTask()}
                             
                            ]
                        );
                      }, 50);
                         
                      }else if(res.code==500){
                        console.log('else if');
                        global.setTimeout(() => {
                        Alert.alert(
                            'Warning',
                            'Task already assigned !',
                            [
                              {text: 'Ok', onPress: () => console.log('cancel press!!!!!!')}
                             
                            ]
                        );
                      }, 50);
                      }else{ 
                       let error = res;
                       throw error;
                       } 
                     } catch (error) { 
             //this.setState({error: error});
            
             console.log(JSON.stringify(error));
            // console.log(JSON.parse(error));
            }
        }else{
    console.log('erorororororo');
        }
          
      }
      backMyTask(){
        const { navigate } = this.props.navigation;
        navigate('MyTask')
      }
 readMoreCount(description){
     return description.length;
 }
 readMore(description){
	 if(description.length>24){
		 return description.substring(0,22)+'...';
	 }else{
		 return description;
	 }

 }
 DateChangeFormate(duedate){
	 if(duedate){
  var date = duedate;
  console.log(duedate)
//		var date = "2017-12-04";
    //var arr1 = date.split(' ');
   var nDate = date.split(' ');
	  var arr2 = nDate[0].split('-');
	  var month;
	  if(arr2[1]=='01'){
		  month = 'Jan';
	  }else if(arr2[1]=='02'){
		month = 'Feb';
	}else if(arr2[1]=='03'){
		month = 'March';
	}else if(arr2[1]=='04'){
		month = 'April';
	}else if(arr2[1]=='05'){
		month = 'May';
	}else if(arr2[1]=='06'){
		month = 'June';
	}else if(arr2[1]=='07'){
		month = 'July';
	}else if(arr2[1]=='08'){
		month = 'Aug';
	}else if(arr2[1]=='09'){
		month = 'Sep';
	}else if(arr2[1]=='10'){
		month = 'Oct';
	}else if(arr2[1]=='11'){
		month = 'Nov';
	}else{
		month = 'Dec';
	} 
	  var dt = month +' '+arr2[2]+','+arr2[0];
	  return dt;
}
	 //alert(dt);
	  
  }	
  indianDateFormat(dueDate){
    var arr = dueDate.split('-');
    var formattedDate = arr[2]+'-'+arr[1]+'-'+arr[0];
    return formattedDate;
  }	

  readMoreTitle(description){
    if(description.length>32){
        return description.substring(0,30)+'...';
    }else{
        return description;
    }

}
readMoreClick(id,status){
	if(status!='expired'){
		this.setState({makeOffer:true});
		console.log(status,id);
		this.fetchTaskDetail(id);
		this.getTaskOffer(id);
		this.fetchTaskComment(id);
	}else{
		console.log('task is expired');
	}


}
fetchTaskDetail(taskId){
	// fetch('http://api.yellotasker.com/api/v1/getPostTask?taskId='+params.id)
	 fetch('http://api.yellotasker.com/api/v1/getPostTask?taskId='+taskId)
 .then((response) => response.json())
 .then((responseJson) =>{
   console.log(responseJson.data);
	this.setState({
	 taskData:responseJson.data,
 })
 console.log(this.state.taskData);
 this.setState({singleTask : responseJson.data[0].status});
 this.setState({updatedlocationType : responseJson.data[0].locationType,})
 this.setState({updateTaskTitle : responseJson.data[0].title})
 this.setState({updatedTaskDescription : responseJson.data[0].description})
 this.setState({date_in : responseJson.data[0].dueDate})
 this.setState({updatedTaskAmount : responseJson.data[0].totalAmount})
 this.setState({updatedTaskBudget : responseJson.data[0].budgetType})
 this.setState({updatedTaskaddress : responseJson.data[0].address})
 
 
 global.singleTask = responseJson.data[0].status;
 global.updatedlocationType = responseJson.data[0].locationType;
 var useName = responseJson.data[0].user_detail.first_name+' '+responseJson.data[0].user_detail.last_name;
        /* updateTaskTitle : '',
        updatedTaskDescription : '',
        updatedTaskAmount : '',
        date_in : '',*/
 this.setState({postedOn: responseJson.data[0].created_at})
 this.setState({taskPosterUserName : useName});
 if(responseJson.data[0].user_detail.profile_image){
 this.setState({userProfileImage:responseJson.data[0].user_detail.profile_image});
 }
 
 })
 .catch((error) =>{
    // alert('fetchTaskDetailerror'+error);
    console.log('Erororororororororororororororrorrororororroro');
 });
 }
 getTaskOffer(taskId){
	 
			 fetch('http://api.yellotasker.com/api/v1/taskOffer/'+taskId)
			 .then((response) => response.json())
			 .then((responseJson) =>{
				 console.log('------------------------------------------');
                 console.log(responseJson.data);
                 this.setState({taskPosted_details:responseJson.data})
						  var offerResult = responseJson.data[0];
						 // offerResult = offerResult[0].offer_details;
						  console.log(offerResult);
						  //console.log('offer list');
						// console.log('offer Length'+offerResult[0].interested_users.lenght);
						  if(responseJson.data.length>0){
							  console.log('if..............');
							  console.log(offerResult);
						 this.setState({
							 taskOffers:offerResult.offer_details
                         })
                         //interestedUserId
                         for(var i=0; i < offerResult.offer_details.length;i++){
                             if(offerResult.offer_details[i].interestedUserId==global.userId){
                                 console.log('if Statement run');
                                 console.log(offerResult.offer_details[i]);
                                 var interested_user = offerResult.offer_details[i].interested_user[0];
                                 var offerUserName = interested_user.first_name+' '+ interested_user.last_name;
                                 var offerComment = offerResult.offer_details[i].comment;
                                 var offerPrice = offerResult.offer_details[i].offerPrice;
                                 var offerCompletionDate = offerResult.offer_details[i].completionDate;
                                 var offerId = offerResult.offer_details[i].id;
                                 console.log(offerUserName,offerComment,offerPrice,offerCompletionDate,offerId);
                                 this.setState({offerUserName : offerUserName,offerComment : offerComment,offerPrice:offerPrice,updatedOfferPrice:offerPrice,updatedOfferComment : offerComment,offerCompletionDate:offerCompletionDate,offerId:offerId})
                                 
                             }else{
                                console.log('else Statement run');
                                 
                             }
                         }
						 this.setState({taskOfferLength : offerResult.offer_details.length})
						 console.log(this.state.taskOfferLength);
                         console.log(this.state.taskOffers);

                            if(responseJson.data[0].seeker_user_detail){
                                  var seeker_user_detail = responseJson.data[0].seeker_user_detail; 
 
                                this.setState({seekerName : seeker_user_detail.first_name+' '+ seeker_user_detail.last_name});
                                if(seeker_user_detail.profile_image){
                                var seekerProfileImage = seeker_user_detail.profile_image;
                                }else{
                                    var seekerProfileImage = "http://yellotasker.com/assets/img/poster-big.png";
                                }
                                this.setState({seekerProfileImage : seekerProfileImage});    
                            }
 
					 }else{
						
						 console.log('else+++++offerResult.length'+offerResult.length);
					 }
					   // alert(JSON.stringify(this.state.taskOffer));
						 
						  this.setState({visible: false}); 
			 })
			 .catch((error) =>{
			 // alert('fetchTaskDetailerror'+error);
			 });
   }
   fetchTaskComment(taskId){
	 
	// this.setState({visible: true}); 
 fetch('http://api.yellotasker.com/api/v1/comment/post?getCommentBy=task&taskId='+taskId)
 .then((response) => response.json())
 .then((responseJson) =>{
   // alert(JSON.stringify(responseJson));
	 data = responseJson.data; // here we have all products data
	
   
 // var stubComment = 
 // let commentLoop = this.state.commentLoop;
 let  commentLoop = this.prepareCommentLoop(data);
 console.log(JSON.stringify(data));
 console.log(data.length);
 if(data.length>0){
  data = data.reverse();
     console.log('if------------------');
 this.setState({
	 dataSource: this.state.dataSource.cloneWithRows(data)
 })
 this.setState({totalComment : data.length})
 console.log(data);
 console.log(this.state.dataSource);
 }else{
     console.log('else***********************');
     console.log(data);
     this.setState({
      dataSource: this.state.dataSource.cloneWithRows(data)
    })
    this.setState({totalComment : data.length})
 }
 this.setState({visible: false}); 
 })
 .catch((error) =>{
	// alert('fetchTaskCommenterror'+error);
 });
 }
 prepareCommentLoop(resp){
	 var commonIndexes = [];
	 for(var i=0; i < resp.length;i++){
	   for(var j=0;j < resp.length;j++){
		 if(resp[i].commentId == resp[j].id){
		   if(!resp[j].comments) resp[j].comments = [];
		   resp[j].comments.push(resp[i]);
		   commonIndexes.push(resp[i]);
		   //resp.splice(j,1);
		 }else{
		   if(resp[j].comments){
			   this.prepareCommentLoop(resp[j].comments);
			 }
		 }
	   }
	 }
	 for(var i=0;i<commonIndexes.length;i++){
	 for(var j=0 ; j<resp.length; j++){
	   if(resp[j].id == commonIndexes[i].id){
		 resp.splice(j,1);
	   }
	 }
   }
 }
   resPress = (text)=>{
	console.log(JSON.stringify(text));
	 let idd = JSON.stringify(text);
 this.setState({replyId: idd});
 
 }

 _deleteInvoice = (section) => {
    console.log(section);
}
 _renderHender(headerData){
     return (<View>
     <Animatable.View duration={400} style={{ flexDirection:'row',
    backgroundColor: '#efeb10',
    padding: 10,
   marginTop : 10,
   alignItems:'center',}} transition="backgroundColor">
     
       <Text style={styles.headerText}>Comment</Text>
     </Animatable.View></View>)
 }
 _content(section, i, isActive){
    
     return(
        
        <Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
        <View style={{flex:1,flexDirection:'column',}} >
        <Text style={{color:'#333',paddingVertical:5,fontSize : 14,}}>{section.description}</Text>  
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                   
                   <Text style={{flex:0.5,alignItems:'flex-start',}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',}}>Due Date</Text> 
                    </Text>
                    <Text style={{flex:0.5,}}>{this.DateChangeFormate(section.dueDate)}</Text>
            
        </View>
  
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Budget Type</Text>
                            <Text style={{flex:0.5,}}>
                            {section.budgetType}
                            </Text>
        </View>
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Total Amount</Text>
                            <Text style={{flex:0.5,}}>
                            {section.totalAmount} MYR
                            </Text>
                            </View>
                            <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                            <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}> 
      {/* <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center'}}>{Icons.locationArrow} </FontAwesome>
   */} Location  
		  </Text>
                            <Text style={{flex:0.5,}}>
                            <Text>{section.address ==null ||section.address =='' ? section.locationType : section.address}</Text>
                            </Text>
                            </View>
      </View>
      </Animatable.View>
     
     )
 }
 _content2(section, i, isActive){
   //console.log(this.state);
   console.log(global.singleTask);
     return(
           
        <Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
           <View style={{ paddingVertical: 10, paddingHorizontal: 10,borderRadius: 4, }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>COMMENTS ABOUT THIS TASK</Text>
                            <Text style={{ paddingVertical: 10, fontSize: 12, fontWeight: 'normal', color: '#333', }}>Comment below for more details and remember that for your safety, not to share personal information e.g. email and phone numbers.</Text>
                        
                        </View>
                       {global.singleTask=='open'&& <View style={styles.mainBrowses}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Ask a question"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}

       ></TextInput>
     
     <Text style={{textAlign:'right',color:'#000000'}} >Send</Text>
                       </View>
                       </View> }
                       <View style={styles.mainBrowsess}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                   <Text style={{paddingVertical:4,}}>I am in parramatta would there in 10 minuets Offer includes 15% Airtasker fees and a man with van</Text>
     <View stylee={{flexDirection:'row',flex:1,paddingVertical:4,}}>
     <Text style={{fontSize: 14,color:'#337ab7',paddingVertical:4,}} >1 min ago</Text>
 
     </View>
                       </View>
                       </View>
                 
                                          </Animatable.View>
     )
 }
 
 _renderHeader22(section, i, isActive) {
    // console.log(section);
     return (
       <Animatable.View duration={400} style={[styles.header]} transition="backgroundColor">
       <Image style={{width: 30, height: 30}} source={{
         uri:section.category_group_image}} />
         <Text style={styles.headerText}>text</Text>
       </Animatable.View>
     );
   }
 
   _renderContent22(section, i, isActive) {
     return (
       <Animatable.View duration={400}  style={[styles.content]} transition="backgroundColor">
        
         
         <Text>asdfffsfsdf</Text>
 
       </Animatable.View>
     );
   }
   _setSection5(section) {
    this.setState({ activeSection5: section });
  }
  _header(section, i, isActive){
  //  console.log(section);
    return(<View>
        <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#eee',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,flex:1,}} transition="backgroundColor">
          <Text style={{color:'#484848',fontWeight:'600',paddingVertical:10,fontSize:16,flex:0.9}}>{section.interested_user[0].first_name} {section.interested_user[0].last_name}</Text>
          <Text style={{flex:0.1,textAlign:'right',paddingVertical:10,}}><FontAwesome style={{fontSize: 18,color:'#484848',}} >{Icons.angleDown}</FontAwesome></Text>
         
          </Animatable.View></View>)
  }
 
 _content4(section, i, isActive){
    var _this =this;
      return(
        <View key={i}>
                          <View style={{flexDirection: 'column',	borderColor: '#ccc',borderTopWidth: 1,	paddingHorizontal:'1%',paddingVertical:'3%',}}>
                        
                        <View style={{flex:1,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,borderColor:'#ccc',borderWidth:1,borderRadius:4}}>
                        <View style={{flexDirection:'row',marginTop:6}}>
                      <View style={{flex:0.3,flexDirection:'column'}}>
                      {section.interested_user[0].profile_image!=null &&  <Image source={{uri : section.interested_user[0].profile_image}} style={styles.menImage}/>}
                     
                     {section.interested_user[0].profile_image==null &&  <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage}/>}
                      <Text style={{marginTop:5,color:'#337ab7',marginLeft:7}}> {section.interested_user[0].first_name} {section.interested_user[0].last_name}</Text>
                      <View style={{flexDirection:'row',marginTop:6,marginLeft:7}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                     </View>
                     </View>
                     <View style={{flex:0.2,paddingVertical:4}}>
                     <TouchableHighlight>
                          <Text style={styles.PercentBtn }>
                         80%
                          </Text>
                          </TouchableHighlight>
                      </View>
                      <View style={{flex:0.5,}}>
                      <Text style={{marginTop:5,textAlign:'center',color:'#337ab7'}} onPress={()=>this.onViewOfferdUser(section)}> View Details</Text>
                      </View>
                      
                       </View>
                        </View>  
                      {this.state.clickViewDetails==section.interestedUserId && <View style={{flex:1,flexDirection:'column',borderColor:'#ccc',borderWidth:1,borderRadius:4,marginVertical:10}}>
                             <View style={{ backgroundColor:'#ccc',paddingVertical:10,flex:1}}>
                                   <Text style={{textAlign:'center',color:'#fff',fontWeight:'bold'}}>Offer Details</Text>
                             </View>
                             <View style={{ paddingVertical:10,flex:1,flexDirection:'column',paddingHorizontal:10}}>
                                   
                                   <View style={{paddingVertical:2,flexDirection:'row'}}>
                                         <Text style={{fontWeight:'bold'}}> offer price</Text>
                                         <Text> : {section.offerPrice} MYR</Text>
                                   </View>
                                   <View style={{paddingVertical:2,flexDirection:'row'}}>
                                         <Text style={{fontWeight:'bold'}}> offer comment</Text>
                                         <Text> : {section.comment}</Text>
                                   </View>
                                   <View style={{paddingVertical:2,flexDirection:'row'}}>
                                         <Text style={{fontWeight:'bold'}}> completion date</Text>
                                         <Text> : {section.completionDate}</Text>
                                   </View>

                             </View>
                        </View>  }                  
                    </View>
                    </View>
      
      )
  }
 _descriptionContent(){
     return(
        <FlatList 
        data={this.state.taskData}
        keyExtractor={(x,i)=>i}
        renderItem={({item})=>
        <View style={{flex:1,flexDirection:'column',paddingHorizontal:'1%',paddingVertical:'3%',borderBottomWidth:1}}>
        <Text style={{color:'#333',paddingVertical:5,fontSize : 12,}}>{item.description}</Text>  
        <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                   
                   <Text style={{flex:0.5,alignItems:'flex-start',}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',fontSize : 12}}>Due Date</Text> 
                    </Text>
                    <Text style={{flex:0.5,}}>{this.indianDateFormat(item.dueDate)}</Text>
            
        </View>
        </View>
}
/>
     )
 }
 _feedBackContent(){
     return(
        <Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
        <View style={{flex:1,flexDirection:'column',}} >
              <Text>Thanks for choosing this task lorem ipsum dolor sit amet is the dummy text for describing.</Text>
        </View>
        <View style={{flexDirection:'row',paddingVertical:10}}>
			<Text style={{paddingVertical:3,color:'#000',alignItems:'flex-start',fontWeight:'400'}}>Rating</Text>
		  <View style={{flexDirection:'row',alignItems:'center',paddingLeft:15,}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
		  </View>
		  </View>
          <Text style={{flexDirection:'row',color:'#000',paddingVertical:10,fontWeight:'400'}}> Review</Text>
          <TextInput
       placeholder=""  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}

       ></TextInput>
       <Text style={styles.followBtn}> Submit</Text>
        </Animatable.View>
     )
 }
_msgContent(){
    return(
        <Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
           <View style={{ paddingVertical: 10, paddingHorizontal: 8,borderRadius: 4, }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>5 MESSAGES FROM DOER</Text>
                            <Text style={{ paddingVertical: 10, fontSize: 12, fontWeight: 'normal', color: '#333', }}>Comment below for more details and remember that for your safety, not to share personal information e.g. email and phone numbers.</Text>
                        
                        </View>
                        <View style={styles.mainBrowsess}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Ask a question"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}

       ></TextInput>
     
     <Text style={{textAlign:'right',color:'#000000'}} >Send</Text>
                       </View>
                       </View>
                       <View style={styles.mainBrowsess}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.menImage} />
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                   <Text style={{paddingVertical:4,}}>I am in parramatta would there in 10 minuets Offer includes 15% Airtasker fees and a man with van</Text>
     <View stylee={{flexDirection:'row',flex:1,paddingVertical:4,}}>
     <Text style={{fontSize: 14,color:'#337ab7',paddingVertical:4,}} >1 min ago</Text>
 
     </View>
                       </View>
                       </View>

                                          </Animatable.View>
     )
}
 
 _setSection(section) {
     console.log(section);
    this.setState({ activeSection: section });
  }
  _setSection1(section) {
    this.setState({ activeSection1: section });
  }
  _setSection2(section) {
    this.setState({ activeSection2: section });
  }
  _setSection3(section) {
    this.setState({ activeSection3: section });
  }
  _setSection4(section) {
    this.setState({ activeSection4: section });
  }
  checkIsload(){
      var isLoad = this.state.isLoad;
      var isNoLoad = this.state.isNoLoad
      var taskOfferLength = this.state.taskOfferLength;
      console.log('isLoad'+isLoad);
      console.log('taskofferLenght'+taskOfferLength);
      if(taskOfferLength>0){
        this.setState({isNoLoad : false});
          if(isLoad){
                this.setState({isLoad : false});
          }else{
            this.setState({isLoad : true});
                
          }
      }else{
       // this.setState({isNoLoad : true});
       if(isNoLoad){
        this.setState({isNoLoad : false});
       }else{
        this.setState({isNoLoad : true});
       }
      }
  }
  checkIssload(){

  }
  onBrowsePressed(){
	const { navigate } = this.props.navigation;
	navigate('Browse'); 
 }
 onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
   const { navigate } = this.props.navigation;
   global.category_id = null;
   navigate('Mainview');
  }
  onMyTaskPressed(){
	let userId = global.userId;
   // alert('userId'+userId);
	const { navigate } = this.props.navigation;
	if(userId){
	
   navigate('MyTask'); 
   }else{
	Alert.alert(
	   'Message',
	   'Please Login to go to My task',
	   [
		 {text : 'OK', onPress:()=>{this.onLoginPressed()}}
	   ]
	  );
   }
  }
  onMyProfilePressed(){
    let userId = global.userId;
    // alert('userId'+userId);
   const { navigate } = this.props.navigation;
   if(userId){
   
    navigate('Profile'); 
    }else{
   Alert.alert(
      'Message',
      'Please Login to go to Profile',
      [
      {text : 'OK', onPress:()=>{this.onLoginPressed()}}
      ]
     );
    }
  }
  async postCommentFun(){
    var commentDescription = this.state.commentDescription;
    if(commentDescription){
      let userId = global.userId;
      const { params } = this.props.navigation.state;
      var taskID =params.id;
      try {
        this.setState({visible: true});
        let response  = await fetch('http://api.yellotasker.com/api/v1/comment/post', {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
             body: JSON.stringify({     
                userId:userId,
                taskId:taskID,
                commentDescription:commentDescription
       
           })
         });
          
         let res  = await response.text();
         this.setState({visible: false});
      //   console.log(response.status);
         if(response.status >=200){
           let formErrorsss = JSON.parse(res);
            this.setState({visible: false});
          //  console.log("res token: " + res);
           if(formErrorsss.code==500){
           }
           if(formErrorsss.status==1){
            this.setState({commentDescription : null});
           // this.setState({replyId : null}); fetchTaskComment
           this.fetchTaskComment(taskID);
           }
         }else{
             let errors = res;
             throw errors;
             //alert(errors);
         }
      } catch (errors) {
            // alert("catch errors " + errors);
      }
   
    }else{
      Alert.alert(
           'Warning',
           'Comment Description must be required!',
           [
             {text : 'OK'}
           ]
      );
    }
  }
 
  async postMessage(){
    var msgPosted = this.state.msgPosted;
    if(msgPosted){
      this.setState({msgPosted : null});
      let userId = global.userId;
      const { params } = this.props.navigation.state;
      var taskID =params.id;
      console.log(taskID);
      try {
        this.setState({visible: true});
        let response  = await fetch('http://api.yellotasker.com/api/v1/addPersonalMessage', {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
             body: JSON.stringify({     
                userId:userId,
                taskId:taskID,
                comments:msgPosted
       
           })
         });
          
         let res  = await response.text();
         this.setState({visible: false});
         console.log(res);
         if(response.status >=200){
           let formErrorsss = JSON.parse(res);
            this.setState({visible: false});
          //  console.log("res token: " + res);
           if(formErrorsss.code==500){
           }
           if(formErrorsss.status==1){
           this.setState({msgPosted:''})
            this.fetchPostMsg(taskID);
           // this.setState({replyId : null});
           }
         }else{
             let errors = res;
             throw errors;
             //alert(errors);
         }
      } catch (errors) {
            // alert("catch errors " + errors);
      }
   
    }else{
      Alert.alert(
           'Warning',
           'Comment Msg must be required!',
           [
             {text : 'OK'}
           ]
      );
    }
  }
  async postReplyOnComment(){
    let userId = global.userId;
    const { params } = this.props.navigation.state;
    let taskId = params.id;
    let commentDescription = this.state.replycommentDescription;
    commentDescription = commentDescription.trim();
    let commentId = this.state.replyId;
    if(commentDescription){
        this.setState({visible:true});
    try {
       // this.setState({visible: true});  replycommentDescription
 
        let response  = await fetch('http://api.yellotasker.com/api/v1/comment/post?commentReply=yes', {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
          body: JSON.stringify({
            
        userId:userId,
        taskId:taskId,
        commentDescription:commentDescription,
        commentId:commentId
       
           })
         });
          
         let res  = await response.text();
         this.setState({visible:false});
      console.log(response.status);
         if(response.status >=200){
           let formErrorsss = JSON.parse(res);
            this.setState({visible: false});
          console.log("res token: " + formErrorsss);
           if(formErrorsss.code==500){
           
           }
           if(formErrorsss.status==1){
            this.setState({replyId : null});
            this.setState({replycommentDescription : null});
            
            const { navigate } = this.props.navigation;
           // navigate('SingleTask',{id:taskId});
           this.fetchTaskComment(taskId);
          
            //alert(result.data.id);
           }
         }else{
             let errors = res;
             throw errors;
             //alert(errors);
         }
      } catch (errors) {
            // alert("catch errors " + errors);
      }
    }else{
        Alert.alert(
            'Warning',
            'Comment field must be required',
            [
            {text : 'OK', onPress:()=>{this.onLoginPressed()}}
            ]
           );
    }
  }
  fetchPostMsg(taskId){
	 console.log(taskId);
    // this.setState({visible: true}); 
   fetch('http://api.yellotasker.com/api/v1/getPersonalMessage?taskId='+taskId)
   .then((response) => response.json())
   .then((responseJson) =>{
     console.log(JSON.stringify(responseJson));
   var data = responseJson.data; // here we have all products data
   //  data = data.reverse();
     
 
  // console.log(JSON.stringify(data));
   console.log(data.length);
   if(data.length>0){
       console.log('if-----------sfgdssdfsdfs-------');
   this.setState({
    dataSources: this.state.dataSources.cloneWithRows(data)
   })
   console.log(data);
   console.log(this.state.dataSources);
   this.setState({totalMsg : data.length})
   }else{
       console.log('else*******sdfdfdsfdsfdsfdfdsf****************');
   }
   this.setState({visible: false}); 
   })
   .catch((error) =>{
    alert('fetchTaskCommenterror'+error);
   });
   }
   deleteCommentPress(commentId){
      console.log(commentId);
      Alert.alert(
        'Confirmation',
        'Are sure want to delete comment?',
        [
          {text: 'Yes', onPress: () => this.deleteComment(commentId)},
          {text: 'No', onPress: () => console.log('cancel press!!!!!!')},
        ]
    );
  
   }
   async deleteComment(commetId){
    let userId = global.userId;
    const { params } = this.props.navigation.state;
    let taskId = params.id;
    try {
      this.setState({visible: true});
       let response  = await fetch('http://api.yellotasker.com/api/v1/comment/delete', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
         body: JSON.stringify({ 
          id:commetId,
          taskId:taskId,
          userId:userId
          })
        });
        let res  = await response.text();
        this.setState({visible:false});
        if(response.status >=200){
          let formErrorsss = JSON.parse(res);
           this.setState({visible: false});
          if(formErrorsss.code==500){
          }
          if(formErrorsss.status==1){
          this.fetchTaskComment(taskId);
          }
        }else{
            let errors = res;
            throw errors;
        }
     } catch (errors) {
           // alert("catch errors " + errors);
     }
   }
   onPressUpdateTask(){
    let userId = global.userId;
    const { params } = this.props.navigation.state;
    let taskId = params.id;
    var offerPrice= this.state.updatedOfferPrice;
    var offerId = this.state.offerId;
    console.log(offerId,userId);
    var offerComment = this.state.updatedOfferComment;
    if(offerPrice==''){
      this.setState({isErrorView:true,errorMsg:'Offer Price must be required!'})
    }else {
      this.setState({isErrorView:false,errorMsg:''})
      console.log('else no validation')
    this.updateTaskFun(offerId,offerComment,offerPrice,taskId);
    } 
  }
   async updateTaskFun(offerId,offerComment,offerPrice,taskId){

try {
  this.setState({visible: true});
   let response  = await fetch('http://api.yellotasker.com/api/v1/updateOffer/'+offerId, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
     body: JSON.stringify({ 
        comment:offerComment,
        offerPrice : offerPrice
        
    
      })
    });
    let res  = await response.text();
    this.setState({visible:false});
    console.log(res);
    if(response.status >=200){
      let formErrorsss = JSON.parse(res);
       this.setState({visible: false});
      if(formErrorsss.code==500){
      }
      if(formErrorsss.status==1){
     this.getTaskOffer(taskId);
     this.setState({isOfferEdit:!this.state.isOfferEdit})
      }
    }else{
        let errors = res;
        throw errors;
    }
 } catch (errors) {
      console.log(JSON.stringify(errors));
 }


   }
	render(){
        var self = this;
		//const { params } = self.props.navigation.state;
		 const { navigate } = self.props.navigation;
		return(
				
		<View style={styles.mainviewStyle}>
      
		
   <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <View style={{flexDirection:'row',paddingVertical:10,backgroundColor:'#000000'}}>
                <View style={{flex:0.2,alignItems:'flex-start',paddingVertical:12,paddingLeft:10}}>
				<TouchableHighlight onPress={()=>navigate('MyTask')} underlayColor={'transparent'}>
                <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.arrowLeft}</FontAwesome>
                </TouchableHighlight> 
                </View>
             <View  style={{flex:0.6,alignItems:'center'}}>
             <Text >
                <Image source={require('../img/app-logo.png')}/>
                </Text>
                </View>
                <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:12,paddingRight:10}}>
                <TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
  
   <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
  
   </TouchableHighlight>
                </View>             
             </View>
   <ScrollView style={styles.scrollViewStyle}>
             <View style={{paddingHorizontal:'2%'}} >
             <FlatList 
              data={this.state.taskData}
              keyExtractor={(x,i)=>i}
              renderItem={({item})=>

              <View>
              <View style={{flex:1,flexDirection:'column',paddingHorizontal:'1%',paddingVertical:'3%',borderBottomWidth:1}}>
                <View style={{flexDirection:'row'}}>
                       <View style={{flex:0.6}}></View>
                       <View style={{flex:0.3,flexDirection:'row'}}>
                       <Text style={styles.openBtn}>{item.status}</Text>
                       {item.status == 'open' && 
                       <Text style={{paddingHorizontal:15}}  onPress={this.onDeletePress.bind(this,item.id)}> <FontAwesome style={{fontSize: 18,color:'#34495e',flexDirection:'row',paddingTop:5}}>{Icons.trash}</FontAwesome></Text>
                       }
                        {item.status == 'open' && 
                       <Text onPress={()=>this.setState({isEdit:true})}> <FontAwesome style={{fontSize: 18,color:'#34495e',flexDirection:'row',paddingTop:3}} >{Icons.pencil}</FontAwesome></Text>
                      }
                       </View>
                </View>
                 <Text style={{color:'#34495e',fontSize:14,paddingVertical:8,fontWeight:'600'}}>{item.title}
                  </Text>
              </View>
             
              </View>
      }
      />


         <View style={{paddingVertical:8}}>
     
        <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,flex:1,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16,flex:0.9}} onPress={()=>this.setState({isDescriptionShow:!this.state.isDescriptionShow})}>Description</Text>
              <Text style={{flex:0.1,textAlign:'right',paddingVertical:10,}} onPress={()=>this.setState({isDescriptionShow:!this.state.isDescriptionShow})}><FontAwesome style={{fontSize: 18,color:'#fff',}} >{Icons.angleDown}</FontAwesome></Text>
            </Animatable.View>

            {this.state.isDescriptionShow && <View>
              {!this.state.isEdit&&<View>
              <FlatList 
        data={this.state.taskData}
        keyExtractor={(x,i)=>i}
        renderItem={({item})=>
                       
        <Animatable.View duration={400}  style={{ padding: 20,
          backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
      <View style={{flex:1,flexDirection:'column',}} >
      <Text style={{color:'#333',paddingVertical:5,fontSize : 14,}}>{item.description}</Text>  
      <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>  
                 <Text style={{flex:0.5,alignItems:'flex-start',}}><FontAwesome style={{fontSize: 10,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',}}>Due Date</Text> 
                  </Text>
                  <Text style={{flex:0.5,}}>{this.DateChangeFormate(item.dueDate)}</Text>
      </View>
      <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                          <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Budget Type</Text>
                          <Text style={{flex:0.5,}}>
                          {item.budgetType}
                          </Text>
      </View>
      <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                          <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}>Total Amount</Text>
                          <Text style={{flex:0.5,}}>
                          {item.totalAmount} MYR
                          </Text>
                          </View>
                          <View style={{flexDirection : 'row',flex:1,paddingVertical:5}}>
                          <Text style={{flex:0.5,alignItems:'flex-start',fontWeight: 'bold',color:'#000000',}}> 
    {/* <FontAwesome style={{fontSize: 10,color:'#000000',flexDirection:'row',alignItems:'center'}}>{Icons.locationArrow} </FontAwesome> */}
    Location 
    </Text>
                          <Text style={{flex:0.5,}}>
                          <Text>{item.address ==null ||item.address =='' ? item.locationType : item.address}</Text>
                          </Text>
                          </View>
    </View>
    </Animatable.View>
                       
                      }
                      />  
                       </View>}
                       {this.state.isEdit&&<View style={{backgroundColor: '#eee',paddingVertical:10}}>
           {this.state.isErrorView &&<View>
             <Text style={{color:'red',paddingVertical:8}}>{this.state.errorMsg}</Text>
            </View> } 
            <View style={{backgroundColor: '#eee',}}> 
                    <View style={styles.inputDiv}>
                        <Text style={styles.titleLabel}>Task title<Text style={styles.streck}>*</Text></Text>
                              <TextInput  placeholder="Task Title"  
                                underlineColorAndroid = "transparent" returnKeyType="next" 
                                keyboardType="default"
                                autoCapitalize="none"
                                autoCorrect={false} style={styles.InputTexts}
                                value={this.state.updateTaskTitle}
                                onChangeText={(text)=>this.setState({updateTaskTitle : text})} />
                    
                    </View>
                    <View style={styles.inputDiv}>
                        <Text style={styles.titleLabel}>Description<Text style={styles.streck}>*</Text></Text>
                              <TextInput  placeholder="Task Description"  
                                underlineColorAndroid = "transparent" returnKeyType="next" 
                                keyboardType="default"
                                autoCapitalize="none"
                                autoCorrect={false} style={styles.TextArea}
                                multiline={true}
                                numberOfLines={2}
                                value={this.state.updatedTaskDescription}
                                onChangeText={(text)=>this.setState({updatedTaskDescription : text})} />
                    
                    </View>
                    <View style={styles.inputDiv}>
                        <Text style={styles.titleLabel}>Due Date<Text style={styles.streck}>*</Text></Text>
                        <DatePicker
      style ={{width: '100%',borderRadius:5,borderColor: 'gray',}}
      date={this.state.date_in}
      mode="date"
      format="YYYY-MM-DD"
      mode="date"
      placeholder="Select Date"
      showIcon={true}
      ref={(input)=>this.dateInput=input}
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      customStyles={{
       dateInput: {
          alignItems : 'flex-start',
          padding:7,borderRadius:5,  
      },
     }}
    onDateChange={(date_in) => {this.setState({date_in: date_in});}}/>
                    
                    </View>
                    <View style={styles.inputDiv}>
                        <Text style={styles.titleLabel}>Budget Type<Text style={styles.streck}>*</Text></Text>
                              
                                        <Dropdown
              ref={this.typographyRef}
              onChangeText={this.onChangeText}
              label={this.state.budgetLabel}
              data={this.state.budgetTypeOption}
              pickerStyle={styles.textContainer}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              dropdownOffset = {{top: 42, left: 0}}
        value = {this.state.updatedTaskBudget}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',paddingHorizontal:5}}
             
            />
                    
                    </View>
                    <View style={styles.inputDiv}>
                        <Text style={styles.titleLabel}>Total Amount<Text style={styles.streck}>*</Text></Text>
                              <TextInput  placeholder="Task Amount"  
                                underlineColorAndroid = "transparent" returnKeyType="next" 
                                keyboardType="default"
                                autoCapitalize="none"
                                autoCorrect={false} style={styles.InputTexts}
                                value={`${this.state.updatedTaskAmount}`}
                                onChangeText = {(text)=>this.setState({updatedTaskAmount : text})} />
                    
                    </View>
                    <View style={styles.inputDiv}>
                        <Text style={styles.titleLabel}>Is this a physical or digital task?<Text style={styles.streck}>*</Text></Text>
                              
                                        <Dropdown
              ref={this.typographyRefLocation}
              onChangeText={this.onChangeLocation}
              label={this.state.locationLabel}
              data={this.state.locationTypeOption}
              pickerStyle={styles.textContainer}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              dropdownOffset = {{top: 42, left: 0}}
              value={this.state.updatedlocationType}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',paddingHorizontal:5}}
             
            />
                    
                    </View>
                    {global.updatedlocationType!="Digital task" &&
                    <View style={styles.inputDiv}>
                        <Text style={styles.titleLabel}>Suburb<Text style={styles.streck}>*</Text></Text>
                              <TextInput  placeholder="Suburb"  
                                underlineColorAndroid = "transparent" returnKeyType="next" 
                                keyboardType="numeric"
                                autoCapitalize="none"
                                autoCorrect={false} style={styles.InputTexts}
                                value={this.state.updatedTaskaddress}
                                onChangeText = {(text)=>this.setState({updatedTaskaddress : text})} />
                    
                    </View>
                    }

                    <View style={{flex:1,flexDirection:'row'}}>
                         <Text style={{flex:0.3,paddingVertical:15,paddingHorizontal:15,backgroundColor:'#34495e',color:'#fff',textAlign:'center',borderRadius:4,borderWidth:1,overflow:'hidden',borderColor:'#34495e'}} onPress={()=>this.onPressUpdateTask()}>Update</Text>
                         <Text style={{flex:0.3,paddingVertical:15,paddingHorizontal:15,backgroundColor:'#34495e',color:'#fff',textAlign:'center',borderRadius:4,borderWidth:1,overflow:'hidden',borderColor:'#34495e',marginLeft:4}} onPress={()=>this.setState({isEdit:!this.state.isEdit})}>Cancel</Text>
                    </View>
            </View>

            
                 
                       </View>}

            </View>}
      </View>
   
 
<View style={{paddingVertical:6}}> 
      <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,flex:1,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16,flex:0.9}} onPress={()=>this.setState({isCommentView : !this.state.isCommentView})}> {this.state.singleTask!='open'&&<Text>Comment  History</Text>}{this.state.singleTask=='open'&&<Text>Comment  </Text>} </Text>
              <Text style={{flex:0.1,textAlign:'right',paddingVertical:10,}} onPress={()=>this.setState({isCommentView : !this.state.isCommentView})}><FontAwesome style={{fontSize: 18,color:'#fff',}} >{Icons.angleDown}</FontAwesome></Text>
          
           </Animatable.View>

{this.state.isCommentView &&<Animatable.View duration={400}  style={{ padding: 20,
            backgroundColor: '#eee',borderBottomWidth:1}} transition="backgroundColor">
           <View style={{ paddingVertical: 10, paddingHorizontal: 10,borderRadius: 4, }}>
                            
                            
           {this.state.totalComment==0&&<Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>NO COMMENTS ABOUT THIS TASK</Text>}
                            {this.state.totalComment>0&&<Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000000', textAlign: 'center', }}>COMMENTS ABOUT THIS TASK</Text>}
                            
                            {this.state.totalComment>0&&<Text style={{ paddingVertical: 10, fontSize: 12, fontWeight: 'normal', color: '#333', }}>Comment below for more details and remember that for your safety, not to share personal information e.g. email and phone numbers.</Text>
                            }
                        </View>
                       {this.state.singleTask=='open'&& <View style={styles.mainBrowses}>
                       <View style={styles.browseMen}>
                           <Image source={{uri : this.state.userProfileImage}} style={styles.menImage}/>
                       </View>
                       <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Ask a question"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}
      onChangeText = {(text)=>this.setState({commentDescription : text})}
      value={this.state.commentDescription}
       ></TextInput>
     
     <Text style={{textAlign:'right',color:'#000000'}} onPress={()=>this.postCommentFun()}>Send</Text>
                       </View>
                       </View> }
                       <View style={styles.mainBrowsess}>
                       <ListView
		dataSource={this.state.dataSource}
		renderRow={(comment)=>
                       <View>
                        <View style={styles.mainsBrowse}>
                       <View style={styles.browseMen}>
                       {comment.user_detail.profile_image &&  <Image source={{uri : comment.user_detail.profile_image}} style={styles.replymensImage} />}
                       {!comment.user_detail.profile_image &&  <Image source={{uri : 'http://yellotasker.com/assets/img/task-person.png'}} style={styles.replymensImage} />}
                           
                       </View>
                       <View style = {{flex:0.8,flexDirection:'column',paddingHorizontal:10,}}>
                       {comment.user_detail && <View style={{flex:1,flexDirection:'row'}}>
                       <Text style={{color:'#337ab7',flex:0.8}}>
                           {comment.user_detail.first_name} {comment.user_detail.last_name}
                       </Text>
                      {comment.user_detail.id==global.userId && <Text style={{flex:0.2,textAlign:'right'}} onPress={()=>this.deleteCommentPress(comment.id)}> <FontAwesome style={{fontSize: 18,color:'#34495e',flexDirection:'row',paddingTop:5}}>{Icons.trash}</FontAwesome></Text>
                       }
                       </View>
                       }
                       <Text style = {styles.browseSubContent} >{comment.commentDescription}</Text> 
                       
                       <View style={{flexDirection:'row',}}>
                          <View>
                            <Text style={{fontWeight:'bold',fontSize:14,color:'#000000'}}><TimeAgo time={comment.created_at}/></Text>
                       </View>
                       <View>

                            <Text style={{fontWeight:'normal',fontSize:14,paddingLeft:25}} onPress={()=>this.resPress(comment.id)}> 
                            <FontAwesome style={{fontSize: 14,color:'#337ab7'}}>{Icons.reply}</FontAwesome>  Reply</Text>
                       </View>
                       </View>
                 </View>
                       </View>
                      
                       {comment.id==this.state.replyId && <View style={styles.mainsBrowse}>
                        <View style={styles.browseMenTwo}>
                            <Image source={{uri : global.profileImage}} style={styles.replymenImage} />
                        </View>
                        <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4,}}>
                        <TextInput
        placeholder=""  
        underlineColorAndroid = "transparent" returnKeyType="next" 
              keyboardType="email-address"
       autoCapitalize="none"
       autoCorrect={false} style={styles.TextArea}
       multiline={true}
       numberOfLines={3}
       onChangeText={(text) => this.setState({replycommentDescription : text})}      
       
        ></TextInput>
     
      <Text style={{textAlign:'right',color:'#000000'}} onPress={this.postReplyOnComment.bind(this)}>Send</Text>
                        </View>
                        </View>
}
                       
                       {comment.comments && 
                       <ChildComponent status={this.state.status} result={comment.comments}/>
                       }
                     <View>
                         <Text>
                                 
                         </Text>
                     </View>
                  
                           </View>

}
/>
                       </View>
                 
                                          </Animatable.View>
 }
</View>

      <View style={{paddingVertical:6}}> 
      <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,flex:1,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16,flex:0.9}} onPress={()=>this.checkIsload()}> {this.state.singleTask!='open'&&<Text>Offer  History</Text>}{this.state.singleTask=='open'&&<Text>Offer  </Text>} </Text>
              <Text style={{flex:0.1,textAlign:'right',paddingVertical:10,}} onPress={()=>this.checkIsload()}><FontAwesome style={{fontSize: 18,color:'#fff',}} >{Icons.angleDown}</FontAwesome></Text>
          
           </Animatable.View>
           {this.state.isNoLoad &&  <Animatable.View duration={400} style={{ flexDirection:'row',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,flex:1,}} transition="backgroundColor">
              <Text style={{color:'#484848',fontWeight:'normal',paddingVertical:10,fontSize:14,flex:0.9}} onPress={()=>this.checkIsload()}> Currently there is no offer for this task </Text>
           </Animatable.View>}
           {this.state.isLoad && <View style={{paddingVertical:6,paddingHorizontal:10}}>            
        <Accordion
           activeSection={this.state.activeSection4}
           sections={this.state.taskOffers}
           renderHeader={this._header}
          renderContent={this._content4.bind(this)}
          duration={400}
          onChange={this._setSection4.bind(this)}
          underlayColor={'transparent'}
      />
      </View>}
      </View>
    
      {this.state.singleTask!='assigned'&&<View style={{paddingVertical:6}}> 
      <Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#34495e',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,flex:1,}} transition="backgroundColor">
              <Text style={{color:'#fff',fontWeight:'600',paddingVertical:10,fontSize:16,flex:0.9}} onPress={()=>this.setState({isOfferLoad : !this.state.isOfferLoad})}>My Offer </Text>
              <Text style={{flex:0.1,textAlign:'right',paddingVertical:10,}} onPress={()=>this.setState({isOfferLoad : !this.state.isOfferLoad})}><FontAwesome style={{fontSize: 18,color:'#fff',}} >{Icons.angleDown}</FontAwesome></Text>
          
           </Animatable.View>
           {this.state.isOfferLoad &&   <View><Animatable.View duration={400} style={{ flexDirection:'row', backgroundColor:'#eee',padding: 10,
          marginTop : 10,overflow:"hidden",borderRadius: 6,flex:1,}} transition="backgroundColor">
          <Text style={{color:'#484848',fontWeight:'600',paddingVertical:10,fontSize:16,flex:0.8}}>{this.state.offerUserName}</Text>
          <Text style={{paddingVertical:10,flex:0.1,textAlign:'center'}} onPress={()=>this.setState({isOfferEdit : true})}><FontAwesome style={{fontSize: 18,color:'#484848',}} >{Icons.pencil}</FontAwesome></Text>
          <Text style={{paddingVertical:10,flex:0.1,textAlign:'center'}} onPress={()=>this.onDeletePress()}><FontAwesome style={{fontSize: 18,color:'#484848',}} >{Icons.trash}</FontAwesome></Text>
        
         
         
          </Animatable.View>
         {!this.state.isOfferEdit && <View style={{flex:1,flexDirection:'column',backgroundColor:'#fff',borderRadius:6,marginVertical:10,borderColor:'#eee',paddingVertical:10,paddingHorizontal:10,borderWidth:2}}>

              <View style={{flexDirection:'row',flex:1}}>
              <Text style={{flex:0.5}}>offer price</Text>
                  <Text style={{flex:0.5}}>{this.state.offerPrice} MYR</Text>
              </View>        
              <View style={{flexDirection:'row',flex:1}}>
              <Text style={{flex:0.5}}>offer comment</Text>
                  <Text style={{flex:0.5}}>{this.state.offerComment}</Text>
              </View>  
              <View style={{flexDirection:'row',flex:1}}>
              <Text style={{flex:0.5}}>completion Date</Text>
                  <Text style={{flex:0.5}}>{this.indianDateFormat(this.state.offerCompletionDate)}</Text>
              </View>                 
          
          </View> } 
          {this.state.isOfferEdit && <View style={{flex:1,flexDirection:'column',backgroundColor:'#eee',borderRadius:6,marginVertical:10,borderColor:'#eee',paddingVertical:10,paddingHorizontal:10,borderWidth:2}}>

          <View style={{backgroundColor: '#eee',}}> 
          {this.state.isErrorView &&<View>
             <Text style={{color:'red',paddingVertical:8}}>{this.state.errorMsg}</Text>
            </View> }
          <View style={styles.inputDiv}>
              <Text style={styles.titleLabel}>Bid price<Text style={styles.streck}>*</Text></Text>
                    <TextInput  placeholder="Bid price"  
                      underlineColorAndroid = "transparent" returnKeyType="next" 
                      keyboardType="numeric"
                      autoCapitalize="none"
                      autoCorrect={false} style={styles.InputTexts}
                      value={`${this.state.updatedOfferPrice}`}
                      onChangeText={(text)=>this.setState({updatedOfferPrice : text})} />
          
          </View>
          <View style={styles.inputDiv}>
              <Text style={styles.titleLabel}>Comment</Text>
                    <TextInput  placeholder="Comment"  
                      underlineColorAndroid = "transparent" returnKeyType="next" 
                      keyboardType="numeric"
                      autoCapitalize="none"
                      autoCorrect={false} style={styles.InputTexts}
                      value={this.state.updatedOfferComment}
                      onChangeText={(text)=>this.setState({updatedOfferComment : text})} />
          
          </View>
          <View style={{flexDirection:'row',flex:1,backgroundColor:'#fff',paddingVertical:10,paddingHorizontal:10,borderRadius:6,overflow:'hidden'}}>
            <Text style={{flex:0.5}}>completion Date</Text>
                <Text style={{flex:0.5}}>{this.indianDateFormat(this.state.offerCompletionDate)}</Text>
            </View> 
            <View style={{flex:1,flexDirection:'row',paddingVertical:10}}>
                         <Text style={{flex:0.3,paddingVertical:15,paddingHorizontal:15,backgroundColor:'#34495e',color:'#fff',textAlign:'center',borderRadius:4,borderWidth:1,overflow:'hidden',borderColor:'#34495e'}} onPress={()=>this.onPressUpdateTask()}>Update</Text>
                         <Text style={{flex:0.3,paddingVertical:15,paddingHorizontal:15,backgroundColor:'#34495e',color:'#fff',textAlign:'center',borderRadius:4,borderWidth:1,overflow:'hidden',borderColor:'#34495e',marginLeft:4}} onPress={()=>this.setState({isOfferEdit:!this.state.isOfferEdit})}>Cancel</Text>
                    </View>
            </View>        
             
                           

            </View> } 
          </View>}
         
      </View>}

{
  /* 
        <View style={[{paddingVertical:6},this.state.singleTask!='assigned'&&{marginBottom:80}]}>
                    <View style={{flexDirection:'column',borderBottomWidth : 1, borderColor : '#ccc',}}>
                        <Text style={{color:'#000',textAlign:'center',fontSize:16}}>Posted By :</Text>
                            <View style={styles.postedBy}>  
                                    <View style={styles.browseMen}>
                                        <Image source={{uri : this.state.userProfileImage}} style={styles.replymensImage} />
                                    </View>
                                    <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4}}>
                                              <View style={{flexDirection:'row',marginTop:6}}>
                                                <View style={{flex:1,flexDirection:'row'}}><Text style={{marginTop:5,textAlign:'center',color:'#337ab7',flex:0.4}}> {this.state.taskPosterUserName} </Text>
                                                      <Text style={{flexDirection:'row',marginTop:6,marginLeft:7,flex:0.5,textAlign:'center'}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                                                      </Text>
                                                </View>
                                              </View>
                                          <View style={{flexDirection:'row'}}>
                                                <Text style={{fontWeight:'bold',fontSize:14,color:'#000000',textAlign:'right',}}></Text>  
                                          </View>
                                    </View>                      
                            </View>
                    </View>
                    <View style={{flexDirection:'column',borderBottomWidth : 1, borderColor : '#ccc',paddingVertical:6}}>
                           <Text style={{color:'#000',textAlign:'center',fontSize:16}}>Posted On :</Text>
                           <View style={styles.postedBy}>
                              <Text style={{flex:1,textAlign:'center',fontSize: 16,paddingVertical:10}}><FontAwesome style={{fontSize: 16,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',}}>Posted On</Text> <Text>{this.DateChangeFormate(this.state.postedOn)}</Text>   
                              </Text>                  
                           </View>
                    </View>
        </View> */
}

    
        <View style={[{paddingVertical:6,borderBottomWidth : 1, borderColor : '#ccc',},this.state.singleTask!='assigned'&&{marginBottom:80}]}>
        <View style={{flexDirection:'row',paddingHorizontal:12}}>
            <View style={{flex:0.5,flexDirection:'column'}}>
            <Text style={{color:'#000',fontSize:16}}>Posted To :</Text>
            <View style={{flexDirection:'row',alignItems: 'center',paddingVertical:4}}>
             <Image source={{uri : this.state.userProfileImage}} style={{height:30,width:30,borderRadius:15}} /> 
                                    <Text style={{textAlign:'center',color:'#337ab7',}}>  {this.state.taskPosterUserName}</Text>
             </View>
             <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                                                        </View>                
            </View>
            <View style={{flex:0.5,flexDirection:'column',
        alignItems: 'flex-end',paddingRight:5}}>
            <Text style={{color:'#000',fontSize:16}}>Posted On :</Text>
            <Text>{this.DateChangeFormate(this.state.postedOn)}</Text>  
            </View>
        </View>
                    
      </View> 
       
        {/* {this.state.singleTask=='assigned'&& <View style={{paddingVertical:6,marginBottom:80}}>
        <View style={{flexDirection:'column',borderBottomWidth : 1, borderColor : '#ccc',}}>
                   <Text style={{color:'#000',textAlign:'center',fontSize:16}}>Assigned To :</Text>
                          <View style={styles.postedBy}>
                          
                                <View style={styles.browseMen}>
                                    <Image source={{uri : this.state.seekerProfileImage}} style={styles.replymensImage} />
                                </View>
                                <View style={{flex:0.8,flexDirection:'column',paddingHorizontal:10,paddingVertical:4}}>
                                          <View style={{flexDirection:'row',marginTop:6}}>
                                              <View style={{flex:0.7,flexDirection:'row'}}><Text style={{marginTop:5,textAlign:'center',color:'#337ab7'}}>{this.state.seekerName}</Text>
                                                          <View style={{flexDirection:'row',marginTop:6,marginLeft:7}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                                                        </View>
                                              </View>
                                      
                                          </View>
                              
                                  <View style={{flexDirection:'row'}}>
                                      <Text style={{fontWeight:'bold',fontSize:14,color:'#000000',textAlign:'right',}}></Text>  
                                  </View>
                            
                                </View>                      
                         </View>
        </View>
                    <View style={{flexDirection:'column',borderBottomWidth : 1, borderColor : '#ccc',}}>
                          <Text style={{color:'#000',textAlign:'center',fontSize:16}}>Assigned On :</Text>
                          <View style={styles.postedBy}>
                                <Text style={{flex:1,textAlign:'center',fontSize: 16,paddingVertical:10}}><FontAwesome style={{fontSize: 16,color:'#000000',}}>{Icons.calendar}</FontAwesome> <Text style={{fontWeight: 'bold',color:'#000000',}}>Assigned On</Text> <Text>{this.DateChangeFormate(this.state.postedOn)}</Text>  
                                </Text>                               
                          </View>
                    </View>
      </View> } */}
      {this.state.singleTask=='assigned' && <View style={{paddingVertical:6,marginBottom:80}}>
        <View style={{flexDirection:'row',paddingHorizontal:12}}>
            <View style={{flex:0.5,flexDirection:'column'}}>
            <Text style={{color:'#000',fontSize:16}}>Assigned To :</Text>
            <View style={{flexDirection:'row',alignItems: 'center',paddingVertical:4}}>
              <Image source={{uri : this.state.seekerProfileImage}} style={{height:30,width:30,borderRadius:15}} /> 
              <Text style={{textAlign:'center',color:'#337ab7',}}>  {this.state.seekerName}</Text>
             </View>
             <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                                                        </View>                
            </View>
            <View style={{flex:0.5,flexDirection:'column',
        alignItems: 'flex-end',paddingRight:5}}>
            <Text style={{color:'#000',fontSize:16}}>Assigned On :</Text>
            <Text>{this.DateChangeFormate(this.state.postedOn)}</Text>  
            </View>
        </View>
                    
      </View> }
      
      </View>
    {/*  <View style={{ flex: 1,marginBottom:80 }}>
          <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.setState({ active: !this.state.active })}>
            <Icon name="share" />
            <Button style={{ backgroundColor: '#34A34F' }}>
              <Icon name="logo-whatsapp" />
            </Button>
            <Button style={{ backgroundColor: '#3B5998' }}>
              <Icon name="logo-facebook" />
            </Button>
            <Button disabled style={{ backgroundColor: '#DD5144' }}>
              <Icon name="mail" />
            </Button>
          </Fab>
        </View> */}
      </ScrollView>
      <View style={styles.footer}>
		<View style={styles.browseButtons}>
		  <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
		    <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
		  </TouchableHighlight>
			<Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
		</View>
	  <View style={styles.postButtons}>
        <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>	 
		     <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
			 </TouchableHighlight>
		 <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
		</View>
		<View style={styles.myTaskButtons}>
		<TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
		  <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.clipboard}</FontAwesome>
			</TouchableHighlight>
				 <Text style={[styles.footerText,{color:'#efeb10'}]} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
			 </View>
		 {/* <View style={styles.messageButtons}>
		  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>	 
			<Text style={styles.footerText}>Message</Text>
    </View> */}
    <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
		</View>    
      </View>
        )
	}
	
		
	
}
