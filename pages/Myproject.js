import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
export default class secondStep extends Component {

  static navigationOptions = {
    title: 'Yellotasker',
    headerStyle: { backgroundColor: '#2c3e50', },
  headerTitleStyle: { color: '#ffffff',alignSelf : 'center',fontWeight:'normal', fontSize: 18 },
  headerLeft: null,
  };
  constructor(){
    super();
    this.state ={
      taskLocatio:"",
      taskAddress:"",
      taskZipcode:"",
       date_in: '2017-10-01',
  date_out: '2046-10-01',
    }
  }
    onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
    
   navigate('Mainview'); 
   }else{
 navigate('Login'); 
   }
  }
  onNextStep(){
    global.taskLocatio = this.state.taskLocatio;
    global.taskAddress = this.state.taskAddress;
    global.taskZipcode = this.state.taskZipcode;
  const { navigate } = this.props.navigation;
   navigate('ThirdStep'); 
}
   
  render() {
    return(
      <View style={styles.mainviewStyle}>
        <ScrollView style = {styles.scrollViewStyle}>
         <Image source={require('./img/banner-login.jpg')} style={styles.bannerImg}/>
               <View style = {styles.formViewStyle} >
                     
                         <View style = {styles.buttonDiv}>
                <Text style={styles.extrasText}>Where would the task be completed?</Text>
             </View>
       
           <View style={styles.buttonDiv}>
          <TextInput placeholder="Sydney, NSW, 2000, Australia" onChangeText={(text)=>this.setState({taskLocatio:text})} style={styles.InputText} underlineColorAndroid = "transparent" returnKeyType="next" />
          </View>
           <View style = {styles.lablesStyle}>
            <Text style={styles.extrasText}>Address</Text>
          </View>
          <View style={styles.buttonDiv}>
            <TextInput multiline={true} onChangeText={(text)=>this.setState({taskAddress:text})} numberOfLines={2} style={styles.InputText}  underlineColorAndroid = "transparent" returnKeyType="next"></TextInput>
       </View>
        <View style = {styles.lablesStyle}>
                <Text style={styles.extrasText}>Zipcode</Text>
             </View>
             <View style={styles.buttonDiv}>
          <TextInput placeholder="" onChangeText={(text)=>this.setState({taskZipcode:text})} style={styles.InputText} underlineColorAndroid = "transparent"></TextInput>
          </View>
    <View style={styles.buttonDiv}>     
 <DatePicker
      style ={{width: '100%'}}
      date={this.state.date_in}
      mode="date"
      format="YYYY-MM-DD"
      minDate="2017-10-01"
      maxDate="2046-12-31"
      mode="date"
          placeholder="Due Date"
      showIcon={false}
      customStyles={{
       dateInput: {
          alignItems : 'flex-start',
          padding:5
      },
     }}
    onDateChange={(date_in) => {this.setState({date_in: date_in});}}/>
</View>

       <View style={styles.buttonDiv}>
       <TouchableHighlight style={styles.LoginDiv} onPress={this.onNextStep.bind(this)}>
         <Text style={styles.extraText}>Continue</Text>
      </TouchableHighlight>
       </View>

               </View>
          
        </ScrollView>

          <View style={styles.footer}>
          <View style={styles.browseButtons}>
               <Image style={styles.imagestyle} source={require('./img/search.png')} />
              <Text style={styles.footerText}>Browse</Text>
          </View>
          <View style={styles.postButtons}>
               <Image style={styles.imagestyle} source={require('./img/post.png')} onPress={this.onTaskPressed.bind(this)}/>
              <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
          </View>
         <View style={styles.myTaskButtons}>
               <Image style={styles.imagestyle} source={require('./img/task.png')} />
              <Text style={styles.footerText}>My Task</Text>
          </View>
           <View style={styles.messageButtons}>
               <Image style={styles.imagestyle} source={require('./img/message.png')} />
              <Text style={styles.footerText}>Message</Text>
          </View>
          </View>
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},
footer: {
  position: 'absolute',
  flex:0.1,
  left: 0,
  right: 0,
  bottom: -10,
  backgroundColor:'#2c3e50',
  flexDirection:'row',
  height:70,
  alignItems:'center',
},
postButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
browseButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
myTaskButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,
   
},
messageButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   
},
footerText: {
  color:'white',
  alignItems:'center',
  fontSize:14,
 

},
textStyle: {
  alignSelf: 'center',
  color: 'orange'
},
scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
},
bannerImg : {
  width:'100%',
  height : 130,
},
formViewStyle : {

  paddingVertical : '3%',
  paddingHorizontal : '3%',
},
buttonDiv : {
     paddingVertical : '1%',
    },
extrasText : {
  color : '#000000',
      fontSize: 16,
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
},
LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 10, 
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#efeb10',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
      fontWeight:'normal',
},


});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App