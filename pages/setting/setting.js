import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,BackHandler,NetInfo} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

import { Container, Content, List, ListItem,Drawer,Icon ,Tab, Tabs, ScrollableTab} from 'native-base';

import SideBar from '../sideBar';
import Account from './account';
import Skill from './skill';
export default class setting extends Component {

    static navigationOptions = {
     header: null ,
  };
  constructor(props){
    super(props);
    global.count = 0;
    this.state ={
      taskLocatio:"",
      taskAddress:"",
      taskZipcode:"",
       date_in: '',
  date_out: '2046-10-01',
  error : "",
  taskFromHome : false,
  value :"",
  tastDate : "Release payment once done",
  taskOnDate : false,
    }
  }
  componentDidMount(){
   
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
this.getUserInfo();
}
componentWillUnmount() {
  //Forgetting to remove the listener will cause pop executes multiple times
  BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
}
handleBack() {
  global.count = global.count+1;
  //alert('count'+global.count);
  if(global.count>1){  
    Alert.alert(
      'Confirmation',
      'Are sure want to quit this App',
      [
        {text: 'OK', onPress: () => BackHandler.exitApp()},
      ]
  );
  }else{
    //this.setState({touchMsg : true});
    
    global.setTimeout(() => {
      global.count = 0;
    }, 1000);
  }
    
  return true;
  
    }
    getUserInfo = async() =>{
      console.log('Call Function...'+global.userId);
     
       const response = await fetch(
      'http://api.yellotasker.com/api/v1/userDetail/'+global.userId
     );
     const json = await response.json();
     data = json.data;
     AsyncStorage.setItem('userInfo', JSON.stringify(data), () => {
     
      AsyncStorage.getItem('userInfo', (err, result) => {
         console.log(result);
        });
   
  });
    }
  handleOnFromHome(value){
    this.setState({value:value})
    this.setState({taskFromHome:true})
}
handleOnRemote(value){
  this.setState({value:value})
  this.setState({taskFromHome:false})
}
handleOnTommorrow(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:false})
  let d  = new Date();
  let n = d.getDate()+1;
  n = n <10  ? "0" + n : n;
  let mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
  let datass = d.getFullYear()+'-'+mm+'-'+n;
 // alert(datass);
 this.setState({date_in:datass});
 this.setState({taskShowDate:true})
 
}
handleOnToday(value){
  this.setState({tastDate:value});
}
handleOnWeek(value){
  this.setState({tastDate:value})
}
handleOnCertainDate(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:true})
  this.setState({taskShowDate:false})
  
}
    onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }

  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
    
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
    onMyProfilePressed(){
      let userId = global.userId;
      // alert('userId'+userId);
       const { navigate } = this.props.navigation;
       if(userId){
       
      navigate('Profile'); 
      }else{
       Alert.alert(
        'Message',
        'Please Login to go to Profile',
        [
        {text : 'OK', onPress:()=>{this.onLoginPressed()}}
        ]
       );
      }
      }
   onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
    onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onNextStep(){
 if(this.state.tastDate==""){
this.setState({error: ' Please enter required fields'});
  }else{
   // global.taskLocatio = this.state.taskLocatio;
    const { navigate } = this.props.navigation;
   navigate('Browse');
}  
}
closeDrawer = () => {
  this.drawer._root.close()
  };
  openDrawer = () => {
  this.drawer._root.open()
  };
  onBackPressed(){
    const { navigate } = this.props.navigation;
    navigate('Dashboard'); 
 }
  render() {
    return(
      <Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >
<View style={styles.mainviewStyle}>
<View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
<View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
<Text>


</Text>

</View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
<Image source={require('../img/app-logo.png')} />
</Text>
</View>
<View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
<TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>

<FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>

</TouchableHighlight>
</View>

</View>
 
       <View style = {styles.formViewStyle} >      
           
       <Tabs tabBarUnderlineStyle= {{ backgroundColor: '#efeb10',height: 2,}} renderTabBar={()=> <ScrollableTab />} tabContainerStyle={{height:50}}>
       <Tab heading="Account" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000',}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>

       <Account />
       </Tab>
       <Tab heading="Skill" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>

<Skill />
</Tab>
<Tab heading="Badges" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>

</Tab>
<Tab heading="Task Alerts Keywords" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>

</Tab>
<Tab heading="Mobile" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>

</Tab>
<Tab heading="Portfolio" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>

</Tab>
<Tab heading="Password" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>
</Tab>
       </Tabs>
     </View>
 <View style={styles.footer}>
 <View style={styles.browseButtons}>
 <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
 </View>
 <View style={styles.postButtons}>
 <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
 </View>
 <View style={styles.myTaskButtons}>
 <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
 </View>
 <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
 </View>
      </View>
      </Drawer>
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},
footer: {
  
   position: 'absolute',
  flex:0.1,
  left: 0,
  right: 0,
  bottom: -10,
  backgroundColor:'#000000',
  flexDirection:'row',
  height:70,
  alignItems:'center',
},
postButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
browseButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
myTaskButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,
   
},
messageButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   
},
footerText: {
  color:'white',
  alignItems:'center',
  fontSize:14,
 

},
textStyle: {
  alignSelf: 'center',
  color: 'orange'
},
scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
      
},
bannerImg : {
  width:'100%',
  height : 130,
},
formViewStyle : {
  flex:1,
  marginBottom:60,
  
},
buttonDiv : {
     paddingVertical : '1%',
    },
    secondButtonDiv : {
      paddingVertical : '2%',
     },
radioDiv : {
  flex:1,
  flexDirection:'row',
  paddingVertical : '1%',
},
secRadioDiv:{
  flex:1,
  flexDirection:'row',
  paddingVertical : '2%',
},
buttonsDiv : {
   paddingVertical : '2%',
   marginBottom : 70,
},
extrasText : {
  color : '#000000',
      fontSize: 16,
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
   textAlignVertical: 'top',
},

LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 10, 
    borderRadius:25,
    borderWidth: 1,
    borderColor: '#efeb10',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
      fontWeight:'normal',
},
 errorText : {
color : '#dd4b39',
paddingTop : 5,
  },
  abc :{
    color:'#d1d3d4',
      },
      radioContainer : {
          borderWidth : 2,
          borderColor : '#d1d3d4',
      },
      radioOuterDiv : {
        flex:0.5,
         borderWidth : 1,
          borderColor : 'gray',
          paddingVertical:5,
          paddingHorizontal:5,
        borderRadius:5,
      },
      radioText : {
        paddingHorizontal:7,
       
        textAlignVertical : 'center',
      },
      radioSecOuterDiv : {
        marginLeft:3,
        flex:0.5,
        borderWidth : 1,
         borderColor : 'gray',
         paddingVertical:5,
         paddingHorizontal:5,
       borderRadius:5,
      },
      tabcontain : {
        backgroundColor : '#efeb10',
        height:40,
        padding:0,
      }

});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App