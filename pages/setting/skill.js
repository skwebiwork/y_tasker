import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,BackHandler,NetInfo,Modal} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon ,Tab, Tabs, ScrollableTab} from 'native-base';
import SideBar from '../sideBar';
import Account from './account';
export default class skill extends Component {

    static navigationOptions = {
     header: null ,
  };
  constructor(props){
    super(props);
    global.count = 0;
    this.state ={
            skill:'',
            getArround : '',
            language : '',
            qualification : '',
            experience : '',
            successImg : false,
            visible : false,
    }
  }
  componentDidMount(){
        AsyncStorage.getItem('userInfo', (err, result) => {
               // console.log(result);
                if(result != 'null'){
                        result = JSON.parse(result);
                        if(result.skills != 'null'){
                        this.setState({skill : result.skills});
                        }
                        if(result.language != 'null'){
                        this.setState({language : result.language});
                        }
                        if(result.qualification != 'null'){
                        this.setState({qualification : result.qualification});
                        }
                        if(result.workExperience != 'null'){
                        this.setState({experience : result.workExperience});
                        }
                        if(result.modeOfreach != 'null'){
                        this.setState({getArround : result.modeOfreach});
                        }

                }
               });
}
isChecked(modeOfreach,check){
console.log(modeOfreach);
if(modeOfreach!= null){
var a = modeOfreach.indexOf(check);
if(a<0){
     //  return false;
}else{
       // return true;
}
}
}
onCloseSuccess(){
        this.setState({successImg : false});
        
}
async saveSkill(){
        let UsreId = global.userId;
console.log('saveSkills Call');
        if(UsreId){
         let skills = this.state.skill;
         let language = this.state.language;
         let qualification = this.state.qualification;
         let workExperience = this.state.experience;
         try {
                let response  = await fetch('http://api.yellotasker.com/api/v1/user/updateProfile/'+userId, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'application/json',
                   },
                  body: JSON.stringify({  
                        skills:skills,
                        language:language,
                        qualification:qualification,
                        workExperience : workExperience
                   })
                 });
                 let res  = await response.text();
                 let formErrorsss = JSON.parse(res)
                 if(response.status >=200){
                  
                   console.log(formErrorsss);
                    this.setState({visible: false});
                   if(formErrorsss.code==500){
                        console.log(formErrorsss);
                   }
                   if(formErrorsss.status==1){
                this.setState({successImg: true});
                console.log(formErrorsss.status);
                   }
                 }else{
                     let errors = res;
                     throw errors;
                 }
              } catch (errors) {
               // console.log('error');
                //let formErrorsss = JSON.parse(errors)
                console.log(errors);
              }

        }else{
                console.log('Login First!!!!!')
        }
}
  render() {
    return(  
<View style={styles.mainviewStyle}>
<Modal  animationType="slide"
      transparent={true} visible={this.state.successImg}>
<View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#FFFFFF',
    height: 300,
    width: 300,
    borderRadius: 3,
   }}> 
		<View style={{backgroundColor:'#efeb10',height:50,flexDirection:'row'}} >
    <Text style={{color:'#000',textAlign:'center',paddingLeft:24,paddingVertical:10,flex:.9}}> Skill Saved SuccessFully </Text>
		<Text style={{flex:.1,paddingVertical:10,color:'#000',textAlign:'center',fontWeight:'bold'}} onPress={this.onCloseSuccess.bind(this)} >X</Text>
		</View>
   <View style={{alignItems: 'center',}}>
		<Image source={require('../img/checkmark.gif')} style={{height : 200,width:100}}/>
	 </View>
	 <Text style={{textAlign:'center'}}> Skills updated</Text>
	 </View>
	 </View>
   </Modal>
   <Modal
    animationType="slide"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
     <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <ScrollView style = {styles.scrollViewStyle}>
            <View style = {styles.formViewStyle} >    
            <View style={styles.inputDiv}>
                        <Text style={  {paddingVertical:3,
    color:'#000',fontSize:18,borderBottomWidth : 1,}}>Skill's</Text>
                        
                </View>   
                <View style={styles.inputDiv}>
                        <Text style={styles.inputLabel}> These are your skills. Keep them updated with any new skills you learn so other members can know what you can offer.</Text>
                        <TextInput
                                placeholder="eg cleaning,driving and graphic designing" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
                                underlineColorAndroid = "transparent" returnKeyType="next" 
                                keyboardType="default"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(val) => this.setState({skill: val})}
                                placeholderTextColor='#000'
                                value={this.state.skill}
                                >
                        </TextInput>
                </View>  
                    
                    
                <View style={styles.inputDiv}>
                        <Text style={{paddingVertical:8,color:'#000'}}> How do you get arrond ? </Text>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox label='Bicycle' onChange={(label) => console.log('I am checked', label)} checked={this.isChecked(this.state.getArround,'Bicycle')} />    
                        <CheckBox label='Car' onChange={(label) => console.log('I am checked', label)} checked={this.isChecked(this.state.getArround,'Car')}/>  
                        <CheckBox label='Online' onChange={(label) => console.log('I am checked', label)} checked={this.isChecked(this.state.getArround,'Online')}/>    
                        <CheckBox label='Scooter' onChange={(label) => console.log('I am checked', label)} checked={this.isChecked(this.state.getArround,'Scooter')}/>
                        </View>
                        <View style={{flexDirection:'row'}}>
                       
                        <CheckBox label='Truck' onChange={(checked) => console.log('I am checked', checked)} checked={this.isChecked(this.state.getArround,'Truck')}/>    
                        <CheckBox label='Walk' onChange={(checked) => console.log('I am checked', checked)} checked={this.isChecked(this.state.getArround,'Walk')}/>
                        </View>
                    </View>
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}> What language can you speak/write ?</Text>
                            <TextInput
                                    placeholder="eg. English and French" style={styles.InputText} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({language: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.language}
                                    >
                            </TextInput>
                    </View>
                   
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}> What qualification have you got ?</Text>
                            <TextInput
                                    placeholder="eg. Higher School Certificate(HSC) Accredited Barista" style={styles.InputText} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({qualification: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.qualification}
                                    >
                            </TextInput>
                    </View>
                    
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}> Whats your work experience ?</Text>
                            <TextInput
                                    placeholder="eg. 3 Years as a Barista at The Cafe" style={[styles.InputText,{ height:100,textAlignVertical: 'top',}]} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    multiline={true} numberOfLines={3}
                                    onChangeText={(val) => this.setState({experience: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.experience}
                                    >
                            </TextInput>
                    </View>
                   
                    <View style={styles.inputDiv}>
                       
                        <View style={{flexDirection:'row'}}>
                           <Text style={{flex:0.3,overflow:"hidden",textAlign:'center',borderRadius:18,backgroundColor:'#efeb10',paddingHorizontal:20,paddingVertical:10}} onPress={this.saveSkill.bind(this)}>Save Skill</Text>
                           <View style={{flex:0.1}}></View>
                          
                        </View>
                    </View>
            </View>
                
        
</ScrollView>
</View>
      
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},

scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
      
},
formViewStyle : {
  flex:1,
  paddingVertical : '3%',
  
},
inputDiv : {
    paddingVertical:6,
    paddingHorizontal : 20,
},
inputLabel : {
    paddingVertical:3,
    color:'#000',
},
InputText : {
    paddingVertical : 10,
    borderRadius:5,
    borderColor: '#000',
     borderWidth: 1,
     paddingHorizontal: 5,
     backgroundColor:'#fff',
     fontSize :14,
  },
  errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 1,
  },
  placeholderInput : {
      color:'#000',
      fontSize :14,
  }

});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App