import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,BackHandler,NetInfo,Modal,Alert} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon ,Tab, Tabs, ScrollableTab,Thumbnail} from 'native-base';
import SideBar from '../sideBar';
export default class account extends Component {

    static navigationOptions = {
     header: null ,
  };
  constructor(props){
    super(props);
    global.count = 0;
    this.state ={
        FirstName : '',
        LastName : '',
        TagLine : '',
        Location : '',
        email : '',
        profile_image :'http://api.yellotasker.com/storage/image/1515070413.jpg', 
        description : '',
        day : '',
        month : '',
        year : '',
        abn : '',
        successImg : false,
        visible : false,
    }
  }
  componentDidMount(){
        AsyncStorage.getItem('userInfo', (err, result) => {
                
               // let a = Array.isArray(result.role_type);
                //console.log(a);
                 if(result != 'null'){
                         result = JSON.parse(result);
                         if(result.first_name != 'null'){
                         this.setState({FirstName : result.first_name});
                         }
                         if(result.last_name != 'null'){
                         this.setState({LastName : result.last_name});
                         }
                         if(result.tagLine != 'null'){
                         this.setState({TagLine : result.tagLine});
                         }
                         if(result.location != 'null'){
                         this.setState({Location : result.location});
                         }
                         if(result.email != 'null'){
                         this.setState({email : result.email});
                         }
                         if(result.profile_image != null){
                         this.setState({profile_image : result.profile_image});
                         console.log('condition true');
                         }
                         if(result.about_me != 'null'){
                         this.setState({description : result.about_me});
                         }
                         if(result.birthday !='null'){
                                 var dob = result.birthday;
                                var d = dob.split('-');
                                if(d[0]!='undefined'){
                                this.setState({day : d[0]});
                                this.setState({month : d[1]});
                                this.setState({year : d[2]});
                        }
                                
                         }
 
                 }
                });
}
onCloseSuccess(){
        this.setState({successImg : false});
        
}
async saveAccount(){
        let UsreId = global.userId;
      
        if(UsreId){
                this.setState({visible: true});
                //console.log(userId);
        
        let fName = this.state.FirstName;
        let lName = this.state.LastName;
        let tagLine = this.state.TagLine;
        let location = this.state.Location;
        let emailId = this.state.email;
        let description = this.state.description;
        let bday=this.state.day;
        let bmonth = this.state.month;
        let byear = this.state.year;
        let bDate = bday+'-'+bmonth+'-'+byear;
       // console.log('bDate'+bDate);
        try {
                let response  = await fetch('http://api.yellotasker.com/api/v1/user/updateProfile/'+userId, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'application/json',
                   },
                  body: JSON.stringify({  
                        first_name:fName,
                        last_name:lName,
                        tagLine:tagLine,
                        location : location,
                        email : emailId,
                        about_me : description,
                        birthday : bDate
                     

        
                   })
                 });
                 let res  = await response.text();
                 let formErrorsss = JSON.parse(res)
                 if(response.status >=200){
                  
                   console.log(formErrorsss);
                    this.setState({visible: false});
                   if(formErrorsss.code==500){
                        console.log(formErrorsss);
                   }
                   if(formErrorsss.status==1){
                this.setState({successImg: true});
                console.log(formErrorsss.status);
                   }
                 }else{
                     let errors = res;
                     throw errors;
                 }
              } catch (errors) {
               // console.log('error');
                //let formErrorsss = JSON.parse(errors)
                console.log(errors);
              }
        
}else{
        console.log('Please Login First');
}
}
deactivateAccount(){
        Alert.alert(
                'Confirmation',
                'Are you sure want to deactivate your account!',
                [
                {text : 'OK', onPress:()=>{this.confirmDeactivate()}}
                ]
               );
}
confirmDeactivate(){
        console.log('call confirmDeactivate function');
}
  render() {
    return(  
<View style={styles.mainviewStyle}>
<Modal  animationType="slide"
      transparent={true} visible={this.state.successImg}>
			<View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#FFFFFF',
    height: 300,
    width: 300,
    borderRadius: 3,
   }}> 
		<View style={{backgroundColor:'#efeb10',height:50,flexDirection:'row'}} >
    <Text style={{color:'#000',textAlign:'center',paddingLeft:24,paddingVertical:10,flex:.9}}> Accounts Details Saved SuccessFully </Text>
		<Text style={{flex:.1,paddingVertical:10,color:'#000',textAlign:'center',fontWeight:'bold'}} onPress={this.onCloseSuccess.bind(this)} >X</Text>
		</View>
   <View style={{alignItems: 'center',}}>
		<Image source={require('../img/checkmark.gif')} style={{height : 200,width:100}}/>
	 </View>
	 <Text style={{textAlign:'center'}}> Account details updated</Text>
	 </View>
	 </View>
   </Modal>
   <Modal
    animationType="slide"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
     <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
   <ScrollView style = {styles.scrollViewStyle}>
            <View style = {styles.formViewStyle} >    
            <View style={styles.inputDiv}>
                        <Text style={ {paddingVertical:8, color:'#000',}}>Upload Avatar</Text>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                             <Thumbnail square source={{uri: this.state.profile_image}} />
                             <Text style={{marginLeft : 10,overflow:"hidden",textAlign:'center',borderRadius:18,backgroundColor:'#efeb10',paddingHorizontal:10,paddingVertical:10}}> Upload Photo</Text>
                        </View>
                </View>   
                <View style={styles.inputDiv}>
                        <Text style={styles.inputLabel}>First Name</Text>
                        <TextInput
                                placeholder="First Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
                                underlineColorAndroid = "transparent" returnKeyType="next" 
                                keyboardType="default"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(val) => this.setState({FirstName: val})}
                                placeholderTextColor='#000'
                                value={this.state.FirstName}
                                >
                        </TextInput>
                </View>  
                    <View style={styles.inputDiv}>
                        <Text style={styles.inputLabel}> Last Name</Text>
                            <TextInput
                                    placeholder="Last Name" style={styles.InputText} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({LastName: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.LastName}
                                    >
                            </TextInput>
                    </View>
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}>Tagline</Text>
                                <TextInput
                                        placeholder="TagLine" style={styles.InputText} 
                                        underlineColorAndroid = "transparent" returnKeyType="next" 
                                        keyboardType="default"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        onChangeText={(val) => this.setState({TagLine: val})}
                                        placeholderTextColor='#000'
                                        value={this.state.TagLine}
                                        >
                                </TextInput>
                    </View>
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}> Location</Text>
                            <TextInput
                                    placeholder="Location" style={styles.InputText} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({Location: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.Location}
                                    >
                            </TextInput>
                    </View>
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}> Email</Text>
                            <TextInput
                                    placeholder="Email" style={styles.InputText} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({email: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.email}
                                    >
                            </TextInput>
                    </View>
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}> Birthday</Text>
                            <View style={{flexDirection:'row'}}>
                            <TextInput
                                    placeholder="DD" style={[styles.InputText,{paddingHorizontal:20}]} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="numeric"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({day: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.day}
                                     >
                            </TextInput>
                            <TextInput
                                    placeholder="MM" style={[styles.InputText,{marginHorizontal:15,paddingHorizontal:20}]} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="numeric"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({month: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.month}
                                    >
                            </TextInput>
                            <TextInput
                                    placeholder="YYYY" style={[styles.InputText,{paddingHorizontal:20}]} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="numeric"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({year: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.year}
                                    >
                            </TextInput>
                            </View>
                    </View>
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}> ABN</Text>
                            <TextInput
                                    placeholder="ABN" style={styles.InputText} 
                                    underlineColorAndroid = "transparent" returnKeyType="next" 
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    onChangeText={(val) => this.setState({abn: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.abn}
                                    >
                            </TextInput>
                    </View>
                    
                    <View style={styles.inputDiv}>
                            <Text style={styles.inputLabel}> Description</Text>
                            <TextInput
                                    placeholder="Description" style={[styles.InputText,{ height:100,textAlignVertical: 'top',}]} 
                                    ref={(input)=>this.lnameInput=input}
                                    keyboardType="default"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    multiline={true} numberOfLines={3}
                                    onChangeText={(val) => this.setState({description: val})}
                                    placeholderTextColor='#000'
                                    value={this.state.description}
                                    >
                            </TextInput>
                    </View>
                    <View style={styles.inputDiv}>
                        <Text style={{paddingVertical:8,color:'#000'}}> On Yellotasker I want to </Text>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox label='Posts Task' onChange={(checked) => console.log('I am checked', checked)}/>    
                        <CheckBox label='Earn Money' onChange={(checked) => console.log('I am checked', checked)}/>     
                        </View>
                    </View>
                    <View style={styles.inputDiv}>
                       
                        <View style={{flexDirection:'row'}}>
                           <Text style={{flex:0.3,overflow:"hidden",textAlign:'center',borderRadius:18,backgroundColor:'#efeb10',paddingHorizontal:20,paddingVertical:10}} onPress={this.saveAccount.bind(this)}>Save Profile</Text>
                           <View style={{flex:0.1}}></View>
                           <Text style={{flex:0.6,overflow:"hidden",textAlign:'center',borderRadius:18,backgroundColor:'#000',color:'#fff',paddingHorizontal:10,paddingVertical:10,paddingLeft:15}} onPress={this.deactivateAccount.bind(this)}>Deactivate My Account</Text>
                        </View>
                    </View>
            </View>
                
        </ScrollView>

</View>
      
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},

scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
      
},
formViewStyle : {
  flex:1,
  paddingVertical : '3%',
  
},
inputDiv : {
    paddingVertical:6,
    paddingHorizontal : 20,
},
inputLabel : {
    paddingVertical:3,
    color:'#000',
},
InputText : {
    paddingVertical : 10,
    borderRadius:5,
    borderColor: '#000',
     borderWidth: 1,
     paddingHorizontal: 5,
     backgroundColor:'#fff',
     fontSize :14,
  },
  errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 1,
  },
  placeholderInput : {
      color:'#000',
      fontSize :14,
  },
  Img : {
        height: 72,
        width : 72,
    },

});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App