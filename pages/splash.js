import React, {Component} from "react";
import {
  StyleSheet,
  Text,
  View,ActivityIndicator,Image
} from "react-native";
import SplashScreen from "rn-splash-screen";
import Home from "./home/home";

export default class splash extends Component {
  state = {
    isLoading: false,
    isFinishedloading : true,
  };

  componentDidMount() {
    global.setTimeout(() => {
      this.setState({isLoading: true});
    }, 3000);
  }

  componentDidUpdate() {
    if (!this.state.isLoading) {
      // Hide splash screen
     SplashScreen.hide();
    }
  }

  render() {
   

    SplashScreen.hide();
    
           //after everything has finished loading - show my app.
           if (this.state.isLoading) 
            {
                return (
                    <Home />
                );
            }
    
          // Until then show the same image as the splash screen with an ActivityIndicator.
            return (
               <View style={styles.container}>
                 
                    <ActivityIndicator  style={styles.indicator} color="#000000" size = "large"></ActivityIndicator>
                     <Text>loading...... </Text>
               </View>
            );

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:'#ffffff',
  },
  image: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
});