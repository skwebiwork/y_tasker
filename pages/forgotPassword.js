/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,Alert,
  View,
  Image,Button,TouchableOpacity,ScrollView,TouchableHighlight,TextInput,KeyboardAvoidingView,Modal,AsyncStorage,ProgressBar,ActivityIndicator
} from 'react-native';
//import Spinner from 'react-native-loading-spinner-overlay';
import EStyleSheet from 'react-native-extended-stylesheet';
export default class forgotPassword extends Component {
  
 
   static navigationOptions = {
     header: null ,
  };
   constructor(){
    super()

    this.state = {
      email:"",
      password:"",
      error:"",
    LodingStatus:false,
     modalVisible: false,
     loading : true,
     visible : false,
     emailValid : false,
     emailValidMsg: false,
    }
  }
 setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  validate = (text) => {
//console.log(text);
let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
if(reg.test(text) === false)
{
console.log("Email is Not Correct");
//this.setState({error: 'Please enter valid email'});
this.setState({email:text})
this.setState({emailValid: false});
return false;
  }
else {
  this.setState({email:text})
  console.log("Email is Correct");
  this.setState({error: ''});
  this.setState({emailValid: true});
}
}

  async onloginPressed() {
    try {
      global.userId = "";
      const { navigate } = this.props.navigation;
      this.setState({visible: true});
    
      let response = await fetch('http://api.yellotasker.com/api/v1/user/forgotPassword', {
                            method: 'POST',
                            headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({

                                 email: this.state.email,
                                

                            })
                          });
      let res = await response.text();
      this.setState({visible: false});
      if(response.status >= 200){
        console.log(response);
       // this.setState({LodingStatus: false});
         let result= JSON.parse(res); 
        // alert(result.message);
        // this.setState({error: result.message});
         if(result.status==1){
          global.userId = result.data.id;
         // this.setState({error: result.message});
         // navigate('Dashboard');
         //alert(result.message);
         global.setTimeout(() => {
         Alert.alert(
            'Success',
            'Reset password link has been sucessfully sent. Please check your email.'
           );
          }, 50);
         }else{
          global.setTimeout(() => {
            Alert.alert(
                'Error',
                'Oh no! The address you provided is not in our system'
               );
              }, 50);
         }

      }else{
        let error = res;
        throw error;
      }
    } catch (error) {
         this.setState({error: error});
    }
  }
  onloginPresseds(){
     if(this.state.email==""){
             this.setState({error: 'Please enter required fields'});
             this.setState({ emailValidMsg: true});
     }else if(this.state.emailValid==false){
        this.setState({error: 'Please enter valid email'});
        this.setState({ emailValidMsg: true});
     }else{
      this.setState({ emailValidMsg: false});
             this.onloginPressed();
             

     }

    
  }
  onsignupPressed(){
    const { navigate } = this.props.navigation;
    navigate('SignUp');
  }
  onskipPressed(){
    const { navigate } = this.props.navigation;
   navigate('Dashboard'); 
  }
  SignIn(){
    const { navigate } = this.props.navigation;
   navigate('Login'); 
  }

  render() {
    return ( 

      <View style={styles.LoadingView}>
          <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('./img/loading.png')}/>
     
   </View>
   </View>
   </Modal>
   

       <View style={{alignItems:'center',backgroundColor:'#000000',paddingVertical:10,}}>
               <Image source={require('./img/app-logo.png')} />
      </View>
       
      <ScrollView style={styles.contentContainer}>
     
        <View  style={styles.mainContainer}>
             <Image source={require('./img/banner-login.jpg')} style={styles.bannerImg}/>
              <View style={styles.container}>
                    <View style={styles.buttonsDiv}>
         
         <Text style={styles.instructionText}>
         Enter your email below and we will send you instructions on how to reset your password
            </Text>
            <Text style={styles.errorText}>{this.state.error}</Text>
       </View>
      
       <View style={styles.buttonDiv}>
      
      <TextInput
       placeholder="Email" style={[styles.InputText,this.state.emailValidMsg&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
      keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false}
     onChangeText={(text) => this.validate(text)}
      value={this.state.email}
       ></TextInput>
       </View>
      
     
       <View style={styles.buttonsssDiv}  keyboardShouldPersistTaps='always'>
              <View style={{ flex: 0.4 }}>
                    <TouchableHighlight style={styles.buttontouch} onPress={this.SignIn.bind(this)}>
                    <Text style={styles.buttonText}>
                      Cancel
                    </Text>
                    </TouchableHighlight>
              </View>
              <View style={{ flex: 0.2}}></View>

              <View style={{ flex: 0.4 }}>
                  <TouchableHighlight style={styles.buttontouchLogin} underlayColor='#efeb10' onPress={this.onloginPresseds.bind(this)}>
                      <Text style={styles.buttonTextLogin}>
                        Send
                      </Text>
                  </TouchableHighlight>
              </View>
      </View>
      
       <View style={styles.buttonssDiv} keyboardShouldPersistTaps='always'>
      <TouchableHighlight style={styles.skipDiv} onPress={this.onskipPressed.bind(this)}>
         <Text style={styles.facebokText}>
              Skip & Explore
                         </Text>
      </TouchableHighlight>
       </View>
      </View>
              </View> 
      
        

  </ScrollView>
  </View>
    );
  }
}


const styles = EStyleSheet.create({
  contentContainer: {
   
  },
  LoadingView : {
    
  },
  mainContainer:{
        borderTopWidth : 4,
      borderColor: '#efeb10',
  },
bannerImg : {
  width:'100%',
  height : 115,
},
container : {
  flex: 1,
    backgroundColor: '#ffffff',
     flexDirection:'column', 
      paddingHorizontal : 10,
     paddingVertical : 12,
    },
    buttonsDiv : {
     paddingVertical : 12,
     paddingHorizontal : 20,
     marginTop:10,
    },
    buttonDiv : {
     paddingVertical : 12,
     paddingHorizontal : 20,
    },
    buttonssDiv : {
      paddingVertical : 12,
     paddingHorizontal : 20,
       marginBottom : 60,
    },
    buttonsssDiv : {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical : '4%',
        paddingHorizontal : '9%',
    },
  facebookDiv : {
    paddingVertical : 11,
    backgroundColor : '#3b5998',
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#3b5998',
 
  },
  googleDiv : {
    paddingVertical : 11,
    backgroundColor : '#dd4b39',
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#dd4b39',
 
  },
  errorText : {
color : '#dd4b39',
paddingTop : 5,
  },
  facebokText : {
     color : '#fff',
      textAlign: 'center',
      fontSize: 18,
  },
  instructionText : {
    color : '#000',
     textAlign: 'center',
     fontSize: 16,
 },
LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 11, 
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#efeb10',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
},
extrasText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 17,
},
forgateDiv : {
  paddingVertical : 12, 
  paddingHorizontal : 20,
  flex:1,
    flexDirection:'row',
},
smText : {
color : '#666',
fontSize: 12,  
},
smTexts : {
color : '#666',
fontSize: 12,
textAlign: 'right',
},
skipDiv : {
    paddingHorizontal : 8,
    backgroundColor : '#000000',
  paddingVertical : 12,
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#000000',
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
},  buttontouch:{
    
        paddingVertical : 8,
     paddingHorizontal : 20,
      flex : 0.6,
        backgroundColor : '#000000',
      
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#000000',
        marginLeft :'1%',
      },
        buttonText: {
        fontSize: 16,
        color: '#FFF',
        textAlign: 'center',
    },
    buttontouchLogin:{
      flex : 0.6,
       paddingVertical : 8,
     paddingHorizontal : 24,
        
        backgroundColor : '#efeb10',
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#efeb10',
      
         marginRight :'1%',
      },
        buttonTextLogin: {
        fontSize: 16,
        color: '#000000',
        textAlign: 'center',
    },
    errorInput : {
      borderColor: '#dd4b39',
      borderWidth: 1,
    },
});
AppRegistry.registerComponent('forgotPassword', () => forgotPassword);
