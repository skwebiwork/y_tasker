import EStyleSheet from 'react-native-extended-stylesheet';
const styles = EStyleSheet.create({
    contentContainer: {
      
    },
     contentContainer: {
      
    },
    mainContainer:{
          borderTopWidth : 4,
        borderColor: '#efeb10',
    },
  bannerImg : {
    width:'100%',
    height : 115,
  },
  container : {
    flex: 1,
      backgroundColor: '#ffffff',
       flexDirection:'column', 
        paddingHorizontal : 20,
       paddingVertical : 12,
      },
      buttonssDiv : {
       paddingVertical : 10,
       paddingHorizontal : 20,
       marginTop:10,
      },
      buttonDiv : {
       paddingVertical : 10,
       paddingHorizontal : 20,
      },
      buttonSec : {
      
        paddingHorizontal : 20,
      },
      buttonsDiv : {
          paddingVertical : 10,
       paddingHorizontal : 20,
       marginBottom : 60,
      },
    facebookDiv : {
      paddingVertical : 11,
      backgroundColor : '#3b5998',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#3b5998',
   
    },
    instagramDiv : {
      paddingVertical : 11,
      backgroundColor : '#833ab4',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#833ab4',
   
    },
    googleDiv : {
      paddingVertical : 11,
      backgroundColor : '#dd4b39',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#dd4b39',
   
    },
    errorText : {
  color : '#dd4b39',
  paddingTop : 5,
    },
    facebokText : {
       color : '#fff',
        textAlign: 'center',
        fontSize: 18,
    },
  LoginDiv : {
    backgroundColor : '#efeb10',
    paddingVertical : 11, 
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#efeb10',
  
  },
  extraText : {
    color : '#000000',
    textAlign: 'center',
        fontSize: 18,
  },
  extrasText : {
    color : '#000000',
    textAlign: 'center',
        fontSize: 16,
  },
  forgateDiv : {
    
    paddingHorizontal : 20,
      flexDirection:'row',
  },
  smText : {
  color : '#666',
  fontSize: 12,
  paddingHorizontal : 8,
  },
  smTexts : {
  color : '#666',
  fontSize: 12,
  paddingHorizontal : 8,
  textAlign:'right',
  },
  skipDiv : {
      paddingHorizontal : 8,
      backgroundColor : '#000000',
    paddingVertical : 12,
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#000000',
  },
  InputText : {
    paddingVertical : 10,
    borderRadius:5,
    borderColor: '#000',
     borderWidth: 1,
     paddingHorizontal: 5,
    
  },
  errorInput : {
    borderColor: '#dd4b39',
    borderWidth: 1,
  },
  });
export default styles;
  