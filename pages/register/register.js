import React, { Component } from 'react';
import {
   AppRegistry,
  StyleSheet,
  Text,
  View,Image,Button,TouchableOpacity,ScrollView,TouchableHighlight,TextInput,KeyboardAvoidingView,AsyncStorage,Modal,Alert,Dimensions,BackHandler,ActivityIndicator
} from 'react-native';
//import Spinner from 'react-native-loading-spinner-overlay';
import styles from "./registerStyle";
var {width, height} = Dimensions.get('window');
export default class register extends Component {
  constructor() {
    super();

    this.state = {
         firstName :"",
         lastName:"",
         email: "",
         password: "",
         password_conformation: "",
         errors: [],
         LodingStatus:false,
         visible : false,
         emailValids : false,
         passwordValids : false,
         firstNameError : false,
         emailError : false,
         passwordError : false,
         confirmPasswordError : false,

    }
  }
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    }
    componentWillUnmount() {
      //Forgetting to remove the listener will cause pop executes multiple times
      BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
    }
    handleBack() {
  global.count = global.count+1;
  //alert('count'+global.count);
  if(global.count>1){
    Alert.alert(
      'Confirmation',
      'Are sure want to quit this App',
      [
        {text: 'OK', onPress: () => BackHandler.exitApp()},
      ]
  );
  }else{
    //this.setState({touchMsg : true});
    
    global.setTimeout(() => {
      global.count = 0;
    }, 1000);
  }
    
  return true;
  
    }
  emailValidate = (text) => {
//console.log(text);
let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
if(reg.test(text) === false)
{
 let errorsArray = [];
        // errorsArray.push('Please enter valid email');
         //this.setState({errors: errorsArray});
//this.setState({error: 'Please enter valid email'});
this.setState({email:text})
this.setState({emailValids: false});
return false;
  }
else {
  this.setState({email:text})
  console.log("Email is Correct");
  let errorsArray = [];
       //  errorsArray.push('');
 
  this.setState({errors: errorsArray});
  this.setState({emailValids: true});
}
}

passwordValidate = (text)=> {
let password = this.state.password;
if(password==text){
this.setState({password_conformation:text})
 this.setState({passwordValids: true});
 let errorsArray = [];
         //errorsArray.push('');
 
 this.setState({errors: errorsArray});
}else{
this.setState({password_conformation:text})
 let errorsArray = [];
        // errorsArray.push('Password and Confirm Password should be same');
      //   this.setState({errors: errorsArray});
this.setState({passwordValids: false});

}

}
  async onRegisterPressed() {
    try {
      //this.setState({LodingStatus: true});
      this.setState({visible: true});
      let response  = await fetch('http://api.yellotasker.com/api/v1/user/signup', {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           first_name :this.state.firstName,
           last_name:this.state.lastName,
           email: this.state.email,
           password: this.state.password
           //password_conformation: this.state.password_conformation

         })
       });
        
       let res  = await response.text();
       this.setState({visible: false});
       console.log(response.status);

       console.log(JSON.stringify(res));
       if(response.status >=200){
         let formErrorsss = JSON.parse(res);
        // this.setState({LodingStatus: false});
        
        // alert("res token: " + res);
         if(formErrorsss.code==500){
         let errorsArray = [];
         errorsArray.push(formErrorsss.message);
         this.setState({errors: errorsArray});
         }
         if(formErrorsss.status==1){
          global.userId = formErrorsss.data.id;
          global.FirstName = formErrorsss.data.first_name;
          global.LastName = formErrorsss.data.last_name;
          let _id =  formErrorsss.data.id.toString();
         // global.userId = result.data.id;
          
         let _firstName = formErrorsss.data.first_name.toString();
         let _lastName = formErrorsss.data.last_name.toString();
       AsyncStorage.setItem("loginId", _id);
       AsyncStorage.setItem("FirstName", _firstName);
       AsyncStorage.setItem("LastName", _lastName);
          
          this.setState({error: formErrorsss.message});
        
          const { navigate } = this.props.navigation;
          navigate('Dashboard');
          //alert(result.data.id);
         }
       }else{
           let errors = res;
           throw errors;
       }
    } catch (errors) {
           console.log("catch errors " + errors);

           let formErrors = JSON.parse(errors);
           let errorsArray = [];
           for(let key in formErrors){
             if(formErrors[key].length > 1){
               formErrors[key].map(error => errorsArray.push('${key} ${error}'))
             }else{
               errorsArray.push('${key} ${formErrors[key]}')
             }
           }
           this.setState({errors: errorsArray});
    }
  }
  onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onskipPressed(){
    const { navigate } = this.props.navigation;
   navigate('Dashboard'); 
  }
  onRegisterPressedValidate(){
    if(this.state.firstName==""){
               let errorsArray = [];
         errorsArray.push('Please enter required fields');
         this.setState({ firstNameError : true});
         this.setState({errors: errorsArray});
     } else if(this.state.email==""){
      let errorsArray = [];
         errorsArray.push('Please enter required fields');
         this.setState({ firstNameError : false});
         this.setState({ emailError : true});
         this.setState({errors: errorsArray});
    }else if(this.state.emailValids==false){
      let errorsArray = [];
         errorsArray.push('Please enter valid email');
         this.setState({ emailError : true});
         this.setState({errors: errorsArray});
       // this.setState({error: 'Please enter valid email'});
     }else if(this.state.password==""){
           // this.setState({error: 'Password is required!'});
           let errorsArray = [];
            errorsArray.push('Please enter required fields');
            this.setState({passwordError : true});
            this.setState({ firstNameError : false});
            this.setState({ emailError : false});
         this.setState({errors: errorsArray});
     }else if(this.state.password_conformation==""){
      let errorsArray = [];
         errorsArray.push('Confirm Password is required!');
         this.setState({confirmPasswordError : true});
         this.setState({passwordError : false});
            this.setState({ firstNameError : false});
            this.setState({ emailError : false});
         this.setState({errors: errorsArray});

     }else if(this.state.passwordValids==false){
       let errorsArray = [];
         errorsArray.push('Password and Confirm should be same');
         this.setState({confirmPasswordError : true});
         this.setState({errors: errorsArray});
       // this.setState({error: 'Password and Confirm should be same'});
     }
     else{
      this.setState({ firstNameError : false});
        this.setState({ emailError : false});
      this.setState({passwordError : false});
        this.setState({confirmPasswordError : false});
             this.onRegisterPressed();
     }
  }
   static navigationOptions = {
     header: null ,
  };
  render() {
    return (
      <View>
      <View style={{alignItems:'center',backgroundColor:'#000000',paddingVertical:10,}}>
               <Image source={require('../img/app-logo.png')} />
      </View>
      <ScrollView contentContainerStyle={styles.contentContainer}>
    

   <Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
     <Text style={{color:'#333333',textAlign:'center',}}>Creating account....</Text>
   </View>
   </View>
   </Modal>
          <View style={styles.mainContainer}>
             
      {/* <Image source={require('../img/banner-login.jpg')} style={styles.bannerImg}/> */}
              <View style={styles.container}>
              <Errors errors={this.state.errors} />
       
             <View style={styles.buttonDiv}>
      <TextInput
       placeholder="First Name" style={[styles.InputText,this.state.firstNameError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       onSubmitEditing={()=>this.lnameInput.focus()}
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(val) => this.setState({firstName: val})}
       ></TextInput>
       </View>
       <View style={styles.buttonDiv}>
      <TextInput
       placeholder="Last Name" style={styles.InputText} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       onSubmitEditing={()=>this.emailInput.focus()}
       ref={(input)=>this.lnameInput=input}
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(val) => this.setState({lastName: val})}
       ></TextInput>
       </View>
       <View style={styles.buttonDiv}>
      <TextInput
       placeholder="Email" style={[styles.InputText,this.state.emailError&&styles.errorInput]} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       onSubmitEditing={()=>this.passwordInput.focus()}
       ref={(input)=>this.emailInput=input}
      keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.emailValidate(text)}
      value={this.state.email}
       ></TextInput>
       </View>
       <View style={styles.buttonDiv}>
      <TextInput
       placeholder="Password"
        style={[styles.InputText,this.state.passwordError&&styles.errorInput]}  
        underlineColorAndroid = "transparent" returnKeyType="go"
        onChangeText={(val) => this.setState({password: val})}
        ref={(input)=>this.passwordInput=input}
        onSubmitEditing={()=>this.cpasswordInput.focus()} secureTextEntry
        >

      </TextInput>
       </View>
       <View style={styles.buttonDiv}>
      <TextInput
       placeholder="Confirm Password"
        style={[styles.InputText,this.state.confirmPasswordError&&styles.errorInput]}  
        underlineColorAndroid = "transparent" returnKeyType="go"
         secureTextEntry
        ref={(input)=>this.cpasswordInput=input}
        onChangeText={(text) => this.passwordValidate(text)}
      value={this.state.password_conformation}
        >
      </TextInput>
       </View>
       <View style={styles.buttonDiv}>
      <TouchableHighlight style={styles.LoginDiv} underlayColor='#efeb10' onPress={this.onRegisterPressedValidate.bind(this)}>
         <Text style={styles.extraText}>
              Sign up  
                         </Text>
      </TouchableHighlight>
       </View>
       <View style={styles.forgateDiv}>
      
         <Text style={styles.smText} onPress={this.onLoginPressed.bind(this)}>
              Already a member? Login
            </Text>
            
      
       </View>
       <View style={styles.buttonSec}>
      
      <Text style={styles.extrasText}>
           Or 
         </Text>
        
        
          </View>
       <View style={styles.buttonssDiv}>
      <TouchableHighlight style={styles.facebookDiv}>
         <Text style={styles.facebokText}>
              Sign up with Facebook
            </Text>
      </TouchableHighlight>
       </View>
       <View style={styles.buttonssDiv}>
      <TouchableHighlight style={styles.instagramDiv}>
         <Text style={styles.facebokText}>
              Sign up with Instagram
            </Text>
      </TouchableHighlight>
       </View>
             <View style={styles.buttonDiv}>
      <TouchableHighlight style={styles.googleDiv}>
         <Text style={styles.facebokText}>
              Sign up with G+
            </Text>
      </TouchableHighlight>
       </View>
       <View style={styles.buttonsDiv}>
      <TouchableHighlight style={styles.skipDiv} onPress={this.onskipPressed.bind(this)}>
         <Text style={styles.facebokText}>
              Skip & Explore
                         </Text>
      </TouchableHighlight>
       </View>
      </View>
              </View> 
        

  </ScrollView>
  </View>
    );
  }
}
const Errors = (props) => {
  return (
    <View>
       {props.errors.map((error, i) => <Text key={i} style={styles.errorText}>{error}</Text>)}
    </View>
  );
}

//AppRegistry.registerComponent('register', () => register);
