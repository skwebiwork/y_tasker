import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,BackHandler,NetInfo} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from '../sideBar';
var molpay = require("molpay-mobile-xdk-reactnative-beta");
export default class forthStep extends Component {

    static navigationOptions = {
     header: null ,
  };
  constructor(props){
    super(props);
    global.count = 0;
    this.state ={
      taskLocatio:"",
      taskAddress:"",
      taskZipcode:"",
       date_in: '',
  date_out: '2046-10-01',
  error : "",
  taskFromHome : false,
  value :"",
  tastDate : "Pay to yelloTasker before task starts",
  taskOnDate : false,
  isShowPayment : false,
  molPayresult:[],
  amount : '',
  txn_ID : '',
  order_id : '',
  msgType : '',
  status_code : '',
  paydate : '',
  taskTitle : '',
  payTime : '',
    }
  }
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
   this.addResult();
}
componentWillUnmount() {
  //Forgetting to remove the listener will cause pop executes multiple times
  BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
}
handleBack() {
  global.count = global.count+1;
  //alert('count'+global.count);
  if(global.count>1){
    Alert.alert(
      'Confirmation',
      'Are sure want to quit this App',
      [
        {text: 'OK', onPress: () => BackHandler.exitApp()},
      ]
  );
  }else{
    //this.setState({touchMsg : true});
    
    global.setTimeout(() => {
      global.count = 0;
    }, 1000);
  }
    
  return true;
  
    }
addResult(){
  const { params } = this.props.navigation.state;
  console.log(params.molPayresult);
  console.log(params.taskData);
  console.log(params.molPayresult);
  var molPayresult = JSON.parse(params.molPayresult);

  console.log(molPayresult);
  var dt = Number(molPayresult.paydate);
  var d = new Date();
 
  this.setState({amount:molPayresult.amount});
  this.setState({txn_ID:molPayresult.txn_ID});
  this.setState({order_id:molPayresult.order_id});
  this.setState({msgType:molPayresult.msgType});
  var statusCode = molPayresult.status_code;
//  statusCode = statusCode.toString();
  console.log(statusCode);
  if(statusCode=='00'){
    var status = "Sucess";
  }else{
    var status = "Failed";
  }
  this.setState({status_code:status});
  this.setState({paydate:d.toLocaleDateString()});
  this.setState({payTime:d.toLocaleTimeString()});
  this.setState({taskTitle:params.taskData.title});
  
console.log(d.toLocaleString())     // 7/25/2016, 1:35:07 PM
console.log(d.toLocaleDateString())

}

    onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
   const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }
  onMyProfilePressed(){
    let userId = global.userId;
    // alert('userId'+userId);
   const { navigate } = this.props.navigation;
   if(userId){
   
    navigate('Profile'); 
    }else{
   Alert.alert(
      'Message',
      'Please Login to go to Profile',
      [
      {text : 'OK', onPress:()=>{this.onLoginPressed()}}
      ]
     );
    }
  }
  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
    
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
   onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
    onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onNextStep(){
 if(this.state.tastDate==""){
this.setState({error: ' Please enter required fields'});
  }else{
    this.updateTask();
   // global.taskLocatio = this.state.taskLocatio;
   // const { navigate } = this.props.navigation;
 //  navigate('Browse');
}  
}
closeDrawer = () => {
  this.drawer._root.close()
  };
  openDrawer = () => {
  this.drawer._root.open()
  };
  onBackPressed(){
    const { navigate } = this.props.navigation;
    navigate('Mainview'); 
 }


 async updateTask() {
  try {
    //this.setState({LodingStatus: true});
    const { params } = this.props.navigation.state;
    var taskId =params.taskId;
    this.setState({visible: true});
    let response  = await fetch('http://api.yellotasker.com/api/v1/updatePostTask', {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
        paymentMode :this.state.tastDate,
        taskId:taskId,
        title : params.title
       
         //password_conformation: this.state.password_conformation

       })
     });
      
     let res  = await response.text();
     this.setState({visible: false});
     console.log(response.status);

     console.log(JSON.stringify(res));
     if(response.status >=200){
       let formErrorsss = JSON.parse(res);
      // this.setState({LodingStatus: false});
      
      // alert("res token: " + res);
       if(formErrorsss.code==500){
      
       
       }
       if(formErrorsss.status==1){
       
      
      
        const { navigate } = this.props.navigation;
        navigate('Dashboard');
        //alert(result.data.id);
       }
     }else{
         let errors = res;
         throw errors;
     }
  } catch (errors) {
         console.log("catch errors " + errors);

      //   this.setState({errors: errorsArray});
  }
}
  render() {
    return(
      <Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >
<View style={styles.mainviewStyle}>
<View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
<View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
<Text>


</Text>

</View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
<Image source={require('../img/app-logo.png')} />
</Text>
</View>
<View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
<TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>

<FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>

</TouchableHighlight>
</View>

</View>
 <ScrollView style = {styles.scrollViewStyle}>
       <View style = {styles.formViewStyle} >      
        <Text style={{textAlign:'center',fontWeight : 'bold',fontSize:16,paddingVertical:10}}>Transaction Summary</Text>
          <View style={{flex:1,flexDirection:'column',paddingVertical:15,paddingHorizontal:10,backgroundColor:'#e6f1f7'}}>
              
                 <View style={styles.mainDiv}>
                    <View style={styles.leftDiv}>
                   <Text style={{textAlign:'center'}}> Transaction ID </Text>
                    </View>
                    <View style={styles.rightDiv}>
                   <Text style={{textAlign:'center'}}> {this.state.txn_ID}</Text>
                    </View>
                 </View>
                 <View style={styles.secondDiv}>
                    <View style={styles.leftDiv}>
                   <Text style={{textAlign:'center'}}> Amount Paid </Text>
                    </View>
                    <View style={styles.rightDiv}>
                   <Text style={{textAlign:'center'}}> {this.state.amount}</Text>
                    </View>
                 </View>
                 <View style={styles.secondDiv}>
                    <View style={styles.leftDiv}>
                   <Text style={{textAlign:'center'}}> Date of transaction </Text>
                    </View>
                    <View style={styles.rightDiv}>
                   <Text style={{textAlign:'center'}}> {this.state.paydate}</Text>
                    </View>
                 </View>
                 <View style={styles.secondDiv}>
                    <View style={styles.leftDiv}>
                   <Text style={{textAlign:'center'}}> Time of transaction </Text>
                    </View>
                    <View style={styles.rightDiv}>
                   <Text style={{textAlign:'center'}}> 
                   {this.state.payTime}</Text>
                    </View>
                 </View>
                 <View style={styles.secondDiv}>
                    <View style={styles.leftDiv}>
                   <Text style={{textAlign:'center'}}> Payment method </Text>
                    </View>
                    <View style={styles.rightDiv}>
                   <Text style={{textAlign:'center'}}> Online</Text>
                    </View>
                 </View>
                 <View style={styles.secondDiv}>
                    <View style={styles.leftDiv}>
                   <Text style={{textAlign:'center'}}> Transaction Status </Text>
                    </View>
                    <View style={styles.rightDiv}>
                   <Text style={{textAlign:'center'}}> {this.state.status_code}</Text>
                    </View>
                 </View>
                 <View style={styles.secondDiv}>
                    <View style={styles.leftDiv}>
                   <Text style={{textAlign:'center'}}> Orderid </Text>
                    </View>
                    <View style={styles.rightDiv}>
                   <Text style={{textAlign:'center'}}> {this.state.order_id}</Text>
                    </View>
                 </View>
                 <View style={styles.secondDiv}>
                    <View style={styles.leftDiv}>
                   <Text style={{textAlign:'center'}}> Task title </Text>
                    </View>
                    <View style={styles.rightDiv}>
                   <Text style={{textAlign:'center'}}> {this.state.taskTitle}</Text>
                    </View>
                 </View>

               
               {/*  <View style={styles.secondDiv}>
                    <Text style={{flex:1,paddingVertical:15,paddingHorizontal:15}}>
                       Please call at 1234567890 to raise any issues or mail us at support@yellotasker.com
                    </Text>
                 </View> */}
                 <View style={styles.secondDiv}>
                    <Text style={{flex:1,borderRadius:5,backgroundColor : '#efeb10',textAlign:'center',paddingVertical:15,paddingHorizontal:15,overflow:'hidden'}} onPress={()=>this.onBrowsePressed()}>
                           Continue
                    </Text>
                 </View>
          </View>

     


     
    
 

     </View>
          
 </ScrollView>

 <View style={styles.footer}>
 <View style={styles.browseButtons}>
 <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
   <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
   </TouchableHighlight>
     <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
 </View>
 <View style={styles.postButtons}>
       <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
                     
                  <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.plusSquare}</FontAwesome>
                    
                     </TouchableHighlight>
                    <Text style={[styles.footerText,{color:'#efeb10'}]} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
                </View>
 <View style={styles.myTaskButtons}>
 <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
   <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
     </TouchableHighlight>
          <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
      </View>
  {/* <View style={styles.messageButtons}>
   <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>
      
     <Text style={styles.footerText}>Message</Text>
 </View> */}
 <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
 </View>
      </View>
      </Drawer>
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},
footer: {
  
   position: 'absolute',
  flex:0.1,
  left: 0,
  right: 0,
  bottom: -10,
  backgroundColor:'#000000',
  flexDirection:'row',
  height:70,
  alignItems:'center',
},
postButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
browseButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
myTaskButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,
   
},
messageButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   
},
footerText: {
  color:'white',
  alignItems:'center',
  fontSize:14,
 

},
textStyle: {
  alignSelf: 'center',
  color: 'orange'
},
scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
      
},
bannerImg : {
  width:'100%',
  height : 130,
},
formViewStyle : {
  flex:1,
  paddingVertical : '3%',
  paddingHorizontal : '3%',
},
buttonDiv : {
     paddingVertical : '1%',
    },
    secondButtonDiv : {
      paddingVertical : '2%',
     },
radioDiv : {
  flex:1,
  flexDirection:'row',
  paddingVertical : '1%',
},
secRadioDiv:{
  flex:1,
  flexDirection:'row',
  paddingVertical : '2%',
},
buttonsDiv : {
   paddingVertical : '2%',
   marginBottom : 70,
},
extrasText : {
  color : '#000000',
      fontSize: 16,
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
   textAlignVertical: 'top',
},

LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 10, 
    borderRadius:25,
    borderWidth: 1,
    borderColor: '#efeb10',

},
mainDiv : {paddingHorizontal:10,flexDirection:'row',marginVertical:5},
leftDiv : {flex:0.5,backgroundColor:'#d1d9de',paddingVertical:8},
rightDiv : {flex:0.5,backgroundColor:'#fff',paddingVertical:8,},
secondDiv : {paddingHorizontal:10,flexDirection:'row',marginVertical:5},
});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App