import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,BackHandler,NetInfo} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from '../sideBar';
var molpay = require("molpay-mobile-xdk-reactnative-beta");
export default class forthStep extends Component {

    static navigationOptions = {
     header: null ,
  };
  constructor(props){
    super(props);
    global.count = 0;
    this.state ={
      taskLocatio:"",
      taskAddress:"",
      taskZipcode:"",
       date_in: '',
  date_out: '2046-10-01',
  error : "",
  taskFromHome : false,
  value :"",
  tastDate : "Pay to yelloTasker before task starts",
  taskOnDate : false,
  isShowPayment : false,
  molPayresult:[],
  
  isTrue : false,
    }
  }
  componentDidMount(){
   
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);

}
componentWillUnmount() {
  //Forgetting to remove the listener will cause pop executes multiple times
  BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
}
handleBack() {
  global.count = global.count+1;
  //alert('count'+global.count);
  if(global.count>1){
    Alert.alert(
      'Confirmation',
      'Are sure want to quit this App',
      [
        {text: 'OK', onPress: () => BackHandler.exitApp()},
      ]
  );
  }else{
    //this.setState({touchMsg : true});
    
    global.setTimeout(() => {
      global.count = 0;
    }, 1000);
  }
    
  return true;
  
    }
  handleOnFromHome(value){
    this.setState({value:value})
    this.setState({taskFromHome:true})
}
handleOnRemote(value){
  this.setState({value:value})
  this.setState({taskFromHome:false})
}
handleOnTommorrow(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:false})
  let d  = new Date();
  let n = d.getDate()+1;
  n = n <10  ? "0" + n : n;
  let mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
  let datass = d.getFullYear()+'-'+mm+'-'+n;
 // alert(datass);
 this.setState({date_in:datass});
 this.setState({taskShowDate:true})
 
}
handleOnToday(value){
  this.setState({tastDate:value});
  this.setState({isShowPayment : false})
}
handleOnWeek(value){
  this.setState({tastDate:value});
  this.getMolPay();
  this.setState({isShowPayment : true})
}
handleOnCertainDate(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:true})
  this.setState({taskShowDate:false})
  
}
    onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
   const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }

  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
    
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
    onMyProfilePressed(){
      let userId = global.userId;
      // alert('userId'+userId);
     const { navigate } = this.props.navigation;
     if(userId){
     
      navigate('Profile'); 
      }else{
     Alert.alert(
        'Message',
        'Please Login to go to Profile',
        [
        {text : 'OK', onPress:()=>{this.onLoginPressed()}}
        ]
       );
      }
    }
    
   onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
    onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onNextStep(){
 if(this.state.tastDate==""){
this.setState({error: ' Please enter required fields'});
  }else{
    this.updateTask();
   // global.taskLocatio = this.state.taskLocatio;
   // const { navigate } = this.props.navigation;
 //  navigate('Browse');
}  
}
closeDrawer = () => {
  this.drawer._root.close()
  };
  openDrawer = () => {
  this.drawer._root.open()
  };
  onBackPressed(){
    const { navigate } = this.props.navigation;
    navigate('Mainview'); 
 }
 async getMolPay(){
  
  try {
    //this.setState({LodingStatus: true});
    const { params } = this.props.navigation.state;
    var taskId =params.taskId;
    var amount = params.amount;
    var userId = global.userId;
    console.log(taskId,amount,userId)
    this.setState({visible: true});
    let response  = await fetch('http://api.yellotasker.com/api/v1/molpay', {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify(
        {
          "userId": userId,
          "taskId": taskId,
          "amount": amount
        }
       )
     });
      
     let res  = await response.text();
     this.setState({visible: false});
     if(response.status >=200){
       let formErrorsss = JSON.parse(res);
      // this.setState({LodingStatus: false});
      console.log(res);
      this.setState({molPayresult:formErrorsss.data});
      // alert("res token: " + res);
       if(formErrorsss.code==500){
      
       
       }
      
     }else{
         let errors = res;
         throw errors;
     }
  } catch (errors) {
         console.log("catch errors " + errors);

      //   this.setState({errors: errorsArray});
  }
 }
 buttonClicked(){
   var result = this.state.molPayresult;
   console.log(result);
 // result = result.data;
  console.log(result.amount);
  //console.log(result.oid​);
  var paymentDetails = {
    // Mandatory String. A value more than '1.00'
    'mp_amount': result.amount,
    // Mandatory String. Values obtained from MOLPay
    'mp_username': 'api_SB_yellotasker',
    'mp_password': 'api_9KDXWVkc',
    'mp_merchant_ID': 'SB_yellotasker',
    'mp_app_name': 'yellotasker',
    'mp_verification_key': '6b959200bb1233fce28e069422e23ece',
    // Mandatory String. Payment values
    'mp_order_ID': result.oid,
    'mp_currency': 'MYR',
    'mp_country': 'MY',
    // Optional String.
    'mp_channel': '', // Use 'multi' for all available channels option. For individual channel seletion, please refer to "Channel Parameter" in "Channel Lists" in the MOLPay API Spec for Merchant pdf. 
    'mp_bill_description': result.bill_desc,
    'mp_bill_name': result.bill_name,
    'mp_bill_email': result.bill_email,
    'mp_bill_mobile': result.bill_mobile,
    // 'mp_channel_editing': false, // Option to allow channel selection.
    'mp_editing_enabled': true, // Option to allow billing information editing.
    // Optional for Escrow
    'mp_is_escrow': '', // Optional for Escrow, put "1" to enable escrow
    // Optional for credit card BIN restrictions
    'mp_bin_lock': ['414170', '414171'], // Optional for credit card BIN restrictions
    'mp_bin_lock_err_msg': 'Only UOB allowed', // Optional for credit card BIN restrictions
    // For transaction request use only, do not use this on payment process
    'mp_transaction_id': '', // Optional, provide a valid cash channel transaction id here will display a payment instruction screen.
    'mp_request_type': '', // Optional, set 'Status' when doing a transactionRequest
    // Optional, use this to customize the UI theme for the payment info screen, the original XDK custom.css file is provided at Example project source for reference and implementation.
    'mp_custom_css_url': '',
    // Optional, set the token id to nominate a preferred token as the default selection, set "new" to allow new card only
    'mp_preferred_token': '',
    // Optional, credit card transaction type, set "AUTH" to authorize the transaction
    'mp_tcctype': '',
    // Optional, set true to process this transaction through the recurring api, please refer the MOLPay Recurring API pdf  
    'mp_is_recurring': false,
    // Optional for channels restriction 
    'mp_allowed_channels': [],
    // Optional for sandboxed development environment, set boolean value to enable.
    'mp_sandbox_mode': false,
    // Optional, required a valid mp_channel value, this will skip the payment info page and go direct to the payment screen.
    'mp_express_mode': false,
    'mp_dev_mode': true,
    // 'mp_timeout' : 30,
    // "mp_bill_description_edit_disabled": true,
};
  var c = this;
  // start molpay payment
  const { navigate } = this.props.navigation;
  const { params } = this.props.navigation.state;
  molpay.startMolpay(paymentDetails,function(data){
      //callback after payment success
      c.setState({
        string : data,
        isTrue : true
      })
      console.log(data);
     
navigate('Transaction',{molPayresult : data,taskData :params }); 
  });
var res = this.state.isTrue;
if(res){
  const { navigate } = this.props.navigation;
  const { params } = this.props.navigation.state;
navigate('Transaction',{molPayresult : this.state.data,taskData :params }); 
}
  
}
 async updateTask() {
  try {
    //this.setState({LodingStatus: true});
    const { params } = this.props.navigation.state;
    var taskId =params.taskId;
    this.setState({visible: true});
    let response  = await fetch('http://api.yellotasker.com/api/v1/updatePostTask', {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
        paymentMode :this.state.tastDate,
        taskId:taskId,
        title : params.title
         //password_conformation: this.state.password_conformation

       })
     });
     let res  = await response.text();
     this.setState({visible: false});
     console.log(response.status);
     console.log(JSON.stringify(res));
     if(response.status >=200){
       let formErrorsss = JSON.parse(res);
      // this.setState({LodingStatus: false});
      // alert("res token: " + res);
       if(formErrorsss.code==500){
       }
       if(formErrorsss.status==1){
        const { navigate } = this.props.navigation;
        navigate('Dashboard');
        //alert(result.data.id);
       }
     }else{
         let errors = res;
         throw errors;
     }
  } catch (errors) {
         console.log("catch errors " + errors);

      //   this.setState({errors: errorsArray});
  }
}
  render() {
    return(
      <Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >
<View style={styles.mainviewStyle}>
<View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
<View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
<Text>


</Text>

</View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
<Image source={require('../img/app-logo.png')} />
</Text>
</View>
<View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
<TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>

<FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>

</TouchableHighlight>
</View>

</View>
 <ScrollView style = {styles.scrollViewStyle}>
       <View style = {styles.formViewStyle} >      
        <Text style={styles.errorText}>{this.state.error}</Text>     
        <Text style={{textAlign:'center',fontWeight : 'bold'}}>Your Post Succesfully added</Text>
         
<View style={styles.secRadioDiv}>
              <View style={{paddingHorizontal:5,}}> 
               <RadioButton currentValue={this.state.tastDate} innerCircleColor ='#d1d3d4' value={'Pay to yelloTasker before task starts'} onPress={this.handleOnToday.bind(this)} outerCircleSize={20} innerCircleSize={10}>
                <Text style={styles.radioText}>You release payment at the time of assigning {"\n"} task to offers posted..</Text>
                 </RadioButton>
                 </View>
              
                
                 
        </View>
        <View style={styles.secRadioDiv}>
              <View style={{paddingHorizontal:5,}}>
               <RadioButton currentValue={this.state.tastDate} innerCircleColor ='#d1d3d4' value={'Release payment once done'} onPress={this.handleOnWeek.bind(this)} outerCircleSize={20} innerCircleSize={10}>
                <Text style={styles.radioText}>You are releasing payment now.</Text>
                 </RadioButton>
                 </View>
              
                
                 
        </View>
     


        
         <View style={styles.buttonDiv}>
      
         <Text style={styles.extrasText}>
           For your safety, please do not share personal information, e.g., email, phone or address.    
            </Text>
        </View>
        {this.state.isShowPayment && <View style={{marginTop:'55%',marginBottom:'5%'}}>
          <TouchableHighlight style={styles.molPayDiv} onPress={this.buttonClicked.bind(this)}>
        <Text style={styles.molPayText}>
             Pay With Molpay 
                        </Text>
     </TouchableHighlight>
        </View>}
          <View style={styles.buttonsDiv}>
                  <TouchableHighlight style={styles.LoginDiv} underlayColor='#efeb10' onPress={this.onNextStep.bind(this)}>
                       <Text style={styles.extraText}>Finish</Text>
                  </TouchableHighlight>
          </View>

     </View>
          
 </ScrollView>

 <View style={styles.footer}>
 <View style={styles.browseButtons}>
 <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
   <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
   </TouchableHighlight>
     <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
 </View>
 <View style={styles.postButtons}>
       <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
                     
                  <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.plusSquare}</FontAwesome>
                    
                     </TouchableHighlight>
                    <Text style={[styles.footerText,{color:'#efeb10'}]} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
                </View>
 <View style={styles.myTaskButtons}>
 <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
   <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
     </TouchableHighlight>
          <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
      </View>
  {/* <View style={styles.messageButtons}>
   <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>
      
     <Text style={styles.footerText}>Message</Text>
 </View> */}
 <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
 </View>
      </View>
      </Drawer>
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},
footer: {
  
   position: 'absolute',
  flex:0.1,
  left: 0,
  right: 0,
  bottom: -10,
  backgroundColor:'#000000',
  flexDirection:'row',
  height:70,
  alignItems:'center',
},
postButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
browseButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
myTaskButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,
   
},
messageButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   
},
footerText: {
  color:'white',
  alignItems:'center',
  fontSize:14,
 

},
textStyle: {
  alignSelf: 'center',
  color: 'orange'
},
scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
      
},
bannerImg : {
  width:'100%',
  height : 130,
},
formViewStyle : {
  flex:1,
  paddingVertical : '3%',
  paddingHorizontal : '3%',
},
buttonDiv : {
     paddingVertical : '1%',
    },
    secondButtonDiv : {
      paddingVertical : '2%',
     },
radioDiv : {
  flex:1,
  flexDirection:'row',
  paddingVertical : '1%',
},
secRadioDiv:{
  flex:1,
  flexDirection:'row',
  paddingVertical : '2%',
},
buttonsDiv : {
   paddingVertical : '2%',
   marginBottom : 70,
},
extrasText : {
  color : '#000000',
      fontSize: 16,
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
   textAlignVertical: 'top',
},

LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 10, 
    borderRadius:25,
    borderWidth: 1,
    borderColor: '#efeb10',

},
molPayDiv : {
  backgroundColor : '#000',
  paddingVertical : 10, 
    borderRadius:25,
    borderWidth: 1,
    borderColor: '#000',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
      fontWeight:'normal',
},
molPayText : {
  color : '#fff',
  textAlign: 'center',
      fontSize: 18,
      fontWeight:'normal',
},
 errorText : {
color : '#dd4b39',
paddingTop : 5,
  },
  abc :{
    color:'#d1d3d4',
      },
      radioContainer : {
          borderWidth : 2,
          borderColor : '#d1d3d4',
      },
      radioOuterDiv : {
        flex:0.5,
         borderWidth : 1,
          borderColor : 'gray',
          paddingVertical:5,
          paddingHorizontal:5,
        borderRadius:5,
      },
      radioText : {
        paddingHorizontal:7,
       
        textAlignVertical : 'center',
      },
      radioSecOuterDiv : {
        marginLeft:3,
        flex:0.5,
        borderWidth : 1,
         borderColor : 'gray',
         paddingVertical:5,
         paddingHorizontal:5,
       borderRadius:5,
      }

});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App