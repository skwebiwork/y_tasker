import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,Keyboard,Dimensions,TextInput,Button,Alert,AsyncStorage,BackAndroid,NetInfo,BackHandler} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon,Badge } from 'native-base';
import SideBar from '../sideBar';
var {width, height} = Dimensions.get('window');
export default class mainview extends Component {

  static navigationOptions = {
     header: null ,
  };
  constructor(){
    super();
    global.count = 0;
    this.state ={
      taskTitle:"",
      taskDescription:"",
      error:"",
      wordCount : 5000,
      touchMsg : false,
      visible: false,
      LodingStatus : false,
      taskData: [],
      describeValiMsg : false,
      taskValiMsg : false,
      status : "",
      taskHeadingPlaceHolder:"e.g. Clean my 2 bedroom apartment",
      taskDescriptionPlaceHolder:"e.g. Clean my 2 bedroom apartment",
    } 
  }
  componentDidMount(){
    let userId = global.userId;
   const { navigate } = this.props.navigation;
   const { params } = this.props.navigation.state;
   let category_id= global.category_id;
  if(category_id){
    this.setState({taskHeadingPlaceHolder : global.category_title});
    this.setState({taskDescriptionPlaceHolder : global.category_description});
  }
    if(userId){
      
    // navigate('Mainview'); 
     }else{
      
    //navigate('Login'); 
     }
     BackHandler.addEventListener('hardwareBackPress', this.handleBack);
     NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
       NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({ status: isConnected }); }
       );
       if(global.taskTitle){
        // alert(global.taskTitle);
        this.setState({taskTitle : global.taskTitle});
       }
       if(global.taskDescription){
         this.setState({taskDescription:global.taskDescription});
        //this.state.taskDescription;
       }
  }
  componentWillUnmount() {
		//Forgetting to remove the listener will cause pop executes multiple times
		BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
    }
    handleBack() {
      global.count = global.count+1;
      if(global.count>1){
        Alert.alert(
        'Confirmation',
        'Are sure want to quit this App',
        [
          {text: 'OK', onPress: () => BackHandler.exitApp()},
        ]
      );
      }else{  
        global.setTimeout(() => {
        global.count = 0;
        }, 1000);
      }		  
      return true;		
        }
      handleConnectionChange = (isConnected) => {
      this.setState({ status: isConnected });
       let networkInfo = this.state.status;
       if(isConnected){
    //	this.setState({ visible: false });
      this.setState({ netInfoMsg: false });	   
       }else{
    //	this.setState({ visible: true });
      this.setState({ netInfoMsg: true });		
       }
    }
onNextStep(){
  var taskTitle = this.state.taskTitle;
  var taskDescription = this.state.taskDescription;
  //alert(taskTitle.length);
  if((taskTitle.length)==""){
this.setState({error: 'Please enter task title fields'});
this.setState({taskValiMsg : true});
  }else if((taskTitle.length)<15){
    this.setState({error : 'required minimum 15 characters'});
    this.setState({taskValiMsg : true});
 }else if((taskDescription.length)<15){
  this.setState({error : 'task description required minimum 15 characters'});
  this.setState({describeValiMsg : true});
 }else if(this.state.taskDescription==""){
  this.setState({describeValiMsg : true});
this.setState({error: 'Please enter task description  fields'});
  }else{
     global.taskTitle = this.state.taskTitle;
   global.taskDescription = this.state.taskDescription;
   const { navigate } = this.props.navigation;
   navigate('SecondStep');
  }
}
onTaskPressed(){
  let userId = global.userId;
 // alert('userId'+userId);
 const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
}
onLoginPressed(){
  const { navigate } = this.props.navigation;
navigate('Login');
}
onBrowsePressed(){
   const { navigate } = this.props.navigation;
   navigate('Browse'); 
}
onMyTaskPressed(){
  let userId = global.userId;
   // alert('userId'+userId);
  const { navigate } = this.props.navigation;
  if(userId){
  
   navigate('MyTask'); 
   }else{
  Alert.alert(
     'Message',
     'Please Login to go to My task',
     [
     {text : 'OK', onPress:()=>{this.onLoginPressed()}}
     ]
    );
   }
  }
  onMyProfilePressed(){
    let userId = global.userId;
    // alert('userId'+userId);
   const { navigate } = this.props.navigation;
   if(userId){
   
    navigate('Profile'); 
    }else{
   Alert.alert(
      'Message',
      'Please Login to go to Profile',
      [
      {text : 'OK', onPress:()=>{this.onLoginPressed()}}
      ]
     );
    }
  }
  changeWordFun=(val)=>{
    this.setState({taskDescription: val});
  //let word = this.state.wordCount;
  let remaining = 5000-val.length;
    this.setState({wordCount : remaining })
    if(val.length<15){
       this.setState({error : 'task description required minimum 15 characters'});
       this.setState({describeValiMsg : true});

    }else{
      this.setState({error : ''});
       this.setState({describeValiMsg : false});
    }
  }
  taskChangeFun = (val)=>{
   this.setState({taskTitle: val});
    if(val.length<5){
      this.setState({error : 'task title required minimum 5 characters'});
      this.setState({taskValiMsg : true});

   }else{
     this.setState({error : ''});
      this.setState({taskValiMsg : false});
   }
    
  }
  onBackPressed(){
    const { navigate } = this.props.navigation;
    navigate('Dashboard'); 
 }
  closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
  render() {
    return(
      <Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >	
      <View style={styles.mainviewStyle}>
      <View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
      <View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
      <Text>
    
     
      </Text>
      
      </View>
   <View  style={{flex:0.6,alignItems:'center',}}>
      <Text onPress={() => this.props.navigation.navigate("Dashboard")}>
         <Image source={require('../img/app-logo.png')} />
      </Text>
      </View>
      <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
      <TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
     
      <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
     
      </TouchableHighlight>
      </View>
   
   </View>

        <ScrollView style = {styles.scrollViewStyle}>
      
               <View style = {styles.formViewStyle} >
                <View style={{flex:1,flexDirection:'column',paddingTop:10}}>
                <View style={{paddingVertical:2,backgroundColor:'#ccc',marginBottom:-18,}}></View>
                   <View style={{flexDirection:'row', }}>
                        <View style={{ width:'33.33%',alignItems:'center',}}>
                        <View>
                          <Badge style={{ backgroundColor: 'yellow',height:28,width:28,alignItems:'center' }}>
                              <Text style={{ color: '#000', }}>1</Text>
                      </Badge>
                      </View>
                              <Text style={{fontSize:16,paddingVertical:5,}}>Details</Text>
                        </View>
                        <View style={{ width:'33.33%',alignItems:'center',}}>
                        <View>
                          <TouchableHighlight onPress={this.onNextStep.bind(this)} underlayColor= "transparent">
                        <Badge style={{ backgroundColor: 'black',height:28,width:28,alignItems:'center' }} onPress={this.onNextStep.bind(this)}> 
                      <Text style={{ color: '#fff',}} >2</Text>
                      </Badge>
                      </TouchableHighlight>
                      </View>
                      <Text style={{fontSize:16,paddingVertical:5}}>Location</Text>
                        </View>
                        <View style={{ width:'33.33%',alignItems:'center',}}>
                        <View>
                        <Badge style={{ backgroundColor: 'black',height:28,width:28,alignItems:'center' }}>
                      <Text style={{ color: '#fff', }}>3</Text>
                      </Badge>
                      </View>
                      <Text style={{fontSize:16,paddingVertical:5}}>Budget</Text>
                        </View>

                   </View>
                   

                </View>

                     <Text style={styles.errorText}>{this.state.error}</Text>
                        <View style={styles.buttonDiv}>
      
         <Text style={styles.extrasText}>
         What task do you need done?     
            </Text>
        </View>
       <View style={styles.buttonDiv}>
          <TextInput
          placeholder={this.state.taskHeadingPlaceHolder} style={[styles.InputText,this.state.taskValiMsg && styles.textInputAlt]} 
          underlineColorAndroid = "transparent" returnKeyType="next" 
          onChangeText={(val) => this.taskChangeFun(val)}
          keyboardType="default"
          autoCapitalize="none"
          autoCorrect={false}
            onSubmitEditing={()=>this.descriptionInput.focus()}
            value={this.state.taskTitle}
            autoFocus={true}
          ></TextInput>
       </View>
       <View style={styles.buttonDiv}>
      
         <Text style={styles.extrasText}>
            Describe your task in more detail      
            </Text>
        </View>
       <View style={styles.buttonDiv}>
      <TextInput
      
      multiline={true} numberOfLines={6}
     style={[styles.SecondInputText, this.state.describeValiMsg && styles.textInputAlt]} 
       placeholder={this.state.taskDescriptionPlaceHolder}  
       underlineColorAndroid = "transparent" returnKeyType="done" 
       onChangeText={(val) => this.changeWordFun(val)}
      keyboardType="default"
      autoCapitalize="none"
      autoCorrect={false}
       ref={(input)=>this.descriptionInput=input}
       maxLength={5000}
       value={this.state.taskDescription}
       blurOnSubmit={true}
    
       onSubmitEditing={Keyboard.dismiss}
       ></TextInput>
       </View>
       <View>
            <Text style={{textAlign:'right',fontSize: 14,}}>{this.state.wordCount} characters remaining</Text>
       </View>

          <View style={styles.buttonDivSec}>
      
         <Text style={styles.extrasText}>
           For your safety, please do not share personal information, e.g., email, phone or address.    
            </Text>
        </View>
      </View>
          
        </ScrollView>
        <View style={styles.buttonssDiv}>
      
      <TouchableHighlight style={styles.LoginDiv} onPress={this.onNextStep.bind(this)}>
        <Text style={styles.extraText}>
             Continue  
                        </Text>
     </TouchableHighlight>
      </View>
        <View style={styles.footer}>
                <View style={styles.browseButtons}>
                <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
                  </TouchableHighlight>
                    <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
                </View>
                <View style={styles.postButtons}>
       <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
                     
                  <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.plusSquare}</FontAwesome>
                    
                     </TouchableHighlight>
                    <Text style={[styles.footerText,{color:'#efeb10'}]} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
                </View>
                <View style={styles.myTaskButtons}>
                <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
                    </TouchableHighlight>
                         <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
                     </View>
                 {/* <View style={styles.messageButtons}>
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>
                     
                    <Text style={styles.footerText}>Message</Text>
                </View> */}
                <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
                </View>
      </View>
    </Drawer>
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},
mainStyle : {
      flex :0.9,
      flexDirection:'column',
},
scrollViewStyle: {
  
  borderTopWidth : 4,
      borderColor: '#efeb10',
},
bannerImg : {
  width:'100%',
  height : 130,
},
formViewStyle : {

  paddingVertical : '3%',
  paddingHorizontal : '3%',
},
buttonDiv : {
     paddingVertical : '1%',
    },
    buttonDivSec : {
      paddingVertical : 15,
     },
buttonsDiv : {
   paddingVertical : '2%',
   marginBottom : 70,
},buttonssDiv : {
  
  position: 'absolute',
  flex:1,
  left: 0,
  right: 0,
  bottom: 10,
  height:100,
  paddingHorizontal : '3%',
},
extrasText : {
  color : '#000000',
      fontSize: 16,
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
   
},
'@media (max-width: 360)': {
  SecondInputText : {
    paddingVertical : 12,
    borderRadius:5,
    borderColor: 'gray',
     borderWidth: 1,
     paddingHorizontal: 5,
     marginBottom : 4,
    textAlignVertical: 'top',
    minHeight:120,
    
  },
},
SecondInputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 4,
  textAlignVertical: 'top',
  minHeight:200,
  
},
LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 10, 
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#efeb10',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
      fontWeight:'normal',
},
 errorText : {
color : '#dd4b39',
paddingTop : 5,
  },

footer: {
   position: 'absolute',
  flex:0.1,
  left: 0,
  right: 0,
  bottom: -10,
  backgroundColor:'#000000',
  flexDirection:'row',
  height:70,
  alignItems:'center',
  
},
postButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
browseButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
myTaskButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,
},
messageButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
},
footerText: {
  color:'white',
  alignItems:'center',
  fontSize:14,
},
textStyle: {
  alignSelf: 'center',
  color: 'orange'
},
textInputAlt : {
  borderColor: '#dd4b39',
},
});
//AppRegistry.registerComponent('mainview', () => mainview) //Entry Point    and Root Component of The App