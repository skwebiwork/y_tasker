import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,BackHandler,NetInfo} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon,Badge } from 'native-base';
import SideBar from '../sideBar';

export default class secondStep extends Component {

    static navigationOptions = {
     header: null ,
  };
  constructor(props){
    super(props);
    global.count = 0;
    this.state ={
      taskLocatio:"",
      taskAddress:"",
      taskZipcode:"",
       date_in: '',
  date_out: '2046-10-01',
  error : "",
  taskFromHome : false,
  value :"",
  tastDate : "",
  taskOnDate : false,
  mindate : '',
    }
  }
  componentDidMount(){
   
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    if(global.taskAddress){
      this.setState({taskAddress:global.taskAddress});
    }
    if(global.taskZipcode){
      this.setState({taskZipcode:global.taskZipcode});
    }
    if(global.taskDueDate){
      this.setState({date_in:global.taskDueDate});
    }
    if(global.locationType){
      this.setState({value : global.locationType});
    }
    if(global.tastDate){
      this.setState({tastDate:global.tastDate});
      
     // this.setState({taskOnDate:true})
     if(global.tastDate=='By a certain date'){
      this.setState({taskOnDate:true})

     }else{
      this.setState({taskShowDate:true})
      
     }
    }

}
componentWillUnmount() {
  //Forgetting to remove the listener will cause pop executes multiple times
  BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  
 // global.taskAddress = this.state.taskAddress;
    //global.taskZipcode = this.state.taskZipcode;
    //global.taskDueDate = this.state.date_in;
    //global.locationType = this.state.value;
    this.minDateFun();
}
handleBack() {
  global.count = global.count+1;
  //alert('count'+global.count);
  if(global.count>1){
    Alert.alert(
      'Confirmation',
      'Are sure want to quit this App',
      [
        {text: 'OK', onPress: () => BackHandler.exitApp()},
      ]
  );
  }else{
    //this.setState({touchMsg : true});
    
    global.setTimeout(() => {
      global.count = 0;
    }, 1000);
  }
    
  return true;
  
    }
    minDateFun(){
      let d  = new Date();
      let n = d.getDate();
      n = n <10  ? "0" + n : n;
      let mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
      //let datass = d.getFullYear()+'-'+mm+'-'+n;   yyyy-mm-dd
      let datass = n+'-'+mm+'-'+d.getFullYear();
      this.setState({mindate : datass});
    }
  handleOnFromHome(value){
    this.setState({value:value})
    this.setState({taskFromHome:true})
}
handleOnRemote(value){
  this.setState({value:value})
  this.setState({taskFromHome:false})
}
handleOnTommorrow(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:false})
  var tomorrow = this.addDays(new Date(), 1);
  let d  = new Date();
  let n = d.getDate()+1;
  n = n <10  ? "0" + n : n;
  let mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
  //let datass = d.getFullYear()+'-'+mm+'-'+n;   yyyy-mm-dd
  let datass = n+'-'+mm+'-'+d.getFullYear();
 // alert(datass);
 this.setState({date_in:datass});
 this.setState({taskShowDate:true})
 
}
handleOnToday(value){
  this.setState({taskShowDate:true})
  
  this.setState({tastDate:value})
  this.setState({taskOnDate:false})
  //let currntDate = new Date().toISOString().slice(0,10);

  // currntDate = currntDate.getDate()+1+'-'+currntDate.getMonth()+1+''+currntDate.getFullYear();
 // alert(currntDate);
 let d  = new Date();
  let n = d.getDate();
  n = n <10  ? "0" + n : n;
  let mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
  //let datass = d.getFullYear()+'-'+mm+'-'+n;   yyyy-mm-dd
  let datass = n+'-'+mm+'-'+d.getFullYear();
 this.setState({date_in:datass});
 
}
handleOnWeek(value){
  this.setState({taskShowDate:true})
  
  this.setState({tastDate:value})
  this.setState({taskOnDate:false})
 // let d  = new Date();
 

var nextWeek = this.addDays(new Date(), 7);
  let n = nextWeek.getDate();
  n = n < 10 ? "0" + n : n;
  let mm = nextWeek.getMonth() < 9 ? "0" + (nextWeek.getMonth() + 1) : (nextWeek.getMonth() + 1);
  
  let datass = n+'-'+mm+'-'+nextWeek.getFullYear();
//  let datass = d.getFullYear()+'-'+mm+'-'+n;
  //let datass = n+'/'+d.getMonth()+'/'+d.getFullYear();
 // alert(datass);
 this.setState({date_in:datass});
 

}
addDays(dateObj, numDays) {
  dateObj.setDate(dateObj.getDate() + numDays);
  return dateObj;
}
handleOnCertainDate(value){
  this.setState({date_in:''});
  this.setState({tastDate:value})
  this.setState({taskOnDate:true})
  this.setState({taskShowDate:false})
  
}
    onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
   const { navigate } = this.props.navigation;
   global.category_id = null;
   navigate('Mainview');
  }

  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
    
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
   onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
    onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onNextStep(){
    if(this.state.value==""){
this.setState({error: 'Please provided required fields'});
    }else if(this.state.tastDate==""){
this.setState({error: ' Please provided required fields'});
  
  }else{
   // global.taskLocatio = this.state.taskLocatio;
    global.taskAddress = this.state.taskAddress;
    global.taskZipcode = this.state.taskZipcode;
    var dt = this.state.date_in;
    console.log('date'+dt);
    var dtArr = dt.split('-');
    global.taskDueDate = dtArr[2]+'-'+dtArr[1]+'-'+dtArr[0];
    global.locationType = this.state.value;
    global.tastDate = this.state.tastDate;
    const { navigate } = this.props.navigation;
    console.log(global.taskDueDate);
    var d = new Date(); // for now
var hh = d.getHours(); // => 9
var mm =d.getMinutes(); // =>  30
var ss = d.getSeconds();
var tt = global.taskDueDate+'T'+hh+':'+mm+':'+ss+'Z';
global.taskDueDate = tt ;
console.log(tt);
  navigate('ThirdStep');
}  
}
onMyProfilePressed(){
  let userId = global.userId;
  // alert('userId'+userId);
 const { navigate } = this.props.navigation;
 if(userId){
 
  navigate('Profile'); 
  }else{
 Alert.alert(
    'Message',
    'Please Login to go to Profile',
    [
    {text : 'OK', onPress:()=>{this.onLoginPressed()}}
    ]
   );
  }
}
closeDrawer = () => {
  this.drawer._root.close()
  };
  openDrawer = () => {
  this.drawer._root.open()
  };
  onBackPressed(){
    const { navigate } = this.props.navigation;
    navigate('Mainview'); 
 }
  render() {
    return(
      <Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >
<View style={styles.mainviewStyle}>
<View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
<View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
<Text>


</Text>

</View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
<Image source={require('../img/app-logo.png')} />
</Text>
</View>
<View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
<TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>

<FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>

</TouchableHighlight>
</View>

</View>
 <ScrollView style = {styles.scrollViewStyle}>
       <View style = {styles.formViewStyle} >   
       <View style={{flex:1,flexDirection:'column',paddingTop:15}}>
                <View style={{paddingVertical:2,backgroundColor:'#ccc',marginBottom:-18,}}></View>
                   <View style={{flexDirection:'row', }}>
                        <View style={{ width:'33.33%',alignItems:'center',}}>
                        <View>
                          <TouchableHighlight onPress={() => this.props.navigation.navigate("Mainview")} underlayColor= "transparent">
                          <Badge style={{ backgroundColor: 'black',height:28,width:28,alignItems:'center' }}>
                              <Text style={{ color: '#fff', }} >1</Text>
                      </Badge>
                      </TouchableHighlight>
                      </View>
                              <Text style={{fontSize:16,paddingVertical:5,}}>Details</Text>
                        </View>
                        <View style={{ width:'33.33%',alignItems:'center',}}>
                        <View>
                        <Badge style={{ backgroundColor: 'yellow',height:28,width:28,alignItems:'center' }} > 
                      <Text style={{ color: '#000',}} >2</Text>
                      </Badge>
                      </View>
                      <Text style={{fontSize:16,paddingVertical:5}}>Location</Text>
                        </View>
                        <View style={{ width:'33.33%',alignItems:'center',}}>
                        <View>
                          <TouchableHighlight onPress={this.onNextStep.bind(this)} underlayColor= "transparent">
                        <Badge style={{ backgroundColor: 'black',height:28,width:28,alignItems:'center' }}>
                      <Text style={{ color: '#fff', }} >3</Text>
                      </Badge>
                      </TouchableHighlight>
                      </View>
                      <Text style={{fontSize:16,paddingVertical:5}}>Budget</Text>
                        </View>

                   </View>
                   

                </View>   
                          <Text style={styles.errorText}>{this.state.error}</Text>     
         {/* <View style = {styles.buttonDiv}>
                <Text style={styles.extrasText}>Where would the task be completed? <Text style={{color:'red'}}>*</Text></Text>
          </View>
          <View style={styles.radioDiv}>
         
              <View style={styles.radioOuterDiv}>
               <RadioButton currentValue={this.state.value} innerCircleColor ='#d1d3d4' value={'Come to work place'} onPress={this.handleOnFromHome.bind(this)} >
                <Text  style={styles.radioText}>Come to work place</Text>
                 </RadioButton>
                 </View>
              <View style={styles.radioSecOuterDiv}>
                 <RadioButton currentValue={this.state.value}  innerCircleColor ='#d1d3d4' value={'Work remotely'} onPress={this.handleOnRemote.bind(this)}>
                 <Text style={styles.radioText }>Work remotely</Text>
                 </RadioButton>
                 </View>
                
                 
        </View> */}
        <View style = {styles.buttonDiv}>
                <Text style={styles.extrasText}>Where would the task be completed? <Text style={{color:'red'}}>*</Text></Text>
          </View>
        <View style={{paddingHorizontal:5,paddingVertical:5,borderRadius:5,borderColor : 'gray',borderWidth : 0.5,}}>
      
          <View style = {styles.buttonDiv}>
        <RadioButton currentValue={this.state.value} outerCircleWidth={1} innerCircleColor ='#efeb10'  outerCircleColor='black' value={'Physical task'} onPress={this.handleOnFromHome.bind(this)} >
        <Text style={{textAlignVertical: "center",paddingHorizontal:6,paddingVertical:1}}>Physical task</Text>
         </RadioButton>
         
         </View>
         <View style = {styles.buttonDiv}>
            <RadioButton currentValue={this.state.value} outerCircleWidth={1} outerCircleColor='#000'  innerCircleColor ='#efeb10' value={'Digital task'} onPress={this.handleOnRemote.bind(this)}>
                 <Text style={{textAlign: "center",paddingHorizontal:6,paddingVertical:1}}>Digital task</Text>
                 </RadioButton>
                 </View>
        </View>
         
        { this.state.taskFromHome && <View style={{paddingVertical:5}}>
         
          <View style = {styles.lablesStyle}>
                 <Text style={styles.extrasText}>Suburb <Text style={{color:'red'}}>*</Text></Text>
          </View>
          <View style={styles.buttonDiv}>
                 <TextInput ref={(input)=>this.addressInput=input}  multiline={true} onChangeText={(text)=>this.setState({taskAddress:text})} numberOfLines={4} style={styles.InputText}  underlineColorAndroid = "transparent" returnKeyType="next"></TextInput>
          </View>
         
         {/* <View style = {styles.lablesStyle}>
                  <Text style={styles.extrasText}>Zipcode</Text>
          </View>

          <View style={styles.buttonDiv}>
                <TextInput placeholder="" ref={(input)=>this.zipcodeInput=input}  onChangeText={(text)=>this.setState({taskZipcode:text})} style={styles.InputText} underlineColorAndroid = "transparent" returnKeyType="next"></TextInput>
          </View>
          */}
          </View>
        }
         <View style = {styles.secondButtonDiv}>
                <Text style={styles.extrasText}>Need task completed by <Text style={{color:'red'}}>*</Text> </Text>

          </View>
          <View style={{paddingHorizontal:5,paddingVertical:5,borderRadius:5,borderColor : 'gray',borderWidth : 0.5,}}>
      
      <View style = {styles.buttonDiv}>
      <RadioButton currentValue={this.state.tastDate} innerCircleColor ='#efeb10' outerCircleWidth={1}  outerCircleColor='black' value={'Today'} onPress={this.handleOnToday.bind(this)}>
                <Text style={styles.radioTextStyle}>Today</Text>
                 </RadioButton>
     </View>
     <View style = {styles.buttonDiv}>
                <RadioButton currentValue={this.state.tastDate} outerCircleWidth={1} innerCircleColor ='#efeb10' outerCircleWidth={1} outerCircleColor='black' value={'Tommorow'} onPress={this.handleOnTommorrow.bind(this)}>
                 <Text style={styles.radioTextStyle}>Tomorrow</Text>
                 </RadioButton>
      </View>
      <View style = {styles.buttonDiv}>
      <RadioButton currentValue={this.state.tastDate} innerCircleColor ='#efeb10'  outerCircleWidth={1} outerCircleColor='black' value={'Within 1 week'} onPress={this.handleOnWeek.bind(this)}>
      <Text style={styles.radioTextStyle}>Within 1 week</Text>
       </RadioButton>
      </View>
      <View style = {styles.buttonDiv}>
      <RadioButton currentValue={this.state.tastDate}  innerCircleColor ='#efeb10' outerCircleWidth={1} outerCircleColor='black' value={'By a certain date'} onPress={this.handleOnCertainDate.bind(this)}>
      <Text style={styles.radioTextStyle}>By a certain date</Text>
      </RadioButton>
      </View>
    </View>


          {/* 
<View style={styles.secRadioDiv}>
              <View style={styles.radioOuterDiv}>
               <RadioButton currentValue={this.state.tastDate} innerCircleColor ='#d1d3d4' value={'Today'} onPress={this.handleOnToday.bind(this)}>
                <Text style={styles.radioText}>Today</Text>
                 </RadioButton>
                 </View>
              <View style={styles.radioSecOuterDiv}>
                 <RadioButton currentValue={this.state.tastDate}  innerCircleColor ='#d1d3d4' value={'Tommorow'} onPress={this.handleOnTommorrow.bind(this)}>
                 <Text style={styles.radioText}>Tomorrow</Text>
                 </RadioButton>
                 </View>
                
                 
        </View>
        <View style={styles.secRadioDiv}>
              <View style={styles.radioOuterDiv}>
               <RadioButton currentValue={this.state.tastDate} innerCircleColor ='#d1d3d4' value={'Within 1 week'} onPress={this.handleOnWeek.bind(this)}>
                <Text style={styles.radioText}>Within 1 week</Text>
                 </RadioButton>
                 </View>
              <View style={styles.radioSecOuterDiv}>
                 <RadioButton currentValue={this.state.tastDate}  innerCircleColor ='#d1d3d4' value={'By a certain date'} onPress={this.handleOnCertainDate.bind(this)}>
                 <Text style={styles.radioText}>By a certain date</Text>
                 </RadioButton>
                 </View>
                
                 
        </View>
        */}
        {this.state.taskShowDate && <View style={{paddingVertical:5}}>
          
          <View style = {styles.lablesStyle}>
                  <Text style={styles.extrasText}>Due date</Text>
          </View>
             <View style={styles.buttonDiv}>  
             <TextInput value={this.state.date_in} style={styles.InputText} 
       underlineColorAndroid = "transparent" editable={false} selectTextOnFocus={false}></TextInput>
             </View>
          
          </View>
          }


        { this.state.taskOnDate && <View style={{paddingVertical:5}}>
{/* <View style={styles.buttonDiv}>
      
         <Text style={styles.extrasText}>
          We value your work details and contact number so, we promise we will not share contact details untill or less a user has shown interest in your task.   
            </Text>
        </View> */}
        <View style = {styles.lablesStyle}>
                  <Text style={styles.extrasText}>Due date</Text>
          </View>
             <View style={styles.buttonDiv}>     
 <DatePicker
      style ={{width: '100%',borderRadius:5,borderColor: 'gray',}}
      date={this.state.date_in}
      mode="date"
      format="DD-MM-YYYY"
      minDate={new Date()}
      maxDate="31-12-2046"
      mode="date"
     placeholder="Select Date"
      showIcon={true}
      ref={(input)=>this.dateInput=input}
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      customStyles={{
       dateInput: {
          alignItems : 'flex-start',
          padding:7,borderRadius:5,  
      },
     }}
    onDateChange={(date_in) => {this.setState({date_in: date_in});}}/>
</View>
</View>
        }
         <View style={styles.buttonDiv}>
      
         <Text style={styles.textFooter}>
           For your safety, please do not share personal information, e.g., email, phone or address.    
            </Text>
        </View>
       
          <View style={styles.buttonsDiv}>
                  <TouchableHighlight style={styles.LoginDiv} underlayColor='#efeb10' onPress={this.onNextStep.bind(this)}>
                       <Text style={styles.extraText}>Continue</Text>
                  </TouchableHighlight>
          </View>
       
     </View>
          
 </ScrollView>{/*
 { !this.state.taskFromHome &&
          <View style={styles.buttonssDiv}>
                  <TouchableHighlight style={styles.LoginDiv} underlayColor='#efeb10' onPress={this.onNextStep.bind(this)}>
                       <Text style={styles.extraText}>Continue</Text>
                  </TouchableHighlight>
          </View>
        }
        */}
 <View style={styles.footer}>
                <View style={styles.browseButtons}>
                <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
                  </TouchableHighlight>
                    <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
                </View>
                <View style={styles.postButtons}>
       <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
                     
                  <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.plusSquare}</FontAwesome>
                    
                     </TouchableHighlight>
                    <Text style={[styles.footerText,{color:'#efeb10'}]} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
                </View>
                <View style={styles.myTaskButtons}>
                <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
                    </TouchableHighlight>
                         <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
                     </View>
                 {/* <View style={styles.messageButtons}>
                  <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>
                     
                    <Text style={styles.footerText}>Message</Text>
                </View> */}
                <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
                </View>
      </View>
      </Drawer>
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},
footer: {
  
   position: 'absolute',
  flex:0.1,
  left: 0,
  right: 0,
  bottom: -10,
  backgroundColor:'#000000',
  flexDirection:'row',
  height:70,
  alignItems:'center',
},
postButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
browseButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
myTaskButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,
   
},
messageButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   
},
footerText: {
  color:'white',
  alignItems:'center',
  fontSize:14,
 

},
textStyle: {
  alignSelf: 'center',
  color: 'orange'
},
scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
      
},
bannerImg : {
  width:'100%',
  height : 130,
},
formViewStyle : {
  flex:1,
  paddingVertical : '3%',
  paddingHorizontal : '3%',
},
buttonDiv : {
     paddingVertical : '1%',
    },
    secondButtonDiv : {
      paddingVertical : '2%',
     },
radioDiv : {
  flex:1,
  flexDirection:'row',
  paddingVertical : '1%',
},
secRadioDiv:{
  flex:1,
  flexDirection:'row',
  paddingVertical : '2%',
},
buttonsDiv : {
   paddingVertical : '3%',
   marginBottom : 60,
},
buttonssDiv : {
  
  position: 'absolute',
  flex:1,
  left: 0,
  right: 0,
  bottom: 10,
  height:100,
  paddingHorizontal : '3%',
},
extrasText : {
      color : '#000000',
      fontSize: 16,
      paddingVertical:4
},
textFooter:{
  color : '#000000',
  fontSize: 15,
  paddingVertical:8
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
   textAlignVertical: 'top',
},

LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 10, 
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#efeb10',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
      fontWeight:'normal',
},
 errorText : {
color : '#dd4b39',
paddingTop : 5,
  },
  abc :{
    color:'#d1d3d4',
      },
      radioContainer : {
          borderWidth : 2,
          borderColor : '#d1d3d4',
      },
      radioOuterDiv : {
        flex:0.5,
         borderWidth : 1,
          borderColor : 'gray',
          paddingVertical:5,
          paddingHorizontal:5,
        borderRadius:5,
      },
      radioText : {
        paddingHorizontal:7,
       
        justifyContent : 'center',
      },
      radioSecOuterDiv : {
        marginLeft:3,
        flex:0.5,
        borderWidth : 1,
         borderColor : 'gray',
         paddingVertical:5,
         paddingHorizontal:5,
       borderRadius:5,
      },
      radioTextStyle : {
        textAlignVertical: "center",
        paddingHorizontal:6,
        paddingVertical:1
      }

});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App