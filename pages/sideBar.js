import React, { Component } from 'react';
import {View,StyleSheet,TouchableHighlight,ScrollView,AppRegistry,Alert,Image,TextInput,Button,AsyncStorage,Modal,Dimensions,ListView} from "react-native";
//import{Content,Card,CardItem,Body} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch,Card,CardItem,Thumbnail} from 'native-base';
const routes = ["Home", "Chat", "Profile"];
export default class sideBar extends Component {
 

  render() {
   // const { navigate } = this.props.navigation;
    return (
     <Container style={{backgroundColor:'#efeb10'}}>
     <ScrollView contentContainerStyle={styles.contentContainer}>

     <List>
            {global.userId && 
            <ListItem style={{backgroundColor:'#efeb10',}}>
            <View style={{alignItems:'center',flex:1}}>
            
             <Thumbnail large source={{uri : global.profileImage}} />
        <Text>Hello, {global.FirstName} {global.LastName} </Text>
        </View>
      </ListItem>
            }
            {!global.userId && 
            <ListItem style={{backgroundColor:'#efeb10',marginTop:25}}  button
            onPress={() => this.props.navigation.navigate("SignUp")}>
        <Text>Register</Text>
      </ListItem>
      
            }
            {!global.userId && 
            <ListItem style={{backgroundColor:'#efeb10',}}  button
            onPress={() => this.props.navigation.navigate("Login")}> 
        <Text>Login</Text>
      </ListItem>
      
            }
            <ListItem style={{backgroundColor:'#efeb10',}} button
                  onPress={() => this.props.navigation.navigate("Dashboard")}>
              <Text>Home</Text>
            </ListItem>
            <ListItem style={{backgroundColor:'#efeb10',}} button
                  onPress={() => this.props.navigation.navigate("UserDashboard")}>
              <Text>Dashboard</Text>
            </ListItem>
            <ListItem style={{backgroundColor:'#efeb10',}}  button
                  onPress={() => this.props.navigation.navigate("Mainview")}>
              <Text>Post Task</Text>
            </ListItem>
            <ListItem style={{backgroundColor:'#efeb10',}} button
                  onPress={() => this.props.navigation.navigate("Browse")}>
              <Text>Browse Task</Text>
            </ListItem>
            {global.userId &&
            <ListItem style={{backgroundColor:'#efeb10',}} button
            onPress={() => this.props.navigation.navigate("Profile")}>
              <Text>My Profile</Text>
            </ListItem>
            }
            {global.userId &&
            <ListItem style={{backgroundColor:'#efeb10',}} button
            onPress={() => this.props.navigation.navigate("Setting")}>
              <Text>Setting</Text>
            </ListItem>
            }
            {global.userId &&
            <ListItem style={{backgroundColor:'#efeb10',}} button
            onPress={() => this.props.navigation.navigate("MyTask")}>
              <Text>My Task</Text>
            </ListItem>
            }
            <ListItem style={{backgroundColor:'#efeb10',}} button
                  onPress={() => this.props.navigation.navigate("Category")}>
              <Text>Categories</Text>
            </ListItem>
            {global.userId &&
            <ListItem style={{backgroundColor:'#efeb10',}} button
            onPress={() => this.props.navigation.navigate("Reset")}>
              <Text>Change Password</Text>
            </ListItem>
            }            
            
            <ListItem style={{backgroundColor:'#efeb10',
          }}>
              <Text>How it works</Text>
            </ListItem>
            <ListItem style={{backgroundColor:'#efeb10',}}>
              <Text>Help</Text>
            </ListItem>
            <ListItem style={{backgroundColor:'#efeb10',}}>
              <Text>Faq</Text>
            </ListItem>
            <ListItem style={{backgroundColor:'#efeb10',}}>
              <Text>Support</Text>
            </ListItem>
            {global.userId &&
            <ListItem style={{backgroundColor:'#efeb10',}} button
            onPress={() => this.props.navigation.navigate("Logout")}>
              <Text>Logout</Text>
            </ListItem>
            }
          </List>

          </ScrollView>
      
      </Container>
      
    );
  }
}
const styles = EStyleSheet.create({
    LoginDiv : {
        backgroundColor : '#efeb10',
        paddingVertical : 11, 
          borderRadius:5,
          borderWidth: 1,
          borderColor: '#ccc',
      
      },
      extraText : {
        color : '#000000',
        textAlign: 'center',
            fontSize: 18,
      },
      contentContainer: {
        backgroundColor : '#efeb10',
        paddingVertical : 10,
      }
})    
//AppRegistry.registerComponent('sideBar', () => sideBar) //Entry Point    and Root Component of The App




