/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,Alert,
  View,
  Image,Button,TouchableOpacity,ScrollView,TouchableHighlight,TextInput,KeyboardAvoidingView,Modal,AsyncStorage,ProgressBar,ActivityIndicator
} from 'react-native';
//import Spinner from 'react-native-loading-spinner-overlay';
import EStyleSheet from 'react-native-extended-stylesheet';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from './sideBar';

export default class forgotPassword extends Component {
  
 
   static navigationOptions = {
     header: null ,
  };
   constructor(){
    super()

    this.state = {
      email:"",
      password:"",
      
    LodingStatus:false,
     modalVisible: false,
     loading : true,
     visible : false,
     emailValid : false,
     oldPassword: "",
     errors: [],
    
        passwordValids : false,
     password_conformation: "",
    }
  }
 setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  validate = (text) => {
//console.log(text);
let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
if(reg.test(text) === false)
{
console.log("Email is Not Correct");
//this.setState({error: 'Please enter valid email'});
this.setState({email:text})
this.setState({emailValid: false});
return false;
  }
else {
  this.setState({email:text})
  console.log("Email is Correct");
  this.setState({error: ''});
  this.setState({emailValid: true});
}
}
passwordValidate = (text)=> {
    let password = this.state.password;
    if(password==text){
    this.setState({password_conformation:text})
     this.setState({passwordValids: true});
     let errorsArray = [];
             //errorsArray.push('');
     
     this.setState({errors: errorsArray});
    }else{
    this.setState({password_conformation:text})
     let errorsArray = [];
            // errorsArray.push('Password and Confirm Password should be same');
          //   this.setState({errors: errorsArray});
    this.setState({passwordValids: false});
    
    }
    
    }
    onRegisterPressedValidate(){
        if(this.state.email==""){
          let errorsArray = [];
             errorsArray.push('Please enter required fields');
             this.setState({errors: errorsArray});
        }else if(this.state.emailValids==false){
          let errorsArray = [];
             errorsArray.push('Please enter valid email');
             this.setState({errors: errorsArray});
           // this.setState({error: 'Please enter valid email'});
         }else if(this.state.password==""){
               // this.setState({error: 'Password is required!'});
               let errorsArray = [];
                errorsArray.push('Please enter required fields');
             this.setState({errors: errorsArray});
         }else if(this.state.password_conformation==""){
          let errorsArray = [];
             errorsArray.push('Confirm Password is required!');
             this.setState({errors: errorsArray});
    
         }else if(this.state.passwordValids==false){
           let errorsArray = [];
             errorsArray.push('Password and Confirm should be same');
             this.setState({errors: errorsArray});
           // this.setState({error: 'Password and Confirm should be same'});
         }
         else{
                 this.onRegisterPressed();
         }
      }
  async onloginPressed() {
    try {
      global.userId = "";
      const { navigate } = this.props.navigation;
      this.setState({visible: true});
    
      let response = await fetch('api.yellotasker.com/api/v1/user/updatePassword', {
                            method: 'POST',
                            headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({

                                
                                oldPassword: this.state.oldPassword,
                                newPassword : this.state.password,
                                email: this.state.email,

                            })
                          });
      let res = await response.text();
      if(response.status >= 200){
        this.setState({visible: false});
       // this.setState({LodingStatus: false});
         let result= JSON.parse(res); 
        // alert(result.message);
        // this.setState({error: result.message});
         if(result.status==1){
         // global.userId = result.data.id;
         // this.setState({error: result.message});
         // navigate('Dashboard');
         //alert(result.message);
         Alert.alert(
            'Success',
            'Password updated sucessfully .'
           );
         }else{
            Alert.alert(
                'Error',
                'Oh no! The address you provided is not in our system'
               );
         }

      }else{
        let error = res;
        throw error;
      }
    } catch (error) {
         this.setState({error: error});
    }
  }
  onloginPresseds(){
     if(this.state.email==""){
             this.setState({error: 'Please enter required fields'});
     }else if(this.state.emailValid==false){
        this.setState({error: 'Please enter valid email'});
     }else{
             this.onloginPressed();

     }

    
  }
  onsignupPressed(){
    const { navigate } = this.props.navigation;
    navigate('SignUp');
  }
  onskipPressed(){
    const { navigate } = this.props.navigation;
   navigate('Dashboard'); 
  }
  SignIn(){
    const { navigate } = this.props.navigation;
   navigate('Login'); 
  }

  render() {
    return ( 

      <View style={styles.LoadingView}>
           <Modal
      animated={true}
      transparent={true}
      visible={this.state.visible}>
                   <ActivityIndicator  style={styles.indicator} color="#000000" size = "large"></ActivityIndicator>

   </Modal>

   <View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
      <View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
      <Text>
    
     
      </Text>
      
      </View>
   <View  style={{flex:0.6,alignItems:'center',}}>
   <Text onPress={() => this.props.navigation.navigate("Dashboard")}>
      <Image source={require('./img/app-logo.png')} />
      </Text>
      </View>
      <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
      <TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
     
      <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
     
      </TouchableHighlight>
      </View>
   
   </View>
       
      <ScrollView style={styles.contentContainer}>
     
        <View  style={styles.mainContainer}>
             <Image source={require('./img/banner-login.jpg')} style={styles.bannerImg}/>
              <View style={styles.container}>
              <Errors errors={this.state.errors} />
              <View style={styles.buttonDiv}>
      <TextInput
       placeholder="Email" style={styles.InputText} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
       onSubmitEditing={()=>this.passwordInput.focus()}
       ref={(input)=>this.emailInput=input}
      keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false}
       onChangeText={(text) => this.emailValidate(text)}
      value={this.state.email}
       ></TextInput>
       </View>
       <View style={styles.buttonDiv}>
      
      <TextInput
       placeholder="Password" style={styles.InputText} 
       underlineColorAndroid = "transparent" returnKeyType="next" 
      onSubmitEditing={()=>this.passwordInput.focus()}
      keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false}
     onChangeText={(val) => this.setState({oldPassword:val})} secureTextEntry
      value={this.state.oldPassword}
       ></TextInput>
       </View>
       <View style={styles.buttonDiv}>
      <TextInput
       placeholder="New Password"
        style={styles.InputText}  
        underlineColorAndroid = "transparent" returnKeyType="go"
        onChangeText={(val) => this.setState({password: val})}
        ref={(input)=>this.passwordInput=input}
        onSubmitEditing={()=>this.cpasswordInput.focus()} secureTextEntry
        >

      </TextInput>
       </View>
       <View style={styles.buttonDiv}>
      <TextInput
       placeholder="Confirm Password"
        style={styles.InputText}  
        underlineColorAndroid = "transparent" returnKeyType="go"
         secureTextEntry
        ref={(input)=>this.cpasswordInput=input}
        onChangeText={(text) => this.passwordValidate(text)}
      value={this.state.password_conformation}
        >
      </TextInput>
       </View>
      
    
      
     
      
      
       <View style={styles.buttonssDiv} keyboardShouldPersistTaps='always'>
       <TouchableHighlight style={styles.LoginDiv} underlayColor='#efeb10' onPress={this.onRegisterPressedValidate.bind(this)}>
       <Text style={styles.extraText}>
           Update Password  
                       </Text>
    </TouchableHighlight>
       </View>
      </View>
              </View> 
      
        

  </ScrollView>
  </View>
    );
  }
}
const Errors = (props) => {
    return (
      <View>
         {props.errors.map((error, i) => <Text key={i} style={styles.errorText}>{error}</Text>)}
      </View>
    );
  }

const styles = EStyleSheet.create({
  contentContainer: {
   
  },
  LoadingView : {
    
  },
  mainContainer:{
        borderTopWidth : 4,
      borderColor: '#efeb10',
  },
bannerImg : {
  width:'100%',
  height : 115,
},
container : {
  flex: 1,
    backgroundColor: '#ffffff',
     flexDirection:'column', 
      paddingHorizontal : 10,
     paddingVertical : 12,
    },
    buttonsDiv : {
     paddingVertical : 12,
     paddingHorizontal : 20,
     marginTop:10,
    },
    buttonDiv : {
     paddingVertical : 12,
     paddingHorizontal : 20,
    },
    buttonssDiv : {
      paddingVertical : 12,
     paddingHorizontal : 20,
       marginBottom : 60,
    },
    buttonsssDiv : {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical : '4%',
        paddingHorizontal : '9%',
    },
  facebookDiv : {
    paddingVertical : 11,
    backgroundColor : '#3b5998',
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#3b5998',
 
  },
  googleDiv : {
    paddingVertical : 11,
    backgroundColor : '#dd4b39',
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#dd4b39',
 
  },
  errorText : {
color : '#dd4b39',
paddingTop : 5,
  },
  facebokText : {
     color : '#fff',
      textAlign: 'center',
      fontSize: 18,
  },
  instructionText : {
    color : '#000',
     textAlign: 'center',
     fontSize: 16,
 },
LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 11, 
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#efeb10',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
},
extrasText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 17,
},
forgateDiv : {
  paddingVertical : 12, 
  paddingHorizontal : 20,
  flex:1,
    flexDirection:'row',
},
smText : {
color : '#666',
fontSize: 12,  
},
smTexts : {
color : '#666',
fontSize: 12,
textAlign: 'right',
},
skipDiv : {
    paddingHorizontal : 8,
    backgroundColor : '#000000',
  paddingVertical : 12,
    borderRadius:5,
    borderWidth: 1,
    borderColor: '#000000',
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
},  buttontouch:{
    
        paddingVertical : 8,
     paddingHorizontal : 20,
      flex : 0.6,
        backgroundColor : '#000000',
        color:'#ffffff',
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#000000',
        marginLeft :'1%',
      },
        buttonText: {
        fontSize: 16,
        color: '#FFF',
        textAlign: 'center',
    },
    buttontouchLogin:{
      flex : 0.6,
       paddingVertical : 8,
     paddingHorizontal : 24,
        
        backgroundColor : '#efeb10',
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#efeb10',
        textAlign: 'right',
         marginRight :'1%',
      },
        buttonTextLogin: {
        fontSize: 16,
        color: '#000000',
        textAlign: 'center',
    },
});
AppRegistry.registerComponent('forgotPassword', () => forgotPassword);
