import React, { Component } from 'react';
import { AppRegistry, Text, View } from 'react-native';
//import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
  export default class Example extends Component {
    constructor(props) {
      super(props);
      this.onChangeText = this.onChangeText.bind(this);
      this.typographyRef = this.updateRef.bind(this, 'typography');
      this.state = {
        typography: 'Headline',
        reasone:[],
      };
    }
    componentDidMount(){
      // const { params } = this.props.navigation.state;
      this.getTaskList();
    }
    onChangeText(text) {
     //console.log(text);
    }
    updateRef(name, ref) {
      this[name] = ref;
    }
    getTaskList = async() =>{
       const response = await fetch(
      'http://api.yellotasker.com/api/v1/getReason');
     const json = await response.json();
      console.log(json.data.taskReason.length);
      var res = [];
      for(var i=0;i<json.data.taskReason.length;i++){ 
        res.push({value : json.data.taskReason[i].id,label :json.data.taskReason[i].reasonDescription });
      }
      this.setState({ reasone: res });	
    }
    render() {
      let { typography, } = this.state;
      return (
        <View style={styles.screen}>
          <View style={styles.container}>
            <Dropdown
              ref={this.typographyRef}
              onChangeText={this.onChangeText}
              label='Plese select'
              data={this.state.reasone}
              pickerStyle={styles.textContainer}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
            />
          </View>
        </View>
      );
    }
  }
const styles = {
  screen: {
    flex: 1,
    padding: 4,
    paddingTop: 56,
    backgroundColor: '#E8EAF6',
  },
  container: {
    marginHorizontal: 4,
    marginVertical: 8,
    paddingHorizontal: 8,
  },
  text: {
    textAlign: 'center',
  },
  textContainer: {
    borderColor:'#E8EAF6',
    borderWidth:1,
    borderRadius: 2,
  },
  containerStyle :{
    borderColor:'#E8EAF6',
    borderWidth:1,
    borderRadius: 2,
  },
};