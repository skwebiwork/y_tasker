import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,BackHandler,NetInfo} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon,Thumbnail } from 'native-base';
import SideBar from '../sideBar';

export default class taskerProfile extends Component {

    static navigationOptions = {
     header: null ,
  };
  constructor(props){
    super(props);
    global.count = 0;
    this.state ={
      taskLocatio:"",
      taskAddress:"",
      taskZipcode:"",
       date_in: '',
  date_out: '2046-10-01',
  error : "",
  taskFromHome : false,
  value :"",
  tastDate : "Release payment once done",
  taskOnDate : false,
  profileImage : 'http://yellotasker.com/assets/img/user-intro-pic.png',
    }
  }
  componentDidMount(){
   
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);

}
componentWillUnmount() {
  //Forgetting to remove the listener will cause pop executes multiple times
  BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
}
handleBack() {
  global.count = global.count+1;
  //alert('count'+global.count);
  if(global.count>1){
    Alert.alert(
      'Confirmation',
      'Are sure want to quit this App',
      [
        {text: 'OK', onPress: () => BackHandler.exitApp()},
      ]
  );
  }else{
    //this.setState({touchMsg : true});
    
    global.setTimeout(() => {
      global.count = 0;
    }, 1000);
  }
    
  return true;
  
    }
  handleOnFromHome(value){
    this.setState({value:value})
    this.setState({taskFromHome:true})
}
handleOnRemote(value){
  this.setState({value:value})
  this.setState({taskFromHome:false})
}
handleOnTommorrow(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:false})
  let d  = new Date();
  let n = d.getDate()+1;
  n = n <10  ? "0" + n : n;
  let mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
  let datass = d.getFullYear()+'-'+mm+'-'+n;
 // alert(datass);
 this.setState({date_in:datass});
 this.setState({taskShowDate:true})
 
}
handleOnToday(value){
  this.setState({tastDate:value});
}
handleOnWeek(value){
  this.setState({tastDate:value})
}
handleOnCertainDate(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:true})
  this.setState({taskShowDate:false})
  
}
    onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }

  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
    
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
    onBack(){
        const { params } = this.props.navigation.state;
        console.log(params.id);
    }
   onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
    onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onNextStep(){
 if(this.state.tastDate==""){
this.setState({error: ' Please enter required fields'});
  }else{
   // global.taskLocatio = this.state.taskLocatio;
    const { navigate } = this.props.navigation;
   navigate('Browse');
}  
}
onMyProfilePressed(){
  let userId = global.userId;
  // alert('userId'+userId);
 const { navigate } = this.props.navigation;
 if(userId){
 
  navigate('Profile'); 
  }else{
 Alert.alert(
    'Message',
    'Please Login to go to Profile',
    [
    {text : 'OK', onPress:()=>{this.onLoginPressed()}}
    ]
   );
  }
}
closeDrawer = () => {
  this.drawer._root.close()
  };
  openDrawer = () => {
  this.drawer._root.open()
  };
  onBackPressed(){
    const { navigate } = this.props.navigation;
    navigate('Dashboard'); 
 }
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    return(
      <Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >
<View style={styles.mainviewStyle}>
<View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
<View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
<TouchableHighlight onPress={()=>navigate('SingleTask',{id:params.id})} underlayColor={'transparent'}>
                <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.arrowLeft}</FontAwesome>
                </TouchableHighlight> 

</View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
<Image source={require('../img/app-logo.png')} />
</Text>
</View>
<View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
<TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>

<FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>

</TouchableHighlight>
</View>

</View>
 <ScrollView style = {styles.scrollViewStyle}>
       <View style = {styles.formViewStyle} >      

         <View style={{paddingVertical:5}}>
              <Text style={{textAlign:'center',fontSize:16,color:'#000'}}>Public Profile </Text>
           </View>  

  <View style={{paddingVertical:15,flexDirection:'row',paddingHorizontal:5,flex:1}}>
     <View style={{flex:0.4}}>
        <Image  source={{uri: this.state.profileImage}} style={{height:'100%',width:'80%',borderRadius:10}}/>
     </View>
     <View style={{flex:0.6,flexDirection:'column',paddingHorizontal:1}}> 
          <View style={{flexDirection:'row',paddingVertical:5}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Name :</Text>
          <Text style={{flex:0.6,}}> Isobel Wormald</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Character :</Text>
          <Text style={{flex:0.6,}}> Isobel Wormald</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Status :</Text>
          <Text style={{flex:0.6,}}> Isobel Wormald</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Level :</Text>
          <Text style={{flex:0.6,}}> Isobel Wormald</Text>
          </View>
     </View>
  </View> 

  <View style={{paddingVertical:5,flex:1,flexDirection:'row',alignItems:'center'}}>
  <Image  source={{uri: 'http://yellotasker.com/assets/img/success-btn.png'}} style={{flex:.1,height:30,width:20}}/>
  <Text style={{fontWeight:'bold',paddingLeft:10,flex:0.9}}> 80% Task Completed on time</Text>
  </View>      
  <View style={{paddingVertical:5,flex:1,flexDirection:'row',alignItems:'center'}}>
  <Image  source={{uri: 'http://yellotasker.com/assets/img/success-btn.png'}} style={{flex:.1,height:30,width:20}}/>
  <Text style={{fontWeight:'bold',paddingLeft:10,flex:0.9}}> 80% Job Done</Text>
  </View>  
  <View style={{paddingVertical:5,flex:1,flexDirection:'row',alignItems:'center'}}>
  <Image  source={{uri: 'http://yellotasker.com/assets/img/success-btn.png'}} style={{flex:.1,height:30,width:20}}/>
  <Text style={{fontWeight:'bold',paddingLeft:10,flex:0.9}}> 600$ cash earned</Text>
  </View>  
  <View style={{paddingVertical:15,flex:1,flexDirection:'column',alignItems:'center',borderColor:'#ccc',borderWidth:1,borderRadius:5,backgroundColor:'#f9f9f9',marginTop:20}}>
  <Text style={{marginTop:-30,textAlign:'center',backgroundColor:'#fff',fontSize:16,fontWeight:'bold',paddingHorizontal:10,paddingVertical:4}}>Badges</Text>
  
  <Text style={{fontWeight:'normal',paddingLeft:10,flex:0.9}}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
  </View> 
  <View style={{paddingVertical:15,flex:1,flexDirection:'column',borderColor:'#ccc',paddingHorizontal:5}}>
  
        
          <Text style={{color:'#000',fontWeight:'bold',fontSize:16}}> About</Text>
          <Text style={{fontSize:14}}> I’m new in Sydney, looking for new working experiences! As a PR specialist, i have multitask skills! I used to work in events and hospitality, such as in a office!</Text>
         
  
  </View>
  <View style={{paddingVertical:15,flex:1,flexDirection:'column',borderColor:'#ccc',paddingHorizontal:5}}>
  
        
  <Text style={{color:'#000',fontWeight:'bold',fontSize:16}}> Skill</Text>
  <Text style={{fontSize:14}}> This user has not added any skills yet.</Text>
 

</View>
  <View style={{paddingVertical:15,flex:1,flexDirection:'column',borderColor:'#ccc',borderWidth:1,borderRadius:5,backgroundColor:'#f9f9f9',marginTop:20,alignItems:'center',marginBottom:80}}>
  <Text style={{marginTop:-30,textAlign:'center',backgroundColor:'#fff',fontSize:16,fontWeight:'bold',paddingHorizontal:10,paddingVertical:4}}>Reviews</Text>
  <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: 'http://yellotasker.com/assets/img/recent-pic.png'}} style={{height:35,width:35,flex:0.1}} />

          <View style={{paddingHorizontal:10,flex:0.9,flexDirection:'column'}}> 
          <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                    </View>
                    <Text style={{paddingVertical:3}}>
                    3 days Ago
                    </Text>
                    <Text style={{paddingVertical:3}}>
                   <Text style={{color:'#337ab7'}}>Help out at a fun family event </Text>Shane H. said “What a superstar. Thanks for all the hard work.”
                    </Text>
          </View>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: 'http://yellotasker.com/assets/img/recent-pic.png'}} style={{height:35,width:35,flex:0.1}} />

          <View style={{paddingHorizontal:10,flex:0.9,flexDirection:'column'}}> 
          <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                    </View>
                    <Text style={{paddingVertical:3}}>
                    3 days Ago
                    </Text>
                    <Text style={{paddingVertical:3}}>
                   <Text style={{color:'#337ab7'}}>Help out at a fun family event </Text>Shane H. said “What a superstar. Thanks for all the hard work.”
                    </Text>
          </View>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: 'http://yellotasker.com/assets/img/recent-pic.png'}} style={{height:35,width:35,flex:0.1}} />

          <View style={{paddingHorizontal:10,flex:0.9,flexDirection:'column'}}> 
          <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                    </View>
                    <Text style={{paddingVertical:3}}>
                    3 days Ago
                    </Text>
                    <Text style={{paddingVertical:3}}>
                   <Text style={{color:'#337ab7'}}>Help out at a fun family event </Text>Shane H. said “What a superstar. Thanks for all the hard work.”
                    </Text>
          </View>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: 'http://yellotasker.com/assets/img/recent-pic.png'}} style={{height:35,width:35,flex:0.1}} />

          <View style={{paddingHorizontal:10,flex:0.9,flexDirection:'column'}}> 
          <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                    </View>
                    <Text style={{paddingVertical:3}}>
                    3 days Ago
                    </Text>
                    <Text style={{paddingVertical:3}}>
                   <Text style={{color:'#337ab7'}}>Help out at a fun family event </Text>Shane H. said “What a superstar. Thanks for all the hard work.”
                    </Text>
          </View>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: 'http://yellotasker.com/assets/img/recent-pic.png'}} style={{height:35,width:35,flex:0.1}} />

          <View style={{paddingHorizontal:10,flex:0.9,flexDirection:'column'}}> 
          <View style={{flexDirection:'row',}}><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',paddingHorizontal:2,}}>{Icons.star}</FontAwesome><FontAwesome style={{fontSize: 14,color:'#FFA500',}}>{Icons.star}</FontAwesome>
                    </View>
                    <Text style={{paddingVertical:3}}>
                    3 days Ago
                    </Text>
                    <Text style={{paddingVertical:3}}>
                   <Text style={{color:'#337ab7'}}>Help out at a fun family event </Text>Shane H. said “What a superstar. Thanks for all the hard work.”
                    </Text>
          </View>
          </View>
         
         

  
  </View>
     </View>
          
 </ScrollView>
 <View style={styles.footer}>
 <View style={styles.browseButtons}>
 <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
 </View>
 <View style={styles.postButtons}>
 <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
 </View>
 <View style={styles.myTaskButtons}>
 <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
 </View>
 {/* <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>
 <Text style={styles.footerText}>Message</Text>
 </View> */}
 <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#efeb10'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
 </View>
      </View>
      </Drawer>
    );
  }
}

const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},
footer: {
  
   position: 'absolute',
  flex:0.1,
  left: 0,
  right: 0,
  bottom: -10,
  backgroundColor:'#000000',
  flexDirection:'row',
  height:70,
  alignItems:'center',
},
postButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
browseButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
myTaskButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,
   
},
messageButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   
},
footerText: {
  color:'white',
  alignItems:'center',
  fontSize:14,
 

},
textStyle: {
  alignSelf: 'center',
  color: 'orange'
},
scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
      
},
bannerImg : {
  width:'100%',
  height : 130,
},
formViewStyle : {
  flex:1,
  paddingVertical : '3%',
  paddingHorizontal : '3%',
  
},
buttonDiv : {
     paddingVertical : '1%',
    },
    secondButtonDiv : {
      paddingVertical : '2%',
     },
radioDiv : {
  flex:1,
  flexDirection:'row',
  paddingVertical : '1%',
},
secRadioDiv:{
  flex:1,
  flexDirection:'row',
  paddingVertical : '2%',
},
buttonsDiv : {
   paddingVertical : '2%',
   marginBottom : 70,
},
extrasText : {
  color : '#000000',
      fontSize: 16,
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
   textAlignVertical: 'top',
},

LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 10, 
    borderRadius:25,
    borderWidth: 1,
    borderColor: '#efeb10',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
      fontWeight:'normal',
},
 errorText : {
color : '#dd4b39',
paddingTop : 5,
  },
  abc :{
    color:'#d1d3d4',
      },
      radioContainer : {
          borderWidth : 2,
          borderColor : '#d1d3d4',
      },
      radioOuterDiv : {
        flex:0.5,
         borderWidth : 1,
          borderColor : 'gray',
          paddingVertical:5,
          paddingHorizontal:5,
        borderRadius:5,
      },
      radioText : {
        paddingHorizontal:7,
       
        textAlignVertical : 'center',
      },
      radioSecOuterDiv : {
        marginLeft:3,
        flex:0.5,
        borderWidth : 1,
         borderColor : 'gray',
         paddingVertical:5,
         paddingHorizontal:5,
       borderRadius:5,
      }

});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App