import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,BackHandler,NetInfo,ListView,TouchableOpacity, Modal} from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon,Thumbnail } from 'native-base';
import SideBar from '../sideBar';
import RNPickerSelect from 'react-native-picker-select';
export default class setting extends Component {

    static navigationOptions = {
     header: null ,
  };
  constructor(props){
    super(props);
    global.count = 0;
    this.state ={
      taskLocatio:"",
      taskAddress:"",
      taskZipcode:"",
       date_in: '',
  date_out: '2046-10-01',
  error : "",
  taskFromHome : false,
  value :"",
  tastDate : "Release payment once done",
  taskOnDate : false,
  profileImage : 'http://yellotasker.com/assets/img/user-intro-pic.png',
  userProfile : {},
  dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
  activeTabIndex: 0,
  reasone:[],
  getReason:'',
  showReason:false,
  postedUserId :'',
  reasonId :'',
  isErrorResone : false,
  errorMsgResone : '',
  reasonComment : '',
  successImg : false,
            successTaskHeading : '',
            successTaskContent : '',
    }
  }
  componentDidMount(){
   
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    this.getProfile();
    this.getPortfolio();
    this.getReportTaskReason();
}
componentWillUnmount() {
  //Forgetting to remove the listener will cause pop executes multiple times
  BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
}
handleBack() {
  global.count = global.count+1;
  //alert('count'+global.count);
  if(global.count>1){
    Alert.alert(
      'Confirmation',
      'Are sure want to quit this App',
      [
        {text: 'OK', onPress: () => BackHandler.exitApp()},
      ]
  );
  }else{
    //this.setState({touchMsg : true});
    
    global.setTimeout(() => {
      global.count = 0;
    }, 1000);
  }
    
  return true;
  
    }
    getProfile(){
      const { navigate } = this.props.navigation;
      this.setState({visible: true}); 
       fetch('http://api.yellotasker.com/api/v1/getPublicProfile/'+global.userId)
       .then((response) => response.json())
       .then((responseJson) =>{
      this.setState({visible: false}); 
      console.log(responseJson);
         if(responseJson.code==404){
         //	alert(responseJson.code);
           this.setState({ emptyRow : true});
         }
         if(responseJson.code==200&&responseJson.status==1){
         var data = responseJson.data;
         console.log('userProfileuserProfileuserProfileuserProfileuserProfile')
         console.log(data);
         this.setState({userProfile : data});
         if(data.profile_image){
           this.setState({profileImage :data.profile_image })
         }
         }
       })
       .catch((error) =>{
         console.log(error);
         console.error(error);
       });
     }
     async getPortfolio() {
      try {
        //this.setState({LodingStatus: true});
      
        let response  = await fetch('http://api.yellotasker.com/api/v1/getPortfolioImage', {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
           body: JSON.stringify({
            userId : global.userId

           })
         });
         let res  = await response.json();
       //  console.log(res)
         this.setState({visible: false});  
         if(response.status >=200){
          console.log(res.data);
          var i;
          var datas = res.data;

var dbImgsrc = [];
        for(i=0;i<datas.length;i++){
    dbImgsrc.push({images : datas.images})
        }
          this.setState({ dataSource: this.state.dataSource.cloneWithRows(res.data)});
         }else{
             let errors = res;
             throw errors;
         }
      } catch (errors) {
             console.log("catch errors " + errors);
    
          //   this.setState({errors: errorsArray});
      }
    }
  handleOnFromHome(value){
    this.setState({value:value})
    this.setState({taskFromHome:true})
}
handleOnRemote(value){
  this.setState({value:value})
  this.setState({taskFromHome:false})
}
handleOnTommorrow(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:false})
  let d  = new Date();
  let n = d.getDate()+1;
  n = n <10  ? "0" + n : n;
  let mm = d.getMonth() < 9 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1);
  let datass = d.getFullYear()+'-'+mm+'-'+n;
 // alert(datass);
 this.setState({date_in:datass});
 this.setState({taskShowDate:true})
 
}
handleOnToday(value){
  this.setState({tastDate:value});
}
handleOnWeek(value){
  this.setState({tastDate:value})
}
handleOnCertainDate(value){
  this.setState({tastDate:value})
  this.setState({taskOnDate:true})
  this.setState({taskShowDate:false})
  
}
    onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }

  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
    
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
    onMyProfilePressed(){
      let userId = global.userId;
      // alert('userId'+userId);
     const { navigate } = this.props.navigation;
     if(userId){
     
      navigate('Profile'); 
      }else{
     Alert.alert(
        'Message',
        'Please Login to go to Profile',
        [
        {text : 'OK', onPress:()=>{this.onLoginPressed()}}
        ]
       );
      }
    }
   onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
    onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onNextStep(){
 if(this.state.tastDate==""){
this.setState({error: ' Please enter required fields'});
  }else{
   // global.taskLocatio = this.state.taskLocatio;
    const { navigate } = this.props.navigation;
   navigate('Browse');
}  
}
closeDrawer = () => {
  this.drawer._root.close()
  };
  openDrawer = () => {
  this.drawer._root.open()
  };
  onBackPressed(){
    const { navigate } = this.props.navigation;
    navigate('Dashboard'); 
 }
 skillRender(data){
   console.log(data);
 //var skill= data.skills.split(',');
 var skill = [];
 if(data){
   skill = data.split(',');
 } 
 return skill.map((item,index) =>{
    return  (
      <View key={index}>
        <Text style={{fontWeight:'normal',}}>  {item}</Text>
      </View>
    )
  });
 }
 transportRender(data){
   console.log(data);
   var skill = [];
   if(data){
    skill = JSON.parse(data);
  } 
   //skill = JSON.parse(skill.modeOfreach);
  return skill.map((item) =>{
    return  (
      <View>
        <Text style={{fontWeight:'normal',}}>  {item}</Text>
      </View>
    )
  });
 
 }
 renderRow(data){
  // console.log(data)
  return data.map((item) =>{
    return  (
      <View>
       <Image source={{uri:item.images}} />
      </View>
    )
  });
 }
 DateChangeFormate(duedate){
  if(duedate){
 var date = duedate;
 console.log(duedate)
//		var date = "2017-12-04";
   //var arr1 = date.split(' ');
  var nDate = date.split(' ');
   var arr2 = nDate[0].split('-');
   var month;
   if(arr2[1]=='01'){
     month = 'Jan';
   }else if(arr2[1]=='02'){
   month = 'Feb';
 }else if(arr2[1]=='03'){
   month = 'March';
 }else if(arr2[1]=='04'){
   month = 'April';
 }else if(arr2[1]=='05'){
   month = 'May';
 }else if(arr2[1]=='06'){
   month = 'June';
 }else if(arr2[1]=='07'){
   month = 'July';
 }else if(arr2[1]=='08'){
   month = 'Aug';
 }else if(arr2[1]=='09'){
   month = 'Sep';
 }else if(arr2[1]=='10'){
   month = 'Oct';
 }else if(arr2[1]=='11'){
   month = 'Nov';
 }else{
   month = 'Dec';
 } 
   var dt = month +' '+arr2[2]+','+arr2[0];
   return dt;
}
  //alert(dt);
   
 }	
 onClose(){
this.setState({showReason : !this.state.showReason})
 }
 onSubmitReason(){

    var reasonComment = this.state.reasonComment;
    var getReason = this.state.reasonId;
    console.log(getReason);
    if(getReason==''){
        this.setState({isErrorResone:true,errorMsgResone:'Reason must be required!'})

    }else if(getReason==null){
        this.setState({isErrorResone:true,errorMsgResone:'Reason must be required!'})

    }else if(reasonComment==''){
        this.setState({isErrorResone:true,errorMsgResone:'Comment must be required!'})

    }else{
        this.setState({isErrorResone:false,errorMsgResone:'Reason must be required!'})
       this.reportUser();
    }


 }
 async reportUser(){
   let reportedUserId = global.userId;
   if(reportedUserId){
       var getReason = this.state.getReason;

try {
   let response  = await fetch('http://api.yellotasker.com/api/v1/report/user', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
     body: JSON.stringify({  
       "reasonId": this.state.getReason,
       "comment": this.state.reasonComment,
       "reportedUserId": reportedUserId,
       "postedUserId": this.state.postedUserId

      })
    });
    let res  = await response.text();
    if(response.status >=200){
       this.setState({reportThisTask : false});
      let formErrorsss = JSON.parse(res);
      console.log('res'+formErrorsss);
      console.log(JSON.stringify(res));
       this.setState({visible: false});
      if(formErrorsss.code==500){
   
      }
      if(formErrorsss.status==1){
          
      this.setState({successImg: true});
         
       this.setState({successTaskHeading : 'Reported task successfully'});
       this.setState({successTaskContent : 'Successfully reported this user. Admin will take action and get back to you.'});
      }
    }else{
        let errors = res;
        throw errors;
    }
 } catch (errors) {
 }

}else{
   Alert.alert(
       'Message',
       'Please Login for report task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
}
}
onCloseSuccess(){
  this.setState({successImg : false});
  
}
 getReportTaskReason = async() =>{
  const response = await fetch(
 'http://api.yellotasker.com/api/v1/getReason');
const json = await response.json();
 console.log(json.data.userReason);
 var res = [];

 for(var i=0;i<json.data.userReason.length;i++){ 
   res.push({value : json.data.userReason[i].id,label :json.data.userReason[i].reasonDescription });
 }
 this.setState({ reasone: res });	
}
  render() {
    return(
      <Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >
<View style={styles.mainviewStyle}>

<Modal  transparent={false} visible={this.state.showReason}>
                            <View style={{flexDirection:'row',paddingVertical:10,backgroundColor:'#000000'}}>
                <View style={{flex:0.2,alignItems:'flex-start',paddingVertical:12,paddingLeft:10}}>
                
                </View>
             <View  style={{flex:0.6,alignItems:'center'}}>
             <Text >
                <Image source={require('../img/app-logo.png')}/>
                </Text>
                </View>
                <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:12,paddingRight:10}}>
                
                </View>             
             </View>
             <View style={{backgroundColor:'#efeb10', borderColor: '#ccc',paddingVertical: 12,}}>
                   <Text style={{textAlign : 'center',color:'#000000',fontWeight:'bold'}}>Report this User</Text>
             </View>       
                    <View style={styles.makeOfferContainer}>
                    
                    <View style={{paddingHorizontal:10,paddingVertical:4,}}>
                    <Text style={{paddingVertical:6,fontWeight:'500'}}>Please give us more information regarding this user</Text>
                     {this.state.isErrorResone&&<Text style={{paddingVertical:6,color:'red'}}>{this.state.errorMsgResone}</Text> }
                    {/* <Dropdown
              ref={this.typographyRef}
              onChangeText={this.onChangeText}
              label={this.state.reasonLabel}
              data={this.state.reasone}
              pickerStyle={styles.textContainer}
              containerStyle = {styles.containerStyle}
              itemPadding={4}
              dropdownOffset = {{top: 42, left: 0}}
              value={this.state.getReason}
              itemTextStyle={styles.itemTextStyle}
              labelHeight={10}
              fontSize={14}
              inputContainerStyle={{ borderBottomColor: 'transparent',paddingHorizontal:5}}
             
            /> */}
                <RNPickerSelect           
                      items={this.state.reasone}
                      onValueChange={
                          (item) => {
                           this.setState({reasonId : item})
                            console.log(item);
                            
                          }
                      }
                      style={{ ...pickerSelectStyles }}
                      value={this.state.getReason}
                      
                    
                    />
      </View>
                       <View style={{paddingHorizontal:10,paddingVertical:4,}}>
                       <TextInput
       placeholder="Comment(required)"  
       underlineColorAndroid = "transparent" returnKeyType="next" 
             keyboardType="email-address"
      autoCapitalize="none"
      autoCorrect={false} style={styles.TextArea}
      multiline={true}
      numberOfLines={3}
      onChangeText={(val) => this.setState({reasonComment : val})}
       ></TextInput>
    
                       </View>
                      
                       <View style={{paddingHorizontal:6,paddingVertical:10,}}>
                       <Text style={{ borderRadius: 30, paddingVertical: 10, paddingHorizontal: 25, backgroundColor: '#efeb10', color: '#000000', textAlign: 'center' }} onPress={this.onSubmitReason.bind(this)}> Continue</Text>
                       </View>
            <View style={{paddingHorizontal:6,paddingVertical:10,}}>
               <TouchableHighlight  style={styles.skip}>
               <Text style={{textAlign :'center',fontSize: 20,color:'#ffffff'}} onPress={this.onClose.bind(this)}>Cancel</Text>
                </TouchableHighlight>
            </View>
            </View>
                </Modal> 
                <Modal  animationType="slide"
      transparent={true} visible={this.state.successImg}>
			<View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#FFFFFF',
    height: 300,
    width: 300,
    borderRadius: 3,
   }}> 
		<View style={{backgroundColor:'#efeb10',height:50,flexDirection:'row'}} >
    <Text style={{color:'#000',textAlign:'center',paddingLeft:24,paddingVertical:10,flex:.9}}> {this.state.successTaskHeading} </Text>
		<Text style={{flex:.1,paddingVertical:10,color:'#000',textAlign:'center',fontWeight:'bold'}} onPress={this.onCloseSuccess.bind(this)} >X</Text>
		</View>
   <View style={{alignItems: 'center',}}>
		<Image source={require('../img/checkmark.gif')} style={{height : 200,width:100}}/>
	 </View>
	 <Text style={{textAlign:'center'}}> {this.state.successTaskContent}</Text>
	 </View>
	 </View>
   </Modal>
<View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
<View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
<Text>


</Text>

</View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
<Image source={require('../img/app-logo.png')} />
</Text>
</View>
<View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
<TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>

<FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>

</TouchableHighlight>
</View>

</View>
 <ScrollView style = {styles.scrollViewStyle}>
       <View style = {styles.formViewStyle} >      

         {/* <View style={{paddingVertical:5}}>
              <Text style={{textAlign:'center',fontSize:16,color:'#000'}}>My Tasks Summary </Text>
           </View>   */}

  {/* <View style={{paddingVertical:15,flexDirection:'row',}}>
     <View style={{flex:0.2}}></View>
      <Text style={{flex:0.3,overflow:"hidden",textAlign:'center',borderRadius:18,backgroundColor:'#efeb10',paddingHorizontal:20,paddingVertical:10,color:'#000'}}>As Poster</Text>
     <Text style={{flex:0.3,overflow:"hidden",textAlign:'center',borderRadius:18,backgroundColor:'#000',paddingHorizontal:20,paddingVertical:10,color:'#fff',marginLeft:5}}>As Worker</Text>
  </View>  */}
  <View style={{flex:1,flexDirection:'row',paddingHorizontal:40}}>
        <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 0 }) }}
                   style={[styles.topButtonStyle, this.state.activeTabIndex == 0 ? styles.topButtonActiveStyle : {}]}>
               <Text style={this.state.activeTabIndex == 0 ? styles.topTextActiveStyle : {color:'#fff'}}>As Poster</Text>
               </TouchableOpacity>
               <View style={{ width: 1, height: 50, backgroundColor: '#ecf0f1' }} />
               <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 1 }) }}
                   style={[styles.topButtonStyle, this.state.activeTabIndex == 1 ? styles.topButtonActiveStyle : {}]}>
                   <Text style={this.state.activeTabIndex == 1 ? styles.topTextActiveStyle : {color:'#fff'}}>As Tasker</Text>
               </TouchableOpacity>
        </View>
  <View style={{paddingVertical:15,flexDirection:'row',paddingHorizontal:5,flex:1}}>
     <View style={{flex:0.3}}>
        <Image  source={{uri: this.state.profileImage}} style={{height: 90,
	  width : 90,
	  borderRadius:45}}/>
     </View>
     <View style={{flex:0.7,flexDirection:'column',paddingHorizontal:1}}> 
          <View style={{flexDirection:'row',paddingVertical:5}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Name</Text>
          <Text style={{flex:0.6,}}>: {this.state.userProfile.first_name} {this.state.userProfile.last_name}</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Location</Text>
          <Text style={{flex:0.6,}}>: {this.state.userProfile.location}</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Mebmer</Text>
          <Text style={{flex:0.6,}}>: {this.DateChangeFormate(this.state.userProfile.created_at)}</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5}}>
          <TouchableOpacity onPress={()=>this.setState({showReason : !this.state.showReason,postedUserId : this.state.userProfile.id})}>
          <Text style={{flex:0.8,color:'#000',fontWeight:'normal'}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.flag}</FontAwesome> Report this member</Text>
          </TouchableOpacity>
          </View>
          {/* <View style={{flexDirection:'row',paddingVertical:5}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Level :</Text>
          <Text style={{flex:0.6,}}> Isobel Wormald</Text>
          </View> */}
     </View>
  </View> 

  {/* <View style={{paddingVertical:5,flex:1,flexDirection:'row',alignItems:'center'}}>
  <Image  source={{uri: 'http://yellotasker.com/assets/img/success-btn.png'}} style={{flex:.1,height:30,width:20}}/>
  <Text style={{fontWeight:'bold',paddingLeft:10,flex:0.9}}> 80% Task Completed on time</Text>
  </View>      
  <View style={{paddingVertical:5,flex:1,flexDirection:'row',alignItems:'center'}}>
  <Image  source={{uri: 'http://yellotasker.com/assets/img/success-btn.png'}} style={{flex:.1,height:30,width:20}}/>
  <Text style={{fontWeight:'bold',paddingLeft:10,flex:0.9}}> 80% Job Done</Text>
  </View>  
  <View style={{paddingVertical:5,flex:1,flexDirection:'row',alignItems:'center'}}>
  <Image  source={{uri: 'http://yellotasker.com/assets/img/success-btn.png'}} style={{flex:.1,height:30,width:20}}/>
  <Text style={{fontWeight:'bold',paddingLeft:10,flex:0.9}}> 600$ cash earned</Text>
  </View>   */}
  <View style={{flex:1,flexDirection:'column',borderColor:'#ccc',borderWidth:1,borderRadius:5,backgroundColor:'#f9f9f9',}}>
  <View style={{backgroundColor:'#666666',paddingVertical:8,borderTopRightRadius:5,borderTopLeftRadius:5}} >
  <Text style={{textAlign:'center',fontSize:16,fontWeight:'normal',color:'#fff',}}>Badges</Text>
  </View>
  <View style={{paddingVertical:10,paddingHorizontal:6}}>
  <Text style={{fontWeight:'normal',}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.creditCard}</FontAwesome> Payment Method</Text>
  <Text style={{fontWeight:'normal',paddingVertical:8}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.phone}</FontAwesome> Mobile</Text>
  <Text style={{fontWeight:'normal',}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.facebook}</FontAwesome>  Facebook</Text>

  </View> 
  

  </View>
  <View style={{height:10}} />
  <View style={{flex:1,flexDirection:'column',borderColor:'#ccc',borderWidth:1,borderRadius:5,backgroundColor:'#f9f9f9',}}>
  <View style={{backgroundColor:'#666666',paddingVertical:8,borderTopRightRadius:5,borderTopLeftRadius:5}} >
  <Text style={{textAlign:'center',fontSize:16,fontWeight:'normal',color:'#fff',}}>Skills</Text>
  </View>
  <View style={{paddingVertical:10,paddingHorizontal:6}}>
  {this.skillRender(this.state.userProfile.skills)}
  {/* <Text style={{fontWeight:'normal',}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.creditCard}</FontAwesome> Payment Method</Text>
  <Text style={{fontWeight:'normal',paddingVertical:8}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.phone}</FontAwesome> Mobile</Text>
  <Text style={{fontWeight:'normal',}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.facebook}</FontAwesome>  Facebook</Text> */}
  </View> 
  </View>
  <View style={{height:10}} />
  <View style={{flex:1,flexDirection:'column',borderColor:'#ccc',borderWidth:1,borderRadius:5,backgroundColor:'#f9f9f9',}}>
  <View style={{backgroundColor:'#666666',paddingVertical:8,borderTopRightRadius:5,borderTopLeftRadius:5}} >
  <Text style={{textAlign:'center',fontSize:16,fontWeight:'normal',color:'#fff',}}>Transpotation</Text>
  </View>
  <View style={{paddingVertical:10,paddingHorizontal:6}}>
  {this.transportRender(this.state.userProfile.modeOfreach)}
  {/* <Text style={{fontWeight:'normal',}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.creditCard}</FontAwesome> Payment Method</Text>
  <Text style={{fontWeight:'normal',paddingVertical:8}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.phone}</FontAwesome> Mobile</Text>
  <Text style={{fontWeight:'normal',}}> <FontAwesome style={{fontSize: 15,color:'#666'}}>{Icons.facebook}</FontAwesome>  Facebook</Text>
  </View>  */}
  </View>
  </View>
  <View style={{height:10}} />
  <View style={{flex:1,flexDirection:'column',borderColor:'#ccc',borderWidth:1,borderRadius:5,backgroundColor:'#f9f9f9',marginBottom:80}}>
  <View style={{backgroundColor:'#666666',paddingVertical:8,borderTopRightRadius:5,borderTopLeftRadius:5}} >
  <Text style={{textAlign:'center',fontSize:16,fontWeight:'normal',color:'#fff',}}>Portfolio</Text>
  </View>
  <View style={{paddingVertical:10,paddingHorizontal:6}}>
  <ListView
		dataSource={this.state.dataSource}
		renderRow={(rowData) => { return (
      <View>
        <Image source={{uri:rowData.images}} style={{height:150,width:100}}/>
      </View>
    ) }
        }
        />
 
  </View> 
  </View>
  {/* <View style={{paddingVertical:15,flex:1,flexDirection:'column',alignItems:'center',borderColor:'#ccc',borderWidth:1,borderRadius:5,backgroundColor:'#f9f9f9',marginTop:20}}>
  <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Earning Target :</Text>
          <Text style={{flex:0.6,}}> Lorem ipsum sit emt</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Text style={{flex:0.4,color:'#000',fontWeight:'bold'}}> Due Date :</Text>
          <Text style={{flex:0.6,}}> 25/02/2018</Text>
          </View>
  
  </View> */}
  {/* <View style={{paddingVertical:15,flex:1,flexDirection:'column',borderColor:'#ccc',borderWidth:1,borderRadius:5,backgroundColor:'#f9f9f9',marginTop:20,alignItems:'center',marginBottom:80}}>
  <Text style={{marginTop:-30,textAlign:'center',backgroundColor:'#fff',fontSize:16,fontWeight:'bold',paddingHorizontal:10,paddingVertical:4}}>Recent Activity</Text>
  <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: this.state.profileImage}} style={{height:35,width:35,flex:0.1}} />
          <Text style={{paddingHorizontal:10,flex:0.9}}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>
          </View>
          
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: this.state.profileImage}} style={{height:35,width:35,flex:0.1}} />
          <Text style={{paddingHorizontal:10,flex:0.9}}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: this.state.profileImage}} style={{height:35,width:35,flex:0.1}} />
          <Text style={{paddingHorizontal:10,flex:0.9}}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: this.state.profileImage}} style={{height:35,width:35,flex:0.1}} />
          <Text style={{paddingHorizontal:10,flex:0.9}}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: this.state.profileImage}} style={{height:35,width:35,flex:0.1}} />
          <Text style={{paddingHorizontal:10,flex:0.9}}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>
          </View>
          <View style={{flexDirection:'row',paddingVertical:5,paddingHorizontal:15}}>
          <Image source={{uri: this.state.profileImage}} style={{height:35,width:35,flex:0.1}} />
          <Text style={{paddingHorizontal:10,flex:0.9}}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>
          </View>

  
  </View> */}
     </View>
          
 </ScrollView>
 <View style={styles.footer}>
 <View style={styles.browseButtons}>
 <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
 </View>
 <View style={styles.postButtons}>
 <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
 </View>
 <View style={styles.myTaskButtons}>
 <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
 </View>
 {/* <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.envelope}</FontAwesome>
 <Text style={styles.footerText}>Message</Text>
 </View> */}
 <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#efeb10'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
 
 </View>
      </View>
      </Drawer>
    );
  }
}
const pickerSelectStyles = EStyleSheet.create({
  inputIOS: {
    fontSize: 18,
    paddingTop: 10,
    paddingHorizontal: 8,
    paddingBottom: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color:'gray',
    backgroundColor:'transparent'
  },
  icon:{
    position: 'absolute',
    backgroundColor: 'transparent',
    borderTopWidth: 8,
    borderTopColor: 'gray',
    borderRightWidth: 7,
    borderRightColor: 'transparent',
    borderLeftWidth: 7,
    borderLeftColor: 'transparent',
    width: 0,
    height: 0,
    top: 15,
    right: 10,
  
  }
});
const styles = EStyleSheet.create({
  mainviewStyle: {
  flex: 1,
  flexDirection: 'column',
},
footer: {
  
   position: 'absolute',
  flex:0.1,
  left: 0,
  right: 0,
  bottom: -10,
  backgroundColor:'#000000',
  flexDirection:'row',
  height:70,
  alignItems:'center',
},
postButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

},
browseButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,

}, skip : {
        paddingHorizontal : 8,
        backgroundColor : '#000000',
      paddingVertical : 5,
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#000000',
    },
myTaskButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   borderColor: '#efeb10',
   borderRightWidth: 1,
   
}, TextArea : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
  textAlignVertical: 'top',
  height:100,
},
messageButtons: {
  alignItems:'center',
  justifyContent: 'center',
  flex:1,
   flexDirection:'column',
   
},
footerText: {
  color:'white',
  alignItems:'center',
  fontSize:14,
 

},
textStyle: {
  alignSelf: 'center',
  color: 'orange'
},
scrollViewStyle: {
  flex : 1,
  borderTopWidth : 4,
      borderColor: '#efeb10',
      
},
bannerImg : {
  width:'100%',
  height : 130,
},
formViewStyle : {
  flex:1,
  paddingVertical : '3%',
  paddingHorizontal : '3%',
  
},
buttonDiv : {
     paddingVertical : '1%',
    },
    secondButtonDiv : {
      paddingVertical : '2%',
     },
radioDiv : {
  flex:1,
  flexDirection:'row',
  paddingVertical : '1%',
},
secRadioDiv:{
  flex:1,
  flexDirection:'row',
  paddingVertical : '2%',
},
buttonsDiv : {
   paddingVertical : '2%',
   marginBottom : 70,
},
extrasText : {
  color : '#000000',
      fontSize: 16,
},
InputText : {
  paddingVertical : 12,
  borderRadius:5,
  borderColor: 'gray',
   borderWidth: 1,
   paddingHorizontal: 5,
   marginBottom : 10,
   textAlignVertical: 'top',
},

LoginDiv : {
  backgroundColor : '#efeb10',
  paddingVertical : 10, 
    borderRadius:25,
    borderWidth: 1,
    borderColor: '#efeb10',

},
extraText : {
  color : '#000000',
  textAlign: 'center',
      fontSize: 18,
      fontWeight:'normal',
},
 errorText : {
color : '#dd4b39',
paddingTop : 5,
  },
  abc :{
    color:'#d1d3d4',
      },
      radioContainer : {
          borderWidth : 2,
          borderColor : '#d1d3d4',
      },
      radioOuterDiv : {
        flex:0.5,
         borderWidth : 1,
          borderColor : 'gray',
          paddingVertical:5,
          paddingHorizontal:5,
        borderRadius:5,
      },
      radioText : {
        paddingHorizontal:7,
       
        textAlignVertical : 'center',
      },
      radioSecOuterDiv : {
        marginLeft:3,
        flex:0.5,
        borderWidth : 1,
         borderColor : 'gray',
         paddingVertical:5,
         paddingHorizontal:5,
       borderRadius:5,
      },
      topButtonStyle: {
        flex: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#ecf0f1',
        borderBottomWidth: 2,
        backgroundColor:'#000'
      },
      topButtonActiveStyle: {
        borderBottomColor: '#ecf0f1',
        borderBottomWidth: 2,
        backgroundColor:'#efeb10'
      },
      topTextActiveStyle: {
        color: '#000'
      },

});
//AppRegistry.registerComponent('secondStep', () => secondStep) //Entry Point    and Root Component of The App