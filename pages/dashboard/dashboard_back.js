import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,ProgressBar,Alert,BackHandler} from "react-native";
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from '../sideBar';
import ProgressIndicator from 'react-native-progress-indicator';
import styles from './dashboardStyle';
import FontAwesome, { Icons } from 'react-native-fontawesome';
export default class dashboard extends Component {
   static navigationOptions = {
    header: null,
  };
  constructor(){
    super();
    global.count = 0;
    this.state={
      touchMsg : false,
    }
  }
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
  }
  componentWillUnmount() {
    //Forgetting to remove the listener will cause pop executes multiple times
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  }
  handleBack() {
global.count = global.count+1;
//alert('count'+global.count);
if(global.count>1){
  Alert.alert(
    'Confirmation',
    'Are sure want to quit this App',
    [
      {text: 'OK', onPress: () => BackHandler.exitApp()},
    ]
);
}else{
  //this.setState({touchMsg : true});
  
  global.setTimeout(() => {
    global.count = 0;
  }, 1000);
}
  
return true;

  }
  closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
  onTaskPressed(){
    let userId = global.userId;
    const { navigate } = this.props.navigation;
    if(userId){
   navigate('Mainview'); 
   }else{
    Alert.alert(
       'Message',
       'Please Login to post a task',
       [
         {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
 //navigate('Login'); 
   }
  }
  onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
  render() {
    return(
<Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >
      <View style={styles.mainviewStyle}>
            <View style={{flexDirection:'row',paddingVertical:10,backgroundColor:'#000000'}}>
            <View style={{flex:0.2,alignItems:'flex-start',paddingVertical:12,paddingLeft:10}}>
            <Text>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.arrowLeft}</FontAwesome>
           
            </Text>
            
            </View>
         <View  style={{flex:0.6,alignItems:'center'}}>
            <Image source={require('../img/logo-head.png')}/></View>
            <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:12,paddingRight:10}}>
            <Text onPress={()=>this.props.navigation.navigate("DrawerOpen")}>
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
            </Text>
            </View>
         
         </View>
        <ScrollView style = {styles.scrollViewStyle}>
         <Image source={require('../img/banner-login.jpg')} style={styles.bannerImg}/>
            <View style = {styles.mainCategory}>
                 <View style={{ flex: 1,flexDirection:'row' }}> 
                 <View style={{ flex: 0.8, }}>
                 <Text style={styles.topCategory}>Top categories on Yellotasker </Text>
                 </View>
                 </View>
                  <View style={{ flex: 0.2,paddingLeft: 4}}>
                 <Text style = {styles.more}  onPress={() => this.props.navigation.navigate("Category")}> More > </Text>
                 </View>
            </View>
            <View style={{marginTop:10}}>
            <View style={styles.mainImgDiv}>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/life.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Life {"\n"} Administration</Text>
                  
                  </View>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/home.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Home {"\n"} Administration</Text>
                  </View>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/moving.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Moving {"\n"} Stuff around</Text>
                  </View>

            </View>
            <View style={styles.mainImgDiv}>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/event.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Event {"\n"} Related stuff</Text>
                  </View>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/arranging.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Arranging for {"\n"} Boots on the ground</Text>
                  </View>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/fitness.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Fitness {"\n"} and Wellbeing</Text>
                  </View>

            </View>
           
            <View style={styles.mainImgsDiv}>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/digital.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Digital{"\n"}Tasks</Text>
                  </View>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/calculat.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Things that {"\n"} need research</Text>
                  </View>
                  <View style={styles.ImgDiv}>
                  <TouchableHighlight onPress={this.onTaskPressed.bind(this)} underlayColor='#efeb10'>
                         <Image source={require('../img/cate-more.png')} style={styles.Img}/>
                         </TouchableHighlight>
                         <Text style={styles.ImgText} onPress={this.onTaskPressed.bind(this)}>Others</Text>
                  </View>

            </View>
            </View>
      
          
        </ScrollView>

          <View style={styles.footer}>
          <View style={styles.browseButtons}>
          <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
               <Image style={styles.imagestyle} source={require('../img/search.png')} />
            </TouchableHighlight>
              <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
          </View>
          <View style={styles.postButtons}>
 <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
               <Image style={styles.imagestyle} source={require('../img/post.png')} />
               </TouchableHighlight>
              <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
          </View>
          <View style={styles.myTaskButtons}>
          <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
                    <Image style={styles.imagestyle} source={require('../img/task.png')} />
              </TouchableHighlight>
                   <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
               </View>
           <View style={styles.messageButtons}>
               <Image style={styles.imagestyle} source={require('../img/message.png')} />
              <Text style={styles.footerText}>Message</Text>
          </View>
          </View>
          
      </View>
      </Drawer>
    );
  }
}


//AppRegistry.registerComponent('dashboard', () => dashboard) //Entry Point    and Root Component of The App