import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,ProgressBar,Alert,BackHandler,ActivityIndicator,Modal,ListView,FlatList} from "react-native";
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from '../sideBar';
import ProgressIndicator from 'react-native-progress-indicator';
import styles from './dashboardStyle';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import Grid from 'react-native-grid-component';
export default class dashboard extends Component {
   static navigationOptions = {
    header: null,
  };
  constructor(){
    super();
    global.count = 0;
    this.state={
      touchMsg : false,
      visible : false,
      data : [],
      categoryData : [],
			dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
    }
  }
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    this.categoryList();
  }
  componentWillUnmount() {
    //Forgetting to remove the listener will cause pop executes multiple times
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  }
  handleBack() {
global.count = global.count+1;
//alert('count'+global.count);
if(global.count>1){
  Alert.alert(
    'Confirmation',
    'Are sure want to quit this App',
    [
      {text: 'OK', onPress: () => BackHandler.exitApp()},
    ]
);
}else{
  //this.setState({touchMsg : true});
  
  global.setTimeout(() => {
    global.count = 0;
  }, 1000);
}
  
return true;

  }
 categoryList(){
  AsyncStorage.getItem('DynamicCategory', (err, result) => {
   console.log(result);
    let cat = JSON.parse(result);
    console.log(cat[0].category);
    this.setState({
      // dataSource: this.state.dataSource.cloneWithRows(data)
      categoryData : cat
     })
  });
  let len = this.state.categoryData;
  console.log(len);
 }
  closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
  onTaskPressed(){
    let userId = global.userId;
    const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }
  onPostClick= (id,title,description)=>{
    let catid = JSON.stringify(id);
    let catTitle = JSON.stringify(title);
    let catDes = JSON.stringify(description);
   // alert(title);
    global.category_id = id;
    global.category_title = title;
    global.category_description = description;
    const { navigate } = this.props.navigation;
   navigate('Mainview'); 

  }
  onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
  onMyProfilePressed(){
		let userId = global.userId;
		// alert('userId'+userId);
	   const { navigate } = this.props.navigation;
	   if(userId){
	   
		navigate('Profile'); 
		}else{
	   Alert.alert(
		  'Message',
		  'Please Login to go to Profile',
		  [
		  {text : 'OK', onPress:()=>{this.onLoginPressed()}}
		  ]
		 );
		}
	  }
  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
    _renderItem = (data, i) => <View style={ styles.item} key={i}><View style={styles.ImgDivs}>
    <TouchableHighlight onPress={this.onPostClick.bind(this,data.category_id,data.category_name,data.category_group_name)} underlayColor='#efeb10'>
           <Image source={{uri: data.category_group_image}} style={styles.Img}/>
           </TouchableHighlight>
           
           <Text style={styles.ImgText} onPress={this.onPostClick.bind(this,data.category_id,data.category_name,data.category_group_name)}>{data.category_group_name}</Text>
    </View>
    </View>
  render() {
    return(
<Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} 
     
      >
       <Modal
    animationType="slide"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#FFFFFF',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
      <ActivityIndicator  color="#333" size = "large" style={ {flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80 }
   }></ActivityIndicator>
   </View>
   </View>
   </Modal>
      <View style={styles.mainviewStyle}>
            <View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
            <View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
            <Text>
          
           
            </Text>
            
            </View>
         <View  style={{flex:0.6,alignItems:'center',}}>
         <Text onPress={() => this.props.navigation.navigate("Dashboard")}>
            <Image source={require('../img/app-logo.png')} />
            </Text>
            </View>
            <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
            <TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
           
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
           
            </TouchableHighlight>
            </View>
         
         </View>
        <ScrollView style = {styles.scrollViewStyle}>
      
            <View style = {styles.mainCategory}>
                 <View style={{ flex: 1,flexDirection:'row' }}> 
                 <View style={{ flex: 0.8, }}>
                 <Text style={styles.topCategory}>Top categories on Yellotasker </Text>
                 </View>
                 </View>
                  <View style={{ flex: 0.2,paddingLeft: 4}}>
                 <Text style = {styles.more}  onPress={() => this.props.navigation.navigate("Category")}> More > </Text>
                 </View>
            </View>
        
            <View style={styles.mainImgsDiv}> 
			
    <Grid
        style={styles.list}
        renderItem={this._renderItem}
        data={this.state.categoryData}
        itemsPerRow={3}
      />
                     </View>
          
        </ScrollView>

          <View style={styles.footer}>
          <View style={styles.browseButtons}>
          <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
            <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
            </TouchableHighlight>
              <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
          </View>
          <View style={styles.postButtons}>
 <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
               
            <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
              
               </TouchableHighlight>
              <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
          </View>
          <View style={styles.myTaskButtons}>
          <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
            <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
              </TouchableHighlight>
                   <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
               </View>
               <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
          </View>
          
      </View>
      </Drawer>
    );
  }
}


//AppRegistry.registerComponent('dashboard', () => dashboard) //Entry Point    and Root Component of The App