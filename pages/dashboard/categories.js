import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,Image,ScrollView,Modal
} from 'react-native';
//import {StackNavigator} from 'react-navigation';
import { withNavigation } from 'react-navigation';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-checkbox';
import RadioButton from 'radio-button-react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Content, List, ListItem,Drawer,Icon } from 'native-base';
import SideBar from '../sideBar';
import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
export class SubChildComponent extends Component{
  onCloseSuccess(item){
    console.log(item);
    //const { navigate } = this.props.navigation;
   // navigate('Mainview'); 
   console.log(this.props.result.navv);
   const { navigate } = this.props.result.navv.navigation;
   navigate('CategoryDetail',{category:item}); 
  }
  render(){
  
      if(this.props.result){
          var res = this.props.result.category.map((item,i)=>{
            //console.log(item)
              return (
                  <View style={{flex:1,flexDirection:'row',}} key={i}>
                    <Text style={{paddingVertical:10,color:'#337ab7',textDecorationLine: 'underline'}} onPress={this.onCloseSuccess.bind(this,item)}>{item.category_name}</Text>
                  </View>
              )
          })
      }
       return (
                <View>
                       {res}
                </View>
       )
  }
}
export default class ExampleView extends Component {
  constructor(props){
    super(props)
    this.navigate = this.props.navigation.navigate;  
  this.state = {
    activeSection: false,
    collapsed: true,
    catefory : [],
    visible : false,
  }
  }
  componentDidMount(){
    console.log(this.props);
    this.categoryList();

}
componentWillUnmount() {
  //Forgetting to remove the listener will cause pop executes multiple times
}
categoryList(){
  this.setState({ visible: true });	
  fetch('http://api.yellotasker.com/api/v1/category')
  .then((response) => response.json())
  .then((responseJson) =>{
    data = responseJson.data; // here we have all products data
    console.log('result category..........');
  console.log(JSON.stringify(data));
   let res = data;
   this.setState({catefory : res});
   console.log(this.state.catefory);
  let aa = Array.isArray(this.state.catefory);
  console.log(aa);
  this.setState({ visible: false });	
  })
  .catch((error) =>{
    console.error(error);
  });
}
  _toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
  }

  _setSection(section) {
    this.setState({ activeSection: section });
  }

  _renderHeader(section, i, isActive) {
   // console.log(section);
    return (
      <Animatable.View duration={400} style={[styles.header]} transition="backgroundColor">
      <Image style={{width: 30, height: 30}} source={{
        uri:section.category_group_image}} />
        <Text style={styles.headerText}>{section.category_group_name}</Text>
      </Animatable.View>
    );
  }

  _renderContent(section, i, isActive) {
    const { navigate } = this.props.navigation;
    this.props.navv = navigate;
    section.navv = this.props;
    return (
      <Animatable.View duration={400}  style={[styles.content]} transition="backgroundColor">
       
        
        <SubChildComponent result={section} />

      </Animatable.View>
    );
  }

    onTaskPressed(){
    let userId = global.userId;
   // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }
	onMyProfilePressed(){
		let userId = global.userId;
		// alert('userId'+userId);
	   const { navigate } = this.props.navigation;
	   if(userId){
	   
		navigate('Profile'); 
		}else{
	   Alert.alert(
		  'Message',
		  'Please Login to go to Profile',
		  [
		  {text : 'OK', onPress:()=>{this.onLoginPressed()}}
		  ]
		 );
		}
	  }
  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
    
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
   onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
    onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onNextStep(){
 if(this.state.tastDate==""){
this.setState({error: ' Please enter required fields'});
  }else{
   // global.taskLocatio = this.state.taskLocatio;
    const { navigate } = this.props.navigation;
   navigate('Browse');
}  
}
closeDrawer = () => {
  this.drawer._root.close()
  };
  openDrawer = () => {
  this.drawer._root.open()
  };
  onBackPressed(){
    const { navigate } = this.props.navigation;
    navigate('Dashboard'); 
 }
  render() {
    return(
      <Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} >
<View style={styles.mainviewStyle}>
<Modal
    animationType="fade"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: 'transparent',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
     <Image source={require('../img/loading.png')}/>
   </View>
   </View>
   </Modal>
<View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
<View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
<Text>


</Text>

</View>
<View  style={{flex:0.6,alignItems:'center',}}>
<Text onPress={() => this.props.navigation.navigate("Dashboard")}>
<Image source={require('../img/app-logo.png')} />
</Text>
</View>
<View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
<TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>

<FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>

</TouchableHighlight>
</View>

</View>
 <ScrollView style = {styles.scrollViewStyle}>
 <Image source={require('../img/banner-login.jpg')} style={styles.bannerImg}/>
       <View style = {styles.formViewStyle} >      
       <View style={styles.container}>
      
      <Accordion
        activeSection={this.state.activeSection}
        sections={this.state.catefory}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent.bind(this)}
        duration={400}
        onChange={this._setSection.bind(this)}
        underlayColor={'transparent'}
      />
    </View>
        
     </View>
          
 </ScrollView>
 <View style={styles.footer}>
 <View style={styles.browseButtons}>
 <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
 </View>
 <View style={styles.postButtons}>
 <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
 </View>
 <View style={styles.myTaskButtons}>
 <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
 </TouchableHighlight>
 <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
 </View>
 <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#fff'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
 </View>
      </View>
      </Drawer>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    marginBottom:50,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
    backgroundColor: '#efeb10',
  },
  header: {
    flexDirection:'row',
    backgroundColor: '#efeb10',
    padding: 10,
   marginTop : 10,
   alignItems:'center',
  },
  headerText: {
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '300',
    paddingLeft:10,
    color:'#000',
   
  },
  content: {
    padding: 20,
    backgroundColor: '#eee',
   

  },
  active: {
    backgroundColor: '#eee',
  },
  inactive: {
    backgroundColor: '#eee',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#eee',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },  mainviewStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  footer: {
    
     position: 'absolute',
    flex:0.1,
    left: 0,
    right: 0,
    bottom: -10,
    backgroundColor:'#000000',
    flexDirection:'row',
    height:70,
    alignItems:'center',
  },
  postButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
     flexDirection:'column',
     borderColor: '#efeb10',
     borderRightWidth: 1,
  
  },
  browseButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
     flexDirection:'column',
     borderColor: '#efeb10',
     borderRightWidth: 1,
  
  },
  myTaskButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
     flexDirection:'column',
     borderColor: '#efeb10',
     borderRightWidth: 1,
     
  },
  messageButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
     flexDirection:'column',
     
  },
  footerText: {
    color:'white',
    alignItems:'center',
    fontSize:14,
   
  
  },
  textStyle: {
    alignSelf: 'center',
    color: 'orange'
  },
  scrollViewStyle: {
    flex : 1,
    borderTopWidth : 4,
        borderColor: '#efeb10',
        
  },
  bannerImg : {
    width:'100%',
    height : 130,
  },
  formViewStyle : {
    flex:1,
    paddingVertical : '3%',
    paddingHorizontal : '3%',
  },
  buttonDiv : {
       paddingVertical : '1%',
      },
      secondButtonDiv : {
        paddingVertical : '2%',
       },
  radioDiv : {
    flex:1,
    flexDirection:'row',
    paddingVertical : '1%',
  },
  secRadioDiv:{
    flex:1,
    flexDirection:'row',
    paddingVertical : '2%',
  },
  buttonsDiv : {
     paddingVertical : '2%',
     marginBottom : 70,
  },
  extrasText : {
    color : '#000000',
        fontSize: 16,
  },
  InputText : {
    paddingVertical : 12,
    borderRadius:5,
    borderColor: 'gray',
     borderWidth: 1,
     paddingHorizontal: 5,
     marginBottom : 10,
     textAlignVertical: 'top',
  },
  
  LoginDiv : {
    backgroundColor : '#efeb10',
    paddingVertical : 10, 
      borderRadius:25,
      borderWidth: 1,
      borderColor: '#efeb10',
  
  },
  extraText : {
    color : '#000000',
    textAlign: 'center',
        fontSize: 18,
        fontWeight:'normal',
  },
   errorText : {
  color : '#dd4b39',
  paddingTop : 5,
    },
    abc :{
      color:'#d1d3d4',
        },
        radioContainer : {
            borderWidth : 2,
            borderColor : '#d1d3d4',
        },
        radioOuterDiv : {
          flex:0.5,
           borderWidth : 1,
            borderColor : 'gray',
            paddingVertical:5,
            paddingHorizontal:5,
          borderRadius:5,
        },
        radioText : {
          paddingHorizontal:7,
         
          textAlignVertical : 'center',
        },
        radioSecOuterDiv : {
          marginLeft:3,
          flex:0.5,
          borderWidth : 1,
           borderColor : 'gray',
           paddingVertical:5,
           paddingHorizontal:5,
         borderRadius:5,
        },
        bannerImg : {
          width:'100%',
          height : 130,
        },
});