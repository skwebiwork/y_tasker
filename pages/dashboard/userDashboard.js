import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,ProgressBar,Alert,BackHandler,ActivityIndicator,Modal,ListView,FlatList} from "react-native";
import { Container, Content, List, ListItem,Drawer,Icon,Tab, Tabs, ScrollableTab } from 'native-base';
import SideBar from '../sideBar';
import ProgressIndicator from 'react-native-progress-indicator';
import styles from './userDashboardStyle';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import Grid from 'react-native-grid-component';
import Dashboard from './dashboardMenu/index'
import PaymentHistory from './PaymentHistory/index';
import Notifications from '../notification/index'
export default class dashboard extends Component {
   static navigationOptions = {
    header: null,
  };
  constructor(){
    super();
    global.count = 0;
    this.state={
      touchMsg : false,
      visible : false,
      data : [],
      categoryData : [],
			dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
    }
  }
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    this.categoryList();
  }
  componentWillUnmount() {
    //Forgetting to remove the listener will cause pop executes multiple times
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  }
  handleBack() {
global.count = global.count+1;
//alert('count'+global.count);
if(global.count>1){
  Alert.alert(
    'Confirmation',
    'Are sure want to quit this App',
    [
      {text: 'OK', onPress: () => BackHandler.exitApp()},
    ]
);
}else{
  //this.setState({touchMsg : true});
  
  global.setTimeout(() => {
    global.count = 0;
  }, 1000);
}
  
return true;

  }
 categoryList(){
  AsyncStorage.getItem('DynamicCategory', (err, result) => {
   console.log(result);
    let cat = JSON.parse(result);
    console.log(cat[0].category);
    this.setState({
      // dataSource: this.state.dataSource.cloneWithRows(data)
      categoryData : cat
     })
  });
  let len = this.state.categoryData;
  console.log(len);
 }
  closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
  onTaskPressed(){
    let userId = global.userId;
    const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }
  onPostClick= (id,title,description)=>{
    let catid = JSON.stringify(id);
    let catTitle = JSON.stringify(title);
    let catDes = JSON.stringify(description);
   // alert(title);
    global.category_id = id;
    global.category_title = title;
    global.category_description = description;
    const { navigate } = this.props.navigation;
   navigate('Mainview'); 

  }
  onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
    onMyProfilePressed(){
      let userId = global.userId;
      // alert('userId'+userId);
     const { navigate } = this.props.navigation;
     if(userId){
     
      navigate('Profile'); 
      }else{
     Alert.alert(
        'Message',
        'Please Login to go to Profile',
        [
        {text : 'OK', onPress:()=>{this.onLoginPressed()}}
        ]
       );
      }
    }
    onBackPressed(){
      const { navigate } = this.props.navigation;
      navigate('Dashboard');
     }
  render() {
    return(
<Drawer
			ref={(ref) => { this.drawer = ref; }}
			content={<SideBar navigator={this.navigator} />}
			onClose={() => this.closeDrawer()} 
      >
       <Modal
    animationType="slide"
      transparent={true}
      visible={this.state.visible}>
      <View style={{flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'}} >
      <View style={{backgroundColor: '#FFFFFF',
    height: 100,
    width: 200,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'space-around'}}> 
      <ActivityIndicator  color="#333" size = "large" style={ {flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80 }
   }></ActivityIndicator>
   </View>
   </View>
   </Modal>
      <View style={styles.mainviewStyle}>
            <View style={{flexDirection:'row',backgroundColor:'#000000',alignItems: 'center',paddingVertical:10}}>
            <View style={{flex:0.2,alignItems:'flex-start',paddingLeft:10}}>
                <TouchableHighlight onPress={this.onBackPressed.bind(this)} underlayColor={'transparent'}>
                    <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.arrowLeft}</FontAwesome>
                </TouchableHighlight> 
            </View>
         <View  style={{flex:0.6,alignItems:'center',}}>
         <Text onPress={() => this.props.navigation.navigate("Dashboard")}>
            <Image source={require('../img/app-logo.png')} />
            </Text>
            </View>
            <View style={{flex:0.2,alignItems:'flex-end',paddingVertical:8,paddingRight:4}}>
            <TouchableHighlight style={{backgroundColor : '#000',padding:5,}} onPress={()=>this.props.navigation.navigate("DrawerOpen")} underlayColor={'transparent'}>
           
            <FontAwesome style={{fontSize: 18,color:'#fff'}}>{Icons.navicon}</FontAwesome>
           
            </TouchableHighlight>
            </View>
         
         </View>
         <View style={{flex:1,}}>
      
        <Tabs tabBarUnderlineStyle= {{ backgroundColor: '#efeb10',height: 2,}} renderTabBar={()=> <ScrollableTab />} tabContainerStyle={{height:50}}>
       <Tab heading="Dashboard" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000',}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>
              <Dashboard />
      
       </Tab>
       <Tab heading="Payment History" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>
       <PaymentHistory />

</Tab>
<Tab heading="Payment Method" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>
     

</Tab>
{/* <Tab heading="Refer A Friend" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>


</Tab> */}
<Tab heading="Notifications" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>
<View style={{paddingVertical:10,paddingHorizontal:10}}>
          <Text style={{textAlign:'center',color:'#fff',backgroundColor:'#35495E',paddingVertical:10,borderRadius:4,overflow:'hidden'}}>Recent Activity on Yellotasker</Text>
          <Notifications />
          </View>

</Tab>
<Tab heading="Settings" tabStyle={styles.tabcontain} textStyle={{color:'#000',borderColor:'#000000'}} activeTabStyle={{backgroundColor : '#000000', }} activeTextStyle={{color:'#fff'}} tabBarUnderlineStyle={{borderColor : '#ccc',}}>


</Tab>

       </Tabs>
        
            
          
       </View>

          <View style={styles.footer}>
          <View style={styles.browseButtons}>
          <TouchableHighlight onPress={this.onBrowsePressed.bind(this)}>
            <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.search}</FontAwesome>
            </TouchableHighlight>
              <Text style={styles.footerText} onPress={this.onBrowsePressed.bind(this)}>Browse Task</Text>
          </View>
          <View style={styles.postButtons}>
 <TouchableHighlight onPress={this.onTaskPressed.bind(this)}>
               
            <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.plusSquare}</FontAwesome>
              
               </TouchableHighlight>
              <Text style={styles.footerText} onPress={this.onTaskPressed.bind(this)}>Post Task</Text>
          </View>
          <View style={styles.myTaskButtons}>
          <TouchableHighlight onPress={this.onMyTaskPressed.bind(this)}>
            <FontAwesome style={{fontSize: 15,color:'#fff'}}>{Icons.clipboard}</FontAwesome>
              </TouchableHighlight>
                   <Text style={styles.footerText} onPress={this.onMyTaskPressed.bind(this)}>My Task</Text>
               </View>
               <TouchableHighlight style={styles.messageButtons} onPress={this.onMyProfilePressed.bind(this)}>
 <View style={styles.messageButtons}>
 <FontAwesome style={{fontSize: 15,color:'#efeb10'}}>{Icons.userO}</FontAwesome>
 <Text style={[styles.footerText,{color:'#efeb10'}]}>Profile</Text>
 </View>
 </TouchableHighlight>
          </View>
          
      </View>
      </Drawer>
    );
  }
}


//AppRegistry.registerComponent('dashboard', () => dashboard) //Entry Point    and Root Component of The App