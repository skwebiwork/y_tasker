import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    mainviewStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  mainCategory : {
    flex:1,
    flexDirection :'row',
    paddingHorizontal : 6,
    paddingTop:6,
  
  },
  topCategory : {
    
     color : '#2c3e50',
     fontSize:16,
    
     fontWeight : 'bold',
     
     paddingLeft:4,
  },
  more : {
  color : '#ffffff',
  backgroundColor : '#000000',
  borderColor : '#000000',
  textAlign: 'center',
  fontWeight : 'normal',
  borderRadius:12,
  paddingVertical:4,
  overflow:"hidden",
  fontSize:12,
  },
  
  mainImgDiv : {
    flexDirection : 'row',
    paddingVertical:'2%',
  },
  
  mainImgsDiv : {
    flexDirection : 'row',
    paddingVertical:'2%',
    marginBottom:100,
  },
  ImgDiv : {
    width:'33.33%',
    flexDirection : 'column',
    justifyContent: 'center',
    alignItems: 'center',
    
  }, 
  ImgDivs : {
   // width:'33.33%',
    flexDirection : 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical:'5%',
    
  },
  Img : {
      height: 80,
      width : 80,
  },
  ImgText : {
  
  fontSize:12,
  fontWeight : 'normal',
   width : '60%',
   textAlign: 'center',
  },
  footer: {
    position: 'absolute',
    flex:0.1,
    left: 0,
    right: 0,
    bottom: -10,
    backgroundColor:'#000000',
    flexDirection:'row',
    height:70,
    alignItems:'center',
  },
  postButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
     flexDirection:'column',
     borderColor: '#efeb10',
     borderRightWidth: 1,
  
  },
  browseButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
     flexDirection:'column',
     borderColor: '#efeb10',
     borderRightWidth: 1,
  
  },
  myTaskButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
     flexDirection:'column',
     borderColor: '#efeb10',
     borderRightWidth: 1,
     
  },
  messageButtons: {
    alignItems:'center',
    justifyContent: 'center',
    flex:1,
     flexDirection:'column',
     
  },
  footerText: {
    color:'white',
    alignItems:'center',
    fontSize:14,
   
  
  },
  '@media (max-width: 360)': {
    footerText: {
      color:'white',
      alignItems:'center',
      fontSize:13,
     
    
    },
    topCategory : {
    
      color : '#2c3e50',
      fontSize:13,
     
      fontWeight : 'bold',
      
      paddingLeft:4,
   },
   ImgText : {
  
    fontSize:12,
    fontWeight : 'normal',
     width : '70%',
     textAlign: 'center',
     
    },
    
  },
  textStyle: {
    alignSelf: 'center',
    color: 'orange'
  },
  scrollViewStyle: {
    borderTopWidth : 4,
    borderColor: '#efeb10',
    backgroundColor:'#efeb10',
  },
  buttonDiv : {
       paddingVertical : 10,
       paddingHorizontal : 20,
      },
      buttonsDiv : {
       paddingVertical : 4,
       paddingHorizontal : 20,
      },
    facebookDiv : {
      paddingVertical : 12,
      backgroundColor : '#3b5998',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#3b5998',
   
    },
    googleDiv : {
      paddingVertical : 12,
      backgroundColor : '#dd4b39',
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#dd4b39',
   
    },
    facebokText : {
       color : '#fff',
        textAlign: 'center',
        fontSize: 18,
    },
    imagestyle : {
      height:15,
      width : 15,
    },
    InputText : {
    paddingVertical : 12,
    borderRadius:5,
    borderColor: 'gray',
     borderWidth: 1,
     paddingHorizontal: 5,
     marginBottom : 10,
  },
  InputsText : {
    paddingVertical : 8,
    borderRadius:5,
    borderColor: 'gray',
     borderWidth: 1,
     paddingHorizontal: 5,
  },
  bannerImg : {
    width:'100%',
    height : 130,
  },
  lableStyle : {
    paddingVertical : 5,
    paddingHorizontal : 20,
    color : '#000',
  },
  lablesStyle : {
    paddingHorizontal : 20,
    color : '#000',
  },
  LoginDiv : {
    backgroundColor : '#efeb10',
    paddingVertical : 10, 
      borderRadius:5,
      borderWidth: 1,
      borderColor: '#efeb10',
  },
  extraText : {
    color : '#000000',
    textAlign: 'center',
        fontSize: 18,
  },
  icon: {
    width: 24,
    height: 24,
  },
  item: {
    flex: 1,
   
    margin: 1,
    paddingVertical:'2%',
    
  },
  list: {
    flex: 1
  },
  });

  export default styles;