import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,ProgressBar,Alert,BackHandler,ActivityIndicator,Modal,ListView,FlatList, TouchableOpacity} from "react-native";
import { Container, Content, List, ListItem,Drawer,Icon,Tab, Tabs, ScrollableTab } from 'native-base';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./paymentHistoryStyle";
import Notifications from '../../notification/index';
class FlatListItem extends Component{
  
    render(){

         return(
           <View>
           <View style={{flex:1,borderBottomColor:'#e4e4e4',borderBottomWidth:1, }}>
           <TouchableHighlight underlayColor='transparent'>
               <View style={styles.oddDiv}>
               <View style={{flex:0.5,paddingVertical:10,borderLeftColor:'#e4e4e4',borderLeftWidth:1,backgroundColor:'#fff',paddingHorizontal:2,alignItems:'center',justifyContent:'center'}}>
               
              <Text style={{fontSize:14,color:'#000', textAlign: 'center',
             }}>{this.props.item.task_title}</Text>
  
         
               </View>
               <View style={{flex:0.2,flexDirection:'column',paddingHorizontal:5,paddingVertical:8,borderLeftColor:'#e4e4e4',borderLeftWidth:1,backgroundColor:'#fff',alignItems:'center',justifyContent:'center'}}>
              
               <Text style={{fontSize:13,color:'#000',textAlign:'center',}}>{this.props.item.total_price} MYR</Text>
               
              
               </View>
               
               {this.props.item.status=='Failed'&&<View style={{flex:0.3,borderLeftColor:'#e4e4e4',borderLeftWidth:1,backgroundColor:'#ff0000',alignItems:'center',justifyContent:'center'}}>
                       <Text style={{color:'#fff',textAlign:'center',paddingVertical:10,fontWeight:'bold'}}>{this.props.item.status}</Text>
               </View>}
               {this.props.item.status=='Success'&&<View style={{flex:0.3,borderLeftColor:'#e4e4e4',borderLeftWidth:1,backgroundColor:'#008000',alignItems:'center',justifyContent:'center'}}>
                       <Text style={{color:'#fff',textAlign:'center',paddingVertical:10,fontWeight:'bold'}}>{this.props.item.status}</Text>
               </View>}
           </View>
         
        </TouchableHighlight>
             </View> 
  
             
             </View>
         )
  
    }
  }
export default class Index extends Component {
   static navigationOptions = {
    header: null,
  };
  constructor(){
    super();
    global.count = 0;
    this.state={
        activeTabIndex : 0,
      touchMsg : false,
      visible : false,
      data : [],
      categoryData : [],
      dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
      as_doer:{},
      as_poster :{},
      netEarned : 0.00,
      earnedItem :[],
      outgoingItem :[],
      totalCount : 0,
      
    }
    
  
}
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
   this.getuserDashboard();
   this.earnedDetails();
   this.outgoingDetails();
  }
  earnedDetails = async() =>{
    this.setState({ visible: true });
    const response = await fetch(
        'http://api.yellotasker.com/api/v1/user/payments-histroy/earned?userId='+global.userId
    );
    const json = await response.json();
    if(json.status=='1'&&json.code=='200'){
        //console.log(json.data);
        var data = json.data; 
        this.setState({earnedItem : data.outgoing,totalCount : data.length()})
    }
  }
  outgoingDetails = async() =>{
    this.setState({ visible: true });
    const response = await fetch(
        'http://api.yellotasker.com/api/v1/user/payments-histroy/outgoing?userId='+global.userId
    );
    const json = await response.json();
    if(json.status=='1'&&json.code=='200'){
        //console.log(json.data);
        var data = json.data; 
        this.setState({earnedItem : data.outgoing,totalCount : data.length()})
    }
  }
  getuserDashboard = async () => {
		this.setState({ visible: true });
		const response = await fetch(
			'http://api.yellotasker.com/api/v1/userDashboard/99'
		);
		const json = await response.json();
        if(json.status=='1'&&json.code=='200'){
            //console.log(json.data);
            var data = json.data; 
            this.setState({as_doer:data.user_Task_Summary.as_doer,as_poster:data.user_Task_Summary.as_poster
            })
        }
	}
    renderOutgoing(){
    return (
        <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
            <View style={{paddingHorizontal : 5,paddingVertical:10,}}>
                <View style={[styles.oddDiv,{borderBottomColor:'#fff',borderBottomWidth:1,}]}>
                    <View style={{flex:0.5,paddingVertical:10,backgroundColor:'#e4e4e4',paddingHorizontal:5,borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontSize:14,color:'#5b6455', textAlign: 'center',}}>Task Name</Text>
                    </View>
                    <View style={{flex:0.2,flexDirection:'column',paddingHorizontal:5,paddingVertical:8,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#e4e4e4',borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{fontSize:13,color:'#5b6455',textAlign:'center'}}>Amount</Text>
                    </View>
                    <View style={{flex:0.3,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#e4e4e4',borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:'#5b6455',textAlign:'center',paddingVertical:10,paddingHorizontal:5,}}>Status</Text>
                    </View>
                </View>
                        <FlatList 
                        data={this.state.outgoingItem}
                        keyExtractor={(x,i)=>i} 
                        renderItem={({item,index})=>{
                            return (
                            <FlatListItem item={item} index={index}>
                            </FlatListItem>
                            );
                        } 
                        }/> 
            </View>
            {this.state.totalCount==0&&<View style={{backgroundColor:'#5b6455',marginHorizontal:10,flex:1}}>
                <Text style={{paddingVertical:10,paddingHorizontal:10,color:'#fff'}}>No Transactions available!</Text>
            </View>}
        </View>
    )
  }
  renderEarner(){
    return (
        <View style={{paddingVertical:10,paddingHorizontal:5,flexDirection:'column',}}>
        <View style={{paddingHorizontal : 5,paddingVertical:10,}}>
            <View style={[styles.oddDiv,{borderBottomColor:'#fff',borderBottomWidth:1,}]}>
                <View style={{flex:0.5,paddingVertical:10,backgroundColor:'#e4e4e4',paddingHorizontal:5,borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{fontSize:14,color:'#5b6455', textAlign: 'center',}}>Task Name</Text>
                </View>
                <View style={{flex:0.2,flexDirection:'column',paddingHorizontal:5,paddingVertical:8,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#e4e4e4',borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{fontSize:13,color:'#5b6455',textAlign:'center'}}>Amount</Text>
                </View>
                <View style={{flex:0.3,borderLeftColor:'#fff',borderLeftWidth:1,backgroundColor:'#e4e4e4',borderBottomColor:'#fff',borderBottomWidth:1,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{color:'#5b6455',textAlign:'center',paddingVertical:10,paddingHorizontal:5,}}>Status</Text>
                </View>
            </View>
                    <FlatList 
                    data={this.state.earnedItem}
                    keyExtractor={(x,i)=>i} 
                    renderItem={({item,index})=>{
                        return (
                        <FlatListItem item={item} index={index}>
                        </FlatListItem>
                        );
                    } 
                    }/> 
        </View>
        {this.state.totalCount==0&&<View style={{backgroundColor:'#5b6455',marginHorizontal:10,flex:1}}>
            <Text style={{paddingVertical:10,paddingHorizontal:10,color:'#fff'}}>No Transactions available!</Text>
        </View>}
    </View>
    )
  }
  componentWillUnmount() {
    //Forgetting to remove the listener will cause pop executes multiple times
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  }
  handleBack() {
global.count = global.count+1;
//alert('count'+global.count);
if(global.count>1){
  Alert.alert(
    'Confirmation',
    'Are sure want to quit this App',
    [
      {text: 'OK', onPress: () => BackHandler.exitApp()},
    ]
);
}else{
  //this.setState({touchMsg : true});
  
  global.setTimeout(() => {
    global.count = 0;
  }, 1000);
}
  
return true;

  }

  closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
  
 
   
  render() {
    return(
      <View style={styles.mainviewStyle}>
        <ScrollView style = {styles.scrollViewStyle}>
        <Text style={{fontSize:16,color:'gray',paddingVertical:20,paddingHorizontal:5}}>Net earned :{this.state.netEarned}</Text>
        <View style={{flex:1,flexDirection:'row',paddingHorizontal:40}}>
        <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 0 }) }}
                   style={[styles.topButtonStyle, this.state.activeTabIndex == 0 ? styles.topButtonActiveStyle : {}]}>
               <Text style={this.state.activeTabIndex == 0 ? styles.topTextActiveStyle : {color:'#fff'}}>Earned</Text>
               </TouchableOpacity>
               <View style={{ width: 1, height: 50, backgroundColor: '#ecf0f1' }} />
               <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 1 }) }}
                   style={[styles.topButtonStyle, this.state.activeTabIndex == 1 ? styles.topButtonActiveStyle : {}]}>
                   <Text style={this.state.activeTabIndex == 1 ? styles.topTextActiveStyle : {color:'#fff'}}>Outgoing</Text>
               </TouchableOpacity>
        </View>
          <View style={{paddingVertical:10,marginBottom:60}}>
          {this.state.activeTabIndex == 0 &&<View>{this.renderEarner()}</View>}
          {this.state.activeTabIndex == 1 &&<View>{this.renderOutgoing()}</View>}
          </View>
        </ScrollView>
      </View>
    );
  }
}


//AppRegistry.registerComponent('dashboard', () => dashboard) //Entry Point    and Root Component of The App