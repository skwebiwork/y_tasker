import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,ProgressBar,Alert,BackHandler,ActivityIndicator,Modal,ListView,FlatList, TouchableOpacity} from "react-native";
import { Container, Content, List, ListItem,Drawer,Icon,Tab, Tabs, ScrollableTab } from 'native-base';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./dashboardStyle";
import Notifications from '../../notification/index'
export default class Index extends Component {
   static navigationOptions = {
    header: null,
  };
  constructor(){
    super();
    global.count = 0;
    this.state={
        activeTabIndex : 0,
      touchMsg : false,
      visible : false,
      data : [],
      categoryData : [],
      dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
      as_doer:{},
      as_poster :{},
    }
    
  
}
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
   this.getuserDashboard();
  }
  getuserDashboard = async () => {
		this.setState({ visible: true });
		const response = await fetch(
			'http://api.yellotasker.com/api/v1/userDashboard/99'
		);
		const json = await response.json();
        if(json.status=='1'&&json.code=='200'){
            //console.log(json.data);
            var data = json.data; 
            this.setState({as_doer:data.user_Task_Summary.as_doer,as_poster:data.user_Task_Summary.as_poster
            })
        }
	}
  renderTasker(){
    return (
      <View>
          <View style={styles.boxWrapper}>
                <View style={styles.innerBox} >
                <Text style={styles.text1Style}>Offered Posted</Text>
                <Text style={styles.text2Style}>{this.state.as_doer.posted_offers}</Text>
                </View>
                <View style={styles.innerBox} >
                <Text style={styles.text1Style}>Offers Assigned </Text>
                    <Text style={styles.text2Style}>{this.state.as_doer.assigned}</Text>
                </View>
                <View style={styles.innerBox} >
                <Text style={styles.text1Style}>Task completed</Text>
                    <Text style={styles.text2Style}>{this.state.as_doer.completed}</Text>
                </View>
         </View>
         <View style={styles.boxWrapper}>
                <View style={styles.innerBox} >
                <Text style={styles.text1Style}>Awaiting payment </Text>
               <Text style={styles.text2Style}>{this.state.as_doer.awaitingPayment}</Text>
           </View>
           <View style={styles.innerBox} >
           <Text style={styles.text1Style}>Closed </Text>
               {/* <Text style={styles.text2Style}>{this.state.as_poster.closed}</Text> */}
           </View>
    </View>
    </View>
    )
  }
  renderPoster(){
    return (
      <View>
       <View style={styles.boxWrapper}>
                <View style={styles.innerBox} >
                    <Text style={styles.text1Style}>Task Posted </Text>
                    <Text style={styles.text2Style}>{this.state.as_poster.posted_offers}</Text>
                </View>
                <View style={styles.innerBox} >
                    <Text style={styles.text1Style}>Task in progress </Text>
                    <Text style={styles.text2Style}>{this.state.as_poster.assigned}</Text>
                </View>
      </View>
      <View style={styles.boxWrapper}>
           <View style={styles.innerBox} >
               <Text style={styles.text1Style}>Reopen </Text>
               <Text style={styles.text2Style}>{this.state.as_poster.reopen}</Text>
           </View>
           <View style={styles.innerBox}>
               <Text style={styles.text1Style}>Closed </Text>
               <Text style={styles.text2Style}>{this.state.as_poster.closed}</Text>
           </View>
    </View>
    </View>
    )
  }
  componentWillUnmount() {
    //Forgetting to remove the listener will cause pop executes multiple times
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  }
  handleBack() {
global.count = global.count+1;
//alert('count'+global.count);
if(global.count>1){
  Alert.alert(
    'Confirmation',
    'Are sure want to quit this App',
    [
      {text: 'OK', onPress: () => BackHandler.exitApp()},
    ]
);
}else{
  //this.setState({touchMsg : true});
  
  global.setTimeout(() => {
    global.count = 0;
  }, 1000);
}
  
return true;

  }

  closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
  onTaskPressed(){
    let userId = global.userId;
    const { navigate } = this.props.navigation;
    global.category_id = null;
    navigate('Mainview');
  }
  onPostClick= (id,title,description)=>{
    let catid = JSON.stringify(id);
    let catTitle = JSON.stringify(title);
    let catDes = JSON.stringify(description);
   // alert(title);
    global.category_id = id;
    global.category_title = title;
    global.category_description = description;
    const { navigate } = this.props.navigation;
   navigate('Mainview'); 

  }
  onLoginPressed(){
    const { navigate } = this.props.navigation;
 navigate('Login');
  }
  onBrowsePressed(){
     const { navigate } = this.props.navigation;
     navigate('Browse'); 
  }
  onMyTaskPressed(){
    let userId = global.userId;
     // alert('userId'+userId);
    const { navigate } = this.props.navigation;
    if(userId){
     navigate('MyTask'); 
     }else{
    Alert.alert(
       'Message',
       'Please Login to go to My task',
       [
       {text : 'OK', onPress:()=>{this.onLoginPressed()}}
       ]
      );
     }
    }
    onBackPressed(){
      const { navigate } = this.props.navigation;
      navigate('Dashboard');
     }

  render() {
    return(

      <View style={styles.mainviewStyle}>
           
        <ScrollView style = {styles.scrollViewStyle}>
               <Text style={{textAlign:'center',fontSize:16,color:'gray',paddingVertical:20}}>My Task summary</Text>
        
       
        <View style={{flex:1,flexDirection:'row',paddingHorizontal:40}}>
        <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 0 }) }}
                   style={[styles.topButtonStyle, this.state.activeTabIndex == 0 ? styles.topButtonActiveStyle : {}]}>
               <Text style={this.state.activeTabIndex == 0 ? styles.topTextActiveStyle : {color:'#fff'}}>As Poster</Text>
               </TouchableOpacity>
               <View style={{ width: 1, height: 50, backgroundColor: '#ecf0f1' }} />
               <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 1 }) }}
                   style={[styles.topButtonStyle, this.state.activeTabIndex == 1 ? styles.topButtonActiveStyle : {}]}>
                   <Text style={this.state.activeTabIndex == 1 ? styles.topTextActiveStyle : {color:'#fff'}}>As Tasker</Text>
               </TouchableOpacity>
        </View>
            
          <View style={{paddingVertical:10}}>
          {this.state.activeTabIndex == 0 &&<View>{this.renderPoster()}</View>}
          {this.state.activeTabIndex == 1 &&<View>{this.renderTasker()}</View>}

          </View>
          <View style={{paddingVertical:10,paddingHorizontal:10}}>
          <Text style={{textAlign:'center',color:'#fff',backgroundColor:'#35495E',paddingVertical:10,borderRadius:4,overflow:'hidden'}}>Recent Activity on Yellotasker</Text>
          <Notifications />
          </View>
        </ScrollView>

  
          
      </View>
     
    );
  }
}


//AppRegistry.registerComponent('dashboard', () => dashboard) //Entry Point    and Root Component of The App