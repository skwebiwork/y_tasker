
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {
  DrawerNavigator,
} from 'react-navigation';


import Home from './home/home';
//import Home from './dashboard/categoryDetail';
//import Home from './postTask/transaction';
import SecondHome from './home/SecondHome';
//import Products from './About';
import Login from './login/login';
import SignUp from './register/register';
import Mainview from './postTask/mainview';
import Dashboard from './dashboard/dashboard';
import UserDashboard from './dashboard/userDashboard';
//import Dashboard from './postTask/mainview';
import SecondStep from './postTask/secondStep';
import ThirdStep from './postTask/thirdStep';
import ForthStep from './postTask/forthStep';
import Transaction from './postTask/transaction';
import Browse from './browseTask/list';
//import Browse from './selectOption';
import SingleTask from './browseTask/singleTask';
import SideBar from './sideBar';
import ForgotPassword from './forgotPassword';
import MyTask from './myTask/myTask';
import MyTaskDetail from './myTask/myTaskDetail';
import Category from './dashboard/categories';
import CategoryDetail from './dashboard/categoryDetail';
import Reset from './resetPassword';
import Logout from './logout';
import Setting from './setting/setting';
import Profile from './profile/profile';
import TaskerProfile from './profile/taskerProfile';
import singlePostedTask from './myTask/singlePostedTask';
import singlePendingTask from './myTask/singlePendingTask';
import singleAcceptedTask from './myTask/singleAcceptedTask';

const AwesomeProject = DrawerNavigator(
  {
  Home: { screen: Home },
  SecondHome : {screen : SecondHome},
  Login: { screen: Login },
  SignUp: { screen: SignUp },
  Mainview: { screen: Mainview },
  Dashboard: { screen: Dashboard },
   SecondStep: { screen: SecondStep }, 
   ThirdStep: { screen: ThirdStep },
   ForthStep: { screen: ForthStep },
   Browse: { screen: Browse },
   SingleTask: { screen: SingleTask },
   ForgotPassword : {screen : ForgotPassword},
  // SideBar : {screen : SideBar},
   MyTask : {screen : MyTask},
   Category : {screen : Category},
   Reset : {screen : Reset},
   Logout : {screen : Logout},
   MyTaskDetail : {screen : MyTaskDetail},
   Setting : {screen : Setting},
   Profile : {screen : Profile},
   TaskerProfile : {screen : TaskerProfile},
   singlePostedTask : {screen : singlePostedTask},
   CategoryDetail : {screen : CategoryDetail},
   Transaction : {screen : Transaction },
   singlePendingTask : {screen : singlePendingTask},
   singleAcceptedTask : {screen : singleAcceptedTask},
   UserDashboard : {screen : UserDashboard},
},

{
  contentComponent: props => <SideBar {...props} />,
  drawerPosition:'right',
},



);

export default AwesomeProject;
