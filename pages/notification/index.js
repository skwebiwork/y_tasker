import React, { Component } from 'react';
import {View,Text,StyleSheet,TouchableHighlight,ScrollView,Image,AppRegistry,TextInput,Button,AsyncStorage,ProgressBar,Alert,BackHandler,ActivityIndicator,Modal,ListView,FlatList, TouchableOpacity} from "react-native";
import { Container, Content, List, ListItem,Drawer,Icon,Tab, Tabs, ScrollableTab } from 'native-base';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import styles from "./notificationStyle";
export default class Index extends Component {
   static navigationOptions = {
    header: null,
  };
  constructor(){
    super();
    global.count = 0;
    this.state={
        activeTabIndex : 0,
      touchMsg : false,
      visible : false,
      data : [],
      categoryData : [],
			dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
    }
}
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    this.getNotifications();
  }
  componentWillUnmount() {
    //Forgetting to remove the listener will cause pop executes multiple times
    BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
  }
  handleBack() {
global.count = global.count+1;
//alert('count'+global.count);
if(global.count>1){
  Alert.alert(
    'Confirmation',
    'Are sure want to quit this App',
    [
      {text: 'OK', onPress: () => BackHandler.exitApp()},
    ]
);
}else{
  //this.setState({touchMsg : true});
  global.setTimeout(() => {
    global.count = 0;
  }, 1000);
}
return true;
  }
    getNotifications = async (shortByValue) => {
		this.setState({ visible: true });
		const response = await fetch(
			'http://api.yellotasker.com/api/v1/notifications'
		);
		const json = await response.json();
        if(json.status=='1'&&json.code=='200'){
            //console.log(json.data);
            this.setState({ dataSource: this.state.dataSource.cloneWithRows(json.data)});
        }
	}
 
  closeDrawer = () => {
		this.drawer._root.close()
	  };
	  openDrawer = () => {
		this.drawer._root.open()
	  };
     renderRow(item){       
             return (<View >
                {item.entity_type=='task_add'&&<View style={{backgroundColor:'#f9f9f9',borderBottomColor:'#ccc',borderBottomWidth:1,paddingVertical:10}}>
                <Text style={{paddingHorizontal:10}}>{item.user_details.first_name} {item.user_details.last_name}  posted a task {item.message}</Text></View>}
                {item.entity_type=='comment_add'&&<View style={{backgroundColor:'#f9f9f9',borderBottomColor:'#ccc',borderBottomWidth:1,paddingVertical:10}}><Text style={{paddingHorizontal:10}}>{item.user_details.first_name} {item.user_details.last_name} commented {item.message} on {item.title}</Text></View>}
                {item.entity_type=='offers_add'&&<View style={{backgroundColor:'#f9f9f9',borderBottomColor:'#ccc',borderBottomWidth:1,paddingVertical:10}}><Text style={{paddingHorizontal:10}}>{item.user_details.first_name} {item.user_details.last_name} made an offer on  {item.title}</Text></View>}
                
            </View>
             );
      
    }
  render() {
    return(
      <View style={styles.mainviewStyle}>
           <ListView
		dataSource={this.state.dataSource}
		renderRow={(rowData) => { return this.renderRow(rowData) }
        }
        />
      </View>
    );
  }
}
//AppRegistry.registerComponent('dashboard', () => dashboard) //Entry Point    and Root Component of The App